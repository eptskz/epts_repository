﻿using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace KatoLoader_Console
{
    class Program
    {
        static HttpClient client = new HttpClient();
        static async Task Main(string[] args)
        {
            var cs = "Host=localhost;Username=postgres;Password=1q2w3e4r;Database=eptsdb";
            client.BaseAddress = new Uri("https://data.egov.kz/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            for(int from = 1; from < 18533; from += 100)
            {
                HttpResponseMessage response = await client.GetAsync($"datasets/exportjson?index=kato&version=data&from={from}&count=100");
                if (response.IsSuccessStatusCode)
                {
                    var str = await response.Content.ReadAsStringAsync();
                    List<KatoModel> listKatos = JsonConvert.DeserializeObject<List<KatoModel>>(str);

                    using (var con = new NpgsqlConnection(cs))
                    {
                        con.Open();

                        using var command = new NpgsqlCommand(connection: con, cmdText: null);

                        var sb = new StringBuilder("INSERT INTO public.\"DicKatos\"( \"Id\", \"Code\", \"NameRu\", \"NameKz\", \"Created\", \"AreaType\", \"Level\", \"ParentId\") VALUES ");
                        for (var i = 0; i < listKatos.Count; i++)
                        {
                            var idName =        (i * 8 + 1).ToString();
                            var codeName =      (i * 8 + 2).ToString();
                            var ruName =        (i * 8 + 3).ToString();
                            var kzName =        (i * 8 + 4).ToString();
                            var createdName =   (i * 8 + 5).ToString();
                            var areaTypeName =  (i * 8 + 6).ToString();
                            var levelName =     (i * 8 + 7).ToString();
                            var parentIdName =  (i * 8 + 8).ToString();

                            sb.Append("(@").Append(idName)
                                .Append(", @").Append(codeName)
                                .Append(", @").Append(ruName)
                                .Append(", @").Append(kzName)
                                .Append(", @").Append(createdName)
                                .Append(", @").Append(areaTypeName)
                                .Append(", @").Append(levelName)
                                .Append(", @").Append(parentIdName)
                                .Append("),");

                            command.Parameters.Add(new NpgsqlParameter<int>(idName, listKatos[i].Id));
                            command.Parameters.Add(new NpgsqlParameter<string>(codeName, listKatos[i].Code));
                            command.Parameters.Add(new NpgsqlParameter<string>(ruName, listKatos[i].NameRus));
                            command.Parameters.Add(new NpgsqlParameter<string>(kzName, listKatos[i].NameKaz));
                            command.Parameters.Add(new NpgsqlParameter<DateTime>(createdName, DateTime.Now));
                            command.Parameters.Add(new NpgsqlParameter<int>(areaTypeName, listKatos[i].AreaType));
                            command.Parameters.Add(new NpgsqlParameter<int>(levelName, listKatos[i].Level));
                            command.Parameters.Add(new NpgsqlParameter<int>(parentIdName, listKatos[i].Parent.Value));

                            /*
                            if (i % 10 == 9)
                            {
                                command.CommandText = sb.ToString(0, sb.Length - 1);
                                await command.ExecuteNonQueryAsync();
                                sb = new StringBuilder("INSERT INTO public.\"DicKatos\"( \"Id\", \"Code\", \"NameRu\", \"NameKz\", \"Created\", \"AreaType\", \"Level\", \"ParentId\") VALUES ");
                            }
                            */
                        }
                        
                        command.CommandText = sb.ToString(0, sb.Length - 1);
                        await command.ExecuteNonQueryAsync();
                        
                    }
                }
            }
        }
    }
}

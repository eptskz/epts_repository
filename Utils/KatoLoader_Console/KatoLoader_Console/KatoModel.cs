﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatoLoader_Console
{
    public class KatoModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public bool? IsMarkedToDelete { get; set; }
        public string NameKaz { get; set; }
        public string NameRus { get; set; }
        public int? Parent { get; set; }
        public int Level { get; set; }
        public int AreaType { get; set; }
    }
}

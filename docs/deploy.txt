Environment: Visual Studio 2019

1. Правой кнопкой мыши по проекту EptsProject
2. Выбрать пункт меню Publish (Публиковать) - Folder (В папку)
3. Указать следующие настройки
	- Configuration: Release;
	- Target Framwork: netcoreapp3.1;
	- Deployment Mode: Self-Contained;
	- Target Runtime: linux-x64;
4. После публикации все файлы собрать в архив за исключением:
	- appsettings.json
	- web.config
5. На вебсервере 67 необходимо остановить сервис
	systemctl stop eptshost
6. Скопировать файлы с заменой в /home/eptsuser/epts
7. Запустить сервис
8. На вебсервере 67 необходимо запустить сервис
	systemctl start eptshost
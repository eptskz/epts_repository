﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EptsBL.Utils
{
    public class Utf8StringWriter : StringWriter
    {
        public override System.Text.Encoding Encoding => System.Text.Encoding.UTF8;
    }
}

﻿using DBCore;
using DBCore.Models;
using DBCore.Models.Notifications;
using EptsBL.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EptsBL.Services
{
    public class NotificationService : INotificationService
    {
        private IUnitOfWork _unitOfWork;
        public NotificationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Send(IList<Notification> notificaitons)
        {
            foreach(var n in notificaitons)
            {
                _unitOfWork.NotificationRepository.Insert(n);
            }
            _unitOfWork.Save();
        }

        public void Send(Notification notificaiton, IList<Guid> receiveUserIds)
        {
            foreach (var userId in receiveUserIds)
            {
                var userProfile = _unitOfWork.UserProfileRepository
                    .GetAsIQueryable(ur => ur.Id == userId).FirstOrDefault();
                var n = new Notification()
                {
                    Title = notificaiton.Title,
                    Detail = notificaiton.Detail,
                    SendToUserId = userId,
                    SendToUserName = userProfile.LastName + " "
                    + userProfile.FirstName + " "
                    + userProfile.SecondName,
                    Author = notificaiton.Author,
                    CreateDate = DateTime.Now,
                    ReadDate = null,
                };
                _unitOfWork.NotificationRepository.Insert(n);
            }
            _unitOfWork.Save();
        }
    }
}

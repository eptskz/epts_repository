﻿using EptsBL.Services.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EptsBL.Services
{
    public class CertificateService : ICertificateService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IConfiguration _configuration;
        public CertificateService(IHttpClientFactory clientFactory, IConfiguration configuration)
        {
            _clientFactory = clientFactory;
            _configuration = configuration;
        }

        public async Task<bool> VerifyCertificate(string certificate)
        {
            var httpClient = _clientFactory.CreateClient();

            httpClient.DefaultRequestHeaders.Clear();

            var response = await httpClient.PostAsJsonAsync(_configuration["EcpService"], new
            {
                certificate = certificate
            });
            var stringContent = await response.Content.ReadAsStringAsync();
            return stringContent == "true";
        }
    }
}

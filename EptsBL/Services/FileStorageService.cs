﻿using EptsBL.Services.Interface;
using Microsoft.Extensions.Configuration;
using Minio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EptsBL.Services
{
    public class FileStorageService : IFileStorageService
    {
        private IConfiguration _configuration;
        private MinioClient _client;
        public FileStorageService(IConfiguration config)
        {
            _configuration = config;
            InitializeMinioFromConfig();
        }

        public async Task AddAsync(string bucketName, string objectName, Stream stream, string contentType)
        {
            stream.Seek(0, SeekOrigin.Begin);

            await MakeBucketIfNotExists(bucketName);
            await _client.PutObjectAsync(bucketName, objectName, stream, stream.Length, contentType);
        }

        public async Task<byte[]> GetAsync(string bucketName, string objectName)
        {
            byte[] result = null;
            try
            {
                await _client.GetObjectAsync(bucketName, objectName,
                stream =>
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        stream.CopyTo(memoryStream);
                        result = memoryStream.ToArray();
                    }
                });
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;
        }

        public async Task RemoveAsync(string bucketName, string objectName, bool removeBucketIfEmpty = true)
        {
            await _client.RemoveObjectAsync(bucketName, objectName);

            if (removeBucketIfEmpty && !await _client.ListObjectsAsync(bucketName).Any())            
                await _client.RemoveBucketAsync(bucketName);
        }

        public async Task CopyAsync(string bucketName, string objectName, string destBucketName, string destObjectName = null)
        {
            await _client.CopyObjectAsync(bucketName, objectName, destBucketName, destObjectName);
        }


        private void InitializeMinioFromConfig()
        {
            var minioConfig = _configuration.GetSection("minio");

            var endpoint = minioConfig["endpoint"];
            var withSsl = bool.Parse(minioConfig["withSsl"]);

            var credential = minioConfig.GetSection("credential");

            var accessKey = credential["accessKey"];
            var secretKey = credential["secretKey"];

            _client = withSsl
                ? new MinioClient(endpoint, accessKey, secretKey).WithSSL()
                : new MinioClient(endpoint, accessKey, secretKey);
        }

        private async Task MakeBucketIfNotExists(string bucketName)
        {
            if (!await _client.BucketExistsAsync(bucketName))
                await _client.MakeBucketAsync(bucketName);
        }
    }
}

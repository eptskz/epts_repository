﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace EptsBL.Services.Interface
{
    public interface IFileStorageService
    {
        Task AddAsync(string bucketName, string objectName, Stream stream, string contentType);
        Task<byte[]> GetAsync(string bucketName, string objectName);
        Task RemoveAsync(string bucketName, string objectName, bool removeBucketIfEmpty = true);
    }
}

﻿using DBCore.Models.Notifications;
using System;
using System.Collections.Generic;
using System.Text;

namespace EptsBL.Services.Interface
{
    public interface INotificationService
    {
        void Send(IList<Notification> notificaitons);
        void Send(Notification notificaiton, IList<Guid> receiveUserIds);
    }
}

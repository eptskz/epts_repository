﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EptsBL.Services.Interface
{
    public interface ICertificateService
    {
        Task<bool> VerifyCertificate(string certificate);
    }
}

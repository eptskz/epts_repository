﻿using EptsBL.Services;
using EptsBL.Services.Interface;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace EptsBL
{
    public class EptsBLModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddSingleton<ICertificateService, CertificateService>();
            services.AddSingleton<IFileStorageService, FileStorageService>();
            services.AddTransient<INotificationService, NotificationService>();
        }
    }
}

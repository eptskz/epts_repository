﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Linq.Dynamic.Core;
using DBCore.Enums;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace EptsBL.Extensions
{
    public static class QuerableExtention
    {
        public static IQueryable<T> OrderByCommonField<T>(this IQueryable<T> query, string sortField, SortDirectionEnum direction)
        {
            PropertyInfo prop = typeof(T).GetProperty(sortField, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            if (direction == SortDirectionEnum.Descending)
                return query.OrderBy(prop.Name + " DESC");
            return query.OrderBy(prop.Name);
        }

        public static IQueryable<T> OrderByCommonFields<T>(this IQueryable<T> query, string [] sortFields, SortDirectionEnum[] directions)
        {
            for(int i = 0; i < sortFields.Length; i++)
            {
                var sf = sortFields[i];
                var direction = directions[i];
                PropertyInfo prop = typeof(T).GetProperty(sf, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (direction == SortDirectionEnum.Descending)
                    query = query.OrderBy(prop.Name + " DESC");
                else
                    query = query.OrderBy(prop.Name);
            }

            return query;
        }



        public static string ToSql<TEntity>(this IQueryable<TEntity> query) where TEntity : class
        {
            using var enumerator = query.Provider.Execute<IEnumerable<TEntity>>(query.Expression).GetEnumerator();
            var relationalCommandCache = enumerator.Private("_relationalCommandCache");
            var selectExpression = relationalCommandCache.Private<SelectExpression>("_selectExpression");
            var factory = relationalCommandCache.Private<IQuerySqlGeneratorFactory>("_querySqlGeneratorFactory");

            var sqlGenerator = factory.Create();
            var command = sqlGenerator.GetCommand(selectExpression);

            string sql = command.CommandText;
            return sql;
        }

        private static object Private(this object obj, string privateField) => obj?.GetType().GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(obj);
        private static T Private<T>(this object obj, string privateField) => (T)obj?.GetType().GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(obj);

    }
}

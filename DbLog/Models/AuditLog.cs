﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbLog.Models
{
    public class AuditLog
    {
        public long Id { get; set; }
        public int? VersionNo { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public AuditActionType ActionType { get; set; }
        public DateTime ActionDate { get; set; }
        public string EntityTableName { get; set; }
        public string EntityId { get; set; }
        public string OldEntity { get; set; }
        public string NewEntity { get; set; }
        public string IpAddress { get; set; }
    }

    public enum AuditActionType
    {
        Insert = 1,
        Update = 2,
        Delete = 3,
        Restore = 4,
    }
}

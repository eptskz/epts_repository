﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbLog.Models
{
    public class UserLog
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Message { get; set; }
        public string AdditionalInfo { get; set; }
        public string IpAddress { get; set; }
        public DateTime EventDate { get; set; }
    }
}

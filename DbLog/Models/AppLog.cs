﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbLog.Models
{
    public class AppLog
    {
        public long Id { get; set; }
        public string Message { get; set; }
        public string IpAddress { get; set; }
        public string Exception { get; set; }
        public string Machine { get; set; }
        public string Application { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string LoggedDate { get; set; }
    }
}

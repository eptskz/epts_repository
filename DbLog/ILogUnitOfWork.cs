﻿using DbLog.Models;
using DbLog.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DbLog
{
    public interface ILogUnitOfWork
    {
        UserLogRepository UserLogRepository { get; }
        AuditLogRepository AuditLogRepository { get; }
        AppLogRepository AppLogRepository { get; }

        Task LogUserInfo(UserLog userLog);
        Task LogAuditInfo(AuditLog userLog);
    }
}

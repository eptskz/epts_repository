﻿using DbLog.Models;
using DbLog.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DbLog
{
    public class LogUnitOfWork : ILogUnitOfWork
    {
        private LogContext _logContext;
        public LogUnitOfWork(LogContext logContext)
        {
            _logContext = logContext;
        }
        private UserLogRepository _userLogRepository;
        public UserLogRepository UserLogRepository { 
            get
            {
                return _userLogRepository ?? (_userLogRepository = new UserLogRepository(_logContext));
            }
        }

        private AuditLogRepository _auditLogRepository;
        public AuditLogRepository AuditLogRepository { 
            get
            {
                return _auditLogRepository ?? (_auditLogRepository = new AuditLogRepository(_logContext));
            }
        }

        private AppLogRepository _appLogRepository;
        public AppLogRepository AppLogRepository
        {
            get
            {
                return _appLogRepository ?? (_appLogRepository = new AppLogRepository(_logContext));
            }
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _logContext.SaveChangesAsync(true);
        }

        public async Task LogUserInfo(UserLog userLog)
        {
            await UserLogRepository.InsertAsync(userLog);
            await _logContext.SaveChangesAsync();
        }

        public async Task LogAuditInfo(AuditLog auditLog)
        {
            await AuditLogRepository.InsertAsync(auditLog);
            await _logContext.SaveChangesAsync();
        }
    }
}

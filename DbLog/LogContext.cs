﻿using DbLog.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DbLog
{
    public class LogContext: DbContext
    {
        public LogContext() { }

        public LogContext(DbContextOptions<LogContext> options)
            : base(options) { }

        public DbSet<AuditLog> AuditLogs { get; set; }
        public DbSet<UserLog> UserLogs { get; set; }        
        public DbSet<AppLog> AppLogs { get; set; }
    }
}

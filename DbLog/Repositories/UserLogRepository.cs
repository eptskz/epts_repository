﻿using DbLog.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DbLog.Repositories
{
    public class UserLogRepository
    {
        protected readonly Logger _logger = LogManager.GetCurrentClassLogger();
        protected readonly LogContext _logContext;

        public UserLogRepository(LogContext logContext)
        {
            this._logContext = logContext;
        }

        public virtual IQueryable<UserLog> GetAsIQueryable(
            Expression<Func<UserLog, bool>> filter = null,
            bool isAsNoTracking = false)
        {
            IQueryable<UserLog> query = isAsNoTracking ? _logContext.UserLogs.AsNoTracking() : _logContext.UserLogs;

            if (filter != null)
                query = query.Where(filter);
            return query;
        }

        public async Task<UserLog> GetByIdAsync(long id)
        {
            return await this._logContext.UserLogs.FindAsync(id);
        }

        public async Task<UserLog> InsertAsync(UserLog entity)
        {
            var entityEntry = await this._logContext.UserLogs.AddAsync(entity);
            return entityEntry.Entity;
        }
    }
}

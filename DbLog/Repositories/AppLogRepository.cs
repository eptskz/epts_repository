﻿using DbLog.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DbLog.Repositories
{
    public class AppLogRepository
    {
        protected readonly Logger _logger = LogManager.GetCurrentClassLogger();
        protected readonly LogContext _logContext;

        public AppLogRepository(LogContext logContext)
        {
            this._logContext = logContext;
        }

        public virtual IQueryable<AppLog> GetAsIQueryable(
            Expression<Func<AppLog, bool>> filter = null,
            bool isAsNoTracking = false)
        {
            IQueryable<AppLog> query = isAsNoTracking
                ? _logContext.AppLogs.AsNoTracking()
                : _logContext.AppLogs;

            if (filter != null)
                query = query.Where(filter);
            return query;
        }

        public async Task<AppLog> GetByIdAsync(long id)
        {
            return await this._logContext.AppLogs.FindAsync(id);
        }

        public async Task<AppLog> InsertAsync(AppLog entity)
        {
            var entityEntry = await this._logContext.AppLogs.AddAsync(entity);
            return entityEntry.Entity;
        }
    }
}

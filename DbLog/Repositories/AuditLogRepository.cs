﻿using DbLog.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DbLog.Repositories
{
    public class AuditLogRepository
    {
        protected readonly Logger _logger = LogManager.GetCurrentClassLogger();
        protected readonly LogContext _logContext;

        public AuditLogRepository(LogContext logContext)
        {
            this._logContext = logContext;
        }

        public virtual IQueryable<AuditLog> GetAsIQueryable(
            Expression<Func<AuditLog, bool>> filter = null,
            bool isAsNoTracking = false)
        {
            IQueryable<AuditLog> query = isAsNoTracking
                ? _logContext.AuditLogs.AsNoTracking()
                : _logContext.AuditLogs;

            if (filter != null)
                query = query.Where(filter);
            return query;
        }

        public async Task<AuditLog> GetByIdAsync(long id)
        {
            return await this._logContext.AuditLogs.FindAsync(id);
        }

        public async Task<AuditLog> InsertAsync(AuditLog entity)
        {
            var entityEntry = await this._logContext.AuditLogs.AddAsync(entity);
            return entityEntry.Entity;
        }
    }
}

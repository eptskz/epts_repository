﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbLog.Base
{
    public interface IAuditEntity
    {
        object _Id { get; }
        string _CurrenUserId { get; }
        string _CurrenUserName { get; }
        string _IpAddress { get; }
    }
}

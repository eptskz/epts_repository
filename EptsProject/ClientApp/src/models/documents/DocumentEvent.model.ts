﻿export class DocumentEventModel {
    constructor(
        public Id: number,
        public documentId: string,
        public documentType: string,
        public eventType: number,
        public eventDateTime: string,
        public title: string,
        public description: string,
        public authorId: string,
        public authorName: string,
    ) { }
}
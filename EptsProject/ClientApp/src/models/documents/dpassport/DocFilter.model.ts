﻿export class DocFilterModel {
    constructor(
        public typeCode: string | null = null,
        public search: string | null = null,
    ) { }
}
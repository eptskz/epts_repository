﻿export class MonitoringFilterModel {
    constructor(
        public service: string | null = null,
        public from: string | null = null,
        public till: string | null = null,
    ) { }
}
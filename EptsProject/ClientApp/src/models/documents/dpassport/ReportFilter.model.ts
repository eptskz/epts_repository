﻿export class ReportFilterModel {
    constructor(
        public region: string | null = null,
        public org: string | null = null,
        public kind: string | null = null,
        public status: string | null = null,
        public regKind: string | null = null,
        public from: string | null = null,
        public till: string | null = null,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleLabelLocationModel {
    constructor(
        public id: number = 0,
        public vehicleLabelingDetailId?: number | null,

        public locationText?: string | null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
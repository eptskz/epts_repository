﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleUseRestrictionModel {
    constructor(
        public id: number = 0,
        public vehicleTypeDetailId?: string | null,

        public vehicleUseRestrictionText?: string | null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { BlankNumberModel } from './BlankNumber.model';
import { VinModel } from './Vin.model';
import { VehicleLabelingDetailModel } from './VehicleLabelingDetail.model';
import { VehicleTypeDetailModel } from '@/models/vehicle/VehicleTypeDetail.model';
import { BaseVehicleTypeDetailModel } from '@/models/vehicle/BaseVehicleTypeDetail.model';
import { AssemblyKitSupplierModel } from './AssemblyKitSupplier.model';
import { AssemblyPlantModel } from './AssemblyPlant.model';
import { ReportFilterModel } from '../dpassport/ReportFilter.model';
import { RepresentativeManufacturerModel } from './RepresentativeManufacturer.model';

export class ComplienceDocumentModel {
    constructor(
        public id: string = '00000000-0000-0000-0000-000000000000',
        public docTypeId?: number | null,
        public docTypeCode?: string,
        public docTypeName?: string,
        public docNumber?: string,

        public docDate?: Date | string | null,
        public docDateFormatted?: string | null,
        public docStartDate?: Date | null,
        public docStartDateFormatted?: string | null,        
        public docEndDate?: Date | null,        
        public docEndDateFormatted?: string | null,
        public deleted?: Date | null,

        public countryId?: number | null,
        public country?: any | null,

        public authorityId?: number | null,
        public authorityName?: string | null,
        public authority?: any | null,

        public notManufacturerIndicator: boolean = false,
        public manufacturerId?: number | null,
        public manufacturerName?: string | null,
        public manufacturer?: any | null,

        public notApplicantOrgIndicator: boolean = false,
        public applicantId?: number | null,
        public applicantName?: string | null,
        public applicant?: any | null,

        public notRepresentativeManufacturerIndicator: boolean = false,
        public representativeManufacturers: Array<RepresentativeManufacturerModel> = [],
/*
        public representativeManufacturerId?: number | null,
        public representativeManufacturerName?: string | null,
        public representativeManufacturer?: any | null,
*/
        public notAssemblyPlantIndicator: boolean = false,
        public assemblyPlants: Array<AssemblyPlantModel> = [],

/*
        public assemblyPlantId?: number | null,
        public assemblyPlantName?: string | null,
        public assemblyPlant?: any | null,
*/
        public notAssemblyKitSupplierIndicator: boolean = false,
        public assemblyKitSuppliers: Array<AssemblyKitSupplierModel> = [],
/*
        public assemblyKitSupplierId?: number | null,
        public assemblyKitSupplierName?: string | null,
        public assemblyKitSupplier?: any | null,
*/

        public docDistributionSign: number = 0,
        public batchCount?: number | null,
        public batchSmallSign: number = 0,

        public blankNumbers: BlankNumberModel[] = [],
        public vinNumbers: VinModel[] = [],

        public vehicleLabelingDetail: VehicleLabelingDetailModel = new VehicleLabelingDetailModel(),

        public vehicleTypeDetail: VehicleTypeDetailModel = new VehicleTypeDetailModel(),

        public statusId: number | null = null,
        public statusCode: string | null = 'draw',
        public statusName: string | null = 'Черновик',

        public isChanged: boolean  = false,
        public brandName?: string,
        public type?: string,
        public categoryName?: string,
    ) { }
}
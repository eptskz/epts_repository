﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VinModel {
    constructor(
        public id: number = 0,
        public documentId?: string | null,

        public number?: string | null,
        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
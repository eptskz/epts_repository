﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleVinCharacterDescriptionModel {
    constructor(
        public id: number = 0,
        public vinCharacterDetailId: number = 0,
        public idCharacterValue?: string | null,
        public idCharacterValueText?: string | null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
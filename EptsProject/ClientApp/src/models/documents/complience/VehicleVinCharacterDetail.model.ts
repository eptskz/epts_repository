﻿import { ObjectState } from '@/models/enums/ObjectState.enum';
import { VehicleVinCharacterDescriptionModel } from './VehicleVinCharacterDescription.model';

export class VehicleVinCharacterDetailModel {
    constructor (
        public id: number = 0,
        public vehicleLabelingDetailId?: number | null,

        public idCharacterStartingOrdinal?: number | null,
        public idCharacterEndingOrdinal?: number | null,
        public idCharacterText?: string | null,

        public dataTypeId?: number | null,
        public dataTypeCode?: string | null,
        public dataTypeName?: string | null,


        public isAlpha: boolean = false,
        public isDigit: boolean = false,
        public isIncludeChar: boolean = false,
        public includeCharStr?: string | null,

        public isExcludeChar: boolean = false,
        public excludeCharStr?: string | null,

        public vinCharacterDescriptions: Array<VehicleVinCharacterDescriptionModel> = [],

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
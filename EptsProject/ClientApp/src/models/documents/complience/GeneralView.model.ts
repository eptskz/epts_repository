﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class GeneralViewModel {
    constructor(
        public id: string = '',
        public documentId?: string | null,

        public vehiclePicture?: any | null,
        public fileName?: string | null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
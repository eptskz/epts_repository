﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class ShassisMovePermitionModel {
    constructor(
        public id: number = 0,
        public vehicleTypeDetailId?: string | null,

        public shassisMovePermitionText?: string | null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleSteeringDescriptionModel {
    constructor(
        public id: number = 0,
        public characteristicId?: number | null,

        public description?: string | null,
        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
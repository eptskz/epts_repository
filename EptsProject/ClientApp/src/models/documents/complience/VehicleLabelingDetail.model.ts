﻿import { ObjectState } from '../../enums/ObjectState.enum';
import { VehicleLabelLocationModel } from './VehicleLabelLocation.model';
import { VehicleVinCharacterDetailModel } from './VehicleVinCharacterDetail.model';

export class VehicleLabelingDetailModel {
    constructor(
        public id: number = 0,
        public documentId?: string | null,

        public notManufacturerPlateIndicatorSign: number = 0,
        public vehicleComponentLocationText?: string | null,
        public vehicleLabelLocations: Array<VehicleLabelLocationModel> = [],

        public vehicleIdentificationNumberLocationText?: string | null,
        public vehicleIdentificationNumberLocations: Array<VehicleLabelLocationModel> = [],

        public engineIdentificationNumberLocationText?: string | null,
        public vin?: string | null,

        public vinCharacterDetails: Array<VehicleVinCharacterDetailModel> = [],

        public state: ObjectState = ObjectState.Unchanged
    ) { }
}
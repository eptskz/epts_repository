﻿export class ComplienceDocFilterModel {
    constructor(
        public typeCode: string | null = null,
        public statusCode: string | null = null,
        public showDeleted: boolean = false
    ) { }
}
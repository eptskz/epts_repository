﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class RepresentativeManufacturerModel {
    constructor(
        public id: number = 0,               
        public documentId: string | null = null,
        public document: string | null = null,
        public organizationId: number | null = null,        
        public organization: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
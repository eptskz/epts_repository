﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleRoutingModel {
    constructor(
        public id: number = 0,
        public vehicleTypeDetailId?: string | null,

        public vehicleRoutingText?: string | null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
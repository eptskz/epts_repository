﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleNoteModel {
    constructor(
        public id: number = 0,
        public vehicleTypeDetailId?: string | null,

        public noteText?: string | null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿export class ProcessRequestModel {
    constructor(
        public DocId: string,
        public StatusCode: string,
        public Message: string,
    ) { }
}
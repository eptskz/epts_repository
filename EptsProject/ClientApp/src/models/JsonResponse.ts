﻿export enum Action {
    ShouldLogin, AccessDenied, Ok
}

export class JsonResponse {
    constructor(
        public status: string,
        public text: string,
        public action: Action,
        public data: any
    ) { }
}
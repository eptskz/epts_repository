﻿export class ChangePasswordModel {
    constructor(
        public login?: string,
        public token?: string,
        public oldPassword?: string,
        public password?: string,
        public confirmPassword?: string        
    ) { }
}
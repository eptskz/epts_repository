﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class ModificationModel {
    constructor(
        public id: number = 0,
        public vehicleTypeDetailId: string | null = null,

        public baseVehicleTypeDetailId: number | null = null,

        public name: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
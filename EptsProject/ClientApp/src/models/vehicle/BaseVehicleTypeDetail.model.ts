﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class BaseVehicleTypeDetailModel {
    constructor(
        public id: number = 0,        
        public vehicleTypeDetailId: string | null = null,

        public commercialName: string | null = null,

        public type: string | null = null,

        public makeId: number | null = null,
        public makeName: string | null = null,

        public modification: string | null = null,
        public vin: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
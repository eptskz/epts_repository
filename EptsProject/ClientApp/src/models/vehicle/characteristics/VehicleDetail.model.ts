﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class ECUModelDetailModel {
    constructor(
        public id: number = 0,        
        public engineDetailId: number | null = null,

        public ecuModel: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
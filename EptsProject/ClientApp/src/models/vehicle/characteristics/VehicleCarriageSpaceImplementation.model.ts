﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleCarriageSpaceImplementationModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public carriageSpaceImplementation: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
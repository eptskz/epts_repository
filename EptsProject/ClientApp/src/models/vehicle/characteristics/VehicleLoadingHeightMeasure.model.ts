﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleLoadingHeightMeasureModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public loadingHeight: number | null = null,
        public loadingHeightMax: number | null = null,

        public unitId: number | null = null,
        public unitCode: string | null = '110003',
        public unitName: string | null = 'мм',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
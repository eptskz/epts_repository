﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class PoweredWheelLocationModel {
    constructor(
        public id: number = 0,        
        public vehicleRunningGearDetailId: number | null = null,

        public wheelLocationId: number | null = null,
        public wheelLocationCode: string | null = null,
        public wheelLocationName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
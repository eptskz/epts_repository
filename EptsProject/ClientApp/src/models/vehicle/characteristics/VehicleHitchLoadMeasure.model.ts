﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleHitchLoadMeasureModel {
    constructor(
        public id: number = 0,
        public characteristicId: number | null = null,

        public hitchLoadMeasure: number | null = null,
        public hitchLoadMeasureMax: number | null = null,

        public unitId: number | null = null,
        public unitCode: string | null = '1001',
        public unitName: string | null = 'даН',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
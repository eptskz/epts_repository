﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class EnginePowerDetailModel {
    constructor(
        public id: number = 0,        
        public engineDetailId: number | null = null,

        public engineMaxPowerMeasure: number | null = null,
        public engineMaxPowerMeasureUnitId: number | null = null,
        public engineMaxPowerMeasureUnitCode: string | null = '150214',
        public engineMaxPowerMeasureUnitName: string | null = 'кВт',

        public engineMaxPowerShaftRotationFrequencyMinMeasure: number | null = null,
        public engineMaxPowerShaftRotationFrequencyMaxMeasure: number | null = null,
        public engineMaxPowerShaftRotationFrequencyMeasureUnitId: number | null = null,
        public engineMaxPowerShaftRotationFrequencyMeasureUnitCode: string | null = '150331',
        public engineMaxPowerShaftRotationFrequencyMeasureUnitName: string | null = 'мин⁻¹',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
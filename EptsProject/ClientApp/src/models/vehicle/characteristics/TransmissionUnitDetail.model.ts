﻿import { ObjectState } from '@/models/enums/ObjectState.enum';
import { TransmissionUnitGearDetailModel } from './TransmissionUnitGearDetail.model';

export class TransmissionUnitDetailModel {
    constructor(
        public id: number = 0,
        public clientId: number = 0,
        public transmissionTypeId: number | null = null,        
        public transmissionTypeName: string | null = null,
        public transTypeId: number | null = null,

        // NSI_018
        public unitKindId: number | null = null,        
        public unitKindCode: string | null = null,
        public unitKindName: string | null = null,

        public continuouslyVariableTransmissionIndicator: number | null = 0,

        /// NSI_026
        public axisDistributionId: number | null = null,
        public axisDistributionName: string | null = null,

        public transmissionUnitMakeName: string | null = null,
        public transmissionUnitDescription: string | null = null,

        // NSI_053
        public transmissionBoxTypeId: number | null = null,
        public transmissionBoxTypeName: string | null = null,

        // NSI_051
        public mainGearTypeId: number | null = null,
        public mainGearTypeName: string | null = null,

        public transferCaseType: string | null = null,
        // public transmissionUnitGearQuantity: number | null = null,

        public transmissionUnitGears: Array<TransmissionUnitGearDetailModel> = [],

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
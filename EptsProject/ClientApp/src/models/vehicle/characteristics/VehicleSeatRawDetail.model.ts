﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleSeatRawDetailModel {
    constructor(
        public id: number = 0,        
        public vehicleSeatDetailId: number | null = null,

        public seatRawOrdinal: number | null = null,
        public seatRawQuantity: number | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';
import { PoweredWheelLocationModel } from './PoweredWheelLocation.model';

export class VehicleRunningGearDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public vehicleWheelFormulaId: number | null = null,
        public vehicleWheelFormulaName: string | null = null,

        //public poweredWheelLocationId: number | null = null,
        //public poweredWheelLocationName: string | null = null,
        public poweredWheelLocations: Array<PoweredWheelLocationModel> = [],

        public vehicleAxleQuantity: number | null = null,
        public vehicleWheelQuantity: number | null = null,
        public vehicleWheelLocation: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
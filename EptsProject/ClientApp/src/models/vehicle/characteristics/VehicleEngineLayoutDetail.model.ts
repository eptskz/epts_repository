﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleEngineLayoutDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public engineLocationId: number | null = null,
        public engineLocationName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
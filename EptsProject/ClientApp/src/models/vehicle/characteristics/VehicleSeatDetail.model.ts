﻿import { ObjectState } from '@/models/enums/ObjectState.enum';
import { VehicleSeatRawDetailModel } from './VehicleSeatRawDetail.model';

export class VehicleSeatDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public seatQuantity: number | null = null,
        public seatDescription: string | null = null,

        public vehicleSeatRawDetails: Array<VehicleSeatRawDetailModel> = [],

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class TransmissionUnitGearDetailModel {
    constructor(
        public id: number = 0,        
        public transmissionUnitId: number | null = null,

        /// NSI_080
        public transmissionUnitGearValueId: number | null = null,
        public transmissionUnitGearValueName: string | null = null,

        /// NSI_081
        public transmissionUnitGearTypeId: number | null = null,
        public transmissionUnitGearTypeName: string | null = null,

        public transmissionUnitGearRate: number | null = null,
        public transmissionUnitGearRateMax: number | null = null,

        public transmissionUnitReverseGearIndicator: number = 0,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
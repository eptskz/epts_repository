﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleCabinModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public cabinDescription: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
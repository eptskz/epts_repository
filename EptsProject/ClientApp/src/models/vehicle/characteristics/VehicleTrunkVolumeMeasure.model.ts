﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleTrunkVolumeMeasureModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public trunkVolumeMeasure: number | null = null,        

        public unitId: number | null = null,
        public unitCode: string | null = '130112',
        public unitName: string | null = 'Литр',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
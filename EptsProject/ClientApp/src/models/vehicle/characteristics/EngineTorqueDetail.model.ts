﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class EngineTorqueDetailModel {
    constructor(
        public id: number = 0,        
        public engineDetailId: number | null = null,

        public engineMaxTorqueMeasure: number | null = null,
        public engineMaxTorqueMeasureUnitId: number | null = null,
        public engineMaxTorqueMeasureUnitCode: string | null = '1000',
        public engineMaxTorqueMeasureUnitName: string | null = 'Н·м',

        public engineMaxTorqueMeasureShaftRotationFrequencyMinMeasure: number | null = null,
        public engineMaxTorqueMeasureShaftRotationFrequencyMaxMeasure: number | null = null,
        public engineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId: number | null = null,
        public engineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitCode: string | null = '150331',
        public engineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitName: string | null = 'мин⁻¹',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
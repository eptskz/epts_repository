﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleBrakingSystemDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        // NSI_029
        public vehicleBrakingSystemKindId: number | null = null,
        public vehicleBrakingSystemKindName: string | null = null,

        public vehicleBrakingSystemDescription: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
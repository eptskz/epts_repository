﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleFuelKindModel {
    constructor(
        public id: number = 0,        
        public engineDetailId: number | null = null,

        public fuelKindId: number | null = null,
        public fuelKindCode: string | null = null,
        public fuelKindName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleEquipmentInfoModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public vehicleEquipmentText: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleHybridDesignModel {
    constructor(
        public id: number = 0,
        public characteristicId: number | null = null,

        public vehicleHybridDesignText: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
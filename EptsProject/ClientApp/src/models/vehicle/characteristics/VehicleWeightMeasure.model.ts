﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleWeightMeasureModel {
    constructor(
        public id: number = 0,
        public characteristicId: number | null = null,

        public weight: number | null = null,
        public weightMax: number | null = null,

        public unitId: number | null = null,
        public unitCode: string | null = '140166',
        public unitName: string | null = 'кг',

        public massTypeId: number | null = null,
        public massTypeName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
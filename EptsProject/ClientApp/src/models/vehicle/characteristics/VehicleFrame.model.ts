﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleFrameModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public frameText: string | null = null,        

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleIgnitionDetailModel {
    constructor(
        public id: number = 0,        
        public engineDetailId: number | null = null,

        public vehicleIgnitionTypeId: number | null = null,
        public vehicleIgnitionTypeName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';
import { TransmissionUnitDetailModel } from './TransmissionUnitDetail.model';


export class TransmissionTypeModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public transTypeId: number | null = null,
        public transTypeName: string | null = null,

        public transmissionUnitDetails: Array<TransmissionUnitDetailModel> = [],


        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
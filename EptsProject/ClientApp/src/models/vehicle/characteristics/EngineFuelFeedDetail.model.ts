﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class EngineFuelFeedDetailModel {
    constructor(
        public id: number = 0,        
        public engineDetailId: number | null = null,

        public fuelFeedId: number | null = null,
        public fuelFeedCode: string | null = null,
        public fuelFeedName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';
import { PowerStorageDeviceDetailModel } from '@/models/vehicle/characteristics/PowerStorageDeviceDetail.model';

export class ElectricalMachineDetailModel {
    constructor(
        public id: number = 0,
        public clientId: number = 0,
        public characteristicsDetailId: number | null = null,        

        public electricalMachineKindId: number | null = null,
        public electricalMachineKindName: string | null = null,

        public electricalMachineTypeId: number | null = null,
        public electricalMachineTypeName: string | null = null,

        public vehicleComponentMakeName: string | null = null,
        public vehicleComponentText: string | null = null,

        public electricMotorPowerMeasure: number | null = null,
        public electricMotorPowerMeasureUnitId: number | null = null,
        public electricMotorPowerMeasureUnitCode: string | null = '150214',
        public electricMotorPowerMeasureUnitName: string | null = 'кВт',

        public electricalMachineVoltageMeasure: number | null = null,
        public electricalMachineVoltageMeasureUnitId: number | null = null,
        public electricalMachineVoltageMeasureUnitCode: string | null = '150222',
        public electricalMachineVoltageMeasureUnitName: string | null = 'В',

        public notPowerStorageDeviceDetailIndicator: number | null = 0,
        public powerStorageDeviceDetails: Array<PowerStorageDeviceDetailModel> = [],


        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleTyreKindInfoModel {
    constructor(
        public id: number = 0,        
        public characteristicsDetailId: number | null = null,

        public vehicleTyreKindSize: string | null = null,
        public vehicleTyreKindIndex: string | null = null,

        public vehicleTyreKindSpeedId: number | null = null,
        public vehicleTyreKindSpeedName: string | null = null,

        public isSupplementVehicleTyre: number | null = 0,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleSteeringDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,
        
        public steeringWheelPositionId: number | null = null,
        public steeringWheelPositionCode: string | null = null,
        public steeringWheelPositionName: string | null = null,

        public vehicleSteeringType: string | null = null,
        public vehicleSteeringDescription: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
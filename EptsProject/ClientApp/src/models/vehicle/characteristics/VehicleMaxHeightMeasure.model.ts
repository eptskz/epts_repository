﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleMaxHeightMeasureModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public maxHeightMeasure: number | null = null,
        public maxHeightMeasureMax: number | null = null,

        public unitId: number | null = null,
        public unitCode: string | null = '110003',
        public unitName: string | null = 'мм',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
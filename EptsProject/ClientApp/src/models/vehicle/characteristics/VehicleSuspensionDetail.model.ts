﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleSuspensionDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicsDetailId: number | null = null,

        public vehicleSuspensionKindId: number | null = null,
        public vehicleSuspensionKindName: string | null = null,

        public vehicleSuspensionDescription: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
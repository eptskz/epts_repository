﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleHeightMeasureModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public height: number | null = null,
        public heightMax: number | null = null,

        public unitId: number | null = null,
        public unitCode: string | null = '110003',
        public unitName: string | null = 'мм',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class PowerStorageDeviceDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,
        public electricalMachineDetailId: number | null = null,

        public powerStorageDeviceTypeId: number | null = null,
        public powerStorageDeviceTypeName: string | null = null,

        public powerStorageDeviceDescription: string | null = null,
        public powerStorageDeviceLocation: string | null = null,

        public vehicleRangeMeasure: number | null = null,
        public vehicleRangeUnitId: number | null = null,
        public vehicleRangeUnitCode: string | null = '110008',
        public vehicleRangeUnitName: string | null = 'КМ',

        public powerStorageDeviceVoltageMeasure: number | null = null,
        public powerStorageDeviceVoltageMeasureUnitId: number | null = null,
        public powerStorageDeviceVoltageMeasureUnitCode: string | null = '150222',
        public powerStorageDeviceVoltageMeasureUnitName: string | null = 'В',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
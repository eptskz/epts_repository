﻿import { ObjectState } from '@/models/enums/ObjectState.enum';
import { ECUModelDetailModel } from './ECUModelDetail.model';
import { EngineFuelFeedDetailModel } from './EngineFuelFeedDetail.model';
import { EnginePowerDetailModel } from './EnginePowerDetail.model';
import { EngineTorqueDetailModel } from './EngineTorqueDetail.model';
import { ExhaustDetailModel } from './ExhaustDetail.model';
import { VehicleFuelKindModel } from './VehicleFuelKind.model';
import { VehicleIgnitionDetailModel } from './VehicleIgnitionDetail.model';

export class EngineDetailModel {
    constructor(
        public id: number = 0,
        public clientId: number = 0,
        public characteristicsDetailId: number | null = null,

        public vehicleComponentMakeName: string | null = null,
        public vehicleComponentText: string | null = null,
        
        public engineCylinderQuantity: number | null = null,
        
        public engineCylinderArrangementId: number | null = null,
        public engineCylinderArrangementName: string | null = null,

        public engineCapacityMeasure: number | null = null,
        public engineCapacityMeasureUnitId: number | null = null,
        public engineCapacityMeasureUnitCode: string | null = '130111',
        public engineCapacityMeasureUnitName: string | null = 'см3',

        public engineCompressionRate: string | null = null,


        public enginePowerDetails: Array<EnginePowerDetailModel> = [],

        public engineTorqueDetails: Array<EngineTorqueDetailModel> = [],
        
        // топлива NSI_030
        public notVehicleFuelIndicator: number | null = 0,
        public vehicleFuelKinds: Array<VehicleFuelKindModel> = [],

        // Система питания NSI_056
        public notFuelFeedDetailIndicator: number | null = 0,
        public engineFuelFeedDetails: Array<EngineFuelFeedDetailModel> = [],

        /// <summary>
        /// Блок управления
        /// </summary>
        public ecuModelDetails: Array<ECUModelDetailModel> = [],

        /// <summary>
        /// Система зажигания NSI_055
        /// </summary>
        public notVehicleIgnitionDetailIndicator: number | null = 0,
        public vehicleIgnitionDetails: Array<VehicleIgnitionDetailModel> = [],

        /// <summary>
        /// Система выпуска и нейтрализации отработавших газов
        /// </summary>
        public notExhaustDetailIndicator: number | null = 0,
        public exhaustDetails: Array<ExhaustDetailModel> = [],

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
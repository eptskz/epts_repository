﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehiclePassengerQuantityModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public passengerQuantity: number | null = null,        

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
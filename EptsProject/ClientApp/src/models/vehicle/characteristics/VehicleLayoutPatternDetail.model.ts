﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleLayoutPatternDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public layoutPatternId: number | null = null,
        public layoutPatternName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
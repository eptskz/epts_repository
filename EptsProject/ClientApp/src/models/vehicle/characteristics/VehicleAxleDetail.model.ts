﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleAxleDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public axleOrdinal: number | null = null,
        public drivingAxleIndicator: number | null = null,

        public axleSweptPathMeasure: number | null = null,
        public axleSweptPathMeasureMax: number | null = null,

        public axleSweptPathMeasureUnitId: number | null = null,
        public axleSweptPathMeasureUnitCode: string | null = '110003',
        public axleSweptPathMeasureUnitName: string | null = 'мм',

        public technicallyPermissibleMaxWeightOnAxleMeasure: number | null = null,
        public technicallyPermissibleMaxWeightOnAxleMeasureMax: number | null = null,

        public technicallyPermissibleMaxWeightOnAxleUnitId: number | null = null,
        public technicallyPermissibleMaxWeightOnAxleUnitCode: string | null = '140166',
        public technicallyPermissibleMaxWeightOnAxleUnitName: string | null = 'кг',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleClutchDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicsDetailId: number | null = null,

        public vehicleClutchMakeName: string | null = null,
        public vehicleClutchDescription: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
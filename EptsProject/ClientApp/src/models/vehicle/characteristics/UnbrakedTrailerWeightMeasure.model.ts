﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class UnbrakedTrailerWeightMeasureModel {
    constructor(
        public id: number = 0,
        public characteristicId: number | null = null,

        public weightMeasure: number | null = null,
        public weightMeasureMax: number | null = null,

        public unitId: number | null = null,
        public unitCode: string | null = '140166',
        public unitName: string | null = 'кг',

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
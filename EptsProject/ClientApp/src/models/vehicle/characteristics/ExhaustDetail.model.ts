﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class ExhaustDetailModel {
    constructor(
        public id: number = 0,        
        public engineDetailId: number | null = null,

        public exhaustDescription: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
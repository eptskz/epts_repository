﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleBodyworkDetailModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public doorQuantity: number | null = null,

        public vehicleBodyWorkTypeId: number | null = null,
        public vehicleBodyWorkTypeName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehiclePurposeModel {
    constructor(
        public id: number = 0,        
        public characteristicId: number | null = null,

        public purposeId: number | null = null,
        public purposeCode: string | null = null,
        public purposeName: string | null = null,

        public otherInfoId: string | null = null,
        public otherInfoName: string | null = null,
        

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { VehicleRunningGearDetailModel } from '@/models/vehicle/characteristics/VehicleRunningGearDetail.model';
import { VehicleBodyworkDetailModel } from '@/models/vehicle/characteristics/VehicleBodyworkDetail.model';
import { VehicleSeatDetailModel } from '@/models/vehicle/characteristics/VehicleSeatDetail.model';
import { VehicleLayoutPatternDetailModel } from '@/models/vehicle/characteristics/VehicleLayoutPatternDetail.model';
import { VehicleEngineLayoutDetailModel } from '@/models/vehicle/characteristics/VehicleEngineLayoutDetail.model';
import { VehicleCarriageSpaceImplementationModel } from '@/models/vehicle/characteristics/VehicleCarriageSpaceImplementation.model';
import { VehicleCabinModel } from '@/models/vehicle/characteristics/VehicleCabin.model';
import { VehiclePurposeModel } from '@/models/vehicle/characteristics/VehiclePurpose.model';
import { VehicleFrameModel } from '@/models/vehicle/characteristics/VehicleFrame.model';
import { VehiclePassengerQuantityModel } from '@/models/vehicle/characteristics/VehiclePassengerQuantity.model';
import { VehicleTrunkVolumeMeasureModel } from '@/models/vehicle/characteristics/VehicleTrunkVolumeMeasure.model';

import { VehicleHeightMeasureModel } from '@/models/vehicle/characteristics/VehicleHeightMeasure.model';
import { VehicleLengthMeasureModel } from '@/models/vehicle/characteristics/VehicleLengthMeasure.model';
import { VehicleMaxHeightMeasureModel } from '@/models/vehicle/characteristics/VehicleMaxHeightMeasure.model';
import { VehicleWidthMeasureModel } from '@/models/vehicle/characteristics/VehicleWidthMeasure.model';
import { VehicleWheelbaseMeasureModel } from '@/models/vehicle/characteristics/VehicleWheelbaseMeasure.model';
import { VehicleLoadingHeightMeasureModel } from '@/models/vehicle/characteristics/VehicleLoadingHeightMeasure.model';
import { VehicleAxleDetailModel } from '@/models/vehicle/characteristics/VehicleAxleDetail.model';

import { VehicleWeightMeasureModel } from '@/models/vehicle/characteristics/VehicleWeightMeasure.model';
import { UnbrakedTrailerWeightMeasureModel } from '@/models/vehicle/characteristics/UnbrakedTrailerWeightMeasure.model';
import { BrakedTrailerWeightMeasureModel } from '@/models/vehicle/characteristics/BrakedTrailerWeightMeasure.model';
import { VehicleHitchLoadMeasureModel } from '@/models/vehicle/characteristics/VehicleHitchLoadMeasure.model';

import { EngineDetailModel } from '@/models/vehicle/characteristics/EngineDetail.model';
import { ElectricalMachineDetailModel } from '@/models/vehicle/characteristics/ElectricalMachineDetail.model';

import { VehicleClutchDetailModel } from '@/models/vehicle/characteristics/VehicleClutchDetail.model';
import { TransmissionTypeModel } from '@/models/vehicle/characteristics/TransmissionType.model';

import { VehicleSuspensionDetailModel } from '@/models/vehicle/characteristics/VehicleSuspensionDetail.model';
import { VehicleSteeringDetailModel } from '@/models/vehicle/characteristics/VehicleSteeringDetail.model';
import { VehicleBrakingSystemDetailModel } from '@/models/vehicle/characteristics/VehicleBrakingSystemDetail.model';

import { VehicleEquipmentInfoModel } from '@/models/vehicle/characteristics/VehicleEquipmentInfo.model';
import { VehicleTyreKindInfoModel } from './characteristics/VehicleTyreKindInfo.model';
import { VehicleHybridDesignModel } from './characteristics/VehicleHybridDesign.model';
import { VehicleSteeringDescriptionModel } from '../documents/complience/VehicleSteeringDescription.model';
import { PowerStorageDeviceDetailModel } from './characteristics/PowerStorageDeviceDetail.model';

export class CharacteristicsDetailModel {
    constructor(
        public id: number = 0,
        public vehicleTypeDetailId: string | null = null,

        public vehicleRunningGearDetails: Array<VehicleRunningGearDetailModel> = [],
        public vehicleBodyworkDetails: Array<VehicleBodyworkDetailModel> = [],
        public vehicleSeatDetails: Array<VehicleSeatDetailModel> = [],

        public vehicleLayoutPatterns: Array<VehicleLayoutPatternDetailModel> = [],
        public vehicleEngineLayouts: Array<VehicleEngineLayoutDetailModel> = [],
        public vehicleCarriageSpaceImplementations: Array<VehicleCarriageSpaceImplementationModel> = [],

        public isSpecializedVehicle: number | null = 0,
        public vehicleCabins: Array<VehicleCabinModel> = [],
        public vehiclePurposes: Array<VehiclePurposeModel> = [],
        public vehicleFrames: Array<VehicleFrameModel> = [],
        public vehiclePassengerQuantities: Array<VehiclePassengerQuantityModel> = [],
        public vehicleTrunkVolumeMeasures: Array<VehicleTrunkVolumeMeasureModel> = [],

        public isVariableHeight: number | null = 0,
        public lengthRanges: Array<VehicleLengthMeasureModel> = [],
        public heightRanges: Array<VehicleHeightMeasureModel> = [],
        
        public maxHeightRanges: Array<VehicleMaxHeightMeasureModel> = [],
        public loadingHeightRanges: Array<VehicleLoadingHeightMeasureModel> = [],
        public widthRanges: Array<VehicleWidthMeasureModel> = [],
        public wheelbaseMeasureRanges: Array<VehicleWheelbaseMeasureModel> = [],
        
        public vehicleAxleDetails: Array<VehicleAxleDetailModel> = [],

        public notVehicleTrailerIndicator: number | null = 0,
        public vehicleWeightMeasures: Array<VehicleWeightMeasureModel> = [],
        public unbrakedTrailerWeightMeasures: Array<UnbrakedTrailerWeightMeasureModel> = [],
        public brakedTrailerWeightMeasures: Array<BrakedTrailerWeightMeasureModel> = [],
        public vehicleHitchLoadMeasures: Array<VehicleHitchLoadMeasureModel> = [],

        public notEngineDetailIndicator: number | null = 0,
        public engineTypeId: number | null = null,
        public engineTypeCode: string | null = null,
        public engineTypeName: string | null = null,
        public vehicleHybridDesignText: string | null = null,
        public vehicleHybridDesigns: Array<VehicleHybridDesignModel> = [],
        public engineDetails: Array<EngineDetailModel> = [],

        public notElectricalMachineDetailIndicator: number | null = 0,
        public electricalMachineDetails: Array<ElectricalMachineDetailModel> = [],

        public notPowerStorageDeviceDetailIndicator: number | null = 0,
        public powerStorageDeviceDetails: Array<PowerStorageDeviceDetailModel> = [],

        public notVehicleClutchDetailIndicator: number | null = 0,
        public vehicleClutchDetails: Array<VehicleClutchDetailModel> = [],
        
        public notVehicleTransmissionDetailIndicator: number | null = 0,
        public vehicleTransmissionTypes: Array<TransmissionTypeModel> = [],

        public vehicleSuspensionDetails: Array<VehicleSuspensionDetailModel> = [],

        public notVehicleSteeringDetailIndicator: number | null = 0,
        public vehicleSteeringDetailDescription: string | null = null,
        public vehicleSteeringDescriptions: Array<VehicleSteeringDescriptionModel> = [],
        public vehicleSteeringDetails: Array<VehicleSteeringDetailModel> = [],

        public notVehicleBrakingSystemDetailIndicator: number | null = 0,
        public vehicleBrakingSystemDetails: Array<VehicleBrakingSystemDetailModel> = [],

        public vehicleTyreKindInfos: Array<VehicleTyreKindInfoModel> = [],
        public vehicleEquipmentInfos: Array<VehicleEquipmentInfoModel> = [],
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleTypeModel {
    constructor(
        public id: number = 0,
        public vehicleTypeDetailId: string | null = null,
        public typeValue: string | null = null,
        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
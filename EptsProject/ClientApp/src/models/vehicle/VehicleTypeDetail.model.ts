﻿import { ShassisMovePermitionModel } from '../documents/complience/ShassisMovePermition.model';
import { VehicleNoteModel } from '../documents/complience/VehicleNote.model';
import { VehicleRoutingModel } from '../documents/complience/VehicleRouting.model';
import { VehicleUseRestrictionModel } from '../documents/complience/VehicleUseRestriction.model';
import { BaseVehicleTypeDetailModel } from './BaseVehicleTypeDetail.model';
import { CharacteristicsDetailModel } from './CharacteristicsDetail.model';
import { ModificationModel } from './Modification.model';
import { VehicleCommercialNameModel } from './VehicleCommercialName.model';
import { VehicleTypeModel } from './VehicleType.model';
import { VehicleTypeDicModel } from './VehicleTypeDic.model';

export class VehicleTypeDetailModel {
    constructor(
        public id: string = '00000000-0000-0000-0000-000000000000',
        public documentId: string | null = null,

        public notVehicleMakeNameIndicator: number = 0,
        public makes: Array<VehicleTypeDicModel> = [],

        public notVehicleCommercialNameIndicator: number = 0,
        public commercialName: string | null = null,
        public commercialNames: Array<VehicleCommercialNameModel> = [],

        public type: string | null = null,
        public types: Array<VehicleTypeModel> = [],

        public yearIssue: string | null = null,

        public notVehicleTechCategoryIndicator: number = 0,
        public techCategories: Array<VehicleTypeDicModel> = [],

        public notVehicleClassCategoryIndicator: number = 0,
        public classCategories: Array<VehicleTypeDicModel> = [],

        public notVehicleEcoClassCategoryIndicator: number = 0,
        public ecoClasses: Array<VehicleTypeDicModel> = [],

        public notVehicleModificationIndicator: number = 0,
        public modifications: Array<ModificationModel> = [],

        public notVehicleChassisDesignIndicator: number = 0,
        public chassisDesigns: Array<VehicleTypeDicModel> = [],

        public notBaseVehicleTypeDetailIndicator: number = 0,
        public baseVehicleTypeDetails: BaseVehicleTypeDetailModel[] = [],

        public characteristicsDetail: CharacteristicsDetailModel = new CharacteristicsDetailModel(),

        public vehicleUseRestrictions: VehicleUseRestrictionModel[] = [],
        public shassisMovePermitions: ShassisMovePermitionModel[] = [],
        public vehicleRoutings: VehicleRoutingModel[] = [],
        public vehicleNotes: VehicleNoteModel[] = [],
    ) { }
}
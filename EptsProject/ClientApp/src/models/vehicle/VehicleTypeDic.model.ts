﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleTypeDicModel {
    constructor(
        public id: number = 0,
        public vehicleTypeDetailId: string | null = null,               
        public dicId: number | null = null,
        public dicCode: string | null = null,
        public dicName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class VehicleCommercialNameModel {
    constructor(
        public id: number = 0,
        public vehicleTypeDetailId: string | null = null,
        public commercialName: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
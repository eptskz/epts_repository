﻿export class CdReportRequestModel {
    constructor(

        public organizationIds: Array<number> = [],
        public documentTypeIds: Array<number> = [],
        public statusIds: Array<number> = [],
        public documentNumber: string = '',

        public dateStartFrom?: Date,
        public dateStartFromFormatted?: string,
        public dateStartTo?: Date,
        public dateStartToFormatted?: string,

        public dateEndtFrom?: Date,
        public dateEndTo?: Date,
    ) { }
}
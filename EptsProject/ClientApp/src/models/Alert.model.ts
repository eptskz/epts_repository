﻿export class AlertModel {
    constructor(
        public msg: string = '',
        public type: string = 'info',
        public show: boolean = false
    ) { }
}
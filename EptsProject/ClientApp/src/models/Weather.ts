﻿export class Weather {
    constructor(
        public Datetime: Date,
        public Temperature: number ,
        public Wetness: number,
        public CityId: number,
        public Fio: string,
    ) { }
}
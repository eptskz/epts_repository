﻿export class FileStorageModel {
    constructor(
        public id: string = '00000000-0000-0000-0000-000000000000',
        public title?: string,
        public fileName?: string,
        public created?: Date,
        public modified?: Date,

        public documentType?: string,
        public subPath?: string,
        public mimeType?: string,

        public entityId?: string,

        public ownerId?: string,
        public ownerName?: string,
        public description?: string
    ) { }
}
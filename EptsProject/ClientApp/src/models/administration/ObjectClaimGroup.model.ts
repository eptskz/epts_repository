﻿import { ObjectClaimModel } from "./ObjectClaim.model";

export class ObjectClaimGroupModel {
    constructor(
        public claimGroupName?: string,
        public permissions: Array<ObjectClaimModel> = [],
    ) { }
}
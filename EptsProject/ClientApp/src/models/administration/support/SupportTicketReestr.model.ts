﻿import { TicketCategoryEnum } from "../../enums/TicketCategory.enum";

export class SupportTicketReestrModel {
    constructor(
        public id: string,
        public number?: string,
        public createDate?: Date,
        public title?: string,
        public description?: string,
        public authorId?: string,
        public authorName?: string,
        public category?: TicketCategoryEnum,
        public statusId?: number,
        public statusCode?: string,
        public statusName?: string
    ) { }
}
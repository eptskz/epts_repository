﻿import { TicketCategoryEnum } from "../../enums/TicketCategory.enum";
import { SupportTicketCommentModel } from "./SupportTicketComment.model";

export class SupportTicketModel {
    constructor(
        public id: string = '00000000-0000-0000-0000-000000000000',
        public number?: string,
        public createDate?: Date,
        public title?: string,
        public description?: string,
        public authorId?: string,
        public authorName?: string,
        public category?: TicketCategoryEnum,
        public statusId?: number,
        public statusCode?: string,
        public statusName?: string,
        public comments: Array<SupportTicketCommentModel> = []
    ) { }
}
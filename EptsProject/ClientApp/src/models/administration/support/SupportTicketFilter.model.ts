﻿import { TicketCategoryEnum } from "../../enums/TicketCategory.enum";

export class SupportTicketFilterModel {
    constructor(
        public search: string = '',
        public isShowDeleted: boolean = false,
        public category?: TicketCategoryEnum
    ) { }
}
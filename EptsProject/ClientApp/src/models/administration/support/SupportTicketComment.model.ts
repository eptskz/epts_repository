﻿export class SupportTicketCommentModel {
    constructor(
        public id: number,
        public comment?: string,
        public createDate?: Date,
        public title?: string,
        public authorId?: string,
        public authorName?: string,    
        public ticketId?: string
    ) { }
}
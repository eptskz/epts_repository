﻿export class ObjectClaimModel {
    constructor(
        public id: number = 0,
        public objectId?: string,
        public description?: string,
        public claimValue?: string,

        public objectType?: string,
        public isSelected: boolean = false,
    ) { }
}
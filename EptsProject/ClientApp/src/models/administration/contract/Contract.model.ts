﻿export class ContractModel {
    constructor(
        public id: string = '00000000-0000-0000-0000-000000000000',
        public number?: string,        
        public contractDate?: Date,
        public contractDateFormatted?: string,
        public contractEndDate?: Date,
        public contractEndDateFormatted?: string,
        public description?: string,

        public accountNumber?: string,
        public accountDate?: Date,
        public accountDateFormatted?: string,

        public created?: Date,
        public modified?: Date,
        public deleted?: Date,

        public clientOrgId?: number,
        public clientOrgBin?: string,
        public clientOrgName?: string,

        public clientOrgPhone?: string,
        public clientOrgEmail?: string,
        public clientOrgAddress?: string,

        public statusId?: number,
        public statusCode?: string,
        public statusName?: string,

        public authorId?: string,
        public authorName?: string,

        public reason?: string
    ) { }
}
﻿export class ContractFilterModel {
    constructor(
        public search: string = '',
        public isShowDeleted: boolean = false
    ) { }
}
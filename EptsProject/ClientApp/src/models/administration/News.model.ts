﻿export class NewsModel {
    constructor(
        public id: string = '00000000-0000-0000-0000-000000000000',

        public titleRu?: string,
        public titleKz?: string,
        public titleEn?: string,

        public contentRu?: string,
        public contentKz?: string,
        public contentEn?: string,

        public created?: Date,
        public modified?: Date,
        public deleted?: Date,

        public authorId?: string,
        public authorName?: string
    ) { }
}
﻿import { UserRoleModel } from "./UserRole.model";

export class UserModel {
    constructor(
        public id?: string,
        public userName?: string,
        public email?: string,
        public phoneNumber?: string,
        public password?: string,
        public oldPassword?: string,

        public organizationId?: number,
        public organizationName?: string,

        public statusId?: number,
        public statusCode?: string,
        public statusName?: string,

        public lastName?: string,
        public firstName?: string,
        public secondName?: string,
        public fullName?: string,

        public userRoles: Array<UserRoleModel> = []
    ) { }
}
﻿import { UserRoleModel } from "./UserRole.model";

export class RegRequestModel {
    constructor(
        public id?: string,
        public userName: string = '',
        public email?: string,
        public phoneNumber: string = '',
        public password?: string,

        public organizationName?: string,
        public organizationBin?: string,

        public lastName?: string,
        public firstName?: string,
        public secondName?: string,
        public fullName?: string,
        public certificate?: string,

        public statusId?: number,
        public statusCode?: string,
        public statusName?: string,

        public created?: Date,
        public modified?: Date,
        public deleted?: Date,

        public roleId?: string,        
        public roleName?: string,        
        public userType?: string,

        public isAccept: boolean = false,
        public isExistContract: boolean = false
    ) { }
}
﻿export class UserFilterModel {
    constructor(
        public search: string = '',
        public type: string = '',
    ) { }
}
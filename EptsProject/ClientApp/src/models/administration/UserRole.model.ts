﻿import { ObjectState } from "../enums/ObjectState.enum";

export class UserRoleModel {
    constructor(
        public userId?: string,
        public roleId?: string,

        public state: ObjectState = ObjectState.Unchanged
    ) { }
}
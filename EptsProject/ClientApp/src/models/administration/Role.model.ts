﻿export class RoleModel {
    constructor(
        public id?: string,
        public code?: string,
        public name?: string,
    ) { }
}
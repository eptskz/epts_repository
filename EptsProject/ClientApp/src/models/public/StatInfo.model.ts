﻿export class StatInfoModel {
    constructor(
        public dpassportOttsCount: number = 0,
        public dpassportOtchCount: number = 0,
        public dpassportSbktsCount: number = 0,
    ) { }
}
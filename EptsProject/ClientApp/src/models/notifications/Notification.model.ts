﻿export class NotificationModel {
    constructor(
        public id: string,
        public title?: string,
        public detail?: string,

        public sendToUserId?: string,
        public sendToUserName?: string,
        public createDate?: Date,
        public readDate?: Date,

        public author?: string
    ) { }
}
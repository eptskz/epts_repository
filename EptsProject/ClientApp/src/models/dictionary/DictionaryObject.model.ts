﻿import { ObjectState } from "../enums/ObjectState.enum";

export class DictionaryObjectModel {
    constructor(
        public id: number = 0,
        public dicId: number | null = null,
        public objectId: number | null = null,
        public objectStrId: string | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿export class NsiValueModel {
    constructor(
        public id: number,       
        public code?: string,
        public nameRu?: string,
        public nameKz?: string,
        public nameEn?: string,
        public created?: Date,
        public modified?: Date,

        public nsiId?: number,
        public nsiCode?: string,
        public nsiName?: string,
        
        public wheelQuantity?: number,
        public powerWheelQuantity?: number,
    ) { }
}
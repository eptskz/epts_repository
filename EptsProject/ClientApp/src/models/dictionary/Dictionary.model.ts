﻿export class DictionaryModel {
    constructor(
        public id: number,
        public code?: string,
        public name?: string
    ) { }
}
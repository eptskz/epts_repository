﻿export class PagedSortRequestModel {
    constructor(
        public page: number = 0,
        public pageSize: number = 0,
        public sortField: string[] | null = null,
        public direction: string[] | null = null,

        public filter: any | null = null,
    ) { }
}
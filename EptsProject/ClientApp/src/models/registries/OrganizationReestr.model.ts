﻿import { OrgTypeDicModel } from '@/models/registries/OrgTypeDic.model';
import { ObjectState } from '../enums/ObjectState.enum';

export class OrganizationModel {
    constructor(
        public id: number = 0,        
        public code: string | null = null,
        public name: string | null = null,
        public shortName: string | null = null,
        public bin: string | null = null,

        public organizationTypes: Array<OrgTypeDicModel> = [],

        public juridicalPostalIndex: string | null = null,
        public juridicalAddress: string | null = null,
        public juridicalRegionOrCityId: number | null = null,
        public juridicalDistinctOrCityId: number | null = null,
        public juridicalOkrugOrCityId: number | null = null,
        public juridicalVillageId: number | null = null,
        public jStreetName: string | null = null,
        public jBuildingNumber: string | null = null,
        public jRoomNumber: string | null = null,

        public factPostalIndex: string | null = null,
        public factAddress: string | null = null,
        public factRegionOrCityId: number | null = null,
        public factDistinctOrCityId: number | null = null,
        public factOkrugOrCityId: number | null = null,
        public factVillageId: number | null = null,
        public fstreetName: string | null = null,
        public fbuildingNumber: string | null = null,
        public froomNumber: string | null = null,

        public phone: string | null = null,
        public email: string | null = null,
        public fax: string | null = null,

        public countryId: number | null = null,
        public legalFormId: number | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
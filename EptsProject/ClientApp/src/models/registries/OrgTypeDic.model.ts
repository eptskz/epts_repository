﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class OrgTypeDicModel {
    constructor(
        public id: number = 0,               
        public orgTypeId: number | null = null,        
        public orgTypeCode: string | null = null,
        public orgTypeName: string | null = null,
        public organizationId: number | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
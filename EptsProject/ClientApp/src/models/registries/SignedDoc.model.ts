﻿export class SignedDocModel {
    constructor(
        public id: string = '00000000-0000-0000-0000-000000000000',
        public name: string | null = null,
        public description: string | null = null,

        public createDate: Date | null = null,
        public modifiedDate: Date | null = null,
        public deleteDate: Date | null = null,

        public signedXml: string | null = null,
        public docType: string | null = null,
        public docId: string | null = null
    ) { }
}
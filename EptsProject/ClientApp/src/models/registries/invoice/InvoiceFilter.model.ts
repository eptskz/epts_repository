﻿export class InvoiceFilterModel {
    constructor(
        public search: string = '',
        public isShowDeleted: boolean = false
    ) { }
}
﻿import { ContractModel } from "../../administration/contract/Contract.model";

export class InvoiceModel {
    constructor(
        public id: number = 0,

        public number?: string,
        public periodMonths: Array<string> = [],

        public digitalPassportCount?: number,
        public changeDigitalPassportCount?: number,
        public rate?: number,

        public payableAmount?: number,
        public paidAmount?: number,
        public paidDateTime?: string,

        public fine?: number,
        public accrualTermFine?: number,
        public payableFine?: number,
        public paidFine?: number,
        public paidFineDateTime?: string,

        public balance?: number,

        public created?: Date,
        public modified?: Date,
        public deleted?: Date,

        public contractId?: number,
        public contractNumber?: string,
        public contractDate?: Date,
        public contractDateFormatted?: string,
        public contract?: ContractModel,

        public accountNumber?: string,
        public accountDate?: Date,
        public accountDateFormatted?: string,

        public organizationBin?: string,
        public organizationName?: string,
        
        public statusId?: number,
        public statusCode?: string,
        public statusName?: string,

        public authorId?: string,
        public authorName?: string,

        public description?: string
    ) { }
}
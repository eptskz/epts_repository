﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class OrgDocumentModel {
    constructor(
        public id: number = 0,

        public orgDocType: 'WmiDoc' | 'ConformityAssesmentsDoc' | 'PreferentialAssemblyDoc' | null = null,

        public docTypeId: number | null = null,
        public docTypeCode: number | null = null,
        public docTypeName: number | null = null,

        public docSeries: string | null = null,
        public docNumber: string | null = null,
        public docName: string | null = null,        
        public docDate: Date | null = null,
        public docDateFormatted?: string | null,
        
        public docValidDateStart: Date | null = null,
        public docValidDateStartFormatted?: string | null,
        public docValidDateEnd: Date | null = null,
        public docValidDateEndFormatted?: string | null,

        public tnVedTcCode: string | null = null,

        public agreementOrgName: string | null = null,
        public agreementQuotaSize: number | null = null,

        public techRegulationSubjectTypeId: string | null = null,
        public techRegulationSubjectTypeCode: string | null = null,
        public techRegulationSubjectTypeName: string | null = null,

        public techRegulationSubjectQuantity: number | null = null,

        public techRegulationSubjectUnitId: string | null = null,
        public techRegulationSubjectUnitCode: string = '',
        public techRegulationSubjectUnitName: string | null = null,
        

        public issueCountryId: number | null = null,
        public issueCountryCode: string | null = null,
        public issueCountryName: string | null = null,

        public organizationId: number | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }

    public static readonly WmiDoc = 'WmiDoc';
    public static readonly ConformityAssesmentsDoc = 'ConformityAssesmentsDoc';
    public static readonly PreferentialAssemblyDoc = 'PreferentialAssemblyDoc';
}
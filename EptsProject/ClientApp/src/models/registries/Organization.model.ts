﻿import { OrgTypeDicModel } from '@/models/registries/OrgTypeDic.model';
import { CommunicationInfoModel } from '@/models/registries/CommunicationInfo.model';
import { ObjectState } from '../enums/ObjectState.enum';
import { DictionaryModel } from '../dictionary/Dictionary.model';
import { DictionaryObjectModel } from '../dictionary/DictionaryObject.model';
import { OrgDocumentModel } from './OrgDocument.model';

export class OrganizationModel {
    constructor(
        public id: number = 0,        
        public index: number = 0,

        public code: string | null = null,
        public name: string | null = null,
        public shortName: string | null = null,
        public bin: string | null = null,

        public organizationTypes: Array<OrgTypeDicModel> = [],

        public juridicalPostalIndex: string | null = null,
        public juridicalAddress: string | null = null,

        public juridicalRegionOrCityId: number | null = null,
        public juridicalDistinctOrCityId: number | null = null,
        public juridicalOkrugOrCityId: number | null = null,
        public juridicalVillageId: number | null = null,
        public jStreetName: string | null = null,
        public jBuildingNumber: string | null = null,
        public jRoomNumber: string | null = null,

        public jRegionOrCity: string | null = null,
        public jDistinctOrCity: string | null = null,
        public jOkrugOrCity: string | null = null,
        public jVillage: string | null = null,


        public factPostalIndex: string | null = null,
        public factAddress: string | null = null,

        public factCountryId: number | null = null,
        public factRegionOrCityId: number | null = null,
        public factDistinctOrCityId: number | null = null,
        public factOkrugOrCityId: number | null = null,
        public factVillageId: number | null = null,
        public fStreetName: string | null = null,
        public fBuildingNumber: string | null = null,
        public fRoomNumber: string | null = null,

        public fRegionOrCity: string | null = null,
        public fDistinctOrCity: string | null = null,
        public fOkrugOrCity: string | null = null,
        public fVillage: string | null = null,

        public communicationInfos: Array<CommunicationInfoModel> = [],

        public countryId: number | null = null,
        public legalFormId: number | null = null,

        public registerType: number | null = null,

        public created: Date | null = null,
        public modified: Date | null = null,
        public deleted: Date | null = null,

        public parentId: number | null = null,

        public state: ObjectState = ObjectState.Unchanged,

        public organizationDigPassportTypes: Array<DictionaryObjectModel> = [],
       
        public organizationDocuments: Array<OrgDocumentModel> = [],

        public notOrganizationDocumentIndicator: boolean = false,

        public childOrganizations: Array<OrganizationModel> = [],

        public headLastName: string | null = null,
        public headFirstName: string | null = null,
        public headSecondName: string | null = null
    ) { }
}
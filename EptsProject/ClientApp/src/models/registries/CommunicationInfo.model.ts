﻿import { ObjectState } from '@/models/enums/ObjectState.enum';

export class CommunicationInfoModel {
    constructor(
        public id: number = 0,
        public communicationTypeId: number | null = null,
        public communicationTypeCode: string | null = null,
        public communicationTypeName: string | null = null,
        public communicationValue: string | null = null,

        public organizationId: number | null = null,

        public state: ObjectState = ObjectState.Unchanged,
    ) { }
}
﻿export class OrganizationFilterModel {
    constructor(
        public registerTypeStr: string = '',
        public search: string = '',        
        public parentId?: number,
        public orgTypeCode: string = '',
        public showDeleted: boolean = false,
    ) { }
}
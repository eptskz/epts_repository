﻿export class User {
    constructor(
        public id: string,
        public name: string,
        public role: string,
        public token: string,
        public claims: Array<string>,
        public isPasswordExpire: boolean
    ) { }
}
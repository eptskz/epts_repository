﻿export enum ObjectState {
    Unchanged,
    Added,
    Modified,
    Deleted
}
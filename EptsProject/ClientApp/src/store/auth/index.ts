﻿const namespaced: boolean = true;

export const auth_access = {
    namespaced,
    state: () => ({
        authorized: false,
    }),
    mutations: {
        change_access(state, payload) {
            // `state` указывает на локальное состояние модуля
            state.authorized = payload;
        }
    }
};

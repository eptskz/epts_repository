﻿const namespaced: boolean = true;

export const layout_store = {
    namespaced,
    state: () => ({
        layout: 'public-layout',
        cabinet: 'arm',
    }),
    mutations: {
        set_layout(state, payload) {
            state.layout = payload;
        },
        set_cabinet(state, payload) {
            state.cabinet = payload;
        }
    },
    getters: {
        layout(state) {
            return state.layout;
        },
        cabinet(state) {
            return state.cabinet;
        }
    }
};

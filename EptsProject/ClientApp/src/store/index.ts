import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { RootState } from './types';
import { counter } from './counter/index';
import { auth_access } from './auth/index';
import { layout_store } from './layout/layout-store';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

// Vuex structure based on https://codeburst.io/vuex-and-typescript-3427ba78cfa8

const store: StoreOptions<RootState> = {
  state: {
    version: '1.0.0', // a simple property
  },
  modules: {
      counter,
      auth_access,
      layout_store
    },
    plugins: [createPersistedState({
        storage: window.localStorage,
    })]
};

export default new Vuex.Store<RootState>(store);

﻿import axios from 'axios';
import { Service } from '../service';
import { PagedSortRequestModel } from '@/models/PagedSortRequest.model';
import { ProcessRequestModel } from '@/models/documents/ProcessRequest.model';
import { VehicleCommercialNameModel } from '@/models/vehicle/VehicleCommercialName.model';
import { JsonResponse } from '@/models/JsonResponse';
import { VehicleTypeModel } from '@/models/vehicle/VehicleType.model';
import { ModificationModel } from '@/models/vehicle/Modification.model';
import { BlankNumberModel } from '@/models/documents/complience/BlankNumber.model';
import { EngineDetailModel } from '@/models/vehicle/characteristics/EngineDetail.model';
import { VehicleClutchDetailModel } from '@/models/vehicle/characteristics/VehicleClutchDetail.model';
import { VehicleTyreKindInfoModel } from '@/models/vehicle/characteristics/VehicleTyreKindInfo.model';
import { ElectricalMachineDetailModel } from '@/models/vehicle/characteristics/ElectricalMachineDetail.model';
import { VehicleUseRestrictionModel } from '@/models/documents/complience/VehicleUseRestriction.model';
import { ShassisMovePermitionModel } from '@/models/documents/complience/ShassisMovePermition.model';
import { VehicleRoutingModel } from '@/models/documents/complience/VehicleRouting.model';
import { VehicleNoteModel } from '@/models/documents/complience/VehicleNote.model';
import { TransmissionTypeModel } from '@/models/vehicle/characteristics/TransmissionType.model';
import { VehicleSuspensionDetailModel } from '@/models/vehicle/characteristics/VehicleSuspensionDetail.model';
import { VehicleWeightMeasureModel } from '@/models/vehicle/characteristics/VehicleWeightMeasure.model';
import { UnbrakedTrailerWeightMeasureModel } from '@/models/vehicle/characteristics/UnbrakedTrailerWeightMeasure.model';
import { BrakedTrailerWeightMeasureModel } from '@/models/vehicle/characteristics/BrakedTrailerWeightMeasure.model';
import { VehicleHitchLoadMeasureModel } from '@/models/vehicle/characteristics/VehicleHitchLoadMeasure.model';
import { VehicleRunningGearDetailModel } from '@/models/vehicle/characteristics/VehicleRunningGearDetail.model';
import { VinModel } from '@/models/documents/complience/Vin.model';
import { BaseVehicleTypeDetailModel } from '../../models/vehicle/BaseVehicleTypeDetail.model';
import { VehicleBodyworkDetailModel } from '../../models/vehicle/characteristics/VehicleBodyworkDetail.model';
import { VehicleSeatDetailModel } from '../../models/vehicle/characteristics/VehicleSeatDetail.model';
import { VehicleLayoutPatternDetailModel } from '../../models/vehicle/characteristics/VehicleLayoutPatternDetail.model';
import { VehicleEngineLayoutDetailModel } from '../../models/vehicle/characteristics/VehicleEngineLayoutDetail.model';
import { VehicleCarriageSpaceImplementationModel } from '../../models/vehicle/characteristics/VehicleCarriageSpaceImplementation.model';
import { VehicleCabinModel } from '../../models/vehicle/characteristics/VehicleCabin.model';
import { VehiclePurposeModel } from '../../models/vehicle/characteristics/VehiclePurpose.model';
import { VehicleFrameModel } from '../../models/vehicle/characteristics/VehicleFrame.model';
import { VehiclePassengerQuantityModel } from '../../models/vehicle/characteristics/VehiclePassengerQuantity.model';
import { VehicleTrunkVolumeMeasureModel } from '../../models/vehicle/characteristics/VehicleTrunkVolumeMeasure.model';
import { VehicleLengthMeasureModel } from '../../models/vehicle/characteristics/VehicleLengthMeasure.model';
import { VehicleWidthMeasureModel } from '../../models/vehicle/characteristics/VehicleWidthMeasure.model';
import { VehicleHeightMeasureModel } from '../../models/vehicle/characteristics/VehicleHeightMeasure.model';
import { VehicleLoadingHeightMeasureModel } from '../../models/vehicle/characteristics/VehicleLoadingHeightMeasure.model';
import { VehicleWheelbaseMeasureModel } from '../../models/vehicle/characteristics/VehicleWheelbaseMeasure.model';
import { VehicleMaxHeightMeasureModel } from '../../models/vehicle/characteristics/VehicleMaxHeightMeasure.model';
import { VehicleSteeringDetailModel } from '../../models/vehicle/characteristics/VehicleSteeringDetail.model';
import { VehicleAxleDetailModel } from '../../models/vehicle/characteristics/VehicleAxleDetail.model';
import { VehicleSteeringDescriptionModel } from '../../models/documents/complience/VehicleSteeringDescription.model';
import { VehicleBrakingSystemDetailModel } from '../../models/vehicle/characteristics/VehicleBrakingSystemDetail.model';
import { VehicleEquipmentInfoModel } from '../../models/vehicle/characteristics/VehicleEquipmentInfo.model';
import { VehicleLabelingDetailModel } from '../../models/documents/complience/VehicleLabelingDetail.model';
import { VehicleLabelLocationModel } from '../../models/documents/complience/VehicleLabelLocation.model';
import { VehicleVinCharacterDetailModel } from '../../models/documents/complience/VehicleVinCharacterDetail.model';
import { VehicleTypeDicModel } from '../../models/vehicle/VehicleTypeDic.model';
import { PowerStorageDeviceDetailModel } from '../../models/vehicle/characteristics/PowerStorageDeviceDetail.model';

class ComplienceService extends Service {
    apiUrl = '/api/compliencedocument';
    constructor() {
        super();
    }

    getList(request: PagedSortRequestModel) {
        return axios.post(`${this.apiUrl}/getList`, request);
    }

    get(entityId) {
        return axios.get(`${this.apiUrl}/get`, { params: { id: entityId } });
    }

    getDataToSign(entityId) {
        return axios.get<VehicleTypeModel, JsonResponse>(`${this.apiUrl}/getDataToSign`, { params: { id: entityId } });
    }

    getVehicleRunningGearDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleRunningGearDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleRunningGearDetail(entity: VehicleRunningGearDetailModel) {
        return axios.post<VehicleRunningGearDetailModel, JsonResponse>
            (`${this.apiUrl}/saveVehicleRunningGearDetail`, entity);
    }

    getVehicleBodyworkDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleBodyworkDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleBodyworkDetail(entity: VehicleBodyworkDetailModel) {
        return axios.post<VehicleBodyworkDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleBodyworkDetail`, entity);
    }

    getVehicleSeatDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleSeatDetailsByDocId`, { params: { id: docId } });
    }

    // Компоновка
    getVehicleLayoutDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleLayoutDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleLayoutDetail(entity: VehicleLayoutPatternDetailModel) {
        return axios.post<VehicleLayoutPatternDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleLayoutDetail`, entity);
    }
    saveVehicleEngineLayout(entity: VehicleEngineLayoutDetailModel) {
        return axios.post<VehicleEngineLayoutDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleEngineLayout`, entity);
    }
    saveVehicleCarriageSpaceImplementation(entity: VehicleCarriageSpaceImplementationModel) {
        return axios.post<VehicleCarriageSpaceImplementationModel, JsonResponse>(`${this.apiUrl}/saveVehicleCarriageSpaceImplementation`, entity);
    }
    saveVehicleCabin(entity: VehicleCabinModel) {
        return axios.post<VehicleCabinModel, JsonResponse>(`${this.apiUrl}/saveVehicleCabin`, entity);
    }
    saveVehiclePurpose(entity: VehiclePurposeModel) {
        return axios.post<VehiclePurposeModel, JsonResponse>(`${this.apiUrl}/saveVehiclePurpose`, entity);
    }
    saveVehicleFrame(entity: VehicleFrameModel) {
        return axios.post<VehicleFrameModel, JsonResponse>(`${this.apiUrl}/saveVehicleFrame`, entity);
    }
    saveVehiclePassengerQuantity(entity: VehiclePassengerQuantityModel) {
        return axios.post<VehiclePassengerQuantityModel, JsonResponse>(`${this.apiUrl}/saveVehiclePassengerQuantity`, entity);
    }
    saveVehicleTrunkVolumeMeasure(entity: VehicleTrunkVolumeMeasureModel) {
        return axios.post<VehicleTrunkVolumeMeasureModel, JsonResponse>(`${this.apiUrl}/saveVehicleTrunkVolumeMeasure`, entity);
    }

    // Габаритные размеры
    getVehicleDimensionDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleDimensionDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleLengthMeasure(entity: VehicleLengthMeasureModel) {
        return axios.post<VehicleLengthMeasureModel, JsonResponse>(`${this.apiUrl}/saveVehicleLengthMeasure`, entity);
    }
    saveVehicleWidthMeasure(entity: VehicleWidthMeasureModel) {
        return axios.post<VehicleWidthMeasureModel, JsonResponse>(`${this.apiUrl}/saveVehicleWidthMeasure`, entity);
    }
    saveVehicleHeightMeasure(entity: VehicleHeightMeasureModel) {
        return axios.post<VehicleHeightMeasureModel, JsonResponse>(`${this.apiUrl}/saveVehicleHeightMeasure`, entity);
    }
    saveVehicleLoadingHeightMeasure(entity: VehicleLoadingHeightMeasureModel) {
        return axios.post<VehicleLoadingHeightMeasureModel, JsonResponse>(`${this.apiUrl}/saveVehicleLoadingHeightMeasure`, entity);
    }
    saveVehicleWheelbaseMeasure(entity: VehicleWheelbaseMeasureModel) {
        return axios.post<VehicleWheelbaseMeasureModel, JsonResponse>(`${this.apiUrl}/saveVehicleWheelbaseMeasure`, entity);
    }
    saveVehicleMaxHeightMeasure(entity: VehicleMaxHeightMeasureModel) {
        return axios.post<VehicleMaxHeightMeasureModel, JsonResponse>(`${this.apiUrl}/saveVehicleMaxHeightMeasure`, entity);
    }

    // Колея передних/задних колес
    getVehicleAxleDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleAxleDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleAxleDetail(entity: VehicleAxleDetailModel) {
        return axios.post<VehicleAxleDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleAxleDetail`, entity);
    }

    // Масса
    getVehicleWeightDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleWeightDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleWeightMeasure(entity: VehicleWeightMeasureModel) {
        return axios.post<VehicleWeightMeasureModel, JsonResponse>(`${this.apiUrl}/saveVehicleWeightMeasure`, entity);
    }
    saveUnbrakedTrailerWeight(entity: UnbrakedTrailerWeightMeasureModel) {
        return axios.post<UnbrakedTrailerWeightMeasureModel, JsonResponse>(`${this.apiUrl}/saveUnbrakedTrailerWeight`, entity);
    }
    saveBrakedTrailerWeight(entity: BrakedTrailerWeightMeasureModel) {
        return axios.post<BrakedTrailerWeightMeasureModel, JsonResponse>(`${this.apiUrl}/saveBrakedTrailerWeight`, entity);
    }
    saveVehicleHitchLoadMeasure(entity: VehicleHitchLoadMeasureModel) {
        return axios.post<VehicleHitchLoadMeasureModel, JsonResponse>(`${this.apiUrl}/saveVehicleHitchLoadMeasure`, entity);
    }

    // Двигатель
    getEngineDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetEngineDetailsByDocId`, { params: { id: docId } });
    }
    saveEngineDetail(entity: EngineDetailModel) {
        return axios.post<EngineDetailModel, JsonResponse>(`${this.apiUrl}/saveEngineDetail`, entity);
    }

    // ЭлектроДвигатель
    getElectricalMachineDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetElectricalMachineDetailsByDocId`, { params: { id: docId } });
    }
    saveEMachineDetail(entity: ElectricalMachineDetailModel) {
        return axios.post<ElectricalMachineDetailModel, JsonResponse>(`${this.apiUrl}/saveEMachineDetail`, entity);
    }

    // Устройства накопления устройситв
    getPowerStorageDeviceDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/getPowerStorageDeviceDetailsByDocId`, { params: { id: docId } });
    }
    savePowerStorageDeviceDetail(entity: PowerStorageDeviceDetailModel) {
        return axios.post<PowerStorageDeviceDetailModel, JsonResponse>(`${this.apiUrl}/savePowerStorageDeviceDetail`, entity);
    }

    // Сцепление
    getVehicleClutchDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleClutchDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleClutchDetail(entity: VehicleClutchDetailModel) {
        return axios.post<VehicleClutchDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleClutchDetail`, entity);
    }

    // Transmission Type
    getTransmissionTypesByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetTransmissionTypesByDocId`, { params: { id: docId } });
    }
    saveTransmissionType(entity: TransmissionTypeModel) {
        return axios.post<TransmissionTypeModel, JsonResponse>(`${this.apiUrl}/saveTransmissionType`, entity);
    }

    // Подвеска
    getVehicleSuspensionDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleSuspensionDetailsByDocId`, { params: { id: docId } });
    }   
    saveVehicleSuspensionDetail(entity: VehicleSuspensionDetailModel) {
        return axios.post<VehicleSuspensionDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleSuspensionDetail`, entity);
    }

    // Рулевое управление
    getVehicleSteeringDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleSteeringDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleSteeringDescription(entity: VehicleSteeringDescriptionModel) {
        return axios.post<VehicleSteeringDescriptionModel, JsonResponse>(`${this.apiUrl}/saveVehicleSteeringDescription`, entity);
    }
    saveVehicleSteeringDetail(entity: VehicleSteeringDetailModel) {
        return axios.post<VehicleSteeringDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleSteeringDetail`, entity);
    }

    // Тормозные системы
    getVehicleBrakingSystemDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleBrakingSystemDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleBrakingSystemDetail(entity: VehicleBrakingSystemDetailModel) {
        return axios.post<VehicleBrakingSystemDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleBrakingSystemDetail`, entity);
    }

    // Описание оборудования транспортного средства
    getVehicleEquipmentInfosByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleEquipmentInfosByDocId`, { params: { id: docId } });
    }
    saveVehicleEquipmentInfo(entity: VehicleEquipmentInfoModel) {
        return axios.post<VehicleEquipmentInfoModel, JsonResponse>(`${this.apiUrl}/saveVehicleEquipmentInfo`, entity);
    }


    getVehicleTyreKindInfosByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetVehicleTyreKindInfosByDocId`, { params: { id: docId } });
    }

    getAdditionalDataByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetAdditionalDataByDocId`, { params: { id: docId } });
    }

    getOrganizationDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetOrganizationDetailsByDocId`, { params: { id: docId } });
    }

    getLabelDetailsByDocId(docId) {
        return axios.get(`${this.apiUrl}/GetLabelingDetailsByDocId`, { params: { id: docId } });
    }
    saveVehicleLabelingDetail(entity: VehicleLabelingDetailModel) {
        return axios.post<VehicleLabelingDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleLabelingDetail`, entity);
    }
    saveVehicleIdentificationNumberLocation(entity: VehicleLabelLocationModel) {
        return axios.post<VehicleLabelLocationModel, JsonResponse>(`${this.apiUrl}/saveVehicleIdentificationNumberLocation`, entity);
    }
    saveVehicleLabelLocation(entity: VehicleLabelLocationModel) {
        return axios.post<VehicleLabelLocationModel, JsonResponse>(`${this.apiUrl}/saveVehicleLabelLocation`, entity);
    }
    saveVehicleVinCharacterDetail(entity: VehicleVinCharacterDetailModel) {
        return axios.post<VehicleVinCharacterDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleVinCharacterDetail`, entity);
    }


    save(entity) {
        return axios.post(`${this.apiUrl}/save`, entity);
    }

    delete(entityId) {
        return axios.delete(`${this.apiUrl}/delete`, {
            params: { id: entityId }
        });
    }
    restore(entityId) {
        return axios.put(`${this.apiUrl}/restore`, null, {
            params: { id: entityId }
        });
    }

    getDocumentEventsByDocId(documentId: String) {
        return axios.get(`${this.apiUrl}/GetDocumentEvents`, { params: { docId: documentId } });
    }

    upload(file, fileName, docId, onUploadProgress) {
        let formData = new FormData();

        formData.append("file", file);
        formData.append("fileName", fileName);
        formData.append("documentId", docId);

        return axios.post(`${this.apiUrl}/UploadGeneralView`, formData, {
            headers: {
                "Content-Type": "multipart/form-data"
            },
            onUploadProgress
        });
    }

    deleteGeneralView(entityId) {
        return axios.delete(`${this.apiUrl}/deleteGeneralView`, {
            params: { id: entityId }
        });
    }

    saveImgName(entityId, imgName) {
        return axios.post(`${this.apiUrl}/saveImgName`, null, {
            params: {
                id: entityId,
                fileName: imgName,
            }
        });
    }

    getGeneralViewsByDocId(documentId) {
        return axios.get(`${this.apiUrl}/getGeneralViewList`, { params: { docId: documentId } });
    }

    saveCommercialName(entity: VehicleCommercialNameModel) {
        return axios.post<VehicleCommercialNameModel, JsonResponse>(`${this.apiUrl}/saveCommercialName`, entity);
    }

    saveType(entity: VehicleTypeModel) {
        return axios.post<VehicleTypeModel, JsonResponse>(`${this.apiUrl}/saveType`, entity);
    }

    saveModification(entity: ModificationModel) {
        return axios.post<ModificationModel, JsonResponse>(`${this.apiUrl}/saveModification`, entity);
    }

    saveBlankNumber(entity: BlankNumberModel) {
        return axios.post<BlankNumberModel, JsonResponse>(`${this.apiUrl}/saveBlankNumber`, entity);
    }

    saveBaseVehicleTypeDetail(entity: BaseVehicleTypeDetailModel) {
        return axios.post<BaseVehicleTypeDetailModel, JsonResponse>(`${this.apiUrl}/saveBaseVehicleTypeDetail`, entity);
    }

    saveClassCategory(entity: VehicleTypeDicModel) {
        return axios.post<VehicleTypeDicModel, JsonResponse>(`${this.apiUrl}/saveClassCategory`, entity);
    }

    saveVehicleTyreKindInfo(entity: VehicleTyreKindInfoModel) {
        return axios.post<VehicleTyreKindInfoModel, JsonResponse>(`${this.apiUrl}/saveVehicleTyreKindInfo`, entity);
    }

    saveVehicleUseRestriction(entity: VehicleUseRestrictionModel) {
        return axios.post<VehicleUseRestrictionModel, JsonResponse>(`${this.apiUrl}/saveVehicleUseRestriction`, entity);
    }

    saveShassisMovePermition(entity: ShassisMovePermitionModel) {
        return axios.post<ShassisMovePermitionModel, JsonResponse>(`${this.apiUrl}/saveShassisMovePermition`, entity);
    }

    saveVehicleRouting(entity: VehicleRoutingModel) {
        return axios.post<VehicleRoutingModel, JsonResponse>(`${this.apiUrl}/saveVehicleRouting`, entity);
    }

    saveVehicleNote(entity: VehicleNoteModel) {
        return axios.post<VehicleNoteModel, JsonResponse>(`${this.apiUrl}/saveVehicleNote`, entity);
    }


    saveVinNumber(entity: VinModel) {
        return axios.post<VehicleNoteModel, JsonResponse>(`${this.apiUrl}/saveVinNumber`, entity);
    }

    saveVehicleSeatDetail(entity: VehicleSeatDetailModel) {
        return axios.post<VehicleSeatDetailModel, JsonResponse>(`${this.apiUrl}/saveVehicleSeatDetail`, entity);
    }

    processDoc(processRequest: ProcessRequestModel) {
        return axios.post<ProcessRequestModel, JsonResponse>(`${this.apiUrl}/processRequest`, processRequest);
    }
}

export default new ComplienceService();
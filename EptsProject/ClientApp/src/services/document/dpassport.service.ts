﻿import axios from 'axios';
import { PagedSortRequestModel } from '@/models/PagedSortRequest.model';
import { Service } from '../service';
import { JsonResponse } from '../../models/JsonResponse';
import { SignedDocModel } from '../../models/registries/SignedDoc.model';

class DPassportService extends Service {
    apiUrl = '/api/DigitalPassport';
    constructor() {
        super();
    }

    getList(request: PagedSortRequestModel) {
        return axios.post(`${this.apiUrl}/getList`, request);
    }

    getDraftList(request: PagedSortRequestModel) {
        return axios.post(`${this.apiUrl}/getDraftList`, request);
    }

    getDraft(signId) {
        return axios.get<string, JsonResponse>(`${this.apiUrl}/getDraft`, { params: { signId: signId } });
    }

    getView(id) {
        return axios.get<string, JsonResponse>(`${this.apiUrl}/getView`, { params: { id: id } });
    }

    getPtsCard(passportId) {
        return axios.post(`${this.apiUrl}/GetDigitalPassportCard`, 
            passportId,
            {
                responseType: 'blob', // important
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json-patch+json"
                },
        });
    }

    checkVinCodeExists(vin) {
        return axios.post(`${this.apiUrl}/CheckVinCodeExists`,
            "\"" + vin +"\"",
            {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json-patch+json"
                },
            }
        );
    }

    getOrganizationPrivilegeMode(docId) {
        return axios.get(`/api/Organizations/GetOrg`, { params: { orgId: docId } });
    }

    getDPassportByUniqCode(data: string) {
        return axios.get<string, JsonResponse>(`${this.apiUrl}/GetDPassportByUniqCode`, { params: { ident: data } }

        );
    }

    getDPassportByVinCode(data: string) {
        return axios.get<string, JsonResponse>(`${this.apiUrl}/GetDPassportByVinCode`, { params: { vinCode: data } }

        );
    }

    getBatchUsedCount(data: string) {
        return axios.get<string, JsonResponse>(`${this.apiUrl}/GetBatchUsedCount`, { params: { id: data } }
        
        );
    }

    save(data) {
        return axios.post<JsonResponse>(`${this.apiUrl}/SaveDraft`, data);
    }

    getDataToSign(data: string) {
        return axios.get<string, JsonResponse>(`${this.apiUrl}/GetDataToSign`, { params: { signId: data } });
    }

    saveSignData(signedDoc: SignedDocModel) {
        return axios.post<SignedDocModel, JsonResponse>(`${this.apiUrl}/SaveSignData`, signedDoc);
    }

    getDigitalPassportListByKind(kind) {
        return axios.get<string, JsonResponse>(`${this.apiUrl}/GetDigitalPassportListByKind`, { params: { kind: kind } });
    }

    getReportList(doc: PagedSortRequestModel) {
        return axios.post<PagedSortRequestModel, JsonResponse>(`${this.apiUrl}/GetReportList`, doc);
    }

    getMonitoringList(doc: PagedSortRequestModel) {
        return axios.post<PagedSortRequestModel, JsonResponse>(`${this.apiUrl}/GetMonitoringList`, doc);
    }
}

export default new DPassportService();
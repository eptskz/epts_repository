﻿import axios from 'axios';
import { PagedSortRequestModel } from '@/models/PagedSortRequest.model';
import { Service } from '../service';
import { JsonResponse } from '../../models/JsonResponse';
import { SignedDocModel } from '../../models/registries/SignedDoc.model';

class ChangeApplicationService extends Service {
    apiUrl = '/api/ChangeApplication';
    constructor() {
        super();
    }

    getList(request: PagedSortRequestModel) {
        return axios.post(`${this.apiUrl}/getList`,  request);
    }

    getView(id) {
        return axios.get<string, JsonResponse>(`${this.apiUrl}/getView`, { params: { id: id } });
    }

    getDataToSign(data) {
        return axios.post<string, JsonResponse>(`${this.apiUrl}/GetDataToSign`, data);
    }
    getDataToSignExecute(id, description) {
        return axios.get<string, JsonResponse>(`${this.apiUrl}/GetDataToSignExecute`, { params: { id: id, description: description } });
    }

    saveSignData(signedDoc: SignedDocModel) {
        return axios.post<SignedDocModel, JsonResponse>(`${this.apiUrl}/SaveSignData`, signedDoc);
    }

    saveSignDataExecute(signedDoc: SignedDocModel) {
        return axios.post<SignedDocModel, JsonResponse>(`${this.apiUrl}/SaveSignDataExecute`, signedDoc);
    }
}
export default new ChangeApplicationService();
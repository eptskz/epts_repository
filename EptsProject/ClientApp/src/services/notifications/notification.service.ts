﻿import { Service } from '../service';
import axios from 'axios';
import { JsonResponse } from '../../models/JsonResponse';
import { PagedSortRequestModel } from '../../models/PagedSortRequest.model';
import qs from 'qs';
import { FileStorageModel } from '@/models/filestorage/FileStorage.model';
import { NotificationModel } from '../../models/notifications/Notification.model';

class NotificationService extends Service {
    private apiUrl = '/api/Notification';
    constructor() { super(); }
    
    getList(request: PagedSortRequestModel) {
        return axios.get(`${this.apiUrl}/notifications`, {
            params: {
                page: request.page,
                pageSize: request.pageSize,
                sortField: request.sortField,
                direction: request.direction,
                'filter.search': request.filter && request.filter.search ? request.filter.search : '',
                'filter.isShowDeleted': request.filter && request.filter.isShowDeleted == true ? true : false
            },
            paramsSerializer: params => { return qs.stringify(params); }
        });
    }

    get(id: string) {
        return axios.get<any, JsonResponse>(`${this.apiUrl}/notification/${id}`);
    }

    updateNotification(notification: NotificationModel) {
        return axios.post<NotificationModel, JsonResponse>(`${this.apiUrl}/notification/`, notification);
    }
}
export default new NotificationService();
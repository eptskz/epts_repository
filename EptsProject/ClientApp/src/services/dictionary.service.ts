﻿import { Service } from './service';
import axios from 'axios';
import { NsiModel } from '../models/dictionary/Nsi.model';
import { NsiValueModel } from '../models/dictionary/NsiValue.model';
import { JsonResponse } from '../models/JsonResponse';
import { PagedSortRequestModel } from '../models/PagedSortRequest.model';
import qs from 'qs';

class DictionaryService extends Service {
    private apiUrl = '/api/Dictionaries';
    constructor() {
        super();
    }

    getNsiValues(nsiCode){
        return axios.get(`${this.apiUrl}/GetNsiValues/?nsiCode=${nsiCode}`);
    }
    getNsiValuesFiltered(nsiCode: string, valueCodes: string[]) {
        return axios.post(`${this.apiUrl}/GetNsiValuesFiltered?nsiCode=${nsiCode}`, valueCodes);
    }

    getNsiValueList(nsiCodes) {
        return axios.post(`${this.apiUrl}/GetNsiValues`, nsiCodes);
    }

    getOrgTypes() {
        return axios.get(`${this.apiUrl}/GetOrgTypes`);
    }

    getKatos(parentId: number) {
        return axios.get(`${this.apiUrl}/GetKatos/?parentId=${parentId}`);
    }

    getKatosByParentCode(parentCode: string) {
        return axios.get(`${this.apiUrl}/GetKatosByParentCode/?parentCode=${parentCode}`);
    }

    getStatuses(group) {
        return axios.get(`${this.apiUrl}/GetStatuses/?group=${group}`);
    }

    getDicDocTypes() {
        return axios.get(`${this.apiUrl}/GetDicDocTypes`);
    }

    getNsis(request: PagedSortRequestModel) {
        return axios.get(`${this.apiUrl}/nsis`, {
            params: {
                page: request.page,
                pageSize: request.pageSize,
                sortField: request.sortField,
                direction: request.direction,
                'filter.search': request.filter && request.filter.search ? request.filter.search : ''
            },
            paramsSerializer: params => {
                return qs.stringify(params);
            }
        });
    }
    getNsi(id: number) {
        return axios.get(`${this.apiUrl}/nsi/${id}`);
    }
    createNsi(nsi: NsiModel) {
        return axios.post<NsiModel, JsonResponse>(`${this.apiUrl}/nsi`, nsi);
    }
    updateNsi(nsi: NsiModel) {
        return axios.put<NsiModel, JsonResponse>(`${this.apiUrl}/nsi`, nsi);
    }
    deleteNsi(nsi: NsiModel) {
        return axios.delete<any, JsonResponse>(`${this.apiUrl}/nsi`, {
            params: { id: nsi.id }
        });
    }

    getReestNsiValues(nsiCode: string, request: PagedSortRequestModel) {
        return axios.get(`${this.apiUrl}/nsivalues/${nsiCode}`, {
            params: {
                page: request.page,
                pageSize: request.pageSize,
                sortField: request.sortField,
                direction: request.direction,
                'filter.search': request.filter && request.filter.search ? request.filter.search : ''
            },
            paramsSerializer: params => {
                return qs.stringify(params);
            }
        });
    }
    getNsiValue(id: number) {
        return axios.get(`${this.apiUrl}/nsivalue/${id}`);
    }
    createNsiValue(nsiValue: NsiValueModel) {
        return axios.post<NsiValueModel, JsonResponse>(`${this.apiUrl}/nsivalue`, nsiValue);
    }
    updateNsiValue(nsiValue: NsiValueModel) {
        return axios.put<NsiValueModel, JsonResponse>(`${this.apiUrl}/nsivalue`, nsiValue);
    }
    deleteNsiValue(nsiValue: NsiValueModel) {
        return axios.delete<any, JsonResponse>(`${this.apiUrl}/nsivalue`, {
            params: { id: nsiValue.id }
        });
    }
}

export default new DictionaryService();
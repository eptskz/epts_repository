﻿import axios from 'axios';
import qs from 'qs';

import { JsonResponse } from '../../models/JsonResponse';
import { Service } from '../service';

class StatInfoService extends Service {
    private apiUrl = '/api/statinfo';
    constructor() { super(); }
    
    getStatInfo() {
        return axios.get<any, JsonResponse>(`${this.apiUrl}/public`);
    }
}
export default new StatInfoService();
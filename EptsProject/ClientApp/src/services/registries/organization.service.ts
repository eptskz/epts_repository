﻿import { Service } from '../service';
import axios, { AxiosResponse } from 'axios';
import { OrganizationModel } from '@/models/registries/Organization.model';
import { JsonResponse } from '@/models/JsonResponse';
import { OrgDocumentModel } from '@/models/registries/OrgDocument.model';

class OrganizationService extends Service {
    apiUrl = '/api/Organizations';
    constructor() {
        super();
    }

    getList(params) {
        return axios.get(`${this.apiUrl}/getList/`, { params: params });
    }

    getPagedList(request) {
        return axios.post(`${this.apiUrl}/getPagedList/`, request);
    }

    saveOrg(org: OrganizationModel) {
        return axios.post<OrganizationModel, JsonResponse> (`${this.apiUrl}/save`, org);
    }

    saveOrgDocument(orgDoc: OrgDocumentModel) {
        return axios.post<OrgDocumentModel, JsonResponse>(`${this.apiUrl}/saveDoc`, orgDoc);
    }

    getOrg(params) {
        return axios.get<OrganizationModel>(`${this.apiUrl}/getOrg/`, { params: params });
    }

    getOrgDocs(params) {
        return axios.get<Array<OrgDocumentModel>>(`${this.apiUrl}/getOrgDocs`, { params: params });
    }

    delete(entityId, parentId) {
        return axios.delete<JsonResponse>(`${this.apiUrl}/delete`, {
            params: { id: entityId, parentId }
        });
    }

    restore(entityId) {
        return axios.put<JsonResponse>(`${this.apiUrl}/restore`, null, {
            params: { id: entityId }
        });
    }
}

export default new OrganizationService();
﻿import { Service } from '../service';
import axios from 'axios';
import qs from 'qs';

import { JsonResponse } from '@/models/JsonResponse';
import { PagedSortRequestModel } from '@/models/PagedSortRequest.model';
import { InvoiceModel } from '@/models/registries/invoice/Invoice.model';

class InvoiceService extends Service {
    private apiUrl = '/api/invoice';
    constructor() { super(); }

    getList(request: PagedSortRequestModel) {
        return axios.get(`${this.apiUrl}/invoices/`, {
            params: {
                page: request.page,
                pageSize: request.pageSize,
                sortField: request.sortField,
                direction: request.direction,
                'filter.search': request.filter && request.filter.search ? request.filter.search : '',
                'filter.isShowDeleted': request.filter && request.filter.isShowDeleted == true ? true : false
            },
            paramsSerializer: params => { return qs.stringify(params); }
        });
    }
    get(id: string) {
        return axios.get(`${this.apiUrl}/invoice/${id}`);
    }
    createItem(contract: InvoiceModel) {
        return axios.post<InvoiceModel, JsonResponse>(`${this.apiUrl}/invoice`, contract);
    }
    updateItem(contract: InvoiceModel) {
        return axios.put<InvoiceModel, JsonResponse>(`${this.apiUrl}/invoice`, contract);
    }
    deleteItem(ticket: InvoiceModel) {
        return axios.delete<any, JsonResponse>(`${this.apiUrl}/invoice`, {
            params: { id: ticket.id }
        });
    }
}

export default new InvoiceService();
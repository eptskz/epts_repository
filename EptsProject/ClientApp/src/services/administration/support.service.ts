﻿import { Service } from '../service';
import axios from 'axios';
import { JsonResponse } from '../../models/JsonResponse';
import { PagedSortRequestModel } from '../../models/PagedSortRequest.model';
import qs from 'qs';
import { SupportTicketModel } from '../../models/administration/support/SupportTicket.model';
import { SupportTicketReestrModel } from '../../models/administration/support/SupportTicketReestr.model';
import { SupportTicketCommentModel } from '../../models/administration/support/SupportTicketComment.model';

class SupportService extends Service {
    private apiUrl = '/api/Support';
    constructor() { super(); }

    getList(request: PagedSortRequestModel) {
        return axios.get(`${this.apiUrl}/tickets/`, {
            params: {
                page: request.page,
                pageSize: request.pageSize,
                sortField: request.sortField,
                direction: request.direction,
                'filter.search': request.filter && request.filter.search ? request.filter.search : '',
                'filter.isShowDeleted': request.filter && request.filter.isShowDeleted == true ? true : false,
                'filter.category': request.filter && request.filter.category ? request.filter.category : ''
            },
            paramsSerializer: params => { return qs.stringify(params); }
        });
    }
    get(id: string) {
        return axios.get(`${this.apiUrl}/ticket/${id}`);
    }
    createItem(ticket: SupportTicketModel) {
        return axios.post<SupportTicketModel, JsonResponse>(`${this.apiUrl}/ticket`, ticket);
    }
    updateItem(ticket: SupportTicketModel) {
        return axios.put<SupportTicketModel, JsonResponse>(`${this.apiUrl}/ticket`, ticket);
    }
    deleteItem(ticket: SupportTicketReestrModel) {
        return axios.delete<any, JsonResponse>(`${this.apiUrl}/ticket`, {
            params: { id: ticket.id }
        });
    }
    sendComment(comment: SupportTicketCommentModel) {
        return axios.post<SupportTicketCommentModel, JsonResponse>(`${this.apiUrl}/ticket/comment`, comment);
    }
}

export default new SupportService();
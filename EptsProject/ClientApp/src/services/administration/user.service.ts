﻿import { Service } from '../service';
import axios, { AxiosResponse } from 'axios';
import { PagedSortRequestModel } from '@/models/PagedSortRequest.model';
import { UserModel } from '@/models/administration/User.model';
import { JsonResponse } from '../../models/JsonResponse';

class UserService extends Service {
    apiUrl = '/api/user';
    constructor() { super(); }

    getList(request: PagedSortRequestModel) {
        return axios.post(`${this.apiUrl}/getUserList`, request);
    }
    get(id: string) {
        return axios.get(`${this.apiUrl}/getUser`, {
            params: { userId: id }
        });
    }

    create(user: UserModel) {
        return axios.post<UserModel, JsonResponse>(`${this.apiUrl}/createUser`, user);
    }
    update(user: UserModel) {
        return axios.put<UserModel, JsonResponse>(`${this.apiUrl}/updateUser`, user);
    }
    delete(id: string) {
        return axios.delete(`${this.apiUrl}/deleteUser`, {
            params: { userId: id }
        });
    }
}

export default new UserService();
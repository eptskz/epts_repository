﻿import { Service } from '../service';
import axios, { AxiosResponse } from 'axios';
import { PagedSortRequestModel } from '@/models/PagedSortRequest.model';
import { JsonResponse } from '../../models/JsonResponse';
import { RegRequestModel } from '../../models/administration/RegRequest.model';

class RegrequestService extends Service {
    private readonly apiUrl = '/api/regrequest';
    constructor() { super(); }

    getList(request: PagedSortRequestModel) {
        return axios.post(`${this.apiUrl}/getList`, request);
    }
    
    get(id: string) {
        return axios.get(`${this.apiUrl}/getUser`, {
            params: { requestId: id }
        });
    }

    create(request: RegRequestModel) {
        return axios.post<RegRequestModel, JsonResponse>(`${this.apiUrl}/create`, request);
    }
    checkPsw(psw: String) {
        return axios.get<String, JsonResponse>(`${this.apiUrl}/check-psw`, {
            params: { psw: psw }
        });
    }

    update(request: RegRequestModel) {
        return axios.post<RegRequestModel, JsonResponse>(`${this.apiUrl}/update`, request);
    }
    delete(id: string) {
        return axios.delete(`${this.apiUrl}/delete`, {
            params: { userId: id }
        });
    }
}

export default new RegrequestService();
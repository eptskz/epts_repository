﻿import { Service } from '../service';
import axios from 'axios';
import qs from 'qs';

import { JsonResponse } from '../../models/JsonResponse';
import { PagedSortRequestModel } from '../../models/PagedSortRequest.model';
import { ContractModel } from '../../models/administration/contract/Contract.model';

class ContractService extends Service {
    private apiUrl = '/api/Contract';
    constructor() { super(); }

    getList(request: PagedSortRequestModel) {
        return axios.get(`${this.apiUrl}/contracts/`, {
            params: {
                page: request.page,
                pageSize: request.pageSize,
                sortField: request.sortField,
                direction: request.direction,
                'filter.search': request.filter && request.filter.search ? request.filter.search : '',
                'filter.isShowDeleted': request.filter && request.filter.isShowDeleted == true ? true : false
            },
            paramsSerializer: params => { return qs.stringify(params); }
        });
    }
    get(id: string) {
        return axios.get(`${this.apiUrl}/contract/${id}`);
    }
    createItem(contract: ContractModel) {
        return axios.post<ContractModel, JsonResponse>(`${this.apiUrl}/contract`, contract);
    }
    updateItem(contract: ContractModel) {
        return axios.put<ContractModel, JsonResponse>(`${this.apiUrl}/contract`, contract);
    }
    deleteItem(ticket: ContractModel) {
        return axios.delete<any, JsonResponse>(`${this.apiUrl}/contract`, {
            params: { id: ticket.id }
        });
    }
}

export default new ContractService();
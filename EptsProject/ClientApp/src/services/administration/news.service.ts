﻿import axios from 'axios';
import qs from 'qs';
import { NewsModel } from '../../models/administration/News.model';

import { JsonResponse } from '../../models/JsonResponse';
import { PagedSortRequestModel } from '../../models/PagedSortRequest.model';
import { Service } from '../service';

class NewsService extends Service {
    private apiUrl = '/api/news';
    constructor() { super(); }
    
    getList(request: PagedSortRequestModel) {
        return axios.get(`${this.apiUrl}/news`, {
            params: {
                page: request.page,
                pageSize: request.pageSize,
                sortField: request.sortField,
                direction: request.direction,
                'filter.search': request.filter && request.filter.search ? request.filter.search : '',
                'filter.isShowDeleted': request.filter && request.filter.isShowDeleted == true ? true : false
            },
            paramsSerializer: params => { return qs.stringify(params); }
        });
    }
    get(id: string) {
        return axios.get(`${this.apiUrl}/news/${id}`);
    }
    createItem(contract: NewsModel) {
        return axios.post<NewsModel, JsonResponse>(`${this.apiUrl}/news`, contract);
    }
    updateItem(contract: NewsModel) {
        return axios.put<NewsModel, JsonResponse>(`${this.apiUrl}/news`, contract);
    }
    deleteItem(ticket: NewsModel) {
        return axios.delete<any, JsonResponse>(`${this.apiUrl}/news`, {
            params: { id: ticket.id }
        });
    }
}
export default new NewsService();
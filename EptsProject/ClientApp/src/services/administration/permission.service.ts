﻿import { Service } from '../service';
import axios from 'axios';

import { ObjectClaimModel } from '@/models/administration/ObjectClaim.model';
import { ObjectClaimGroupModel } from '@/models/administration/ObjectClaimGroup.model';

class PermissionService extends Service {
    apiUrl = '/api/permission';
    constructor() { super(); }

    getPermissions() {
        return axios.get(`${this.apiUrl}/getPermissions`);
    }

    getRolePermissions(roleId) {
        return axios.get(`${this.apiUrl}/getRolePermissions`, {
            params: { roleId: roleId }
        });
    }

    setPermissions(claims: Array<ObjectClaimModel>) {
        return axios.post(`${this.apiUrl}/setPermissions`, claims);
    }
}

export default new PermissionService();
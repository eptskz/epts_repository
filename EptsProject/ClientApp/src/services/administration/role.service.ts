﻿import { Service } from '../service';
import axios from 'axios';
import { PagedSortRequestModel } from '@/models/PagedSortRequest.model';
import { RoleModel } from '@/models/administration/Role.model';
import { JsonResponse } from '../../models/JsonResponse';

class RoleService extends Service {
    apiUrl = '/api/role';
    constructor() { super(); }

    getList(request: PagedSortRequestModel) {
        return axios.post(`${this.apiUrl}/getRoleList`, request);
    }
    getExternalList() {
        return axios.post(`${this.apiUrl}/getRoleForExternalList`);
    }

    get(id: string) {
        return axios.get(`${this.apiUrl}/getRole`, {
            params: { roleId: id }
        });
    }

    create(role: RoleModel) {
        return axios.post<RoleModel, JsonResponse>(`${this.apiUrl}/createRole`, role);
    }
    update(role: RoleModel) {
        return axios.put<RoleModel, JsonResponse>(`${this.apiUrl}/updateRole`, role);
    }
    delete(id: string) {
        return axios.delete(`${this.apiUrl}/deleteRole`, {
            params: { roleId: id }
        });
    }
}

export default new RoleService();
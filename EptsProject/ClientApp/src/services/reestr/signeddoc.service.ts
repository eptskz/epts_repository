﻿import { Service } from '../service';
import axios from 'axios';
import { SignedDocModel } from '@/models/registries/SignedDoc.model';
import { JsonResponse } from '../../models/JsonResponse';

class SignedDocService extends Service {
    apiUrl = '/api/SignedDoc';
    constructor() {
        super();
    }

    get(params) {
        return axios.get(`${this.apiUrl}/GetList/`, { params: params });
    }

    save(signedDoc: SignedDocModel) {
        return axios.post<SignedDocModel, JsonResponse>(`${this.apiUrl}/Save`, signedDoc);
    }
}

export default new SignedDocService();
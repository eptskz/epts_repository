﻿import { Service } from '../service';
import axios from 'axios';
import { JsonResponse } from '../../models/JsonResponse';
import { PagedSortRequestModel } from '../../models/PagedSortRequest.model';
import qs from 'qs';
import { FileStorageModel } from '@/models/filestorage/FileStorage.model';

class FileStorageService extends Service {
    private apiUrl = '/api/FileStorage';
    constructor() { super(); }
    /*
    getList(request: PagedSortRequestModel) {
        return axios.get(`${this.apiUrl}/files/`, {
            params: {
                page: request.page,
                pageSize: request.pageSize,
                sortField: request.sortField,
                direction: request.direction,
                'filter.search': request.filter && request.filter.search ? request.filter.search : '',
                'filter.isShowDeleted': request.filter && request.filter.isShowDeleted == true ? true : false
            },
            paramsSerializer: params => { return qs.stringify(params); }
        });
    }*/

    getList(documenttype: string, search: string) {
        return axios.get(`${this.apiUrl}/files/${documenttype}`, {
            params: {
                search: search
            }
        });
    }

    get(id: string) {
        return axios.get<any, JsonResponse>(`${this.apiUrl}/file/${id}`);
    }

    updateFileInfo(fileStorage: FileStorageModel) {
        return axios.post<FileStorageModel, JsonResponse>(`${this.apiUrl}/file/`, fileStorage);
    }

    upload(file: any
        , title: string
        , description: string
        , documentType: string
        , onUploadProgress: any) {
        let formData = new FormData();

        formData.append("file", file);
        formData.append("title", title);
        formData.append("description", description);
        formData.append("documentType", documentType);
        return axios.post<any, JsonResponse>(`${this.apiUrl}/filecontent`, formData, {
            headers: {
                "Content-Type": "multipart/form-data"
            },
            onUploadProgress
        });
    }
    download(file: FileStorageModel) {
        return axios.get<FileStorageModel, JsonResponse>(`${this.apiUrl}/filecontent/${file.documentType}/${file.id}`);
    }
    deleteItem(file: FileStorageModel) {
        return axios.delete<any, JsonResponse>(`${this.apiUrl}/file`, {
            params: { id: file.id }
        });
    }
}
export default new FileStorageService();
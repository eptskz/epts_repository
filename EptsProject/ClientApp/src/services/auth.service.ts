﻿import { Service } from './service';
import axios from 'axios';
import { User } from '../models/User';

class AuthService extends Service {
    apiUrl = '/api/Account';

    authUser: User | null = null;

    constructor() {
        super();
    }

    getCurrentUser(): User {
        if (!this.authUser)
            this.authUser = JSON.parse(localStorage.getItem('user')!) as User;
        return this.authUser;
    }

    hasPermission(permission: string): boolean {
        return this.getCurrentUser().claims.includes(permission);
    }
}

export default new AuthService();
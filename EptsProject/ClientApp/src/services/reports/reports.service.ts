﻿import { Service } from '../service';
import axios from 'axios';
import qs from 'qs';

import { PagedSortRequestModel } from '../../models/PagedSortRequest.model';

class ReportsService extends Service {
    apiCdUrl = '/api/CdReport';
    constructor() {
        super();
    }

    getCdReportData(request: PagedSortRequestModel) {
        return axios.post(`${this.apiCdUrl}/data`, request);
    }

    exportCdReportToExcel(request: PagedSortRequestModel) {
        /*
         {
            params: {
                page: request.page,
                pageSize: request.pageSize,
                sortField: request.sortField,
                direction: request.direction,
                'filter.organizationIds': request.filter && request.filter.organizationIds ? request.filter.organizationIds : [],
                'filter.documentTypeIds': request.filter && request.filter.documentTypeIds ? request.filter.documentTypeIds : [],
                'filter.statusIds': request.filter && request.filter.statusIds ? request.filter.statusIds : [],
                'filter.documentNumber': request.filter && request.filter.documentNumber ? request.filter.documentNumber : '',
                'filter.dateStartFrom': request.filter && request.filter.dateStartFrom ? request.filter.dateStartFrom : '',
                'filter.dateStartTo': request.filter && request.filter.dateStartTo ? request.filter.dateStartTo : '',
            },
            paramsSerializer: params => { return qs.stringify(params); }
        }
         */

        return axios.post(`${this.apiCdUrl}/exportToExcel`, request, {
            responseType: 'blob', // important
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json-patch+json"
            },
        });
    }
}

export default new ReportsService();
import { format } from 'date-fns';

export default (date: Date) => {
    if (date == null) return '';
  return format(new Date(date), 'dd.MM.yyyy');
};

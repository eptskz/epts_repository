﻿import 'core-js/stable';
import 'regenerator-runtime/runtime';
import Vue from 'vue';
//import './plugins/axios';
import vuetify from './plugins/vuetify';
import App from './App.vue';
import router from './router';
import store from '@/store/index';
import i18n from './i18n';
import './registerServiceWorker';
import dateFilter from '@/filters/date.filter';
import datetimeFilter from '@/filters/datetime.filter';
import HighchartsVue from 'highcharts-vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Highcharts from "highcharts";
import VueExpandableImage from 'vue-expandable-image'
import excel from 'vue-excel-export'

import VueMoment from 'vue-moment';
import moment from 'moment';

import DatetimePicker from 'vuetify-datetime-picker'

import VueQuillEditor from 'vue-quill-editor';

import "@/plugins/vuetify-mask.js";

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme


Vue.config.productionTip = false;

Vue.filter('date', dateFilter);
Vue.filter('datetime', datetimeFilter);
import darkUnica from "highcharts/themes/dark-unica";
darkUnica(Highcharts);

Vue.use(HighchartsVue);
Vue.use(VueAxios, axios);
Vue.use(VueMoment, moment);
Vue.use(VueExpandableImage);
Vue.use(excel);
Vue.use(VueQuillEditor, /* { default global options } */);
Vue.use(DatetimePicker);

let vue = new Vue({
    vuetify,
    router,
    store,
    i18n,
    created() { },
    render: (h) => h(App),
}).$mount('#app');

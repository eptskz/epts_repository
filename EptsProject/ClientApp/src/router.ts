import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import ComplienceDocumentList from './views/documents/complience/List.vue';
import ComplienceDocumentForm from './views/documents/complience/Form.vue';
import DigitalPassportList from './views/documents/dpassport/List.vue';
import DigitalPassportDraftList from './views/documents/dpassport/DraftList.vue';
import CreateEptsHandy from './views/documents/dpassport/CreateEptsHandy.vue';
import ChangeApplicationList from './views/documents/dpassport/ChangeApplicationList.vue';
import ChangeApplicationCreate from './views/documents/dpassport/ChangeApplicationCreate.vue'; 
import ChangeApplicationView from './views/documents/dpassport/ChangeApplicationView.vue';
import OrganizationList from './views/registries/organization/OrganizationList.vue';
import OrganizationRootForm from './views/registries/organization/OrganizationRootForm.vue';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: {
            }
        },
        {
            path: '/Home',
            name: 'Home',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/Home.vue'),
            meta: {
            }
        },
        {
            path: '/login',
            name: 'login',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/Login.vue'),
            meta: {
            }
        },
        {
            path: '/registration',
            name: 'registration',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/Registration.vue'),
            meta: {
            }
        },
        {
            path: '/changepassword',
            name: 'changepassword',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/ChangePassword.vue'),
            meta: {
            }
        },
        {
            path: '/compliencedocuments/:docTypeCode',
            name: 'ComplienceDocumentList',
            component: ComplienceDocumentList,
            meta: {
                requiresAuth: true,
                permissions: ['document.otts.read']
            }
        },
        {
            path: '/compliencedocument/:docTypeCode/:id?',
            name: 'ComplienceDocumentForm',
            component: ComplienceDocumentForm,
            meta: {
                requiresAuth: true,
                permissions: ['document.otts.read']
            }
        },

        {
            path: '/digitalpassportdraft',
            name: 'DigitalPassportDraft',
            component: DigitalPassportDraftList,
            meta: {
                requiresAuth: true,
                permissions: ['document.epts.read']
            }
        },
        {
            path: '/digitalpassport',
            name: 'DigitalPassport',
            component: DigitalPassportList,
            meta: {
                requiresAuth: true,
                permissions: ['document.epts.read']
            }
        },
        {
            path: '/create_passport/:kind',
            name: 'CreateEptsHandy',
            component: CreateEptsHandy,
            meta: {
                requiresAuth: true,
                permissions: ['document.epts.edit']
            }
        },
        {
            path: '/edit_epts/:signId',
            name: 'EditEptsHandy',
            component: CreateEptsHandy,
            meta: {
                requiresAuth: true,
                permissions: ['document.epts.edit']
            }
        },
        {
            path: '/view_epts/:id',
            name: 'ViewEptsHandy',
            component: CreateEptsHandy,
            meta: {
                requiresAuth: true,
                permissions: ['document.epts.read']
            }
        },
        {
            path: '/changeapplication',
            name: 'ChangeApplicationList',
            component: ChangeApplicationList,
            meta: {
                requiresAuth: true,
                permissions: ['document.epts.read']
            }
        }, {
            path: '/create_application',
            name: 'ChangeApplicationCreate',
            component: ChangeApplicationCreate,
            meta: {
                requiresAuth: true,
                permissions: ['document.epts.read']
            }
        }, {
            path: '/view_application/:id',
            name: 'ChangeApplicationView',
            component: ChangeApplicationView,
            meta: {
                requiresAuth: true,
                permissions: ['document.epts.read']
            }
        }, 
        {
            path: '/organizations/:registertype',
            name: 'OrganizationList',
            component: OrganizationList,
            meta: {
                requiresAuth: true,
                permissions: ['registry.kiru.read'
                    , 'registry.kirm.read'
                    , 'registry.general.read'
                ]
            }
        },
        {
            path: '/organization/:registertype/:id?',
            name: 'OrganizationRootForm',
            component: OrganizationRootForm,
            meta: {
                requiresAuth: true,
                permissions: [
                    'registry.kiru.edit'
                    , 'registry.kirm.edit'
                    , 'registry.general.edit'
                ]
            }
        },
        {
            path: '/registries/invoices',
            name: 'InvoiceList',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/registries/invoice/InvoiceList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'registry.invoice.read'
                ]
            }
        },
        {
            path: '/registries/invoice/:id',
            name: 'InvoiceForm',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/registries/invoice/InvoiceForm.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'registry.invoice.edit'
                ]
            }
        },
        {
            path: '/registries/invoice/view/:id',
            name: 'InvoiceView',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/registries/invoice/InvoiceView.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'registry.invoice.read'
                ]
            }
        },


        {
            path: '/administration/users/:type?',
            name: 'UserList',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/users/UserList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.user.read'
                ]
            }
        },
        {
            path: '/administration/roles',
            name: 'RoleList',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/roles/RoleList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.role.read'
                ]
            }
        },
        {
            path: '/administration/regrequests',
            name: 'RegRequestList',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/regrequests/RegRequestList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.regrequest.read'
                ]
            }
        },

        {
            path: '/dictionaries/nsi',
            name: 'DicNsiList',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/dictionaries/nsi/DicNsiList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.dictionary.read'
                ]
            }
        },
        {
            path: '/dictionaries/nsi/:id',
            name: 'DicNsiForm',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/dictionaries/nsi/DicNsiForm.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.dictionary.edit'
                ]
            }
        },


        {
            path: '/dictionaries/nsivalues/:nsicode?',
            name: 'DicNsiValueList',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/dictionaries/nsivalue/DicNsiValueList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.dictionary.read'
                ]
            }
        },
        {
            path: '/dictionaries/nsivalue/:id',
            name: 'DicNsiValueForm',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/dictionaries/nsivalue/DicNsiValueForm.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.dictionary.edit'
                ]
            }
        },

        {
            path: '/support/tickets',
            name: 'SupportTickets',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/support/SupportTicketList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.support.read'
                ]
            }
        },
        {
            path: '/support/ticket/:id?',
            name: 'SupportTicketForm',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/support/SupportTicketForm.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.support.processing'
                ]
            }
        },
        {
            path: '/support/ticket/view/:id',
            name: 'SupportTicketView',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/support/SupportTicketView.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.support.read'
                ]
            }
        },

        {
            path: '/administration/contracts',
            name: 'Contracts',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/contracts/ContractList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.contract.read'
                ]
            }
        },
        {
            path: '/administration/contract/:id?',
            name: 'ContractForm',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/contracts/ContractForm.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.contract.edit'
                ]
            }
        },
        {
            path: '/administration/contract/view/:id',
            name: 'ContractView',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/contracts/ConstractView.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.contract.read'
                ]
            }
        },
        {
            path: '/report/digitalpassports',
            name: 'PassportReport',
            component: () => import('./views/reports/Passport.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'reports.epts.view'
                ]
            }
        },
        {
            path: '/monitoring/webservices',
            name: 'MonitoringService',
            component: () => import('./views/monitoring/MonitoringService.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.monitoring.read'
                ]
            }
        },
        {
            path: '/support/instructions',
            name: 'Instructions',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/support/help/InstructionList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.instruction.read'
                ]
            }
        },
        {
            path: '/support/instruction/:id?',
            name: 'InstructionForm',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/support/help/InstructionForm.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.instruction.read'
                ]
            }
        },

        {
            path: '/notifications',
            name: 'notifications',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/notifications/NotificationList.vue'),
            meta: {
            }
        },

        {
            path: '/administration/news',
            name: 'NewsList',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/news/NewsList.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.news.read'
                ]
            }
        },
        {
            path: '/administration/news/:id?',
            name: 'NewsForm',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/administration/news/NewsForm.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'administration.news.read'
                ]
            }
        },

        {
            path: '/reports/cdreport',
            name: 'Cdreport',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/reports/ComplienceDocReport.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'reports.cdreport.view'
                ]
            }
        },


        // public
        {
            path: '/mydigitalpassports',
            name: 'MyDigitalPassports',
            component: () => import(/* webpackChunkName: "fetch-data" */ './views/my/dpassport/MyDigitalPassport.vue'),
            meta: {
                requiresAuth: true,
                permissions: [
                    'external.user.read'
                ]
            }
        }
    ],
});

router.beforeEach((to, from, next) => {

    let user = JSON.parse(localStorage.getItem('user')!);
    if (user) {
        if (user.hasOwnProperty("isPasswordExpire") && user.isPasswordExpire
            && to.fullPath !== '/changepassword') {
            next({
                path: '/changepassword',
                params: { nextUrl: to.fullPath }
            });
        }
    }


    if (to.meta.requiresAuth) {
        if (localStorage.getItem('user') == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            let user = JSON.parse(localStorage.getItem('user')!)

            if (user.claims.some(c => to.meta.permissions.some(p => p === c))) {
                next()
            } else {
                alert("Нет доступа!")
            }

            /*
            if (user.hasOwnProperty("role") && user.role == "Admin" && to.matched.some(record => record.meta.is_admin)) {
                next()
            } else if (user.hasOwnProperty("role") && user.role == "Manager" && to.matched.some(record => record.meta.is_manager)) {
                next()
            } else {
                alert("Нет доступа!")
            }
            next();
            */
        }
    } else {
        next();
    }
});

// export router as default
export default router;
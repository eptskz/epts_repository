class ProcessNcaLayer {
    webSocket: WebSocket | null = null;
    heartbeat_interval: any = null;
    missed_heartbeats: number = 0;
    missed_heartbeats_limit: number = 3;
    missed_heartbeats_limit_min: number = 3;
    missed_heartbeats_limit_max: number = 50;
    isSetLimitMin: boolean = true;
    callback: any = null;
    _afterSignFn: any = null;
    _errorSignFn: any = null;

    _afterGetKeyFn: any = null;
    _errorGetKeyFn: any = null;

    _selectedStorage: string = "PKCS12";
    heartbeat_msg: string = '--heartbeat--';

    constructor() { }

    webSocketInit(callbackM, callbackError) {
        const me = this;
        if (me.webSocket === null || me.webSocket.readyState === 3 || me.webSocket.readyState === 2) {
            me.webSocket = new WebSocket('wss://127.0.0.1:13579/');

            me.webSocket.onopen = function (event) {
                if (me.heartbeat_interval === null) {
                    me.missed_heartbeats = 0;
                    me.heartbeat_interval = setInterval(me.pingLayer, 2000);
                }
                if (callbackM) {
                    me.isSetLimitMin = false;
                    callbackM();
                }
                console.log("Connection opened");
            };

            me.webSocket.onclose = function (event) {
                if (event.wasClean) {
                    console.log('connection has been closed');
                } else {
                    console.log('Connection error');
                    //openDialog();
                }
                console.log('Code: ' + event.code + ' Reason: ' + event.reason);
            };

            me.webSocket.onmessage = function (event) {
                if (event.data === me.heartbeat_msg) {
                    me.missed_heartbeats = 0;
                    return;
                }

                var result = JSON.parse(event.data);

                if (result != null) {
                    var rw = {
                        code: result['code'],
                        message: result['message'],
                        responseObject: result['responseObject'],
                        getResult: function () {
                            return result;
                        },
                        getMessage: function () {
                            return this.message;
                        },
                        getResponseObject: function () {
                            return this.responseObject;
                        },
                        getCode: function () {
                            return this.code;
                        }
                    };
                    if (me.callback)
                        me.callback(rw);
                }
                console.log(event);
                if (me.isSetLimitMin)
                    me.setMissedHeartbeatsLimitToMin();
                me.isSetLimitMin = true;
            };

            me.webSocket.onerror = function (event) {
                if (callbackError) {
                    callbackError();
                }
                console.log('Connection error ');
                console.log(event);
                // openDialog();
            };
            return true;
        } else {
            if (callbackM) {
                me.isSetLimitMin = false;
                callbackM();
            }
        }
        return false;
    }

    signXmlFn(xmlToSign, afterSignFn, errorSignFn) {
        this._afterSignFn = afterSignFn;
        this._errorSignFn = errorSignFn;

        var xmlToSignData = xmlToSign;
        this.signXml(this._selectedStorage, "SIGNATURE", xmlToSignData, this.signXmlBack);
    }

    signXml(storageName, keyType, xmlToSign, callBackP) {
        var signXml = {
            "module": "kz.gov.pki.knca.commonUtils",
            "method": "signXml",
            "args": [storageName, keyType, xmlToSign, "", ""]
        };
        this.callback = callBackP;
        this.setMissedHeartbeatsLimitToMax();
        if(this.webSocket != null)
            this.webSocket.send(JSON.stringify(signXml));
    }

    signXmlBack(result) {
        if (result['code'] === "500") {
            if (result['message'] == 'action.canceled') {
            
            } else {
                alert(result['message']);
            }
        
            if (this._errorSignFn)
                this._errorSignFn(result);
        } else if (result['code'] === "200") {
            var res = result['responseObject'];
            if (this._afterSignFn)
                this._afterSignFn(res);
        }
    }


    getKeyInfoCall(afterGetKeyFn, errorGetKeyFn) {
        this._afterGetKeyFn = afterGetKeyFn;
        this._errorGetKeyFn = errorGetKeyFn;
        var selectedStorage = this._selectedStorage;
        this.getKeyInfo(selectedStorage, this.getKeyInfoBack);
    }

    getKeyInfo(storageName, callBackP) {
        var getKeyInfo = {
            "module": "kz.gov.pki.knca.commonUtils",
            "method": "getKeyInfo",
            "args": [storageName]
        };
        this.callback = callBackP;
        this.setMissedHeartbeatsLimitToMax();
        if (this.webSocket != null)
            this.webSocket.send(JSON.stringify(getKeyInfo));
    }

    getKeyInfoBack(result) {
        if (result['code'] === "500") {
            if (result['message'] == 'action.canceled') {

            } else {
                alert(result['message']);
            }

            if (this._errorSignFn)
                this._errorSignFn(result);
        } else if (result['code'] === "200") {

            var res = result['responseObject'];
            if (this._afterGetKeyFn)
                this._afterGetKeyFn(res);
        }
    }

    findSubjectAttr(attrs, attr) {
        var tmp;
        var numb;

        for (numb = 0; numb < attrs.length; numb++) {
            tmp = attrs[numb].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
            if (tmp.indexOf(attr + '=') === 0) {
                return tmp.substr(attr.length + 1);
            }
        }

        return null;
    }



    pingLayer() {
        console.log("pinging...");
        try {
            this.missed_heartbeats++;
            if (this.missed_heartbeats >= this.missed_heartbeats_limit)
                throw new Error("Too many missed heartbeats.");
            if (this.webSocket != null)
                this.webSocket.send(this.heartbeat_msg);
        } catch (e) {
            clearInterval(this.heartbeat_interval);
            this.heartbeat_interval = null;
            console.warn("Closing connection. Reason: " + e.message);

            if (this.webSocket != null)
                this.webSocket.close();
        }
    }
    setMissedHeartbeatsLimitToMax() {
        this.missed_heartbeats_limit = this.missed_heartbeats_limit_max;
    }
    setMissedHeartbeatsLimitToMin() {
        this.missed_heartbeats_limit = this.missed_heartbeats_limit_min;
    }
};

export default new ProcessNcaLayer();
﻿import moment from 'moment';

class DateHelper {
    constructor() { }
    formatDate(inDate) {
        return inDate
            ? moment(inDate).format("DD.MM.YYYY")
            : "";
    }
    formatDateTime(inDate) {
        return inDate
            ? moment(inDate).format("DD.MM.YYYY HH:mm:ss")
            : "";
    }
}

export default new DateHelper();
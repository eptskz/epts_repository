﻿module.exports = {   
    devServer: {
        host: 'localhost'
    },
    runtimeCompiler: true,
    pluginOptions: {
        i18n: {
            locale: "ru",
            fallbackLocale: "ru",
            localeDir: "locales",
            enableInSFC: false
        }
    }
};
﻿using DBCore.DTOs.Characteristics;
using DBCore.DTOs.Complience.Vehicle;
using DBCore.Models.Vehicle;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers.ComplienceDocuments
{
    public partial class ComplienceDocumentController
    {
        /// <summary>
        /// Сохранить данные о деталях информации
        /// </summary>
        /// <param name="vehicleTypeDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleTypeDetail")]
        public async Task<JsonResponse> SaveVehicleTypeDetail([FromBody] VehicleTypeDetailDto vehicleTypeDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var vehicleTypeDetail = _mapper.Map<VehicleTypeDetailDto, VehicleTypeDetail>(vehicleTypeDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateVehicleTypeDetailAsync(vehicleTypeDetail);
                // _unitOfWork.ComplienceDocumentRepository.GetVehicleTypeDetail
                response.Data = new { entityId };
            } 
            catch(Exception e)
            {
                _logger.LogError("SaveVehicleTypeDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleTypeDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Коммерческое название
        /// </summary>
        /// <param name="commercialNameDto"></param>
        /// <returns></returns>
        [HttpPost("saveCommercialName")]
        public async Task<JsonResponse> SaveCommercialName([FromBody] VehicleCommercialNameDto commercialNameDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var commercialName = _mapper.Map<VehicleCommercialNameDto, VehicleCommercialName>(commercialNameDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateCommercialNameAsync(commercialName);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveCommercialName {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveCommercialName error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Тип
        /// </summary>
        /// <param name="vehicleTypeDto"></param>
        /// <returns></returns>
        [HttpPost("saveType")]
        public async Task<JsonResponse> SaveType([FromBody] VehicleTypeDto vehicleTypeDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var vehicleType = _mapper.Map<VehicleTypeDto, VehicleType>(vehicleTypeDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(vehicleType);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleType {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleType error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        
        /// <summary>
        /// Сохранить Модификацию
        /// </summary>
        /// <param name="modificationDto"></param>
        /// <returns></returns>
        [HttpPost("saveModification")]
        public async Task<JsonResponse> SaveModification([FromBody] ModificationDto modificationDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var modification = _mapper.Map<ModificationDto, VehicleTypeModification>(modificationDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(modification);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveModification {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveModification error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        /// <summary>
        /// Сохранить Класс Категории
        /// </summary>
        /// <param name="classCategoryDto"></param>
        /// <returns></returns>
        [HttpPost("saveClassCategory")]
        public async Task<JsonResponse> SaveClassCategory([FromBody] VehicleTypeDicDto classCategoryDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var classCategory = _mapper.Map<VehicleTypeDicDto, VehicleTypeClassCategory>(classCategoryDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(classCategory);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveClassCategory {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveClassCategory error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        /// <summary>
        /// Сохранить Базовый тип Автомобиля
        /// </summary>
        /// <param name="baseVehicleTypeDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveBaseVehicleTypeDetail")]
        public async Task<JsonResponse> SaveBaseVehicleTypeDetail([FromBody] BaseVehicleTypeDetailDto baseVehicleTypeDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<BaseVehicleTypeDetailDto, BaseVehicleTypeDetail>(baseVehicleTypeDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveBaseVehicleTypeDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveBaseVehicleTypeDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Трансмиссия
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetTransmissionTypesByDocId")]
        public async Task<JsonResponse> GetTransmissionTypesByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                         .Include(p => p.VehicleTransmissionTypes)
                            .ThenInclude(tv => tv.TransType)
                        .Include(p => p.VehicleTransmissionTypes)
                            .ThenInclude(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.UnitKind)
                        .Include(p => p.VehicleTransmissionTypes)
                            .ThenInclude(tv => tv.TransmissionUnitDetails)
                        .Include(p => p.VehicleTransmissionTypes)
                            .ThenInclude(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.AxisDistribution)
                        .Include(p => p.VehicleTransmissionTypes)
                            .ThenInclude(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.TransmissionBoxType)
                        .Include(p => p.VehicleTransmissionTypes)
                            .ThenInclude(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.MainGearType)
                        .Include(p => p.VehicleTransmissionTypes)
                            .ThenInclude(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.TransmissionUnitGears)
                        .Include(p => p.VehicleTransmissionTypes)
                            .ThenInclude(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.TransmissionUnitGears)
                            .ThenInclude(ug => ug.TransmissionUnitGearValue)
                        .Include(p => p.VehicleTransmissionTypes)
                            .ThenInclude(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.TransmissionUnitGears)
                            .ThenInclude(ug => ug.TransmissionUnitGearType)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        private async Task<TransmissionType> getTransmissionTypesById(int id)
        {
            TransmissionType entity = null;
            try
            {   
                entity = await _unitOfWork.ComplienceDocumentRepository.GetTransmissionType().AsNoTracking()
                        .Include(tv => tv.TransType)
                        .Include(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.UnitKind)
                        .Include(tv => tv.TransmissionUnitDetails)
                        .Include(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.AxisDistribution)
                        .Include(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.TransmissionBoxType)
                        .Include(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.MainGearType)
                        .Include(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.TransmissionUnitGears)
                        .Include(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.TransmissionUnitGears)
                            .ThenInclude(ug => ug.TransmissionUnitGearValue)
                        .Include(tv => tv.TransmissionUnitDetails)
                            .ThenInclude(u => u.TransmissionUnitGears)
                            .ThenInclude(ug => ug.TransmissionUnitGearType)
                        .SingleOrDefaultAsync(ch => ch.Id == id);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "getTransmissionTypesById");
            }
            return entity;
        }
        /// <summary>
        /// Сохранить Трансмиссия
        /// </summary>
        /// <param name="transmissionTypeDto"></param>
        /// <returns></returns>
        [HttpPost("saveTransmissionType")]
        public async Task<JsonResponse> SaveTransmissionType([FromBody] TransmissionTypeDto transmissionTypeDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<TransmissionTypeDto, TransmissionType>(transmissionTypeDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateTransmissionTypeAsync(entity);
                var newEntity = await this.getTransmissionTypesById(entityId);
                var entityDto = _mapper.Map<TransmissionType, TransmissionTypeDto>(newEntity);
                response.Data = new { entity = entityDto };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveTransmissionType {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveTransmissionType error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Подвеска
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleSuspensionDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleSuspensionDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(p => p.VehicleSuspensionDetails)
                            .ThenInclude(tv => tv.VehicleSuspensionKind)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Подвеска
        /// </summary>
        /// <param name="vehicleSuspensionDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleSuspensionDetail")]
        public async Task<JsonResponse> SaveVehicleSuspensionDetail([FromBody] VehicleSuspensionDetailDto vehicleSuspensionDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleSuspensionDetailDto, VehicleSuspensionDetail>(vehicleSuspensionDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleSuspensionDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleSuspensionDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Сцепление
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleClutchDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleClutchDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(p => p.VehicleClutchDetails)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Сцепление
        /// </summary>
        /// <param name="vehicleClutchDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleClutchDetail")]
        public async Task<JsonResponse> SaveVehicleClutchDetail([FromBody] VehicleClutchDetailDto vehicleClutchDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var vehicleClutchDetail = _mapper.Map<VehicleClutchDetailDto, VehicleClutchDetail>(vehicleClutchDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(vehicleClutchDetail);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleClutchDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleClutchDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Информация о шинах
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleTyreKindInfosByDocId")]
        public async Task<JsonResponse> GetVehicleTyreKindInfosByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(p => p.VehicleTyreKindInfos)
                                .ThenInclude(tv => tv.VehicleTyreKindSpeed)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Шины
        /// </summary>
        /// <param name="vehicleTyreKindInfoDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleTyreKindInfo")]
        public async Task<JsonResponse> SaveVehicleTyreKindInfo([FromBody] VehicleTyreKindInfoDto vehicleTyreKindInfoDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var vehicleTyreKind = _mapper.Map<VehicleTyreKindInfoDto, VehicleTyreKindInfo>(vehicleTyreKindInfoDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(vehicleTyreKind);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleTyreKindInfo {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleTyreKindInfo error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Дополнительная информация
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetAdditionalDataByDocId")]
        public async Task<JsonResponse> GetAdditionalDataByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository
                                        .GetAsIQueryable(v => v.Deleted == null && v.Id == id).AsNoTracking()
                                        .Include(v => v.DocType)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.VehicleUseRestrictions)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.ShassisMovePermitions)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.VehicleRoutings)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.VehicleNotes)
                                        .SingleOrDefaultAsync();
                _logger.LogDebug("End DateTime = " + DateTime.Now);

                response.Data = _mapper.Map<VehicleTypeDetailDto>(entity.VehicleTypeDetail);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Ограничения на возможность использования на дорогах общего пользования
        /// </summary>
        /// <param name="vehicleUseRestrictionDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleUseRestriction")]
        public async Task<JsonResponse> SaveVehicleUseRestriction([FromBody] VehicleUseRestrictionDto vehicleUseRestrictionDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleUseRestrictionDto, VehicleUseRestriction>(vehicleUseRestrictionDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleUseRestriction {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleUseRestriction error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Возможность передвижения шасси своим ходом по дорогам общего пользования
        /// </summary>
        /// <param name="shassisMovePermitionDto"></param>
        /// <returns></returns>
        [HttpPost("saveShassisMovePermition")]
        public async Task<JsonResponse> SaveShassisMovePermition([FromBody] ShassisMovePermitionDto shassisMovePermitionDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<ShassisMovePermitionDto, ShassisMovePermition>(shassisMovePermitionDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveShassisMovePermition {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveShassisMovePermition error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Возможность использования в качестве маршрутного транспортного средства
        /// </summary>
        /// <param name="vehicleRoutingDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleRouting")]
        public async Task<JsonResponse> SaveVehicleRouting([FromBody] VehicleRoutingDto vehicleRoutingDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleRoutingDto, VehicleRouting>(vehicleRoutingDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleRouting {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleRouting error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Иная информация
        /// </summary>
        /// <param name="vehicleNoteDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleNote")]
        public async Task<JsonResponse> SaveVehicleNote([FromBody] VehicleNoteDto vehicleNoteDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleNoteDto, VehicleNote>(vehicleNoteDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleNote {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleNote error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Масса
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleWeightDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleWeightDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(cd => cd.VehicleWeightMeasures)
                            .ThenInclude(vr => vr.Unit)
                        .Include(cd => cd.VehicleWeightMeasures)
                            .ThenInclude(vr => vr.MassType)
                        .Include(cd => cd.BrakedTrailerWeightMeasures)
                            .ThenInclude(vr => vr.Unit)
                        .Include(cd => cd.UnbrakedTrailerWeightMeasures)
                            .ThenInclude(vr => vr.Unit)
                        .Include(cd => cd.VehicleHitchLoadMeasures)
                            .ThenInclude(vr => vr.Unit)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Масса
        /// </summary>
        /// <param name="vehicleWeightMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleWeightMeasure")]
        public async Task<JsonResponse> SaveVehicleWeightMeasure([FromBody] VehicleWeightMeasureDto vehicleWeightMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleWeightMeasureDto, VehicleWeightMeasure>(vehicleWeightMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleWeightMeasure {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleWeightMeasure error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Масса прицепа без тормозной системы
        /// </summary>
        /// <param name="unbrakedTrailerWeightMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveUnbrakedTrailerWeight")]
        public async Task<JsonResponse> SaveUnbrakedTrailerWeight([FromBody] UnbrakedTrailerWeightMeasureDto unbrakedTrailerWeightMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<UnbrakedTrailerWeightMeasureDto, UnbrakedTrailerWeightMeasure>(unbrakedTrailerWeightMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveUnbrakedTrailerWeight {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveUnbrakedTrailerWeight error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Масса прицепа с тормозной системой
        /// </summary>
        /// <param name="brakedTrailerWeightMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveBrakedTrailerWeight")]
        public async Task<JsonResponse> SaveBrakedTrailerWeight([FromBody] BrakedTrailerWeightMeasureDto brakedTrailerWeightMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<BrakedTrailerWeightMeasureDto, BrakedTrailerWeightMeasure>(brakedTrailerWeightMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveBrakedTrailerWeight {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveBrakedTrailerWeight error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Технически допустимая максимальная нагрузка на опорно сцепное устройство
        /// </summary>
        /// <param name="vehicleHitchLoadMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleHitchLoadMeasure")]
        public async Task<JsonResponse> SaveVehicleHitchLoadMeasure([FromBody] VehicleHitchLoadMeasureDto vehicleHitchLoadMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleHitchLoadMeasureDto, VehicleHitchLoadMeasure>(vehicleHitchLoadMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleHitchLoadMeasure {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleHitchLoadMeasure error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Колесная формула/ Ведущие колеса
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleRunningGearDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleRunningGearDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetVehicleRunningGearDetails()
                        .AsNoTracking()
                        .Include(vr => vr.VehicleWheelFormula)
                        .Include(vr => vr.PoweredWheelLocations)
                        .ThenInclude(wl => wl.WheelLocation)
                        .Where(ch => ch.Characteristic.VehicleTypeDetail.DocumentId == id)
                        .ToListAsync();

                _logger.LogDebug("End DateTime = " + DateTime.Now);

                response.Data = _mapper.Map<ICollection<VehicleRunningGearDetailDto>>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetVehicleRunningGearDetailsByDocId");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        private async Task<VehicleRunningGearDetail> getVehicleRunningGearDetailById(int id)
        {
            VehicleRunningGearDetail entity = null;
            try
            {
                entity = await _unitOfWork.ComplienceDocumentRepository.GetVehicleRunningGearDetails()
                        .AsNoTracking()
                        .Include(vr => vr.VehicleWheelFormula)
                        .Include(vr => vr.PoweredWheelLocations)
                        .ThenInclude(wl => wl.WheelLocation)
                        .Where(ch => ch.Id == id)
                        .SingleOrDefaultAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "getVehicleRunningGearDetailById");
            }
            return entity;
        }
        /// <summary>
        /// Сохранить Колесная формула/ Ведущие колеса
        /// </summary>
        /// <param name="vehicleRunningGearDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleRunningGearDetail")]
        public async Task<JsonResponse> SaveVehicleRunningGearDetail([FromBody] VehicleRunningGearDetailDto vehicleRunningGearDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleRunningGearDetailDto, VehicleRunningGearDetail>(vehicleRunningGearDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateVehicleRunningGearDetailAsync(entity);
                var newEntity = await this.getVehicleRunningGearDetailById(entity.Id);
                var entityDto = _mapper.Map<VehicleRunningGearDetail, VehicleRunningGearDetailDto>(newEntity);
                response.Data = new { entity = entityDto };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleRunningGearDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleRunningGearDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Тип кузова
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getVehicleBodyworkDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleBodyworkDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetVehicleBodyworkDetails().AsNoTracking()
                        .Include(vr => vr.VehicleBodyWorkType)
                        .Where(ch => ch.Characteristic.VehicleTypeDetail.DocumentId == id)
                        .ToListAsync();

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<ICollection<VehicleBodyworkDetailDto>>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Тип Кузова
        /// </summary>
        /// <param name="vehicleBodyworkDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleBodyworkDetail")]
        public async Task<JsonResponse> SaveVehicleBodyworkDetail([FromBody] VehicleBodyworkDetailDto vehicleBodyworkDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleBodyworkDetailDto, VehicleBodyworkDetail>(vehicleBodyworkDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleBodyworkDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleBodyworkDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Количество мест для сидения
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getVehicleSeatDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleSeatDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetVehicleSeatDetails().AsNoTracking()
                        .Include(vr => vr.VehicleSeatRawDetails)
                        .Where(ch => ch.Characteristic.VehicleTypeDetail.DocumentId == id)
                        .ToListAsync();

                response.Data = _mapper.Map<ICollection<VehicleSeatDetailDto>>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "getVehicleSeatDetailsByDocId");
                _nlogger.Error(e, "getVehicleSeatDetailsByDocId error");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Количество мест для сидения
        /// </summary>
        /// <param name="vehicleSeatDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleSeatDetail")]
        public async Task<JsonResponse> SaveVehicleSeatDetail([FromBody] VehicleSeatDetailDto vehicleSeatDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleSeatDetailDto, VehicleSeatDetail>(vehicleSeatDetailDto);
                var savedEntity = await _unitOfWork.ComplienceDocumentRepository.UpdateVehicleSeatDetailAsync(entity);
                response.Data = new { entity = savedEntity };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleBodyworkDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleBodyworkDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Компоновка транспортного средства  (ОТТС/СБКТС)/Компоновка транспортного средства  (ОТШ)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleLayoutDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleLayoutDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(cd => cd.VehicleLayoutPatterns)
                            .ThenInclude(vr => vr.LayoutPattern)
                        .Include(cd => cd.VehicleEngineLayouts)
                            .ThenInclude(vr => vr.EngineLocation)

                        .Include(cd => cd.VehicleCarriageSpaceImplementations)
                        .Include(cd => cd.VehicleCabins)

                        .Include(cd => cd.VehiclePurposes)
                            .ThenInclude(vr => vr.Purpose)
                        .Include(cd => cd.VehiclePurposes)
                            .ThenInclude(vr => vr.OtherInfo)
                        .Include(cd => cd.VehicleFrames)
                        .Include(cd => cd.VehiclePassengerQuantities)
                        .Include(cd => cd.VehicleTrunkVolumeMeasures)
                            .ThenInclude(vr => vr.Unit)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Схема компоновки транспортного средства
        /// </summary>
        /// <param name="vehicleLayoutPatternDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleLayoutDetail")]
        public async Task<JsonResponse> SaveVehicleLayoutDetail([FromBody] VehicleLayoutPatternDto vehicleLayoutPatternDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleLayoutPatternDto, VehicleLayoutPattern>(vehicleLayoutPatternDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleLayoutDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleLayoutDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Расположение двигателя
        /// </summary>
        /// <param name="vehicleEngineLayoutDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleEngineLayout")]
        public async Task<JsonResponse> SaveVehicleEngineLayout([FromBody] VehicleEngineLayoutDto vehicleEngineLayoutDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleEngineLayoutDto, VehicleEngineLayout>(vehicleEngineLayoutDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleEngineLayout {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleEngineLayout error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Исполнение загрузочного пространства
        /// </summary>
        /// <param name="vehicleCarriageSpaceImplementationDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleCarriageSpaceImplementation")]
        public async Task<JsonResponse> SaveVehicleCarriageSpaceImplementation([FromBody] VehicleCarriageSpaceImplementationDto vehicleCarriageSpaceImplementationDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleCarriageSpaceImplementationDto, VehicleCarriageSpaceImplementation>(vehicleCarriageSpaceImplementationDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleCarriageSpaceImplementation {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleCarriageSpaceImplementation error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Кабина
        /// </summary>
        /// <param name="vehicleCabinDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleCabin")]
        public async Task<JsonResponse> SaveVehicleCabin([FromBody] VehicleCabinDto vehicleCabinDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleCabinDto, VehicleCabin>(vehicleCabinDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("saveVehicleCabin {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "saveVehicleCabin error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Назначение
        /// </summary>
        /// <param name="vehiclePurposeDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehiclePurpose")]
        public async Task<JsonResponse> SaveVehiclePurpose([FromBody] VehiclePurposeDto vehiclePurposeDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehiclePurposeDto, VehiclePurpose>(vehiclePurposeDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehiclePurpose {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehiclePurpose error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Рама
        /// </summary>
        /// <param name="vehicleFrameDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleFrame")]
        public async Task<JsonResponse> SaveVehicleFrame([FromBody] VehicleFrameDto vehicleFrameDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleFrameDto, VehicleFrame>(vehicleFrameDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleFrame {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleFrame error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Пассажировместимость
        /// </summary>
        /// <param name="vehiclePassengerQuantityDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehiclePassengerQuantity")]
        public async Task<JsonResponse> SaveVehiclePassengerQuantity([FromBody] VehiclePassengerQuantityDto vehiclePassengerQuantityDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehiclePassengerQuantityDto, VehiclePassengerQuantity>(vehiclePassengerQuantityDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehiclePassengerQuantity {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehiclePassengerQuantity error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Общий объем багажных отделений транспортного средства
        /// </summary>
        /// <param name="vehicleTrunkVolumeMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleTrunkVolumeMeasure")]
        public async Task<JsonResponse> SaveVehicleTrunkVolumeMeasure([FromBody] VehicleTrunkVolumeMeasureDto vehicleTrunkVolumeMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleTrunkVolumeMeasureDto, VehicleTrunkVolumeMeasure>(vehicleTrunkVolumeMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleTrunkVolumeMeasure {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleTrunkVolumeMeasure error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Габаритные размеры
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleDimensionDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleDimensionDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(cd => cd.LengthRanges)
                            .ThenInclude(vr => vr.Unit)
                        .Include(cd => cd.WidthRanges)
                            .ThenInclude(vr => vr.Unit)
                        .Include(cd => cd.HeightRanges)
                            .ThenInclude(vr => vr.Unit)
                        .Include(cd => cd.LoadingHeightRanges)
                            .ThenInclude(vr => vr.Unit)
                        .Include(cd => cd.MaxHeightRanges)
                            .ThenInclude(vr => vr.Unit)
                        .Include(cd => cd.WheelbaseMeasureRanges)
                            .ThenInclude(vr => vr.Unit)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Габариты Длины
        /// </summary>
        /// <param name="vehicleLengthMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleLengthMeasure")]
        public async Task<JsonResponse> SaveVehicleLengthMeasure([FromBody] VehicleLengthMeasureDto vehicleLengthMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleLengthMeasureDto, VehicleLengthMeasure>(vehicleLengthMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleLengthMeasure {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleLengthMeasure error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Габариты Ширина
        /// </summary>
        /// <param name="vehicleWidthMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleWidthMeasure")]
        public async Task<JsonResponse> SaveVehicleWidthMeasure([FromBody] VehicleWidthMeasureDto vehicleWidthMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleWidthMeasureDto, VehicleWidthMeasure>(vehicleWidthMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleWidthMeasure {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleWidthMeasure error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Габариты Высота
        /// </summary>
        /// <param name="vehicleHeightMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleHeightMeasure")]
        public async Task<JsonResponse> SaveVehicleHeightMeasure([FromBody] VehicleHeightMeasureDto vehicleHeightMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleHeightMeasureDto, VehicleHeightMeasure>(vehicleHeightMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleHeightMeasure {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleHeightMeasure error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Габариты Высота погрузочная
        /// </summary>
        /// <param name="vehicleLoadingHeightMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleLoadingHeightMeasure")]
        public async Task<JsonResponse> SaveVehicleLoadingHeightMeasure([FromBody] VehicleLoadingHeightMeasureDto vehicleLoadingHeightMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleLoadingHeightMeasureDto, VehicleLoadingHeightMeasure>(vehicleLoadingHeightMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleLoadingHeightMeasure {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleLoadingHeightMeasure error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Габариты Высота погрузочная
        /// </summary>
        /// <param name="vehicleWheelbaseMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleWheelbaseMeasure")]
        public async Task<JsonResponse> SaveVehicleWheelbaseMeasure([FromBody] VehicleWheelbaseMeasureDto vehicleWheelbaseMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleWheelbaseMeasureDto, VehicleWheelbaseMeasure>(vehicleWheelbaseMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleWheelbaseMeasure {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleWheelbaseMeasure error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Габариты Высота максимально допустимая
        /// </summary>
        /// <param name="vehicleMaxHeightMeasureDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleMaxHeightMeasure")]
        public async Task<JsonResponse> SaveVehicleMaxHeightMeasure([FromBody] VehicleMaxHeightMeasureDto vehicleMaxHeightMeasureDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleMaxHeightMeasureDto, VehicleMaxHeightMeasure>(vehicleMaxHeightMeasureDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleMaxHeightMeasure {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleMaxHeightMeasure error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
    }
}

﻿using DBCore.DTOs.Characteristics;
using DBCore.Models.Vehicle;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers.ComplienceDocuments
{
    public partial class ComplienceDocumentController
    {
        /// <summary>
        /// Списиок Двигатель
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetEngineDetailsByDocId")]
        public async Task<JsonResponse> GetEngineDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);

                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(cd => cd.EngineType)
                        .Include(cd => cd.VehicleHybridDesigns)
                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.EngineCapacityMeasureUnit)
                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.EngineCylinderArrangement)

                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.EnginePowerDetails)
                            .ThenInclude(p => p.EngineMaxPowerMeasureUnit)
                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.EnginePowerDetails)
                            .ThenInclude(p => p.EngineMaxPowerShaftRotationFrequencyMeasureUnit)

                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.EngineTorqueDetails)
                            .ThenInclude(p => p.EngineMaxTorqueMeasureUnit)
                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.EngineTorqueDetails)
                            .ThenInclude(p => p.EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnit)

                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.ECUModelDetails)
                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(tv => tv.VehicleIgnitionDetails)
                            .ThenInclude(vr => vr.VehicleIgnitionType)
                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.ExhaustDetails)
                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.EngineFuelFeedDetails)
                            .ThenInclude(ef => ef.FuelFeed)
                        .Include(cd => cd.EngineDetails)
                            .ThenInclude(vr => vr.VehicleFuelKinds)
                            .ThenInclude(vr => vr.FuelKind)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Двигатель
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetEngineDetailById")]
        public async Task<JsonResponse> GetEngineDetailById(int id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);

                var entity = await _unitOfWork.ComplienceDocumentRepository.GetEngineDetails().AsNoTracking()
                        .Include(vr => vr.EngineCapacityMeasureUnit)
                        .Include(vr => vr.EngineCylinderArrangement)

                        .Include(vr => vr.EnginePowerDetails)
                            .ThenInclude(p => p.EngineMaxPowerMeasureUnit)
                        .Include(vr => vr.EnginePowerDetails)
                            .ThenInclude(p => p.EngineMaxPowerShaftRotationFrequencyMeasureUnit)

                        .Include(vr => vr.EngineTorqueDetails)
                            .ThenInclude(p => p.EngineMaxTorqueMeasureUnit)
                        .Include(vr => vr.EngineTorqueDetails)
                            .ThenInclude(p => p.EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnit)

                        .Include(vr => vr.ECUModelDetails)
                        .Include(tv => tv.VehicleIgnitionDetails)
                            .ThenInclude(vr => vr.VehicleIgnitionType)
                        .Include(vr => vr.ExhaustDetails)
                        .Include(vr => vr.EngineFuelFeedDetails)
                            .ThenInclude(ef => ef.FuelFeed)
                        .Include(vr => vr.VehicleFuelKinds)
                            .ThenInclude(vr => vr.FuelKind)
                        .SingleOrDefaultAsync(ed => ed.Id == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetEngineDetailsById");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        private async Task<EngineDetail> getEngineDetailById(int id)
        {
            EngineDetail entity = null;
            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);

                entity = await _unitOfWork.ComplienceDocumentRepository.GetEngineDetails().AsNoTracking()
                        .Include(vr => vr.EngineCapacityMeasureUnit)
                        .Include(vr => vr.EngineCylinderArrangement)

                        .Include(vr => vr.EnginePowerDetails)
                            .ThenInclude(p => p.EngineMaxPowerMeasureUnit)
                        .Include(vr => vr.EnginePowerDetails)
                            .ThenInclude(p => p.EngineMaxPowerShaftRotationFrequencyMeasureUnit)

                        .Include(vr => vr.EngineTorqueDetails)
                            .ThenInclude(p => p.EngineMaxTorqueMeasureUnit)
                        .Include(vr => vr.EngineTorqueDetails)
                            .ThenInclude(p => p.EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnit)

                        .Include(vr => vr.ECUModelDetails)
                        .Include(tv => tv.VehicleIgnitionDetails)
                            .ThenInclude(vr => vr.VehicleIgnitionType)
                        .Include(vr => vr.ExhaustDetails)
                        .Include(vr => vr.EngineFuelFeedDetails)
                            .ThenInclude(ef => ef.FuelFeed)
                        .Include(vr => vr.VehicleFuelKinds)
                            .ThenInclude(vr => vr.FuelKind)
                        .SingleOrDefaultAsync(ed => ed.Id == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "getEngineDetailById");
            }
            return entity;
        }


        /// <summary>
        /// Электродвигатель/ Машина
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetElectricalMachineDetailsByDocId")]
        public async Task<JsonResponse> GetElectricalMachineDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(cd => cd.ElectricalMachineDetails)
                            .ThenInclude(vr => vr.ElectricalMachineKind)
                        .Include(cd => cd.ElectricalMachineDetails)
                            .ThenInclude(vr => vr.ElectricMotorPowerMeasureUnit)
                        .Include(cd => cd.ElectricalMachineDetails)
                            .ThenInclude(vr => vr.ElectricalMachineVoltageMeasureUnit)
                        .Include(cd => cd.ElectricalMachineDetails)
                            .ThenInclude(vr => vr.ElectricalMachineType)
                        .Include(cd => cd.ElectricalMachineDetails)
                            .ThenInclude(vr => vr.PowerStorageDeviceDetails)
                        .Include(cd => cd.ElectricalMachineDetails)
                            .ThenInclude(vr => vr.PowerStorageDeviceDetails)
                            .ThenInclude(p => p.VehicleRangeUnit)
                        .Include(cd => cd.ElectricalMachineDetails)
                            .ThenInclude(tv => tv.PowerStorageDeviceDetails)
                            .ThenInclude(p => p.PowerStorageDeviceVoltageMeasureUnit)
                        .Include(cd => cd.ElectricalMachineDetails)
                            .ThenInclude(tv => tv.PowerStorageDeviceDetails)
                            .ThenInclude(p => p.PowerStorageDeviceType)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        private async Task<ElectricalMachineDetail> getElectricalMachineDetailById(int id)
        {
            ElectricalMachineDetail entity = null;
            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);

                entity = await _unitOfWork.ComplienceDocumentRepository.GetElectricalMachineDetails().AsNoTracking()
                        .Include(vr => vr.ElectricalMachineKind)
                        .Include(vr => vr.ElectricMotorPowerMeasureUnit)
                        .Include(vr => vr.ElectricalMachineVoltageMeasureUnit)
                        .Include(vr => vr.ElectricalMachineType)
                        .Include(vr => vr.PowerStorageDeviceDetails)
                        .Include(vr => vr.PowerStorageDeviceDetails)
                            .ThenInclude(p => p.VehicleRangeUnit)
                        .Include(tv => tv.PowerStorageDeviceDetails)
                            .ThenInclude(p => p.PowerStorageDeviceVoltageMeasureUnit)
                        .Include(tv => tv.PowerStorageDeviceDetails)
                            .ThenInclude(p => p.PowerStorageDeviceType)
                        .SingleOrDefaultAsync(ch => ch.Id == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "getElectricalMachineDetailById");
            }
            return entity;
        }

        /// <summary>
        /// Сохранить ДВС
        /// </summary>
        /// <param name="engineDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveEngineDetail")]
        public async Task<JsonResponse> SaveEngineDetail([FromBody] EngineDetailDto engineDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var engineDetail = _mapper.Map<EngineDetailDto, EngineDetail>(engineDetailDto);
                var entity = await _unitOfWork.ComplienceDocumentRepository.UpdateEngineDetailStateAsync(engineDetail);
                var newEntity = await this.getEngineDetailById(entity.Id);
                var entityDto = _mapper.Map<EngineDetail, EngineDetailDto>(newEntity);
                response.Data = new { entity = entityDto };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveEngineDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveEngineDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Сохранить Электродвигатель
        /// </summary>
        /// <param name="eMachineDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveEMachineDetail")]
        public async Task<JsonResponse> SaveEMachineDetail([FromBody] ElectricalMachineDetailDto eMachineDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var emachine = _mapper.Map<ElectricalMachineDetailDto, ElectricalMachineDetail>(eMachineDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEMachineDetailStateAsync(emachine);
                var newEntity = await this.getElectricalMachineDetailById(entityId);
                var entityDto = _mapper.Map<ElectricalMachineDetail, ElectricalMachineDetailDto>(newEntity);
                response.Data = new { entity = entityDto };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveEMachineDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveEMachineDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Устройство накопления энергии
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getPowerStorageDeviceDetailsByDocId")]
        public async Task<JsonResponse> GetPowerStorageDeviceDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(vr => vr.PowerStorageDeviceDetails)
                            .ThenInclude(p => p.VehicleRangeUnit)
                        .Include(tv => tv.PowerStorageDeviceDetails)
                            .ThenInclude(p => p.PowerStorageDeviceVoltageMeasureUnit)
                        .Include(tv => tv.PowerStorageDeviceDetails)
                            .ThenInclude(p => p.PowerStorageDeviceType)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }


        /// <summary>
        /// Сохранить Устройство накопления энергии
        /// </summary>
        /// <param name="powerStorageDto"></param>
        /// <returns></returns>
        [HttpPost("savePowerStorageDeviceDetail")]
        public async Task<JsonResponse> SavePowerStorageDeviceDetail([FromBody] PowerStorageDeviceDetailDto powerStorageDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var powerStorage = _mapper.Map<PowerStorageDeviceDetailDto, PowerStorageDeviceDetail>(powerStorageDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(powerStorage);
                response.Data = new { entityId = entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SavePowerStorageDeviceDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SavePowerStorageDeviceDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

    }
}

﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.DTOs.Characteristics;
using DBCore.DTOs.Complience;
using DBCore.DTOs.Complience.Vehicle;
using DBCore.DTOs.Documents;
using DBCore.Models;
using DBCore.Models.ComplienceDocuments;
using DBCore.Models.ComplienceDocuments.Vehicle;
using DBCore.Models.Dictionaries;
using DBCore.Models.DocumentEvents;
using DBCore.Models.Vehicle;
using DBCore.Permissions;
using EptsBL.Extensions;
using EptsBL.Utils;
using EptsProject.Core;
using EptsProject.Helpers;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace EptsProject.Controllers.ComplienceDocuments
{
    /// <summary>
    /// ОТТС / ОТШ
    /// </summary>

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public partial class ComplienceDocumentController : ControllerBase
    {
        private readonly ILogger<ComplienceDocumentController> _logger;
        private readonly NLog.ILogger _nlogger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<AspNetUsers> _userManager;
        private readonly IMapper _mapper;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="userManager"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="mapper"></param>
        public ComplienceDocumentController(ILogger<ComplienceDocumentController> logger
            , UserManager<AspNetUsers> userManager
            , IUnitOfWork unitOfWork
            , IMapper mapper) 
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _userManager = userManager;
            _mapper = mapper;

            _nlogger = NLog.LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Получить список документов по странично
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("GetList")]
        public async Task<JsonResponse> GetList([FromBody] PagedSortRequestDto<ComplienceDocFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var q = _unitOfWork.ComplienceDocumentRepository
                    .GetAsIQueryable().AsNoTracking();
                if (!request.Filter.ShowDeleted)
                {
                    q = q.Where(v => v.Deleted == null);
                }
                if (!string.IsNullOrEmpty(request.Filter.TypeCode))
                {
                    q = q.Where(v => v.DocType.Code == request.Filter.TypeCode);
                } 
                if (!string.IsNullOrEmpty(request.Filter.StatusCode))
                {
                    q = q.Where(v => v.Status.Code == request.Filter.StatusCode);
                }

                var currentUser = await _userManager.GetUserAsync(User);
                if (currentUser != null)
                {
                    if (request.Filter.TypeCode == DictionaryCodes.DocumentTypeCodes.Otts
                        && User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.OTTS.Approval))
                    {
                        q = q.Where(v => v.Status.Code == DictionaryCodes.StatusCodes.Сonsideration
                        || v.Status.Code == DictionaryCodes.StatusCodes.Active);
                    }
                    else if (request.Filter.TypeCode == DictionaryCodes.DocumentTypeCodes.Otch
                        && User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.OTCH.Approval))
                    {
                        q = q.Where(v => v.Status.Code == DictionaryCodes.StatusCodes.Сonsideration
                        || v.Status.Code == DictionaryCodes.StatusCodes.Active);
                    }
                    else if (request.Filter.TypeCode == DictionaryCodes.DocumentTypeCodes.Sbkts
                        && User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.SBKTS.Approval))
                    {
                        q = q.Where(v => v.Status.Code == DictionaryCodes.StatusCodes.Сonsideration
                        || v.Status.Code == DictionaryCodes.StatusCodes.Active);
                    }
                    else
                    {
                        q = q.Where(d => d.ApplicantId == currentUser.OrganizationId
//                            || d.AssemblyKitSupplierId == currentUser.OrganizationId
//                            || d.AssemblyPlantId == currentUser.OrganizationId
                            || d.ManufacturerId == currentUser.OrganizationId
//                            || d.RepresentativeManufacturerId == currentUser.OrganizationId
                            || d.AuthorOrgId == currentUser.OrganizationId);
                    }
                } 
                else
                {
                    q = q.Where(d => false);
                }


                var total = await q.CountAsync();
                var entities = await q.Include(v => v.DocType)
                    .Include(d => d.Status)
                    .Include(d => d.Manufacturer)
                    .Include(d => d.Applicant)
                    .Include(d => d.VehicleTypeDetail)
                        .ThenInclude(d => d.CommercialNames)
                    .Include(d => d.VehicleTypeDetail)
                        .ThenInclude(d => d.Types)
                    .Include(d => d.VehicleTypeDetail)
                        .ThenInclude(d => d.Makes)
                        .ThenInclude(d => d.Make)
                    .Include(d => d.VehicleTypeDetail)
                        .ThenInclude(d => d.TechCategories)
                        .ThenInclude(d => d.Category)
                    .OrderByDescending(d => d.CreateDate)
                    .Skip(request.Page * request.PageSize)
                    .Take(request.PageSize)
                    .ToListAsync();
                response.Data = new
                {
                    Data = _mapper.Map<ICollection<ComplienceDocumenReestrtDto>>(entities),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetList");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Получить документ по ИД
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("Get")]
        public async Task<JsonResponse> Get(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository
                                        .GetAsIQueryable(v => v.Deleted == null && v.Id == id).AsNoTracking()
                                        .Include(v => v.DocType)
                                        .Include(v => v.Status)
                                        .Include(v => v.Authority)
                                        .Include(v => v.BlankNumbers)
                                        .Include(v => v.VinNumbers)                                        
                                        .Include(v => v.VehicleLabelingDetail)
                                        /*    .ThenInclude(vl => vl.VinCharacterDetails)
                                            .ThenInclude(vc => vc.VinCharacterDescriptions)
                                        .Include(v => v.VehicleLabelingDetail)
                                            .ThenInclude(vl => vl.VinCharacterDetails)
                                            .ThenInclude(vc => vc.DataType)
                                        */
                                        .Include(o => o.RepresentativeManufacturers)
                                            .ThenInclude(ot => ot.Organization)
                                        .Include(o => o.AssemblyPlants)
                                            .ThenInclude(ot => ot.Organization)
                                        .Include(o => o.AssemblyKitSuppliers)
                                            .ThenInclude(ot => ot.Organization)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.Makes)
                                            .ThenInclude(m => m.Make)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.CommercialNames)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.Types)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.TechCategories)
                                                .ThenInclude(tc => tc.Category)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.ClassCategories)
                                                .ThenInclude(cc => cc.ClassCategory)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.EcoClasses)
                                            .ThenInclude(ec => ec.EcoClass)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.Modifications)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.ChassisDesigns)
                                                .ThenInclude(vt => vt.ChassisDesign)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.BaseVehicleTypeDetails)
                                                .ThenInclude(b => b.Make)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.CharacteristicsDetail)
                                            .ThenInclude(cd => cd.EngineType)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.CharacteristicsDetail)
                                            .ThenInclude(cd => cd.VehicleHybridDesigns)
                                        .SingleOrDefaultAsync();
                _logger.LogDebug("End DateTime = " + DateTime.Now);

                response.Data = _mapper.Map<ComplienceDocumentDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Получить данные для подписи
        /// </summary>
        /// <param name="id">Ид документа</param>
        /// <returns></returns>
        [HttpGet("getDataToSign")]
        public async Task<JsonResponse> GetDataToSign(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("GetDataToSign Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository
                                        .GetAsIQueryable(v => v.Deleted == null && v.Id == id).AsNoTracking()
                                        .Include(v => v.DocType)
                                        .Include(v => v.Status)
                                        .Include(v => v.Authority)
                                        .Include(o => o.RepresentativeManufacturers)
                                            .ThenInclude(ot => ot.Organization)
                                        .Include(o => o.AssemblyPlants)
                                            .ThenInclude(ot => ot.Organization)
                                        .Include(o => o.AssemblyKitSuppliers)
                                            .ThenInclude(ot => ot.Organization)
                                        .Include(v => v.BlankNumbers)
                                        .Include(v => v.VinNumbers)
                                        .Include(v => v.VehicleLabelingDetail)
                                        .ThenInclude(vl => vl.VinCharacterDetails)
                                            .ThenInclude(vc => vc.VinCharacterDescriptions)
                                        .Include(v => v.VehicleLabelingDetail)
                                            .ThenInclude(vl => vl.VinCharacterDetails)
                                            .ThenInclude(vc => vc.DataType)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.Makes)
                                            .ThenInclude(m => m.Make)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.CommercialNames)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.Types)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.TechCategories)
                                                .ThenInclude(tc => tc.Category)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.ClassCategories)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.EcoClasses)
                                            .ThenInclude(ec => ec.EcoClass)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.Modifications)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.ChassisDesigns)
                                                .ThenInclude(vt => vt.ChassisDesign)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.BaseVehicleTypeDetails)
                                                .ThenInclude(b => b.Make)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.CharacteristicsDetail)
                                            .ThenInclude(cd => cd.EngineType)
                                        .Include(v => v.VehicleTypeDetail)
                                            .ThenInclude(vt => vt.CharacteristicsDetail)
                                            .ThenInclude(cd => cd.VehicleHybridDesigns)
                                        .SingleOrDefaultAsync();
                _logger.LogDebug("GetDataToSign End DateTime = " + DateTime.Now);

                var complDocDto = _mapper.Map<ComplienceDocumentDto>(entity);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(ComplienceDocumentDto));

                using (var textWriter = new Utf8StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, complDocDto);
                    response.Data = new { dataToSign = textWriter.ToString() };
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetDataToSign");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Существует ли Номер документа в БД
        /// </summary>
        /// <param name="docNumber"></param>
        /// <param name="docId"></param>
        /// <returns></returns>
        [HttpGet("IsDocExist")]
        public async Task<JsonResponse> IsDocExist(string docNumber, Guid? docId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var q = _unitOfWork.ComplienceDocumentRepository.GetAsIQueryable(v => v.Deleted == null && v.DocNumber == docNumber);

                if (docId != null)
                {
                    q = q.Where(v => v.Id != docId.Value);
                }

                var isEntityExist = await q.AnyAsync();
                response.Data = isEntityExist;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "IsDocExist");
                response.Text = e.Message;
                response.Status = "Error";
                response.Data = false;
            }

            return response;
        }
        
        /// <summary>
        /// Сохранить документ
        /// </summary>
        /// <param name="compDocDto"></param>
        /// <returns></returns>
        [HttpPost("Save")]
        public async Task<JsonResponse> Save([FromBody] ComplienceDocumentDto compDocDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var currentUser = await _userManager.GetUserAsync(User);
                if (compDocDto.Id == null || compDocDto.Id == Guid.Empty)
                {   
                    var docType = _unitOfWork.DicDocTypeRepository
                        .GetAsIQueryable(d => d.Code == compDocDto.DocTypeCode)
                        .FirstOrDefault();

                    var status = _unitOfWork.DicStatusesRepository
                        .GetAsIQueryable(d => d.Code == "new")
                        .FirstOrDefault();
                    if (docType == null)
                    {
                        response.Text = "There is no such document type";
                        response.Status = "Error";
                    }
                    else
                    {
                        compDocDto.DocTypeId = docType.Id;
                        compDocDto.StatusId = status.Id;
                        compDocDto.AuthorOrgId = currentUser.OrganizationId;

                        var compDoc = _mapper.Map<ComplienceDocumentDto, ComplienceDocument>(compDocDto);

                        compDoc._CurrenUserId = currentUser.Id.ToString();
                        compDoc._CurrenUserName = currentUser.UserName;
                        compDoc._IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString();

                        await _unitOfWork.ComplienceDocumentRepository.AddOrUpdateAsync(compDoc);
                        compDocDto.Id = compDoc.Id;
                        response.Data = compDocDto;
                    }

                    if (compDocDto.Id.HasValue)
                    {
                        var userName = User.Identity.Name;
                        await _unitOfWork.DocEventRepository.InsertAsync(new DocumentEvent()
                        {
                            Title = $"Документ {compDocDto.DocNumber} от {compDocDto.DocDate.Value.ToString("dd.MM.yyyy")}",
                            AuthorId = currentUser.Id.ToString(),
                            AuthorName = currentUser.UserName,
                            DocumentId = compDocDto.Id.Value,
                            EventType = EventTypeEnum.New,
                            DocumentType = compDocDto.DocTypeCode,
                            EventDateTime = DateTime.Now,
                            Description = "Документ создан"
                        });
                        await _unitOfWork.SaveAsync();
                    }
                }
                else
                {
                    var compDoc = _mapper.Map<ComplienceDocumentDto, ComplienceDocument>(compDocDto);
                    compDoc.ModifyDate = DateTime.Now;

                    compDoc._CurrenUserId = currentUser.Id.ToString();
                    compDoc._CurrenUserName = currentUser.UserName;
                    compDoc._IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString();

                    await _unitOfWork.ComplienceDocumentRepository.AddOrUpdateAsync(compDoc);
                    compDocDto.Id = compDoc.Id;

                    response.Data = compDocDto;
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Save {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "Save error");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Удаление документа
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("Delete")]
        public async Task<JsonResponse> Delete(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _unitOfWork.ComplienceDocumentRepository.Delete(id);
                await _unitOfWork.SaveAsync();
                response.Data = true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Delete");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Восстановить документа
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("restore")]
        public async Task<JsonResponse> Restore(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _unitOfWork.ComplienceDocumentRepository.Restore(id);
                await _unitOfWork.SaveAsync();
                response.Data = true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Restore");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Обработка статусов
        /// </summary>
        /// <returns></returns>
        [HttpPost("ProcessRequest")]
        public async Task<JsonResponse> ProcessRequest([FromBody] ProcessRequestDto request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                if (request.StatusCode == DictionaryCodes.StatusCodes.Сonsideration)
                {
                    request.Message = "Отправлен на согласование";
                    response.Data = await ProcessStatusRequest(request, "Документ отправлен на согласование");
                } 
                else if (request.StatusCode == DictionaryCodes.StatusCodes.Rejected)
                {
                    response.Data = await ProcessStatusRequest(request, "Документ отклонен");
                }
                else if (request.StatusCode == DictionaryCodes.StatusCodes.Active)
                {
                    response.Data = await ProcessStatusRequest(request, "Документ одобрен");
                }
            }
            catch (Exception e)
            {
                response.Status = "Error";
                response.Text = e.Message;
            }

            return response;
        }
        private async Task<DictionaryDto> ProcessStatusRequest(ProcessRequestDto request, string title)
        {
            var status = await _unitOfWork.DicStatusesRepository
                       .GetAsIQueryable(d => d.Code == request.StatusCode)
                       .FirstOrDefaultAsync();

            var entity = await _unitOfWork.ComplienceDocumentRepository
                                    .GetAsIQueryable(v => v.Deleted == null && v.Id == request.DocId)
                                    .Include(v => v.DocType)
                                    .SingleOrDefaultAsync();
            entity.StatusId = status.Id;
            var currentUser = await _userManager.GetUserAsync(User);
            await _unitOfWork.DocEventRepository.InsertAsync(new DocumentEvent()
            {
                Title = title,
                AuthorId = currentUser.Id.ToString(),
                AuthorName = currentUser.UserName,
                DocumentId = entity.Id,
                EventType = (EventTypeEnum)Enum.Parse(typeof(EventTypeEnum), request.StatusCode, true),
                DocumentType = entity.DocType.Code,
                EventDateTime = DateTime.Now,
                Description = request.Message
            });
            await _unitOfWork.SaveAsync();

            return _mapper.Map<DictionaryDto>(status);
        }


        /// <summary>
        /// Получить все события документа
        /// </summary>
        /// <param name="docId">Ид документа</param>
        /// <returns></returns>
        [HttpGet("GetDocumentEvents")]
        public async Task<JsonResponse> GetDocumentEvents(Guid docId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var docEvents = await _unitOfWork.DocEventRepository.GetAsIQueryable(d => d.DocumentId == docId)
                    .OrderBy(d => d.EventDateTime)
                    .ToListAsync();

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<ICollection<DocumentEventDto>>(docEvents);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Организации
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetOrganizationDetailsByDocId")]
        public async Task<JsonResponse> GetOrganizationDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var q = _unitOfWork.ComplienceDocumentRepository.GetAsIQueryable().AsNoTracking()
                        .Include(vr => vr.DocType)
                        .Where(ch => ch.Id == id);
                var complienceDocument = await q.Include(o => o.AssemblyPlants).ThenInclude(d => d.Organization).Include(o => o.AssemblyKitSuppliers).ThenInclude(d => d.Organization).SingleOrDefaultAsync(c => c.Id == id);

                var queryOrg = _unitOfWork.OrganizationsRepository.GetAsIQueryable().AsNoTracking()
                    .Include(o => o.OrganizationTypes)
                        .ThenInclude(t => t.OrgType)
                    .Include(o => o.CommunicationInfos)
                        .ThenInclude(ci => ci.CommunicationType);

                if (complienceDocument.AuthorityId != null)                
                    complienceDocument.Authority = await queryOrg.SingleOrDefaultAsync(o => o.Id == complienceDocument.AuthorityId);

                if (complienceDocument.ApplicantId != null)
                    complienceDocument.Applicant = await queryOrg.SingleOrDefaultAsync(o => o.Id == complienceDocument.ApplicantId);

                if (complienceDocument.ManufacturerId != null)
                    complienceDocument.Manufacturer = await queryOrg.SingleOrDefaultAsync(o => o.Id == complienceDocument.ManufacturerId);
/*

                if (complienceDocument.RepresentativeManufacturerId != null)
                    complienceDocument.RepresentativeManufacturer = await queryOrg.SingleOrDefaultAsync(o => o.Id == complienceDocument.RepresentativeManufacturerId);
                if (complienceDocument.AssemblyPlantId != null)
                    complienceDocument.AssemblyPlant = await queryOrg.SingleOrDefaultAsync(o => o.Id == complienceDocument.AssemblyPlantId);

                if (complienceDocument.AssemblyKitSupplierId != null)
                    complienceDocument.AssemblyKitSupplier = await queryOrg.SingleOrDefaultAsync(o => o.Id == complienceDocument.AssemblyKitSupplierId);
*/
                _logger.LogDebug("End DateTime = " + DateTime.Now);

                response.Data = _mapper.Map<ComplienceDocumentOrgDto>(complienceDocument);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Сохранить Номер бланка
        /// </summary>
        /// <param name="blankNumberDto"></param>
        /// <returns></returns>
        [HttpPost("saveBlankNumber")]
        public async Task<JsonResponse> SaveBlankNumber([FromBody] BlankNumberDto blankNumberDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var blankNumber = _mapper.Map<BlankNumberDto, BlankNumber>(blankNumberDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(blankNumber);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveBlankNumber {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveBlankNumber error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Vin Number маленькая партия
        /// </summary>
        /// <param name="vinNumberDto"></param>
        /// <returns></returns>
        [HttpPost("saveVinNumber")]
        public async Task<JsonResponse> SaveVinNumber([FromBody] VinNumberDto vinNumberDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VinNumberDto, VinNumber>(vinNumberDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVinNumber {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVinNumber error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Описание маркировки 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetLabelingDetailsByDocId")]
        public async Task<JsonResponse> GetLabelingDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetAsIQueryable().AsNoTracking()
                        .Include(v => v.VinNumbers)
                        .Include(v => v.VehicleLabelingDetail)
                            .ThenInclude(vl => vl.VinCharacterDetails)
                            .ThenInclude(vc => vc.VinCharacterDescriptions)
                        .Include(v => v.VehicleLabelingDetail)
                            .ThenInclude(vl => vl.VinCharacterDetails)
                            .ThenInclude(vc => vc.DataType)

                        .Include(v => v.VehicleLabelingDetail)
                            .ThenInclude(vl => vl.VehicleIdentificationNumberLocations)
                        .Include(v => v.VehicleLabelingDetail)
                            .ThenInclude(vl => vl.VehicleLabelLocations)                     
                        .SingleOrDefaultAsync(c => c.Id == id);
                _logger.LogDebug("End DateTime = " + DateTime.Now);

                response.Data = _mapper.Map<ComplienceDocLabelingDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Описание маркировки 
        /// </summary>
        /// <param name="vehicleLabelingDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleLabelingDetail")]
        public async Task<JsonResponse> SaveVehicleLabelingDetail([FromBody] VehicleLabelingDetailDto vehicleLabelingDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleLabelingDetailDto, VehicleLabelingDetail>(vehicleLabelingDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleLabelingDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleLabelingDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить VIN Location
        /// </summary>
        /// <param name="vehicleLabelLocationDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleIdentificationNumberLocation")]
        public async Task<JsonResponse> SaveVehicleIdentificationNumberLocation([FromBody] VehicleLabelLocationDto vehicleLabelLocationDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleLabelLocationDto, VehicleIdentificationNumberLocation>(vehicleLabelLocationDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleIdentificationNumberLocation {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleIdentificationNumberLocation error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Label Location
        /// </summary>
        /// <param name="vehicleLabelLocationDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleLabelLocation")]
        public async Task<JsonResponse> SaveVehicleLabelLocation([FromBody] VehicleLabelLocationDto vehicleLabelLocationDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleLabelLocationDto, VehicleLabelLocation>(vehicleLabelLocationDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVinNumber {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVinNumber error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Label Location
        /// </summary>
        /// <param name="vehicleVinCharacterDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleVinCharacterDetail")]
        public async Task<JsonResponse> SaveVehicleVinCharacterDetail([FromBody] VehicleVinCharacterDetailDto vehicleVinCharacterDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleVinCharacterDetailDto, VehicleVinCharacterDetail>(vehicleVinCharacterDetailDto);
                var savedEntity = await _unitOfWork.ComplienceDocumentRepository.UpdateVehicleVinCharacterDetailAsync(entity);
                response.Data = new { entity = savedEntity };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleVinCharacterDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleVinCharacterDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }



        /// <summary>
        /// Колея передних/задних колес
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleAxleDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleAxleDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                         .Include(cd => cd.VehicleAxleDetails)
                            .ThenInclude(vr => vr.AxleSweptPathMeasureUnit)
                         .Include(cd => cd.VehicleAxleDetails)
                            .ThenInclude(vr => vr.TechnicallyPermissibleMaxWeightOnAxleUnit)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Колея передних/задних колес
        /// </summary>
        /// <param name="vehicleAxleDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleAxleDetail")]
        public async Task<JsonResponse> SaveVehicleAxleDetail([FromBody] VehicleAxleDetailDto vehicleAxleDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleAxleDetailDto, VehicleAxleDetail>(vehicleAxleDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleSteeringDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleSteeringDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Рулевое управление
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleSteeringDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleSteeringDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(p => p.VehicleSteeringDescriptions)
                        .Include(p => p.VehicleSteeringDetails)
                            .ThenInclude(ch => ch.SteeringWheelPosition)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Рулевое управление описание
        /// </summary>
        /// <param name="vehicleSteeringDescriptionDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleSteeringDescription")]
        public async Task<JsonResponse> SaveVehicleSteeringDescription([FromBody] VehicleSteeringDescriptionDto vehicleSteeringDescriptionDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleSteeringDescriptionDto, VehicleSteeringDescription>(vehicleSteeringDescriptionDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleSteeringDescription {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleSteeringDescription error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        /// <summary>
        /// Сохранить Рулевое управление информация о рулевом управлении
        /// </summary>
        /// <param name="vehicleSteeringDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleSteeringDetail")]
        public async Task<JsonResponse> SaveVehicleSteeringDetail([FromBody] VehicleSteeringDetailDto vehicleSteeringDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleSteeringDetailDto, VehicleSteeringDetail>(vehicleSteeringDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleSteeringDescription {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleSteeringDescription error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        ///  Тормозные системы
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleBrakingSystemDetailsByDocId")]
        public async Task<JsonResponse> GetVehicleBrakingSystemDetailsByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(p => p.VehicleBrakingSystemDetails)
                            .ThenInclude(tv => tv.VehicleBrakingSystemKind)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Тормозные системы
        /// </summary>
        /// <param name="vehicleBrakingSystemDetailDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleBrakingSystemDetail")]
        public async Task<JsonResponse> SaveVehicleBrakingSystemDetail([FromBody] VehicleBrakingSystemDetailDto vehicleBrakingSystemDetailDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleBrakingSystemDetailDto, VehicleBrakingSystemDetail>(vehicleBrakingSystemDetailDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleBrakingSystemDetail {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleBrakingSystemDetail error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Описание оборудования транспортного средства
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetVehicleEquipmentInfosByDocId")] 
        public async Task<JsonResponse> GetVehicleEquipmentInfosByDocId(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _logger.LogDebug("Start DateTime = " + DateTime.Now);
                var entity = await _unitOfWork.ComplienceDocumentRepository.GetCharacteristicsDetails().AsNoTracking()
                        .Include(p => p.VehicleEquipmentInfos)
                        .SingleOrDefaultAsync(ch => ch.VehicleTypeDetail.DocumentId == id);

                _logger.LogDebug("End DateTime = " + DateTime.Now);
                response.Data = _mapper.Map<CharacteristicsDetailDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        /// <summary>
        /// Сохранить Описание оборудования транспортного средства
        /// </summary>
        /// <param name="vehicleEquipmentInfoDto"></param>
        /// <returns></returns>
        [HttpPost("saveVehicleEquipmentInfo")]
        public async Task<JsonResponse> SaveVehicleEquipmentInfo([FromBody] VehicleEquipmentInfoDto vehicleEquipmentInfoDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = _mapper.Map<VehicleEquipmentInfoDto, VehicleEquipmentInfo>(vehicleEquipmentInfoDto);
                var entityId = await _unitOfWork.ComplienceDocumentRepository.UpdateEntityStateAsync(entity);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveVehicleEquipmentInfo {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveVehicleEquipmentInfo error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        /// <summary>
        /// Загрузка изображения
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("UploadGeneralView")]
        public async Task<JsonResponse> UploadGeneralView([FromForm] UploadFileModel request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var entity = new VehicleGeneralView();
                entity.CreateDate = DateTime.Now;

                if (request.File != null && request.File.Length > 0)
                {
                    entity.DocumentId = request.DocumentId.Value;
                    entity.FileName = request.FileName;
                    entity.OrginalFileName = request.File.FileName;

                    using (var ms = new MemoryStream())
                    {
                        await request.File.CopyToAsync(ms);
                        entity.VehiclePicture = ms.ToArray();
                    }
                    _unitOfWork.VehicleGeneralViewRepository.Insert(entity);
                    await _unitOfWork.SaveAsync();
                }
            } 
            catch(Exception e)
            {
                response.Status = "Error";
                response.Text = e.Message;
            }
            
            return response;
        }

        /// <summary>
        /// Удалить изображение
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("deleteGeneralView")]
        public async Task<JsonResponse> DeleteGeneralView(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _unitOfWork.VehicleGeneralViewRepository.Delete(id);
                await _unitOfWork.SaveAsync();
                response.Data = true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Delete");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Сохранить Название изображения
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [HttpPost("saveImgName")]
        public async Task<JsonResponse> SaveImgName(Guid id, string fileName)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var generalView =  _unitOfWork.VehicleGeneralViewRepository
                    .GetAsIQueryable(v => v.Id == id)
                    .SingleOrDefault();
                generalView.FileName = fileName;
                await _unitOfWork.SaveAsync();
                response.Data = true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "SaveImgName");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Получить Общий вид транспортного средства
        /// </summary>
        /// <param name="docId"></param>
        /// <returns></returns>
        [HttpGet("GetGeneralViewList")]
        public async Task<JsonResponse> GetGeneralViewList(Guid docId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var generalViews = await _unitOfWork.VehicleGeneralViewRepository.GetAsIQueryable(d => d.DocumentId == docId)
                .ToListAsync();
                response.Data = _mapper.Map<ICollection<GeneralViewDto>>(generalViews);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
    }
}

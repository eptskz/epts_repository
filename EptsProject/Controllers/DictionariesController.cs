﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.DTOs.Dictionary;
using DBCore.Models;
using DBCore.Models.NSI;
using EptsBL.Extensions;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers
{
    /// <summary>
    /// Справочники
    /// </summary>
    /// 
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DictionariesController : MainController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        public DictionariesController(ILogger<DictionariesController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , IMapper mapper) : base(logger, unitOfWork, userManager, mapper) { }

        
        [HttpGet("GetNsiValues")]
        public async Task<JsonResponse> GetNsiValue([FromQuery] string nsiCode)
        {
            var response = new JsonResponse {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            if (string.IsNullOrEmpty(nsiCode))
            {
                response.Text = "NsiCode параметр не может быть пустым";
                response.Status = "Error";
            }
            else
            {
                try
                {
                    var nsiValues = await _UnitOfWork.NsiValuesRepository
                        .GetAsIQueryable(v => v.Nsi.Code == nsiCode && v.Deleted == null)
                        .OrderBy(v => v.NameRu)
                        .ToListAsync();
                    response.Data = _mapper.Map<ICollection<DictionaryDto>>(nsiValues);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "GetNsiValue");
                    response.Text = e.Message;
                    response.Status = "Error";
                }
            }
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nsiCode"></param>
        /// <param name="valueCodes"></param>
        /// <returns></returns>
        [HttpPost("GetNsiValuesFiltered")]
        public async Task<JsonResponse> GetNsiValueFiltered([FromQuery] string nsiCode, [FromBody] List<string> valueCodes)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            if (string.IsNullOrEmpty(nsiCode))
            {
                response.Text = "NsiCode параметр не может быть пустым";
                response.Status = "Error";
            }
            else
            {
                try
                {
                    var nsiValues = await _UnitOfWork.NsiValuesRepository
                        .GetAsIQueryable(v => v.Nsi.Code == nsiCode 
                            && v.Deleted == null
                            && valueCodes.Contains(v.Code))
                        .OrderBy(v => v.NameRu)
                        .ToListAsync();
                    response.Data = _mapper.Map<ICollection<DictionaryDto>>(nsiValues);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "GetNsiValue");
                    response.Text = e.Message;
                    response.Status = "Error";
                }
            }
            return response;
        }

        [HttpPost("GetNsiValues")]
        public async Task<JsonResponse> GetNsiValues([FromBody] List<string> nsiIdList)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var nsiValues = await _UnitOfWork.NsiValuesRepository
                    .GetAsIQueryable(v => nsiIdList.Contains(v.Nsi.Code) && v.Deleted == null).Include(v => v.Nsi)
                    .ToListAsync();
                response.Data = _mapper.Map<ICollection<DictionaryDto>>(nsiValues);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNsiValues");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        [HttpGet("GetKatos")]
        public async Task<JsonResponse> GetKatos([FromQuery] int parentId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var katoList = await _UnitOfWork.DicKatosRepository
                    .GetAsIQueryable(k => k.ParentId == parentId && k.Deleted == null)
                    .ToListAsync();
                response.Data = _mapper.Map<ICollection<DictionaryDto>>(katoList);
            } 
            catch(Exception e)
            {
                response.Status = "ERROR";
                response.Text = e.Message;
            }

            return response;
        }

        [HttpGet("GetKatosByParentCode")]
        public async Task<JsonResponse> GetKatosByParentCode([FromQuery] string parentCode)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var katoList = await _UnitOfWork.DicKatosRepository
                    .GetAsIQueryable(k => k.Parent.Code == parentCode && k.Deleted == null)
                    .ToListAsync();
                response.Data = _mapper.Map<ICollection<DictionaryDto>>(katoList);
            }
            catch (Exception e)
            {
                response.Status = "ERROR";
                response.Text = e.Message;
            }

            return response;
        }


        [HttpGet("GetKatoByCode")]
        public async Task<JsonResponse> GetKatoByCode([FromQuery] string code)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var kato = await _UnitOfWork.DicKatosRepository
                    .GetAsIQueryable(k => k.Code == code && k.Deleted == null)
                    .FirstOrDefaultAsync();
                response.Data = _mapper.Map<DictionaryDto>(kato);
            }
            catch (Exception e)
            {
                response.Status = "ERROR";
                response.Text = e.Message;
            }

            return response;
        }


        [AllowAnonymous]
        [HttpGet("GetOrgTypes")]
        public async Task<JsonResponse> GetOrgTypes()
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var orgTypeList = await _UnitOfWork.DicOrganizationTypesRepository
                .GetAsIQueryable(k => k.Deleted == null)
                .ToListAsync();
                response.Data = _mapper.Map<ICollection<DictionaryDto>>(orgTypeList);
            }
            catch (Exception e)
            {
                response.Status = "ERROR";
                response.Text = e.Message;
            }

            return response;
        }

        [AllowAnonymous]
        [HttpGet("GetStatuses")]
        public async Task<JsonResponse> GetStatuses(string group)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var sq = _UnitOfWork.DicStatusesRepository.GetAsIQueryable(k => k.Deleted == null);
                if (!string.IsNullOrEmpty(group))
                {
                    sq = sq.Where(k => k.Group == group);
                }

                var statuses = await sq.ToListAsync();
                response.Data = _mapper.Map<ICollection<DictionaryDto>>(statuses);
            }
            catch (Exception e)
            {
                response.Status = "ERROR";
                response.Text = e.Message;
            }

            return response;
        }

        [AllowAnonymous]
        [HttpGet("GetDicDocTypes")]
        public async Task<JsonResponse> GetDicDocTypes()
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var sq = _UnitOfWork.DicDocTypeRepository.GetAsIQueryable(k => k.Deleted == null);

                var data = await sq.ToListAsync();
                response.Data = _mapper.Map<ICollection<DictionaryDto>>(data);
            }
            catch (Exception e)
            {
                response.Status = "ERROR";
                response.Text = e.Message;
            }

            return response;
        }




        /// <summary>
        /// Получить список справочников НСИ
        /// </summary>
        /// <returns></returns>
        [HttpGet("nsis")]
        public async Task<JsonResponse> GetNsis([FromQuery] PagedSortRequestDto<DictionaryFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var q = _UnitOfWork.NsisRepository
                    .GetAsIQueryable(v => v.Deleted == null);                    

                if (request.Filter != null  && !string.IsNullOrEmpty(request.Filter.Search))
                {
                    q = q.Where(v => v.NameRu.ToLower().StartsWith(request.Filter.Search.ToLower())
                                     || v.NameRu.ToLower().Contains(request.Filter.Search.ToLower())
                                     
                                     || v.NameKz.ToLower().StartsWith(request.Filter.Search.ToLower())
                                     || v.NameKz.ToLower().Contains(request.Filter.Search.ToLower())

                                     || v.NameEn.ToLower().StartsWith(request.Filter.Search.ToLower())
                                     || v.NameEn.ToLower().Contains(request.Filter.Search.ToLower())

                                     || v.Code.ToLower().StartsWith(request.Filter.Search.ToLower())
                                     || v.Code.ToLower().Contains(request.Filter.Search.ToLower())
                                );
                }

                if (request.SortField != null && request.SortField.Length > 0)
                    q = q.OrderByCommonFields(request.SortField, request.Direction);
                else                
                    q = q.OrderByDescending(v => v.Created).ThenBy(v => v.Code);
                                
                var total = await q.CountAsync();
                var pageSize = request.PageSize == 0 ? total : request.PageSize;

                var sqlString = q.Skip(request.Page * pageSize)
                    .Take(pageSize).ToSql();

                var nsis = await q.Skip(request.Page * pageSize)
                    .Take(pageSize)
                    .ToListAsync();

                response.Data = new
                {
                    Data = _mapper.Map<ICollection<NsiDto>>(nsis),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNsis");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("nsi/{id}")]
        public async Task<JsonResponse> GetNsiById([FromRoute] int id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var nsi = _UnitOfWork.NsisRepository.GetAsIQueryable(v => v.Id == id).FirstOrDefault();
                response.Data = _mapper.Map<NsiDto>(nsi);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNsis");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }


        /// <summary>
        /// Создать nsi справочник
        /// </summary>
        /// <param name="nsi"></param>
        /// <returns></returns>
        [HttpPost("nsi")]
        public async Task<JsonResponse> addNsi(NsiDto nsi)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var nsiEntity = _mapper.Map<Nsi>(nsi);
                nsiEntity.Created = DateTime.Now;
                await _UnitOfWork.NsisRepository.InsertAsync(nsiEntity);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "addNsi");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        
        /// <summary>
        /// Обновить nsi справочник
        /// </summary>
        /// <param name="nsi"></param>
        /// <returns></returns>
        [HttpPut("nsi")]
        public async Task<JsonResponse> updateNsi(NsiDto nsi)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var nsiEntity = _mapper.Map<Nsi>(nsi);
                nsiEntity.Modified = DateTime.Now;
                _UnitOfWork.NsisRepository.Update(nsiEntity);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "updateNsi");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("nsi")]
        public async Task<JsonResponse> deleteNsi(int id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var nsi = _UnitOfWork.NsisRepository.GetByID(id);
                nsi.Deleted = DateTime.Now;
                _UnitOfWork.NsisRepository.Update(nsi);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "deleteNsi");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }


        /// <summary>
        /// Получить список значений по НСИ коду
        /// </summary>
        /// <param name="nsiCode"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("nsivalues/{nsicode}")]
        public async Task<JsonResponse> GetNsiValuesByNsiCode([FromRoute] string nsiCode
            , [FromQuery] PagedSortRequestDto<DictionaryFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            if (string.IsNullOrEmpty(nsiCode))
            {
                response.Text = "NsiCode параметр не может быть пустым";
                response.Status = "Error";
            }
            else
            {
                try
                {
                    var q = _UnitOfWork.NsiValuesRepository
                        .GetAsIQueryable(v => v.Nsi.Code == nsiCode && v.Deleted == null);

                    if (request.Filter != null && !string.IsNullOrEmpty(request.Filter.Search))
                    {
                        q = q.Where(v => v.NameRu.ToLower().StartsWith(request.Filter.Search.ToLower())
                                         || v.NameRu.ToLower().Contains(request.Filter.Search.ToLower())

                                         || v.NameKz.ToLower().StartsWith(request.Filter.Search.ToLower())
                                         || v.NameKz.ToLower().Contains(request.Filter.Search.ToLower())

                                         || v.NameEn.ToLower().StartsWith(request.Filter.Search.ToLower())
                                         || v.NameEn.ToLower().Contains(request.Filter.Search.ToLower())

                                         || v.Code.ToLower().StartsWith(request.Filter.Search.ToLower())
                                         || v.Code.ToLower().Contains(request.Filter.Search.ToLower())
                                    );
                    }

                    if (request.SortField != null && request.SortField.Length > 0)                    
                        q = q.OrderByCommonFields(request.SortField, request.Direction);                    
                    else
                        q = q.OrderByDescending(v => v.Created).ThenBy(v => v.Code);
                    

                    var total = await q.CountAsync();
                    var pageSize = request.PageSize == 0 ? total : request.PageSize;
                    var nsiValues = await q.Skip(request.Page * pageSize)
                                        .Take(pageSize)
                                        .ToListAsync();
                    response.Data = new {
                        Data = _mapper.Map<ICollection<NsiValueDto>>(nsiValues),
                        Total = total
                    };
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "GetNsiValue");
                    response.Text = e.Message;
                    response.Status = "Error";
                }
            }
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("nsivalue/{id}")]
        public async Task<JsonResponse> GetNsiValueById([FromRoute] int id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var nsi = _UnitOfWork.NsiValuesRepository.GetAsIQueryable(v => v.Id == id).FirstOrDefault();
                response.Data = _mapper.Map<NsiValueDto>(nsi);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNsis");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Создать значение nsi справочника
        /// </summary>
        /// <param name="nsiValue"></param>
        /// <returns></returns>
        [HttpPost("nsivalue")]
        public async Task<JsonResponse> addNsiValue(NsiValueDto nsiValue)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var nsiValueEntity = _mapper.Map<NsiValue>(nsiValue);
                nsiValueEntity.Created = DateTime.Now;
                await _UnitOfWork.NsiValuesRepository.InsertAsync(nsiValueEntity);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "addNsiValue");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Обновить значение nsi справочника
        /// </summary>
        /// <param name="nsiValue"></param>
        /// <returns></returns>
        [HttpPut("nsivalue")]
        public async Task<JsonResponse> updateNsiValue(NsiValueDto nsiValue)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var nsiValueEntity = _mapper.Map<NsiValue>(nsiValue);
                nsiValueEntity.Modified = DateTime.Now;
                _UnitOfWork.NsiValuesRepository.Update(nsiValueEntity);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "updateNsiValue");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("nsivalue")]
        public async Task<JsonResponse> deleteNsiValue(int id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var nsiValue = _UnitOfWork.NsiValuesRepository.GetByID(id);
                nsiValue.Deleted = DateTime.Now;
                _UnitOfWork.NsiValuesRepository.Update(nsiValue);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "deleteNsiValue");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

    }
}

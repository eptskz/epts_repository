﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.DTOs.Public;
using DBCore.DTOs.Reports;
using DBCore.Models;
using DBCore.Models.Dictionaries;
using DBCore.Models.Reports;
using EptsBL.Extensions;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CdReportController : MainController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        public CdReportController(ILogger<CdReportController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , IMapper mapper) : base(logger, unitOfWork, userManager, mapper) { }


        /// <summary>
        /// Получить информацию для отчета 
        /// </summary>
        /// <returns></returns>
        [HttpPost("data")]
        public async Task<JsonResponse> GetReportData(PagedSortRequestDto<CdReportRequestDto> request)
        {
            var response = new JsonResponse { Text = "", Action = Constants.ACTIONS.OK, Status = "Ok" };
            try
            {
                var q = createQuery(request);

                var total = await q.CountAsync();
                var pageSize = request.PageSize == 0 ? total : request.PageSize;
                var data = await q
                    .Skip(request.Page * pageSize)
                    .Take(pageSize)
                    .ToListAsync();
                response.Data = new
                {
                    Data = data,
                    Total = total
                };  
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetReportData");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }


        [HttpPost("exportToExcel")]
        public async Task<ActionResult> ExportToExcel(PagedSortRequestDto<CdReportRequestDto> request)
        {
            byte[] fileBytes = null;
            try
            {
                var q = createQuery(request);
                var data = await q.ToListAsync();
                var columns = new[]
                {
                    "Организация",
                    "Вид документа",
                    "Номер документа",
                    "Статус",
                    "Дата создания",
                    "Дата подтверждения",
                    "Срок действия с",
                    "Срок действия по",
                    "Количество оформленных ЭП"
                };


                using (ExcelPackage package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("OnlineVoting");
                    int endCol = columns.Count();
                    var header = worksheet.Cells[1, 1, 1, endCol];
                    header.LoadFromArrays(new List<object[]>
                    {
                        columns
                    });
                    int endRow = 0;
                    for (endRow = 0; endRow < data.Count; endRow++)
                    {
                        var cdData = data[endRow];
                        worksheet.Cells[endRow + 2, 1].Value = cdData.AuthorOrgNameRu;
                        worksheet.Cells[endRow + 2, 2].Value = cdData.DocTypeNameRu;
                        worksheet.Cells[endRow + 2, 3].Value = cdData.DocNumber;
                        worksheet.Cells[endRow + 2, 4].Value = cdData.StatusNameRu;
                        worksheet.Cells[endRow + 2, 5].Value = cdData.CreateDate.ToString("d");
                        worksheet.Cells[endRow + 2, 6].Value = cdData.ModifyDate.HasValue ? cdData.ModifyDate.Value.ToString("d") : "";
                        worksheet.Cells[endRow + 2, 7].Value = cdData.DocStartDate.HasValue ? cdData.DocStartDate.Value.ToString("d") : "";
                        worksheet.Cells[endRow + 2, 8].Value = cdData.DocEndDate.HasValue ? cdData.DocEndDate.Value.ToString("d") : "";
                        worksheet.Cells[endRow + 2, 9].Value = cdData.DigitalPassportCount.ToString();
                    }
                    var tableRange = worksheet.Cells[1, 1, endRow + 2, endCol];
                    var table = worksheet.Tables.Add(tableRange, "Otts");
                    table.TableStyle = TableStyles.Medium2;
                    
                    worksheet.Cells[1, 1, Math.Min(endRow + 2, 250), endCol].AutoFitColumns(1, 100);

                    fileBytes = package.GetAsByteArray();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "ExportToExcel");
            }

            var result = new FileContentResult(fileBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.FileDownloadName ="ComplienceDocument_Report_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            return result;
        }
    
        private IQueryable<ComplienceDocReport> createQuery(PagedSortRequestDto<CdReportRequestDto> request)
        {
            var q = _UnitOfWork.ComplienceDocReportRepository.GetAsIQueryable();
            if (request.Filter.OrganizationIds != null && request.Filter.OrganizationIds.Count > 0)
                q = q.Where(c => request.Filter.OrganizationIds.Contains(c.AuthorOrgId));

            if (request.Filter.DocumentTypeIds != null && request.Filter.DocumentTypeIds.Count > 0)
                q = q.Where(c => request.Filter.DocumentTypeIds.Contains(c.DocTypeId));

            if (request.Filter.StatusIds != null && request.Filter.StatusIds.Count > 0)
                q = q.Where(c => request.Filter.StatusIds.Contains(c.StatusId));

            if (!string.IsNullOrEmpty(request.Filter.DocumentNumber))
                q = q.Where(c => c.DocNumber.StartsWith(request.Filter.DocumentNumber)
                                || c.DocNumber.Contains(request.Filter.DocumentNumber));

            if (request.Filter.DateStartFrom != null)
                q = q.Where(c => request.Filter.DateStartFrom <= c.DocStartDate);

            if (request.Filter.DateStartTo != null)
                q = q.Where(c => request.Filter.DateStartTo > c.DocStartDate);


            if (request.Filter.DateEndFrom != null)
                q = q.Where(c => request.Filter.DateEndFrom <= c.DocStartDate);

            if (request.Filter.DateEndTo != null)
                q = q.Where(c => request.Filter.DateEndTo > c.DocStartDate);

           

            if (request.SortField != null && request.SortField.Length > 0)
                q = q.OrderByCommonFields(request.SortField, request.Direction);
            else
                q = q.OrderBy(c => c.AuthorOrgNameRu);

            return q;
        }
    
        
    }
}

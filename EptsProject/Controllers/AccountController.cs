﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBCore;
using DBCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EptsProject.Core;
using EptsProject.Models;
using System.Text.Json;
using Newtonsoft.Json;
using System.Security.Claims;
using EptsBL.Services.Interface;
using DbLog.Repositories;
using DbLog;
using EptsProject.Helpers;
using Microsoft.AspNetCore.DataProtection;
using System.IO;
using DBCore.Models.Dictionaries;
using Microsoft.EntityFrameworkCore;

namespace EptsProject.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : MainController
    {
        RoleManager<AspNetRoles> _roleManager;
        ICertificateService _certificateService;
        ILogUnitOfWork _logUoW;

        public AccountController(ILogger<AccountController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , SignInManager<AspNetUsers> signInManager
            , RoleManager<AspNetRoles> roleManager
            , ICertificateService certificateService
            , ILogUnitOfWork logUnitOfWork) : base(logger, unitOfWork, userManager, signInManager)
        {
            _roleManager = roleManager;
            _certificateService = certificateService;
            _logUoW = logUnitOfWork;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [HttpPost("Login")]
        public async Task<JsonResponse> LoginAsync([FromBody] LoginModel loginModel)
        {
            //HttpContext.GetOwinContext().GetUserManager<UserManager>();
            // TODO удостовериться, что адрес возврата принадлежит приложению с помощью метода Url.IsLocalUrl()
            var response = new JsonResponse { Text = "Неправильный логин или пароль", Action = Constants.ACTIONS.OK};
            try
            {
                var userLog = new DbLog.Models.UserLog()
                {
                    EventDate = DateTime.Now,
                    IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                    UserName = loginModel.Name,
                    Message = "Попытка входа в систему пользователя по логину и паролю"
                };
                await _logUoW.LogUserInfo(userLog);
                _logger.LogInformation("Попытка входа в систему пользователя по логину и паролю. {0}. IpAddress={1}", userLog.UserName, userLog.IpAddress);

                var result = await _signInManager.PasswordSignInAsync(loginModel.Name, loginModel.Password, true, true);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(loginModel.Name);

                    var userProfile = await _UnitOfWork.UserProfileRepository.GetAsIQueryable(p => p.Id == user.Id)
                        .Include(p => p.Status).FirstOrDefaultAsync();

                    if (userProfile != null && (userProfile.Status.Code == DictionaryCodes.StatusCodes.Locked
                            || userProfile.Status.Code == DictionaryCodes.StatusCodes.Deleted))
                    {
                        userLog = new DbLog.Models.UserLog()
                        {
                            EventDate = DateTime.Now,
                            IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                            UserName = loginModel.Name,
                            Message = "Вход не возможен. Пользователь заблокирован",
                            AdditionalInfo = JsonConvert.SerializeObject(new
                            {
                                user.Id,
                                user.UserName
                            })
                        };
                        await _logUoW.LogUserInfo(userLog);
                        _logger.LogInformation(userLog.Message + ". {0}. IpAddress={1}. AdditionalInfo={2}", userLog.UserName, userLog.IpAddress, userLog.AdditionalInfo);

                        response.Status = "Error";
                        response.Text = "Пользователь заблокирован";
                    } 
                    else
                    {
                        userLog = new DbLog.Models.UserLog()
                        {
                            EventDate = DateTime.Now,
                            IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                            UserName = loginModel.Name,
                            Message = "Успешный вход в систему осуществлен по логину и паролю",
                            AdditionalInfo = JsonConvert.SerializeObject(new
                            {
                                user.Id,
                                user.UserName
                            })
                        };
                        await _logUoW.LogUserInfo(userLog);
                        _logger.LogInformation(userLog.Message + ". {0}. IpAddress={1}. AdditionalInfo={2}", userLog.UserName, userLog.IpAddress, userLog.AdditionalInfo);

                        var manager = new AntiForgeryManager(_logger, _UnitOfWork);
                        var token = manager.CreateAppHash();
                        manager.SaveAppHash(user.Id.ToString(), token);
                        var roles = await _userManager.GetRolesAsync(user);
                        IList<string> claims = new List<string>();
                        foreach (var role in roles)
                        {
                            var existRole = await _roleManager.FindByNameAsync(role);
                            var existClaims = await _roleManager.GetClaimsAsync(existRole);
                            claims = claims.Union(existClaims.Select(c => c.Value)).ToList();
                        }

                        response.Status = "Ok";
                        response.Text = "Ok";
                        response.Data = new
                        {
                            Id = user.Id
                            , Name = user.UserName
                            , Role = roles.FirstOrDefault()
                            , Token = token
                            , Claims = claims
                            , OrganizationId = user.OrganizationId
                            , IsPasswordExpire = user.PasswordExpire == null || DateTimeOffset.Now.LocalDateTime > user.PasswordExpire
                        };
                        _logger.LogInformation($"Вошел в систему {loginModel.Name}: {HttpContext.Request.Headers["X-Real-IP"]}");
                    }
                }
                else
                {
                    if (result.IsLockedOut)
                    {
                        response.Status = "Error";
                        response.Text = "Ваша учетная запись заблокирована. Повторите попытку через 10 минут или обратитесь к Администратору.";
                    }
                    else
                    {
                        response.Status = "Error";
                        response.Text = "Введен не верный пароль. Внимание! У Вас есть 3 попытки, после учетная запись будет заблокирована";
                    }
                }
            } 
            catch (Exception e)
            {
                var userLog = new DbLog.Models.UserLog()
                {
                    EventDate = DateTime.Now,
                    IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                    UserName = loginModel.Name,
                    Message = "Не удалось войти в систему по логину и паролю",
                    AdditionalInfo = JsonConvert.SerializeObject(new
                    {
                        e.Message,
                        e.StackTrace
                    })
                };
                await _logUoW.LogUserInfo(userLog);
                _logger.LogInformation(userLog.Message + ". {0}. IpAddress={1}. AdditionalInfo={2}", userLog.UserName, userLog.IpAddress, userLog.AdditionalInfo);

                _logger.LogError($"Не удалось войти в систему по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {loginModel.Name}, {e}");
            }
            return response;
        }

        [HttpPost("LoginByCert")]
        public async Task<JsonResponse> LoginByCert([FromBody] LoginModel loginModel)
        {
            var response = new JsonResponse { 
                Text = "Неправильный логин или пароль"
                , Action = Constants.ACTIONS.OK 
            };
            try
            {
                var userLog = new DbLog.Models.UserLog()
                {
                    EventDate = DateTime.Now,
                    IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                    UserName = loginModel.Name,
                    Message = "Попытка входа в систему пользователя по сертификату"
                };
                await _logUoW.LogUserInfo(userLog);
                _logger.LogInformation(userLog.Message + ". {0}. IpAddress={1}. Additional data={2}", userLog.UserName, userLog.IpAddress, userLog.AdditionalInfo);

                var user = await _signInManager.UserManager.FindByNameAsync(loginModel.Name);
                if (user != null)
                {
                    if (await _certificateService.VerifyCertificate(loginModel.Certificate))
                    {
                        userLog = new DbLog.Models.UserLog()
                        {
                            EventDate = DateTime.Now,
                            IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                            UserName = loginModel.Name,
                            Message = "Успешный вход в систему осуществлен по сертификату",
                            AdditionalInfo = JsonConvert.SerializeObject(new
                            {
                                user.Id,
                                user.UserName
                            })
                        };
                        await _logUoW.LogUserInfo(userLog);
                        _logger.LogInformation(userLog.Message + ". {0}. IpAddress={1}. Additional data={2}", userLog.UserName, userLog.IpAddress, userLog.AdditionalInfo);

                        await _signInManager.SignInAsync(user, true);

                        var manager = new AntiForgeryManager(_logger, _UnitOfWork);
                        var token = manager.CreateAppHash();
                        manager.SaveAppHash(user.Id.ToString(), token);
                        var roles = await _userManager.GetRolesAsync(user);
                        IList<string> claims = new List<string>();
                        foreach (var role in roles)
                        {
                            var existRole = await _roleManager.FindByNameAsync(role);
                            var existClaims = await _roleManager.GetClaimsAsync(existRole);
                            claims = claims.Union(existClaims.Select(c => c.Value)).ToList();
                        }

                        response.Status = "Ok";
                        response.Text = "Ok";
                        response.Data = new
                        {
                            Name = user.UserName,
                            Role = roles.FirstOrDefault(),
                            Token = token,
                            Claims = claims,
                            OrganizationId = user.OrganizationId
                        };
                        _logger.LogInformation($"Вошел в систему {loginModel.Name}: {HttpContext.Request.Headers["X-Real-IP"]}");
                    }
                }
            }
            catch (Exception e)
            {
                var userLog = new DbLog.Models.UserLog()
                {
                    EventDate = DateTime.Now,
                    IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                    UserName = loginModel.Name,
                    Message = "Не удалось войти в систему по сертификату",
                    AdditionalInfo = JsonConvert.SerializeObject(new
                    {
                        e.Message,
                        e.StackTrace
                    })
                };
                await _logUoW.LogUserInfo(userLog);
                _logger.LogInformation(userLog.Message + ". {0}. IpAddress={1}. Additional data={2}", userLog.UserName, userLog.IpAddress, userLog.AdditionalInfo);
                _logger.LogError($"Не удалось войти в систему по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {loginModel.Name}, {e}");
            }
            return response;
        }


        [HttpGet("LogOut")]
        public async Task<JsonResponse> LogOutAsync()
        {
            var response = new JsonResponse { Text = "Не получилось выйти из аккаунта" };
            var name = User.Identity.Name;
            try
            {
                var b = HttpContext.User.Identity.IsAuthenticated;
                await _signInManager.SignOutAsync();
                response.Status = "Ok";
                response.Text = "Ok";
                response.Action = Constants.ACTIONS.OK;
                _logger.LogInformation($"Вышел из системы");

                var userLog = new DbLog.Models.UserLog()
                {
                    EventDate = DateTime.Now,
                    IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                    UserName = name,
                    Message = "Пользователь вышел из системы"
                };
                await _logUoW.LogUserInfo(userLog);
                _logger.LogInformation(userLog.Message + ". {0}. IpAddress={1}", userLog.UserName, userLog.IpAddress);
            }
            catch (Exception e)
            {
                var userLog = new DbLog.Models.UserLog()
                {
                    EventDate = DateTime.Now,
                    IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                    UserName = name,
                    Message = "Пользователь вышел из системы",
                    AdditionalInfo = JsonConvert.SerializeObject(new
                    {
                        e.Message,
                        e.StackTrace
                    })
                };
                await _logUoW.LogUserInfo(userLog);
                _logger.LogInformation(userLog.Message + ". {0}. IpAddress={1}. Additional data={2}", userLog.UserName, userLog.IpAddress, userLog.AdditionalInfo);
                _logger.LogError($"Не удалось выйти из системы по причине: {e}");
            }
            return response;
        }

        [HttpGet("ShouldLogin")]
        public ActionResult<JsonResponse> ShouldLogin()
        {
            _logger.LogInformation(GetLogMessageFormat($"Требуется авторизация"));
            return new JsonResponse { Status = "Ok", Text = "ShouldLogin", Data = new object(), Action = Constants.ACTIONS.ShouldLogin }; 
        }

        [HttpGet("AccessDenied")]
        public ActionResult<JsonResponse> AccessDenied()
        {
            _logger.LogInformation(GetLogMessageFormat($"AccessDenied"));
            return new JsonResponse { Status = "Ok", Text = "AccessDenied", Data = new Object(), Action = Constants.ACTIONS.AccessDenied };
        }


        [HttpGet("UserInfo/{login}")]
        [Authorize]
        public async Task<JsonResponse> GetUserInfo([FromRoute]string login)
        {
            var response = new JsonResponse();
            response.Action = Constants.ACTIONS.OK;

            var user = await _userManager.FindByNameAsync(login);
            var roles = await _userManager.GetRolesAsync(user);
            IList<string> claims = new List<string>();
            foreach (var role in roles)
            {
                var existRole = await _roleManager.FindByNameAsync(role);
                var existClaims = await _roleManager.GetClaimsAsync(existRole);
                claims = claims.Union(existClaims.Select(c => c.Value)).ToList();
            }

            response.Status = "Ok";
            response.Text = "Ok";
            response.Data = new
            {
                Id = user.Id
                , Name = user.UserName
                , Role = roles.FirstOrDefault()                
                , Claims = claims
                , OrganizationId = user.OrganizationId
            };
            return response;
        }

        [HttpPost("changePassword")]
        public async Task<JsonResponse> ChangePassword([FromBody] ChangePasswordModel request)
        {
            var userDetail = await this._userManager.FindByNameAsync(request.Login);
            var response = new JsonResponse();
            response.Action = Constants.ACTIONS.OK;
            response.Text = string.Empty;
            response.Status = "OK";

            if (userDetail == null)
            {
                response.Status = "ERROR";
                response.Text = "Пользователь не найден";
                return response;
            }
            try
            {
                var userLog = new DbLog.Models.UserLog()
                {
                    EventDate = DateTime.Now,
                    IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                    UserName = request.Login,
                    Message = "Смена пароля для пользователей"
                };
                await _logUoW.LogUserInfo(userLog);
                _logger.LogInformation(userLog.Message + ". {0}. IpAddress={1}. Additional data={2}", userLog.UserName, userLog.IpAddress, userLog.AdditionalInfo);

                var identityResult = await _userManager.ChangePasswordAsync(userDetail, request.OldPassword, request.Password);

                userDetail.PasswordExpire = DateTimeOffset.Now.LocalDateTime.AddDays(30);
                await _userManager.UpdateAsync(userDetail);

                if (!identityResult.Succeeded)
                {
                    var error = identityResult.Errors.FirstOrDefault();
                    if (error != null)
                        response.Text = error.Description;
                }

            } 
            catch(Exception e)
            {
                _logger.LogError(e, "ChangePassword");
            }
           
            
            return response;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("user-agreement")]
        public async Task<ActionResult> GetUserAgreement()
        {
            String fileName = "соглашение_пользователей.docx";
            var path = Path.Combine(Directory.GetCurrentDirectory(),
                     "Contents", "Files", fileName);

            byte[] fileBytes = await System.IO.File.ReadAllBytesAsync(path);
            var mimeType = "application/octet-stream";

            if (string.IsNullOrEmpty(fileName) && fileBytes == null)
                return NoContent();

            return new FileContentResult(fileBytes, mimeType)
            {
                FileDownloadName = fileName
            };
        }



        /*
        [HttpPost("resetPassword")]
        public async Task<ActionResult<bool>> ResetPassword([FromBody] ResetPasswordModel request)
        {
            var bytes = HttpContext.RequestServices
                .GetDataProtector("_epts_reset_kupyasoz")
                .Unprotect(Convert.FromBase64String(request.Token));
            long userId;
            using (var ms = new MemoryStream(bytes))
            using (var br = new BinaryReader(ms))
            {
                var dt = DateTime.FromBinary(br.ReadInt64());
                userId = Int64.Parse(br.ReadString());

                if (dt < DateTime.UtcNow)
                    throw new Exception("Время токена истекло");
            }

            await _mediator.Send(new UserResetPassword.Command()
            {
                UserId = userId,
                Password = request.Password,
                IsExternalUser = request.IsExternalUser
            });

            return Ok(true);
        }
        */

        #region Role
        [HttpPost("CreateRole")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<JsonResponse>> CreateRoleAsync(string name)
        {
            var response = new JsonResponse();
            try
            {
                if (name.Trim().Length < 4)
                {
                    response.Text = "Требуется более 3-х символов";
                    return response;
                }
                var role = new AspNetRoles();
                role.Name = name;
                IdentityResult result = await _roleManager.CreateAsync(role);
                if (result.Succeeded)
                {
                    response.Status = "Ok";
                    response.Text = "Роль успешно создана";
                    _logger.LogInformation(GetLogMessageFormat($"Роль- {name} успешно создана"));
                    return response;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(GetLogMessageFormat($"Не удалось создать роль {name} по причине: , {e}"));
            }
            return response;
        }

        [HttpGet("Role")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<List<AspNetRoles>>> GetRoleAsync(string role = "")
        {

            //if (role.Trim() == "")
            //{
                return _UnitOfWork.RolesRepository.Get().ToList();
            //}
        }


        #endregion
        /*
        [HttpGet("CreateUser")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<AspNetUsers>> CreateUser(string email, string userName, int organizationId)
        {
            try
            {
                //IdentityUser user = new IdentityUser { Email = model.Email, UserName = model.Email, Year = model.Year };
                
                AspNetUsers user = new AspNetUsers { Email = email, UserName = userName, OrganizationId = organizationId };
                // добавляем пользователя
                var result = await _userManager.CreateAsync(user, "1q2w3e$R");
                if (result.Succeeded)
                {
                    // установка куки
                    // await _signInManager.SignInAsync(user, false);
                    // return RedirectToAction("Index", "/");
                }
                return user;
            }
            catch (Exception e)
            {
                _logger.LogError(GetLogMessageFormat($"Не удалось создать по причине: , {e}"));
            }
            return null;
        }
        */


    }
}

﻿using AutoMapper;
using DBCore;
using DBCore.Models;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers.Reestrs
{
    /// <summary>
    /// Подписанные документы
    /// </summary>

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]

    public class SignedDocController : ControllerBase
    {
        private readonly NLog.ILogger _nlogger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<AspNetUsers> _userManager;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nlogger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        public SignedDocController(IUnitOfWork unitOfWork
           , UserManager<AspNetUsers> userManager
           , IMapper mapper)
        {
            _userManager = userManager;            
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            this._nlogger = NLog.LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Созранить подписанный документ
        /// </summary>
        /// <param name="signedDoc"></param>
        /// <returns></returns>
        [HttpPost("Save")]
        public async Task<JsonResponse> Save([FromBody] SignedDoc signedDoc)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                if (signedDoc.Id == Guid.Empty)
                {
                    signedDoc.CreateDate = DateTime.Now;
                    var isSuccess = await _unitOfWork.SignedDocRepository.InsertAsync(signedDoc);
                }
                else
                {
                    signedDoc.ModifiedDate = DateTime.Now;
                    var isSuccess = _unitOfWork.SignedDocRepository.Update(signedDoc);
                }

                await _unitOfWork.SaveAsync();
                response.Data = signedDoc;
            }
            catch (Exception e)
            {
                _nlogger.Error(e, "Save SignedDoc");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        /// <summary>
        /// Получить документ по ИД
        /// </summary>
        /// <param name="id">ИД</param>
        /// <returns></returns>
        [HttpGet("Get")]
        public async Task<JsonResponse> Get(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var entity = await _unitOfWork.SignedDocRepository
                                        .GetAsIQueryable(sd => sd.DeleteDate == null && sd.Id == id)                                      
                                        .SingleOrDefaultAsync();
                response.Data = entity;
            }
            catch (Exception e)
            {
                _nlogger.Error(e, "Get SignedDocument");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

    }
}

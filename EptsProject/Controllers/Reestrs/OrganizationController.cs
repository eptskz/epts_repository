﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.DTOs.Registries;
using DBCore.Enums.Organizations;
using DBCore.Models;
using DBCore.Models.Dictionaries;
using DBCore.Models.Registries;
using EptsBL.Extensions;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers.Reestrs
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class OrganizationsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;
        private readonly NLog.ILogger _nlogger;

        private readonly UserManager<AspNetUsers> _userManager;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        public OrganizationsController(ILogger<OrganizationsController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , IMapper mapper)
        {
            _userManager = userManager;
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _nlogger = NLog.LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Получить список организаций
        /// </summary>
        /// <param name="orgTypeCodes"></param>
        /// <returns></returns>
        [HttpGet("GetList")]
        public async Task<JsonResponse> GetList([FromQuery] IList<string> orgTypeCodes)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            
            try
            {
                var query = _unitOfWork.OrganizationsRepository
                    .GetAsIQueryable(v => v.Deleted == null);

                if (orgTypeCodes != null && orgTypeCodes.Count > 0)
                    query = query.Where(v => v.OrganizationTypes.Any(ot => orgTypeCodes.Contains(ot.OrgType.Code)));
                
                var entities = await query
                    .Include(o => o.OrganizationTypes)
                        .ThenInclude(ot => ot.OrgType)
                    .Include(o => o.CommunicationInfos)
                        .ThenInclude(o => o.CommunicationType)
                    .ToListAsync();

                response.Data = _mapper.Map<ICollection<OrganizationRegistrDto>>(entities);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetList");
                response.Text = e.Message;
                response.Status = "Error";
            }
            
            return response;
        }

        /// <summary>
        /// Получить организацию по ИД
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        [HttpGet("GetOrg")]
        public async Task<JsonResponse> GetOrg([FromQuery] int orgId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var query = _unitOfWork.OrganizationsRepository
                    .GetAsIQueryable(v => v.Deleted == null);

                query = query.Where(v => v.Id == orgId);

                var entity = await query
                    .Include(o => o.Country)
                    .Include(o => o.LegalForm)
                    .Include(o => o.OrganizationTypes)
                        .ThenInclude(ot => ot.OrgType)
                    .Include(o => o.CommunicationInfos)
                        .ThenInclude(o => o.CommunicationType)
                    .Include(o => o.OrganizationDocuments)
                        .ThenInclude(d => d.DocType)
                    .Include(o => o.OrganizationDocuments)
                        .ThenInclude(d => d.IssueCountry)
                    .Include(o => o.OrganizationDocuments)
                        .ThenInclude(d => d.TechRegulationSubjectUnit)
                    .Include(o => o.OrganizationDocuments)
                        .ThenInclude(d => d.TechRegulationSubjectType)
                    .Include(o => o.OrganizationDigPassportTypes)
                        .ThenInclude(d => d.DigPassportType)
                    .SingleOrDefaultAsync();

                response.Data = _mapper.Map<OrganizationDto>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Получить список организаций
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("GetPagedList")]
        public async Task<JsonResponse> GetPagedList([FromBody] PagedSortRequestDto<OrganizationFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var query = _unitOfWork.OrganizationsRepository
                    .GetAsIQueryable();

                if (!request.Filter.ShowDeleted)
                {
                    query = query.Where(v => v.Deleted == null);
                }

                if (request.Filter.ParentId != null)
                    query = query.Where(o => o.ChildRefs.Any(pr => pr.ParentId == request.Filter.ParentId));

                var sqlQuery = query.ToSql();

                if (!string.IsNullOrEmpty(request.Filter.RegisterTypeStr))
                {
                    if (request.Filter.RegisterTypeStr.ToLower() == RegisterTypeEnum.RegisterKiru.ToString().ToLower())
                        query = query.Where(o => o.RegisterType == RegisterTypeEnum.RegisterKiru);
                    else if (request.Filter.RegisterTypeStr.ToLower() == RegisterTypeEnum.RegisterKirm.ToString().ToLower())
                        query = query.Where(o => o.RegisterType == RegisterTypeEnum.RegisterKirm);
                    //else
                    //    query = query.Where(o => o.RegisterType == RegisterTypeEnum.RegisterGeneral);
                }

                if (!string.IsNullOrEmpty(request.Filter.Search))
                {
                    query = query.Where(o => o.NameRu.Contains(request.Filter.Search)
                                        || o.Bin.Contains(request.Filter.Search));
                }
                if (!string.IsNullOrEmpty(request.Filter.OrgTypeCode))
                {
                    query = query.Where(o => o.OrganizationTypes.Any(ot => ot.OrgType.Code == request.Filter.OrgTypeCode));
                }

                if (request.SortField != null && request.SortField.Length > 0)
                {
                    List<string> sotFields = new List<string>();
                    foreach(var sortfield in request.SortField)                    
                        sotFields.Add(sortfield == "name" ? "nameRu" : sortfield);
                    
                    query = query.OrderByCommonFields(sotFields.ToArray(), request.Direction);
                }
                else
                {
                    query = query.OrderByDescending(v => v.Created);
                }

                var total = await query.CountAsync();
                var entities = await query
                    .Include(o => o.Status)
                    .Include(o => o.FactRegionOrCity)
                    .Include(o => o.FactDistinctOrCity)
                    .Include(o => o.FactOkrugOrCity)
                    .Include(o => o.FactVillage)

                    .Include(o => o.JuridicalRegionOrCity)
                    .Include(o => o.JuridicalDistinctOrCity)
                    .Include(o => o.JuridicalOkrugOrCity)
                    .Include(o => o.JuridicalVillage)

                    .Include(o => o.OrganizationTypes)
                        .ThenInclude(ot => ot.OrgType)
                    .Include(o => o.CommunicationInfos)
                        .ThenInclude(o => o.CommunicationType)
                    .Skip(request.Page * request.PageSize)
                    .Take(request.PageSize)
                    .ToListAsync();

                response.Data = new
                {
                    Data = _mapper.Map<ICollection<OrganizationRegistrDto>>(entities),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetList");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Найти по названию организацию
        /// </summary>
        /// <param name="name">Название организации</param>
        /// <returns></returns>
        [HttpGet("FindOrgByName")]
        public async Task<JsonResponse> FindOrgByName([FromQuery] string name)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var query = _unitOfWork.OrganizationsRepository
                    .GetAsIQueryable(v => v.Deleted == null);

                if (!string.IsNullOrEmpty(name))
                    query = query.Where(v => v.NameRu.ToLower().Contains(name.ToLower()));

                var entities = await query
                    .Include(o => o.OrganizationTypes)
                        .ThenInclude(ot => ot.OrgType)
                    .ToListAsync();

                response.Data = _mapper.Map<ICollection<OrganizationDto>>(entities);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetList");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Сохранить Организацию
        /// </summary>
        /// <param name="orgDto">Организация</param>
        /// <returns></returns>
        [HttpPost("Save")]
        public async Task<JsonResponse> SaveOrganization([FromBody] OrganizationDto orgDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                foreach (var ci in orgDto.CommunicationInfos)
                    ci.CommunicationTypeId = _unitOfWork.NsiValuesRepository
                        .GetAsIQueryable(nv => nv.Code == ci.CommunicationTypeCode)
                        .FirstOrDefault().Id;

                if (orgDto.Id == 0)
                {
                    bool isBinExist = !string.IsNullOrEmpty(orgDto.Bin)
                        && _unitOfWork.OrganizationsRepository
                        .GetAsIQueryable(q => q.Bin == orgDto.Bin && q.Deleted == null)
                        .Any();

                    if (isBinExist)
                    {
                        response.Status = "Error";
                        response.Text = $"Организация с БИН ({orgDto.Bin}) уже существует";
                    }
                    else
                    {
                        var status = _unitOfWork.DicStatusesRepository
                            .GetAsIQueryable(d => d.Code == DictionaryCodes.StatusCodes.New)
                            .FirstOrDefault();
                        orgDto.StatusId = status.Id;

                        var orgEntity = _mapper.Map<Organization>(orgDto);
                        await _unitOfWork.OrganizationsRepository.AddOrUpdateAsync(orgEntity);
                        orgDto.Id = orgEntity.Id;
                        orgDto.Created = orgEntity.Created;                        

                        response.Data = (await this.GetOrg(orgEntity.Id)).Data;
                    }
                }
                else
                {
                    var orgEntity = _mapper.Map<Organization>(orgDto);
                    await _unitOfWork.OrganizationsRepository.AddOrUpdateAsync(orgEntity);

                    response.Data = _mapper.Map<OrganizationDto>(orgEntity);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "SaveOrganization");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Получить документы организации
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="docTypeCode"></param>
        /// <returns></returns>
        [HttpGet("getOrgDocs")]
        public async Task<JsonResponse> GetOrgDocs([FromQuery] int orgId, [FromQuery] string docTypeCode)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var query = _unitOfWork.OrganizationsRepository
                    .GetOrgDocumentsAsIQueryable();
                if (orgId > 0) 
                    query = query.Where(v => v.OrganizationId == orgId);

                if (!string.IsNullOrEmpty(docTypeCode))
                    query = query.Where(v => v.OrgDocType == docTypeCode);

                var entity = await query                   
                    .Include(o => o.Organization)
                    .Include(o => o.DocType)
                    .Include(o => o.IssueCountry)
                    .Include(o => o.TechRegulationSubjectType)
                    .Include(o => o.TechRegulationSubjectUnit)
                    .ToListAsync();

                response.Data = _mapper.Map<ICollection<OrgDocumentDto>>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "getOrgDocs");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Сохранить Документ Организации
        /// </summary>
        /// <param name="orgDocumentDto"></param>
        /// <returns></returns>
        [HttpPost("saveDoc")]
        public async Task<JsonResponse> SaveDoc([FromBody] OrgDocumentDto orgDocumentDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var orgDocument = _mapper.Map<OrgDocumentDto, OrganizationDocument>(orgDocumentDto);
                var entityId = await _unitOfWork.OrganizationsRepository.UpdateOrgDocument(orgDocument);
                response.Data = new { entityId };
            }
            catch (Exception e)
            {
                _logger.LogError("SaveDoc {0}: {1}", e.Message, e.StackTrace);
                _nlogger.Error(e, "SaveDoc error");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        /// <summary>
        /// Удалить Организацию
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [HttpDelete("delete")]
        public JsonResponse Delete([FromQuery] int id, int? parentId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                if (parentId == null)
                {
                    response.Data = _unitOfWork.OrganizationsRepository.Delete(id);
                }
                else
                {
                    response.Data = _unitOfWork.OrganizationsRepository.DeleteNested(id, parentId.Value);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Delete");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        /// <summary>
        /// Восстановить Организацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("restore")]
        public async Task<JsonResponse> Restore([FromQuery] int id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                _unitOfWork.OrganizationsRepository.Restore(id);
                await _unitOfWork.SaveAsync();
                response.Data = true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Restore");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
    }
}

﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.DTOs.Registries;
using DBCore.DTOs.Registries.Invoice;
using DBCore.Models;
using DBCore.Models.Dictionaries;
using DBCore.Models.Notifications;
using DBCore.Models.Registries.Invoice;
using DBCore.Permissions;
using EptsBL.Extensions;
using EptsBL.Services.Interface;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EptsProject.Controllers.Administration
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class InvoiceController : MainController
    {
        private readonly NLog.ILogger _nlogger;
        private readonly RoleManager<AspNetRoles> _roleManager;
        private readonly INotificationService _notificationService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        /// <param name="roleManager"></param>
        /// <param name="notificationService"></param>
        public InvoiceController(ILogger<InvoiceController> logger
          , IUnitOfWork unitOfWork
          , UserManager<AspNetUsers> userManager
          , IMapper mapper
          , RoleManager<AspNetRoles> roleManager
          , INotificationService notificationService) : base(logger, unitOfWork, userManager, mapper)
        {
            this._nlogger = NLog.LogManager.GetCurrentClassLogger();
            _roleManager = roleManager;
            _notificationService = notificationService;
        }

        /// <summary>
        /// Получить счета на оплату
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("invoices")]
        public async Task<JsonResponse> GetInvoices([FromQuery] PagedSortRequestDto<InvoiceFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
           
            try
            {
                var currentUser = await _userManager.GetUserAsync(User);

                var q = _UnitOfWork.InvoiceRepository.GetAsIQueryable();

                if (!(User.Claims.Any(c => c.Type == CustomClaimTypes.Permissions
                                     && c.Value == RegistryPermissions.Invoice.Edit)
                        && User.Claims.Any(c => c.Type == CustomClaimTypes.Permissions
                                     && c.Value == RegistryPermissions.Invoice.Delete)))
                {
                    q = q.Where(v => v.Contract.ClientOrgId == currentUser.OrganizationId
                        && v.Status.Code != DictionaryCodes.StatusCodes.New);
                }

                if (request.Filter == null)
                {
                    q = q.Where(v => false);
                }
                else
                {
                    if (!request.Filter.IsShowDeleted)
                    {
                        q = q.Where(v => v.Deleted == null);
                    }

                    if (!string.IsNullOrEmpty(request.Filter.Search))
                    {
                        q = q.Where(v => v.Number.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.Number.ToLower().Contains(request.Filter.Search.ToLower())

                                        || v.Description.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.Description.ToLower().Contains(request.Filter.Search.ToLower())
                                    );
                    }
                }                

                if (request.SortField != null && request.SortField.Length > 0)
                    q = q.OrderByCommonFields(request.SortField, request.Direction);
                else
                    q = q.OrderByDescending(v => v.Created).ThenBy(v => v.Number);

                var total = await q.CountAsync();
                var pageSize = request.PageSize == 0 ? total : request.PageSize;
                var invoices = await q.Include(v => v.Status)
                                    .Include(v => v.Contract)
                                        .ThenInclude(c => c.ClientOrg)
                                    .Skip(request.Page * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();
                response.Data = new
                {
                    Data = _mapper.Map<ICollection<InvoiceDto>>(invoices),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetInvoices");
                response.Text = e.Message;
                response.Status = "Error";
            }
            
            return response;
        }

        /// <summary>
        /// Получить контракт по ИД
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("invoice/{id}")]
        public async Task<JsonResponse> GetInvoice([FromRoute] int id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var ticket = await _UnitOfWork.InvoiceRepository
                    .GetAsIQueryable(v => v.Id == id)
                    .Include(t => t.Contract)
                        .ThenInclude(t => t.ClientOrg)
                    .Include(t => t.Status)
                    .FirstOrDefaultAsync();
                response.Data = _mapper.Map<InvoiceDto>(ticket);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetInvoice");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Создать счет
        /// </summary>
        /// <param name="invoiceDto"></param>
        /// <returns></returns>
        [HttpPost("invoice")]
        public async Task<JsonResponse> AddInvoice(InvoiceDto invoiceDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var invoice = _mapper.Map<Invoice>(invoiceDto);
                invoice.Created = DateTime.Now;

                var status = _UnitOfWork.DicStatusesRepository
                    .GetAsIQueryable(ds => ds.Code == DictionaryCodes.StatusCodes.New
                            && ds.Group == DictionaryCodes.StatusCodes.Groups.Invoice)
                    .FirstOrDefault();
                invoice.StatusId = status.Id;

                var currentUser = await _userManager.GetUserAsync(User);
                var userProfile = _UnitOfWork.UserProfileRepository.GetByID(currentUser.Id);
                invoice.AuthorId = currentUser.Id;
                invoice.AuthorName = userProfile.LastName + " " + userProfile.FirstName;

                await _UnitOfWork.InvoiceRepository.InsertAsync(invoice);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AddInvoice");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Обновить счет
        /// </summary>
        /// <param name="invoiceDto"></param>
        /// <returns></returns>
        [HttpPut("invoice")]
        public async Task<JsonResponse> UpdateInvoice(InvoiceDto invoiceDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var invoice = _mapper.Map<Invoice>(invoiceDto);
                invoice.Modified = DateTime.Now;

                var status = _UnitOfWork.DicStatusesRepository
                    .GetAsIQueryable(ds => ds.Code.ToLower() == invoiceDto.StatusCode.ToLower()
                            && ds.Group == DictionaryCodes.StatusCodes.Groups.Invoice)
                    .FirstOrDefault();
                invoice.StatusId = status.Id;

                if (status.Code == DictionaryCodes.StatusCodes.Active)
                {
                    SendNotifications(invoice);
                }

                _UnitOfWork.InvoiceRepository.Update(invoice);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "UpdateInvoice");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// удалить счет
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("invoice")]
        public async Task<JsonResponse> DeleteInvoice(int id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var invoice = _UnitOfWork.InvoiceRepository.GetByID(id);
                invoice.Deleted = DateTime.Now;
                _UnitOfWork.InvoiceRepository.Update(invoice);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeleteInvoice");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }


        private void SendNotifications(Invoice invoice)
        {
            var notification = new Notification()
            {
                Title = "Счет на оплату",
                Detail = "Выставлен счет за " 
                    + invoice.PeriodMonths.Aggregate((current, next) => current + ", " + next)
                    + " на " + invoice.Balance,
                CreateDate = DateTime.Now,
                Author = User.Identity.Name
            };
            var contract = _UnitOfWork.ContractRepository.GetAsIQueryable(c => c.Id == invoice.ContractId)
                .FirstOrDefault();
            if (contract != null)
            {
                var userIds = _UnitOfWork.UserRepository.GetAsIQueryable(u => u.OrganizationId == contract.ClientOrgId)
                    .Select(u => u.Id)
                    .ToList();
                _notificationService.Send(notification, userIds);
            }   
        }
    }
}

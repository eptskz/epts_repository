﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.Models;
using EptsProject.Core;
using EptsProject.Models;
using EptsBL.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.EntityFrameworkCore;
using DBCore.Permissions;

namespace EptsProject.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ChangeApplicationController : MainController
    {
        public ChangeApplicationController(ILogger<ChangeApplicationController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , IMapper mapper) : base(logger, unitOfWork, userManager, mapper)
        {
        }


        [HttpPost("GetList")]
        public async Task<JsonResponse> GetList([FromBody] PagedSortRequestDto<DocFilterDto> request)
        {
            var response = new JsonResponse { Text = "Не удалось найти", Action = Constants.ACTIONS.OK };
            try
            {
                var currentUser = GetUser();
                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Edit))
                {
                    throw new Exception("Данные запрещены");
                }
                var q = _UnitOfWork.ChangeApplicationRepository.GetAsIQueryable(v => v.Deleted == null);
                if (!GetUserRoles().Any(x => x == "administrator" || x == "Национальный оператор"))
                    q = q.Where(v => v.UserCreated.OrganizationId == currentUser.OrganizationId);
                var total = await q.CountAsync();
                var entities = await q.Include(v => v.StatusNsi)
                    .Include(v => v.UserCreated.Organization)
                    .Include(v => v.DigitalPassport)
                    .Include(v => v.DigitalPassport.VehicleMakeNameNsi)
                    .Include(v => v.Files)
                    .Include(v => v.Fields)
                    .Skip(request.Page * request.PageSize)
                    .Take(request.PageSize)
                    .OrderByDescending(v => v.Created)
                    .ToListAsync();
                response.Data = new
                {
                    Data = entities,
                    Total = total
                };
                response.Status = "Ok";
                response.Text = "";
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetList");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        [HttpGet("GetView")]
        public async Task<JsonResponse> GetView(int id)
        {
            var response = new JsonResponse { Text = "Не удалось найти", Action = Constants.ACTIONS.OK };
            try
            {
                var currentUser = GetUser();
                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Edit))
                {
                    throw new Exception("Данные запрещены");
                }
                var q = _UnitOfWork.ChangeApplicationRepository.GetAsIQueryable(v => v.Deleted == null && v.Id == id);
                if (!GetUserRoles().Any(x => x == "administrator" || x == "Национальный оператор"))
                    q = q.Where(v => v.UserCreated.OrganizationId == currentUser.OrganizationId);
                var entity = q.Include(v => v.StatusNsi)
                    .Include(v => v.ChangeStatusNsi)
                    .Include(v => v.UserCreated.Organization)
                    .Include(v => v.DigitalPassport)
                    .Include(v => v.DigitalPassport.VehicleMakeNameNsi)
                    .Include(v => v.Files)
                    .Include(v => v.Fields)
                    .FirstOrDefault();

                response.Data = entity;
                response.Status = "Ok";
                response.Text = "";
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetView");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        [HttpPost("GetDataToSign")]
        public async Task<JsonResponse> GetDataToSign([FromBody] ChangeApplicationDto changeApplicationDto)
        {
            var response = new JsonResponse { Text = "Не удалось взять данные для подписи заявления ЭП", Action = Constants.ACTIONS.OK };
            try
            {
                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Edit))
                    throw new Exception("Данные запрещены");
                if (GetUserRoles().Any(x => x == "administrator" || x == "Национальный оператор"))
                        throw new Exception("Данные запрещены");
                
                _logger.LogInformation($"Change Application GetDataToSign Start DateTime = {DateTime.Now}, ip: { HttpContext.Request.Headers["X-Real-IP"]}");
                var data = _mapper.Map<ChangeApplication>(changeApplicationDto);
                if (data == null) return response;
                ChangeApplicationDto save = null;

                var now = DateTime.Now;

                changeApplicationDto.Created = now;
                changeApplicationDto.UserCreatedId = GetUser().Id;
                changeApplicationDto.SignId = Guid.NewGuid();
                changeApplicationDto.Created = DateTime.Now;
                save = await _UnitOfWork.DraftChangeApplicationMongoRepository.CreateAsync(changeApplicationDto);

                

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(ChangeApplicationDto));
                using (var textWriter = new Utf8StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, changeApplicationDto);
                    response.Data = new { dataToSign = textWriter.ToString(), signId = changeApplicationDto.SignId };
                }
                response.Status = "Ok";
                response.Text = "Данные для подписи";

            }
            catch (Exception e)
            {
                response.Status = "Error";
                _logger.LogError($"Не удалось сериализовать по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            return response;
        }


       

        [HttpPost("SaveSignData")]
        public async Task<JsonResponse> SaveSignData([FromBody] SignedDoc signedDoc)
        {
            var response = new JsonResponse
            {
                Text = "Не удалось подписать и сохранить",
                Action = Constants.ACTIONS.OK,
            };
            var nsiValues = await _UnitOfWork.NsiValuesRepository
                    .GetAsIQueryable(v => v.Nsi.Code == "nsi022" && v.Deleted == null).Include(v => v.Nsi)
                    .ToListAsync();
            using (var transaction = _UnitOfWork.BeginTransaction())
            {
                try
                {
                    if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Edit))
                        throw new Exception("Данные запрещены");
                    
                    if (GetUserRoles().Any(x => x == "administrator" || x == "Национальный оператор"))
                        throw new Exception("Данные запрещены");
                    
                    signedDoc.CreateDate = DateTime.Now;
                    var isSuccess = await _UnitOfWork.SignedDocRepository.InsertAsync(signedDoc);
                    var data = _UnitOfWork.DraftChangeApplicationMongoRepository.GetAsync(new BsonDocument("SignId", new BsonDocument("$eq", signedDoc.DocId))).GetAwaiter().GetResult().FirstOrDefault();
                    if (data == null) throw new Exception("Документ не найден!");
                    
                    var doc = _mapper.Map<ChangeApplication>(data);
                    doc.StatusNsi = nsiValues.FirstOrDefault(x => x.Code == "91");
                    if (data.ChangeStatusNsiCode != "")
                        doc.ChangeStatusNsi = nsiValues.FirstOrDefault(x => x.Code == data.ChangeStatusNsiCode);
                    var save = await _UnitOfWork.ChangeApplicationRepository.InsertAsync(doc);
                    _UnitOfWork.DraftChangeApplicationMongoRepository.RemoveFirst(new BsonDocument("SignId", new BsonDocument("$eq", doc.SignId)));
                    await _UnitOfWork.SaveAsync();
                    response.Data = signedDoc;
                    _UnitOfWork.Commit();
                    response.Text = "Подписано и сохранено УСПЕШНО!";
                    response.Status = "Ok";
                }
                catch (Exception e)
                {
                    _logger.LogError($"Не удалось сохранить по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
                    _UnitOfWork.Rollback();
                    response.Text = e.Message;
                    response.Status = "Error";
                }
            }

            return response;
        }

        // For execute application
        [HttpGet("GetDataToSignExecute")]
        public async Task<JsonResponse> GetDataToSignExecute(int id, string description)
        {
            var response = new JsonResponse { Text = "Не удалось взять данные для подписи заявления ЭП", Action = Constants.ACTIONS.OK };
            try
            {
                _logger.LogInformation($"Change Application GetDataToSign Execute Start DateTime = {DateTime.Now}, ip: { HttpContext.Request.Headers["X-Real-IP"]}");
                var currentUser = GetUser();
                
                if (!GetUserRoles().Any(x => x == "administrator" || x == "Национальный оператор"))
                {
                    throw new Exception("Данные запрещены");
                }
                var q = _UnitOfWork.ChangeApplicationRepository.GetAsIQueryable(v => v.Deleted == null && v.Id == id)
                    .Include(v => v.StatusNsi)
                    .Include(v => v.ChangeStatusNsi)
                    .Include(v => v.Files)
                    .Include(v => v.Fields)
                    .FirstOrDefault(); ;
                if (q == null) return response;
                var changeApplicationDto = _mapper.Map<ChangeApplicationDto>(q);
                if (changeApplicationDto == null) return response;
                ChangeApplicationDto save = null;

                var now = DateTime.Now;

                changeApplicationDto.StatusNsiCode = q.StatusNsi.Code;
                if (q.ChangeStatusNsi != null)
                    changeApplicationDto.ChangeStatusNsiCode = q.ChangeStatusNsi.Code;
                changeApplicationDto.Modified = now;
                changeApplicationDto.UserExecutorId = GetUser().Id;
                changeApplicationDto.ExecuteSignId = Guid.NewGuid();
                if (description != null )
                {
                    changeApplicationDto.DeclineDate = now;
                    changeApplicationDto.DeclineDescription = description;
                    changeApplicationDto.StatusNsiCode = "10";
                }
                save = await _UnitOfWork.DraftChangeApplicationMongoRepository.CreateAsync(changeApplicationDto);



                XmlSerializer xmlSerializer = new XmlSerializer(typeof(ChangeApplicationDto));
                using (var textWriter = new Utf8StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, changeApplicationDto);
                    response.Data = new { dataToSign = textWriter.ToString(), signId = changeApplicationDto.ExecuteSignId };
                }
                response.Status = "Ok";
                response.Text = "Данные для подписи";

            }
            catch (Exception e)
            {
                response.Status = "Error";
                _logger.LogError($"Не удалось сериализовать по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            return response;
        }

        [HttpPost("SaveSignDataExecute")]
        public async Task<JsonResponse> SaveSignDataExecute([FromBody] SignedDoc signedDoc)
        {
            var response = new JsonResponse
            {
                Text = "Не удалось подписать и сохранить",
                Action = Constants.ACTIONS.OK,
            };
            var nsiValues = await _UnitOfWork.NsiValuesRepository
                    .GetAsIQueryable(v => v.Nsi.Code == "nsi022" && v.Deleted == null).Include(v => v.Nsi)
                    .ToListAsync();
            using (var transaction = _UnitOfWork.BeginTransaction())
            {
                try
                {
                   
                    if (!GetUserRoles().Any(x => x == "administrator" || x == "Национальный оператор"))
                        throw new Exception("Данные запрещены");
                    
                    signedDoc.CreateDate = DateTime.Now;
                    var isSuccess = await _UnitOfWork.SignedDocRepository.InsertAsync(signedDoc);
                    var data = _UnitOfWork.DraftChangeApplicationMongoRepository.GetAsync(new BsonDocument("ExecuteSignId", new BsonDocument("$eq", signedDoc.DocId))).GetAwaiter().GetResult().FirstOrDefault();
                    if (data == null) throw new Exception("Документ не найден!");
                    var orig = _UnitOfWork.ChangeApplicationRepository.GetAsIQueryable(v => v.Deleted == null && v.SignId == data.SignId).AsNoTracking()
                    .Include(v => v.StatusNsi)
                    .Include(v => v.ChangeStatusNsi)
                    .Include(v => v.Files)
                    .Include(v => v.Fields)
                    .FirstOrDefault(); ;
                    if (orig == null) return response;

                    var doc = _mapper.Map<ChangeApplication>(data);

                    doc.Id = orig.Id;
                    if (data.ChangeStatusNsiCode != "" && data.ChangeStatusNsiCode != null)
                        doc.ChangeStatusNsi = nsiValues.FirstOrDefault(x => x.Code == data.ChangeStatusNsiCode);
                    if (data.DeclineDescription == null)
                    {
                        doc.StatusNsi = nsiValues.FirstOrDefault(x => x.Code == "05");
                    }
                    else
                        doc.StatusNsi = nsiValues.FirstOrDefault(x => x.Code == "10");
                    doc.Fields = null;
                    doc.Files = null;
                    var save = _UnitOfWork.ChangeApplicationRepository.Update(doc);
                    _UnitOfWork.DraftChangeApplicationMongoRepository.RemoveFirst(new BsonDocument("SignId", new BsonDocument("$eq", doc.SignId)));
                    await _UnitOfWork.SaveAsync();
                    response.Data = signedDoc;
                    _UnitOfWork.Commit();
                    response.Text = "Подписано и сохранено УСПЕШНО!";
                    response.Status = "Ok";
                }
                catch (Exception e)
                {
                    _logger.LogError($"Не удалось сохранить по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
                    _UnitOfWork.Rollback();
                    response.Text = e.Message;
                    response.Status = "Error";
                }
            }

            return response;
        }
    }
}

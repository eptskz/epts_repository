﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.DTOs.Administration;
using DBCore.DTOs.Administration.Support;
using DBCore.Models;
using DBCore.Models.Administration.Contract;
using DBCore.Models.Administration.News;
using DBCore.Models.Administration.Support;
using DBCore.Models.Dictionaries;
using DBCore.Permissions;
using EptsBL.Extensions;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EptsProject.Controllers.Administration
{
    /// <summary>
    /// Новости
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewsController : MainController
    {
        private readonly NLog.ILogger _nlogger;
        private readonly RoleManager<AspNetRoles> _roleManager;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        /// <param name="roleManager"></param>
        public NewsController(ILogger<SupportController> logger
          , IUnitOfWork unitOfWork
          , UserManager<AspNetUsers> userManager
          , IMapper mapper
          , RoleManager<AspNetRoles> roleManager) : base(logger, unitOfWork, userManager, mapper)
        {
            this._nlogger = NLog.LogManager.GetCurrentClassLogger();
            _roleManager = roleManager;
        }

        /// <summary>
        /// Получить Новости
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("news")]
        public async Task<JsonResponse> GetNews([FromQuery] PagedSortRequestDto<GeneralFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
           
            try
            {
                var currentUser = await _userManager.GetUserAsync(User);

                var q = _UnitOfWork.NewsRepository.GetAsIQueryable();

                if (request.Filter == null)
                {
                    q = q.Where(v => false);
                }
                else
                {
                    if (!request.Filter.IsShowDeleted)
                    {
                        q = q.Where(v => v.Deleted == null);
                    }

                    if (!string.IsNullOrEmpty(request.Filter.Search))
                    {
                        q = q.Where(v => v.TitleRu.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.TitleRu.ToLower().Contains(request.Filter.Search.ToLower())

                                        || v.ContentRu.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.ContentRu.ToLower().Contains(request.Filter.Search.ToLower())

                                        || v.TitleKz.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.TitleKz.ToLower().Contains(request.Filter.Search.ToLower())

                                        || v.ContentKz.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.ContentKz.ToLower().Contains(request.Filter.Search.ToLower())
                                    );
                    }
                }

                if (request.SortField != null && request.SortField.Length > 0)
                    q = q.OrderByCommonFields(request.SortField, request.Direction);
                else
                    q = q.OrderByDescending(v => v.Created).ThenBy(v => v.TitleRu);

                var total = await q.CountAsync();
                var pageSize = request.PageSize == 0 ? total : request.PageSize;
                var news = await q.Skip(request.Page * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();
                response.Data = new
                {
                    Data = _mapper.Map<ICollection<NewsDto>>(news),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNews");
                response.Text = e.Message;
                response.Status = "Error";
            }
            
            return response;
        }

        /// <summary>
        /// Получить новость по ИД
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("news/{id}")]
        public async Task<JsonResponse> GetNewsById([FromRoute] Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var news = await _UnitOfWork.NewsRepository.GetAsIQueryable(v => v.Id == id)
                    .FirstOrDefaultAsync();
                response.Data = _mapper.Map<NewsDto>(news);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNewsById");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Получить новость
        /// </summary>
        /// <param name="newsDto"></param>
        /// <returns></returns>
        [HttpPost("news")]
        public async Task<JsonResponse> AddNews(NewsDto newsDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var news = _mapper.Map<News>(newsDto);
                news.Created = DateTime.Now;

                var currentUser = await _userManager.GetUserAsync(User);
                var userProfile = _UnitOfWork.UserProfileRepository.GetByID(currentUser.Id);
                news.AuthorId = currentUser.Id;
                news.AuthorName = userProfile.LastName + " " + userProfile.FirstName;

                await _UnitOfWork.NewsRepository.InsertAsync(news);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AddNews");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Обновить новость
        /// </summary>
        /// <param name="newsDto"></param>
        /// <returns></returns>
        [HttpPut("news")]
        public async Task<JsonResponse> UpdateNews(NewsDto newsDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var news = _mapper.Map<News>(newsDto);
                news.Modified = DateTime.Now;

                _UnitOfWork.NewsRepository.Update(news);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "UpdateNews");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// удалить договор
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("news")]
        public async Task<JsonResponse> DeleteNews(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var news = _UnitOfWork.NewsRepository.GetByID(id);
                news.Deleted = DateTime.Now;
                _UnitOfWork.NewsRepository.Update(news);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeleteNews");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
    }
}

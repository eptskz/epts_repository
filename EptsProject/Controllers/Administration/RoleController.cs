﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBCore;
using DBCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EptsProject.Core;
using EptsProject.Models;
using System.Text.Json;
using Newtonsoft.Json;
using DBCore.DTOs.Administration;
using DBCore.DTOs;
using Microsoft.EntityFrameworkCore;
using EptsBL.Extensions;
using AutoMapper;

namespace EptsProject.Controllers
{
    /// <summary>
    /// Роли
    /// </summary>    
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class RoleController : MainController
    {
        private RoleManager<AspNetRoles> _roleManager;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        /// <param name="roleManager"></param>
        public RoleController(ILogger<RoleController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , IMapper mapper
            , RoleManager<AspNetRoles> roleManager) : base(logger, unitOfWork, userManager, mapper)
        {
            _roleManager = roleManager;
        }

        /// <summary>
        /// Создать Роль
        /// </summary>
        /// <param name="roleDto"></param>
        /// <returns></returns>
        [HttpPost("createRole")]
        public async Task<JsonResponse> CreateRole([FromBody] RoleDto roleDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                AspNetRoles role = this._mapper.Map<AspNetRoles>(roleDto);
                IdentityResult identityResult = await _roleManager.CreateAsync(role);
                if(!identityResult.Succeeded)
                {
                    response.Status = "ERROR";
                    response.Text = identityResult.Errors.FirstOrDefault()?.Description;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CreateRole");
                response.Status = "ERROR";
                response.Text = e.Message;
            }
            return response;
        }

        /// <summary>
        /// Изменить Роль
        /// </summary>
        /// <param name="roleDto"></param>
        /// <returns></returns>
        [HttpPut("updateRole")]
        public async Task<JsonResponse> UpdateRole([FromBody] RoleDto roleDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            
            try
            {
                var role = await _roleManager.FindByIdAsync(roleDto.Id.ToString());
                role.Name = roleDto.Name;
                role.Description = roleDto.Description;
                IdentityResult identityResult = await _roleManager.UpdateAsync(role);
                if (!identityResult.Succeeded)
                {
                    response.Status = "ERROR";
                    response.Text = identityResult.Errors.FirstOrDefault()?.Description;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "UpdateRole");
                response.Status = "ERROR";
                response.Text = e.Message;
            }
            return response;
        }

        /// <summary>
        /// Удалить Роль
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpDelete("deleteRole")]
        public async Task<JsonResponse> DeleteRole(string roleId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            
            try
            {
                var role = await _roleManager.FindByIdAsync(roleId);
                IdentityResult identityResult = await _roleManager.DeleteAsync(role);
                if (!identityResult.Succeeded)
                {
                    response.Status = "ERROR";
                    response.Text = identityResult.Errors.FirstOrDefault()?.Description;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeleteRole");
                response.Status = "ERROR";
                response.Text = e.Message;
            }
            return response;
        }

        /// <summary>
        /// Получить список Ролей
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("getRoleList")]
        public async Task<JsonResponse> GetRoleList([FromBody]PagedSortRequestDto<RoleFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var q = _roleManager.Roles;
                
                if (request.Filter != null && string.IsNullOrEmpty(request.Filter.Search))
                {
                    q = q.Where(u => u.NormalizedName.Contains(request.Filter.Search.ToUpper()));
                }
                var total = await q.CountAsync();
                
                if (request.PageSize > 0)
                    q = q.Skip(request.Page * request.PageSize).Take(request.PageSize);
                
                if (request.SortField != null && request.SortField.Length > 0)
                    q = q.OrderByCommonFields(request.SortField, request.Direction);
                else
                    q = q.OrderBy(r => r.Name);

                var entities = await q.ToListAsync();
                response.Data = new
                {
                    Data = _mapper.Map<ICollection<RoleDto>>(entities),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetRoleList");
            }
            return response;
        }

        /// <summary>
        /// Получить Роль
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("getRole")]
        public async Task<JsonResponse> GetRole(string roleId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var role = await _roleManager.FindByIdAsync(roleId);
                response.Data = _mapper.Map<RoleDto>(role);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetRole");
                response.Status = "Error";
                response.Text = e.Message;
            }
            return response;
        }


        /// <summary>
        /// Получить список для внешнего источника
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("getRoleForExternalList")]
        public async Task<JsonResponse> GetRoleForExternalList()
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var q = _roleManager.Roles.Where(r => r.Name != RoleCodes.Administrator
                    && r.Name != RoleCodes.ExternalUser
                ).OrderBy(r => r.Name);
                var entities = await q.ToListAsync();
                response.Data = _mapper.Map<ICollection<RoleDto>>(entities);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "getRoleForExternalList");
            }
            return response;
        }

    }
}

﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.DTOs.Administration;
using DBCore.DTOs.Administration.Support;
using DBCore.Models;
using DBCore.Models.Administration.Support;
using DBCore.Models.Dictionaries;
using DBCore.Permissions;
using EptsBL.Extensions;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EptsProject.Controllers.Administration
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class SupportController : MainController
    {
        private readonly NLog.ILogger _nlogger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<AspNetUsers> _userManager;
        private readonly IMapper _mapper;
        private readonly RoleManager<AspNetRoles> _roleManager;

        public SupportController(ILogger<SupportController> logger
          , IUnitOfWork unitOfWork
          , UserManager<AspNetUsers> userManager
          , IMapper mapper
          , RoleManager<AspNetRoles> roleManager) : base(logger, unitOfWork, userManager, mapper)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            this._nlogger = NLog.LogManager.GetCurrentClassLogger();
            _roleManager = roleManager;
        }

        /// <summary>
        /// Получить тикеты
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("tickets")]
        public async Task<JsonResponse> GetTickets([FromQuery] PagedSortRequestDto<SupportFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
           
            try
            {
                var currentUser = await _userManager.GetUserAsync(User);
                var canProcess = User.HasClaim(c => c.Type == CustomClaimTypes.Permissions && c.Value == AdministrationPermissions.SupportTicket.Processing);

                var q = _UnitOfWork.SupportTicketRepository.GetAsIQueryable();

                if (!canProcess) {
                    q = q.Where(v => v.AuthorId == currentUser.Id.ToString());
                }

                if (request.Filter == null)
                {
                    q = q.Where(v => false);
                }
                else
                {
                    if (!request.Filter.IsShowDeleted)
                    {
                        q = q.Where(v => v.Deleted == null);
                    }

                    if (!string.IsNullOrEmpty(request.Filter.Search))
                    {
                        q = q.Where(v => v.Title.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.Title.ToLower().Contains(request.Filter.Search.ToLower())

                                        || v.Description.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.Description.ToLower().Contains(request.Filter.Search.ToLower())
                                    );
                    }

                    if (request.Filter.Category != null)
                    {
                        q = q.Where(v => v.Category == request.Filter.Category);
                    }
                }                

                if (request.SortField != null && request.SortField.Length > 0)
                    q = q.OrderByCommonFields(request.SortField, request.Direction);
                else
                    q = q.OrderByDescending(v => v.CreateDate).ThenBy(v => v.Title);

                var total = await q.CountAsync();
                var pageSize = request.PageSize == 0 ? total : request.PageSize;
                var nsiValues = await q.Include(v => v.Status)
                                    .Skip(request.Page * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();
                response.Data = new
                {
                    Data = _mapper.Map<ICollection<SupportTicketReestrDto>>(nsiValues),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNsiValue");
                response.Text = e.Message;
                response.Status = "Error";
            }
            
            return response;
        }

        /// <summary>
        /// Получить тикет по ИД
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("ticket/{id}")]
        public async Task<JsonResponse> GetTicket([FromRoute] Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var ticket = await _UnitOfWork.SupportTicketRepository.GetAsIQueryable(v => v.Id == id)
                    .Include(t => t.Status)
                    .Include(t => t.Comments)
                    .FirstOrDefaultAsync();
                response.Data = _mapper.Map<SupportTicketDto>(ticket);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetTicket");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Создать тикет
        /// </summary>
        /// <param name="ticketDto"></param>
        /// <returns></returns>
        [HttpPost("ticket")]
        public async Task<JsonResponse> AddTicket(SupportTicketDto ticketDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var ticket = _mapper.Map<SupportTicket>(ticketDto);
                ticket.CreateDate = DateTime.Now;
                var currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

                var countTickets = _unitOfWork.SupportTicketRepository.GetAsIQueryable(s => s.CreateDate > currentDate).Count();
                ticket.Number = DateTime.Now.Year 
                    + DateTime.Now.Month.ToString().PadLeft(2, '0') 
                    + DateTime.Now.Day.ToString().PadLeft(2, '0') 
                    + "-" + countTickets.ToString().PadLeft(4, '0');
                var status = _UnitOfWork.DicStatusesRepository
                    .GetAsIQueryable(ds => ds.Code == DictionaryCodes.StatusCodes.New
                            && ds.Group == DictionaryCodes.StatusCodes.Groups.SupportTicket)
                    .FirstOrDefault();
                ticket.StatusId = status.Id;

                var currentUser = await _userManager.GetUserAsync(User);
                var userProfile = _unitOfWork.UserProfileRepository.GetByID(currentUser.Id);
                ticket.AuthorId = currentUser.Id.ToString();
                ticket.AuthorName = userProfile.LastName + " " + userProfile.FirstName;

                await _UnitOfWork.SupportTicketRepository.InsertAsync(ticket);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AddTicket");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Обновить тикет
        /// </summary>
        /// <param name="ticketDto"></param>
        /// <returns></returns>
        [HttpPut("ticket")]
        public async Task<JsonResponse> UpdatTicket(SupportTicketDto ticketDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var ticket = _mapper.Map<SupportTicket>(ticketDto);
                ticket.Modified = DateTime.Now;

                var status = _UnitOfWork.DicStatusesRepository
                    .GetAsIQueryable(ds => ds.Code.ToLower() == ticketDto.StatusCode.ToLower()
                            && ds.Group == DictionaryCodes.StatusCodes.Groups.SupportTicket)
                    .FirstOrDefault();
                ticket.StatusId = status.Id;

                _UnitOfWork.SupportTicketRepository.Update(ticket);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "UpdatTicket");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// удалить тикет
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("ticket")]
        public async Task<JsonResponse> DeletTicket(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var ticket = _UnitOfWork.SupportTicketRepository.GetByID(id);
                ticket.Deleted = DateTime.Now;
                _UnitOfWork.SupportTicketRepository.Update(ticket);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeletTicket");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }



        /// <summary>
        /// Добавить комментарий
        /// </summary>
        /// <param name="commentDto"></param>
        /// <returns></returns>
        [HttpPost("ticket/comment")]
        public async Task<JsonResponse> AddTicketComment([FromBody] SupportTicketCommentDto commentDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var comment = _mapper.Map<SupportTicketComment>(commentDto);
                comment.CreateDate = DateTime.Now;

                var currentUser = await _userManager.GetUserAsync(User);
                comment.AuthorId = currentUser.Id.ToString();

                var userProfile = _unitOfWork.UserProfileRepository.GetByID(currentUser.Id);
                comment.AuthorName = userProfile.LastName + " " + userProfile.FirstName;

                await _UnitOfWork.SupportTicketCommentRepository.InsertAsync(comment);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AddTicketComment");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }


        /// <summary>
        /// удалить comment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("ticket/comment")]
        public async Task<JsonResponse> DeletTicketComment(int id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var comment = _UnitOfWork.SupportTicketCommentRepository.GetByID(id);
                comment.Deleted = DateTime.Now;
                _UnitOfWork.SupportTicketCommentRepository.Update(comment);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeletTicketComment");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
    }
}

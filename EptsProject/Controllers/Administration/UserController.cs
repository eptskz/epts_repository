﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBCore;
using DBCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EptsProject.Core;
using EptsProject.Models;
using System.Text.Json;
using Newtonsoft.Json;
using DBCore.DTOs.Administration;
using DBCore.DTOs;
using Microsoft.EntityFrameworkCore;
using EptsBL.Extensions;
using AutoMapper;
using DBCore.Models.Dictionaries;
using DbLog;
using EptsProject.Helpers;

namespace EptsProject.Controllers
{
    /// <summary>
    /// Пользователи
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : MainController
    {
        private RoleManager<AspNetRoles> _roleManager;
        ILogUnitOfWork _logUoW;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        /// <param name="logUoW"></param>
        /// <param name="roleManager"></param>
        public UserController(ILogger<AccountController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , RoleManager<AspNetRoles> roleManager
            , IMapper mapper
            , ILogUnitOfWork logUoW) : base(logger, unitOfWork, userManager, mapper)
        {
            _roleManager = roleManager;
            _logUoW = logUoW;
        }

        /// <summary>
        /// Создать Пользователя
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        [HttpPost("createUser")]
        public async Task<JsonResponse> CreateUser([FromBody] UserDto userDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                AspNetUsers user = new AspNetUsers {
                    UserName = userDto.UserName
                    , PhoneNumber = userDto.PhoneNumber
                    , Email = userDto.Email
                    , OrganizationId = userDto.OrganizationId 
                };
                IdentityResult identityResult = await _userManager.CreateAsync(user, userDto.Password);
                
                if (identityResult.Succeeded)
                {
                    var userLog = new DbLog.Models.UserLog()
                    {
                        EventDate = DateTime.Now,
                        IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                        UserName = User.Identity.Name,
                        Message = "Создание пользователя",
                        AdditionalInfo = JsonConvert.SerializeObject(user, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        })
                    };
                    await _logUoW.LogUserInfo(userLog);
                    _logger.LogInformation("Создание пользователя: {0}. Автор: {1}. IpAddress: {2}", userLog.AdditionalInfo, userLog.UserName, userLog.IpAddress);

                    foreach (UserRoleDto userRole in userDto.UserRoles)
                    {
                        userRole.UserId = user.Id;
                        if (await _UnitOfWork.UserRolesRepository
                            .GetAsIQueryable(ur => ur.RoleId == userRole.RoleId && ur.UserId == userRole.UserId)
                            .AnyAsync())
                        {
                            if (userRole.State == DBCore.Enums.ObjectStateEnum.Deleted)
                            {
                                _UnitOfWork.UserRolesRepository.Delete(new AspNetUserRoles()
                                {
                                    UserId = userRole.UserId,
                                    RoleId = userRole.RoleId,
                                });
                            }
                        }
                        else
                        {
                            if (userRole.State == DBCore.Enums.ObjectStateEnum.Added)
                            {
                                await _UnitOfWork.UserRolesRepository.InsertAsync(new AspNetUserRoles()
                                {
                                    UserId = userRole.UserId,
                                    RoleId = userRole.RoleId,
                                });
                            }
                        }
                    }

                    await _UnitOfWork.UserProfileRepository.InsertAsync(new UserProfile()
                    {
                        Id = user.Id,
                        CreateDate = DateTime.Now,
                        LastName = userDto.LastName,
                        FirstName = userDto.FirstName,
                        SecondName = userDto.SecondName,
                        StatusId = userDto.StatusId,
                    });
                    await _UnitOfWork.SaveAsync();
                }
                else
                {
                    response.Status = "ERROR";
                    response.Text = identityResult.Errors.First().Description;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CreateUser");

                response.Status = "ERROR";
                response.Text = e.Message;
            }
            return response;
        }

        /// <summary>
        /// Изменить Пользователя
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        [HttpPut("updateUser")]
        public async Task<JsonResponse> UpdateUser([FromBody] UserDto userDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            
            try
            {
                var user = await _userManager.Users.Include(u => u.AspNetUserRoles)
                    .FirstOrDefaultAsync(u => u.Id == userDto.Id);
                user.PhoneNumber = userDto.PhoneNumber;
                user.Email = userDto.Email;
                user.OrganizationId = userDto.OrganizationId;

                foreach (UserRoleDto userRole in userDto.UserRoles)
                {
                    var ur = user.AspNetUserRoles.FirstOrDefault(ur => ur.RoleId == userRole.RoleId && ur.UserId == userRole.UserId);
                    if (ur != null && userRole.State == DBCore.Enums.ObjectStateEnum.Deleted)
                    {
                        user.AspNetUserRoles.Remove(ur);
                    }
                    else if (userRole.State == DBCore.Enums.ObjectStateEnum.Added)
                    {
                        user.AspNetUserRoles.Add(new AspNetUserRoles() { 
                            RoleId = userRole.RoleId
                        });
                    }
                }

                IdentityResult identityResult = await _userManager.UpdateAsync(user);

                if (identityResult.Succeeded)
                {
                    var userLog = new DbLog.Models.UserLog()
                    {
                        EventDate = DateTime.Now,
                        IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                        UserName = User.Identity.Name,
                        Message = "Изменение пользователя",
                        AdditionalInfo = JsonConvert.SerializeObject(user, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        })
                    };
                    await _logUoW.LogUserInfo(userLog);
                    _logger.LogInformation("Изменение пользователя: {0}. Автор: {1}. IpAddress: {2}", userLog.AdditionalInfo, userLog.UserName, userLog.IpAddress);

                    if (!string.IsNullOrEmpty(userDto.Password) && !string.IsNullOrEmpty(userDto.OldPassword))
                    {
                        if (User.IsInRole("administrator"))
                        {
                            var resetToken = await _userManager.GeneratePasswordResetTokenAsync(user);
                            var resetResult = await _userManager.ResetPasswordAsync(user, resetToken, userDto.Password);
                            if (!resetResult.Succeeded)
                            {
                                response.Status = "Error";
                                response.Text = resetResult.Errors.First().Description;
                                return response;
                            }
                        }
                        else
                        {
                            var changeResult = await _userManager.ChangePasswordAsync(user, userDto.OldPassword, userDto.Password);
                            if (!changeResult.Succeeded)
                            {
                                response.Status = "Error";
                                response.Text = changeResult.Errors.First().Description;
                                return response;
                            }
                        }
                    }

                    var userProfile = _UnitOfWork.UserProfileRepository.GetAsIQueryable(p => p.Id == user.Id).FirstOrDefault();
                    if (userProfile != null)
                    {
                        userProfile.ModifyDate = DateTime.Now;
                        userProfile.LastName = userDto.LastName;
                        userProfile.FirstName = userDto.FirstName;
                        userProfile.SecondName = userDto.SecondName;
                        userProfile.StatusId = userDto.StatusId;

                        _UnitOfWork.UserProfileRepository.Update(userProfile);
                    } 
                    else
                    {
                        await _UnitOfWork.UserProfileRepository.InsertAsync(new UserProfile()
                        {
                            Id = user.Id,
                            CreateDate = DateTime.Now,
                            ModifyDate = DateTime.Now,
                            LastName = userDto.LastName,
                            FirstName = userDto.FirstName,
                            SecondName = userDto.SecondName,
                            StatusId = userDto.StatusId,
                        });
                    }
                    
                    await _UnitOfWork.SaveAsync();
                }
                else
                {
                    response.Status = "ERROR";
                    response.Text = identityResult.Errors.First().Description;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "UpdateUser");
                response.Status = "ERROR";
                response.Text = e.Message;
            }
            return response;
        }

        /// <summary>
        /// Удалить Пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete("deleteUser")]
        public async Task<JsonResponse> DeleteUser(string userId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {              
                var userProfile = _UnitOfWork.UserProfileRepository
                    .GetAsIQueryable(p => p.Id == Guid.Parse(userId))
                    .FirstOrDefault();
                if (userProfile != null)
                {
                    userProfile.Deleted = DateTime.Now;
                    userProfile.StatusId = _UnitOfWork.DicStatusesRepository
                        .GetAsIQueryable(d => d.Code == DictionaryCodes.StatusCodes.Deleted
                            && d.Group == DictionaryCodes.StatusCodes.Groups.User)
                        .FirstOrDefault().Id;

                    _UnitOfWork.UserProfileRepository.Update(userProfile);
                    await _UnitOfWork.SaveAsync();
                }
                else
                {
                    var user = await _userManager.FindByIdAsync(userId);
                    IdentityResult identityResult = await _userManager.DeleteAsync(user);
                    if (!identityResult.Succeeded)
                    {
                        response.Status = "Error";
                        response.Text = identityResult.Errors.Count() > 0 ? identityResult.Errors.First().Description : "";
                    }
                }
                var userLog = new DbLog.Models.UserLog()
                {
                    EventDate = DateTime.Now,
                    IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                    UserName = User.Identity.Name,
                    Message = "Удаление пользователя",
                    AdditionalInfo = JsonConvert.SerializeObject(userProfile, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    })
                };
                await _logUoW.LogUserInfo(userLog);
                _logger.LogInformation("Удаление пользователя: {0}. Автор: {1}. IpAddress: {2}", userLog.AdditionalInfo, userLog.UserName, userLog.IpAddress);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeleteUser");
                response.Status = "Error";
                response.Text = e.Message;
            }
            return response;
        }

        /// <summary>
        /// Получить список Пользователей
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("getUserList")]
        public async Task<JsonResponse> GetUserList([FromBody]PagedSortRequestDto<UserFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var q = _userManager.Users.AsQueryable();

                if (request.Filter.Type == "arm")
                {
                    q = q.Where(u => !u.AspNetUserRoles.Any(ur => ur.Role.Name == RoleCodes.ExternalUser));
                } 
                else if (request.Filter.Type == "lk")
                {
                    q = q.Where(u => u.AspNetUserRoles.Any(ur => ur.Role.Name == RoleCodes.ExternalUser));
                }

                //if (!request.Filter.IsShowDeleted)
                //{
                //    q = q.Where(u => u.UserProfile == null || u.UserProfile.Deleted == null);
                //}
                if (!string.IsNullOrEmpty(request.Filter.Search))
                {
                    q = q.Where(u => u.UserName.Contains(request.Filter.Search)
                        || u.UserProfile.LastName.Contains(request.Filter.Search)
                        || u.UserProfile.FirstName.Contains(request.Filter.Search)
                        || u.UserProfile.SecondName.Contains(request.Filter.Search)
                    );
                }
                if (request.SortField != null && request.SortField.Length > 0)
                    q = q.OrderByCommonFields(request.SortField, request.Direction);
                else
                    q = q.OrderByDescending(u => u.UserProfile.CreateDate);

                var total = await q.CountAsync();

                if (request.PageSize > 0)
                    q = q.Skip(request.Page * request.PageSize).Take(request.PageSize);

                var sql = q.ToSql();

                var entities = await q
                    .Include(u => u.UserProfile)
                        .ThenInclude(p => p.Status)
                    .Include(p => p.Organization)
                    .ToListAsync();

                response.Data = new
                {
                    Data = _mapper.Map<ICollection<UserDto>>(entities),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetUserList");
            }
            return response;
        }

        /// <summary>
        /// Получить Пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("getUser")]
        public async Task<JsonResponse> GetUser(Guid userId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var user = await _userManager.Users
                    .Include(u => u.UserProfile)
                        .ThenInclude(u => u.Status)
                    .Include(u => u.Organization)
                    .Include(u => u.AspNetUserRoles)
                        .ThenInclude(u => u.Role)
                    .FirstOrDefaultAsync(u => u.Id == userId);
                response.Data = _mapper.Map<UserDto>(user);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetUser");
                response.Status = "Error";
                response.Text = e.Message;
            }
            return response;
        }


        /// <summary>
        /// Создать Пользователя
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        [HttpPost("addPermission")]
        public async Task<JsonResponse> AddPermission([FromBody] UserDto userDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
               
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CreateUser");

                response.Status = "ERROR";
                response.Text = e.Message;
            }
            return response;
        }
    }
}
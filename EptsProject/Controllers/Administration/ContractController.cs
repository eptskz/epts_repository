﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.DTOs.Administration;
using DBCore.DTOs.Administration.Support;
using DBCore.Models;
using DBCore.Models.Administration.Contract;
using DBCore.Models.Dictionaries;
using DBCore.Permissions;
using EptsBL.Extensions;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EptsProject.Controllers.Administration
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]    
    public class ContractController : MainController
    {
        private readonly NLog.ILogger _nlogger;
        private readonly RoleManager<AspNetRoles> _roleManager;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        /// <param name="roleManager"></param>
        public ContractController(ILogger<SupportController> logger
          , IUnitOfWork unitOfWork
          , UserManager<AspNetUsers> userManager
          , IMapper mapper
          , RoleManager<AspNetRoles> roleManager) : base(logger, unitOfWork, userManager, mapper)
        {
            this._nlogger = NLog.LogManager.GetCurrentClassLogger();
            _roleManager = roleManager;
        }

        /// <summary>
        /// Получить контракты
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("contracts")]
        public async Task<JsonResponse> GetContracts([FromQuery] PagedSortRequestDto<ContractFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
           
            try
            {
                var currentUser = await _userManager.GetUserAsync(User);                

                var q = _UnitOfWork.ContractRepository.GetAsIQueryable();

                if (request.Filter == null 
                    || !User.Claims.Any(c => c.Type == CustomClaimTypes.Permissions 
                                     && c.Value == AdministrationPermissions.Contract.Read))
                {
                    q = q.Where(v => false);
                }
                else
                {
                    if (!request.Filter.IsShowDeleted)
                    {
                        q = q.Where(v => v.Deleted == null);
                    }

                    if (!string.IsNullOrEmpty(request.Filter.Search))
                    {
                        q = q.Where(v => v.Number.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.Number.ToLower().Contains(request.Filter.Search.ToLower())

                                        || v.Description.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.Description.ToLower().Contains(request.Filter.Search.ToLower())
                                    );
                    }
                }

                if (request.SortField != null && request.SortField.Length > 0)
                    q = q.OrderByCommonFields(request.SortField, request.Direction);
                else
                    q = q.OrderByDescending(v => v.Created).ThenBy(v => v.Number);

                var total = await q.CountAsync();
                var pageSize = request.PageSize == 0 ? total : request.PageSize;
                var contracts = await q.Include(v => v.Status)
                                    .Include(v => v.ClientOrg)
                                    .Skip(request.Page * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();
                response.Data = new
                {
                    Data = _mapper.Map<ICollection<ContractDto>>(contracts),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetContracts");
                response.Text = e.Message;
                response.Status = "Error";
            }
            
            return response;
        }

        /// <summary>
        /// Получить контракт по ИД
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("contract/{id}")]
        public async Task<JsonResponse> GetContract([FromRoute] Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var ticket = await _UnitOfWork.ContractRepository.GetAsIQueryable(v => v.Id == id)
                    .Include(t => t.Status)
                    .Include(t => t.ClientOrg)
                    .FirstOrDefaultAsync();
                response.Data = _mapper.Map<ContractDto>(ticket);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetContract");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Создать договор
        /// </summary>
        /// <param name="contractDto"></param>
        /// <returns></returns>
        [HttpPost("contract")]
        public async Task<JsonResponse> AddContract(ContractDto contractDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var contract = _mapper.Map<Contract>(contractDto);
                contract.Created = DateTime.Now;
                //var currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

                /*var countContracts = _unitOfWork.ContractRepository.GetAsIQueryable(s => s.Created > currentDate).Count();
                contract.Number = DateTime.Now.Year 
                    + DateTime.Now.Month.ToString().PadLeft(2, '0') 
                    + DateTime.Now.Day.ToString().PadLeft(2, '0') 
                    + "-" + countContracts.ToString().PadLeft(4, '0');
                */

                var status = _UnitOfWork.DicStatusesRepository
                    .GetAsIQueryable(ds => ds.Code == DictionaryCodes.StatusCodes.New
                            && ds.Group == DictionaryCodes.StatusCodes.Groups.Contract)
                    .FirstOrDefault();
                contract.StatusId = status.Id;

                var currentUser = await _userManager.GetUserAsync(User);
                var userProfile = _UnitOfWork.UserProfileRepository.GetByID(currentUser.Id);
                contract.AuthorId = currentUser.Id;
                contract.AuthorName = userProfile.LastName + " " + userProfile.FirstName;

                await _UnitOfWork.ContractRepository.InsertAsync(contract);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AddContract");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Обновить договор
        /// </summary>
        /// <param name="contractDto"></param>
        /// <returns></returns>
        [HttpPut("contract")]
        public async Task<JsonResponse> UpdateContract(ContractDto contractDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var contract = _mapper.Map<Contract>(contractDto);
                contract.Modified = DateTime.Now;

                var status = _UnitOfWork.DicStatusesRepository
                    .GetAsIQueryable(ds => ds.Code.ToLower() == contractDto.StatusCode.ToLower()
                            && ds.Group == DictionaryCodes.StatusCodes.Groups.Contract)
                    .FirstOrDefault();
                contract.StatusId = status.Id;

                _UnitOfWork.ContractRepository.Update(contract);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "UpdateContract");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// удалить договор
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("contract")]
        public async Task<JsonResponse> DeleteContract(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var contract = _UnitOfWork.ContractRepository.GetByID(id);
                contract.Deleted = DateTime.Now;
                _UnitOfWork.ContractRepository.Update(contract);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeleteContract");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBCore;
using DBCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EptsProject.Core;
using EptsProject.Models;
using System.Text.Json;
using Newtonsoft.Json;
using DBCore.DTOs.Administration;
using DBCore.DTOs;
using Microsoft.EntityFrameworkCore;
using EptsBL.Extensions;
using AutoMapper;
using DBCore.Models.Dictionaries;
using DBCore.Permissions;
using System.ComponentModel;
using System.Security.Claims;

namespace EptsProject.Controllers
{
    /// <summary>
    /// Пользователи
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PermissionController : MainController
    {
        private RoleManager<AspNetRoles> _roleManager;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="userManager"></param>
        /// <param name="mapper"></param>
        /// <param name="roleManager"></param>
        public PermissionController(ILogger<AccountController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , RoleManager<AspNetRoles> roleManager
            , IMapper mapper) : base(logger, unitOfWork, userManager, mapper)
        {
            _roleManager = roleManager;
        }

        /// <summary>
        /// Получить Права
        /// </summary>
        /// <returns></returns>
        [HttpGet("getPermissions")]
        public JsonResponse GetPermissions()
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {                
                response.Data = getAllAvailablePermissions();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetPermissions");

                response.Status = "ERROR";
                response.Text = e.Message;
            }
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("getRolePermissions")]
        public async Task<JsonResponse> GetRolePermissions(string roleId)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var claimGroups = getAllAvailablePermissions();

                var role = await _roleManager.FindByIdAsync(roleId);
                var roleClaims = await _roleManager.GetClaimsAsync(role);

                foreach(var cg in claimGroups)
                    foreach (var p in cg.Permissions)
                    {
                        p.IsSelected = roleClaims.Any(rc => rc.Value == p.ClaimValue);
                        if (p.IsSelected)
                            p.ObjectId = role.Id;
                    }                
                response.Data = claimGroups;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetPermissions");

                response.Status = "ERROR";
                response.Text = e.Message;
            }
            return response;
        }

        private List<ObjectClaimGroupDto> getAllAvailablePermissions()
        {
            var permissionAssembly = typeof(AdministrationPermissions).Assembly;
            var objectPermissions = new List<ObjectClaimDto>();
            var claimGroups = new List<ObjectClaimGroupDto>();
            var permissionTypes = permissionAssembly.GetTypes().Where(t => t.IsClass && t.FullName.EndsWith("Permissions"));
            foreach (Type pt in permissionTypes)
            {
                var claimGroup = new ObjectClaimGroupDto();

                var groupAttrs = pt.GetCustomAttributes(true);
                foreach (object attr in groupAttrs)
                {
                    DescriptionAttribute descAttr = attr as DescriptionAttribute;
                    if (descAttr != null)
                    {
                        claimGroup.ClaimGroupName = descAttr.Description;
                        break;
                    }
                }

                foreach (Type npt in pt.GetNestedTypes())
                {
                    foreach (var fi in npt.GetFields())
                    {
                        var objectClaim = new ObjectClaimDto();

                        object[] attrs = fi.GetCustomAttributes(true);
                        foreach (object attr in attrs)
                        {
                            DescriptionAttribute descAttr = attr as DescriptionAttribute;
                            if (descAttr != null)
                            {
                                objectClaim.Description = descAttr.Description;
                                break;
                            }
                        }

                        objectClaim.ClaimValue = (string)fi.GetValue(null);
                        objectClaim.ObjectId = null;
                        claimGroup.Permissions.Add(objectClaim);
                    }
                }

                claimGroups.Add(claimGroup);
            }
            return claimGroups;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="claimDtos"></param>
        /// <returns></returns>
        [HttpPost("setPermissions")]
        public async Task<JsonResponse> SetPermissions([FromBody]ObjectClaimDto[] claimDtos)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            foreach(var claimDto in claimDtos)
            {
                if (claimDto.ObjectType == "role")
                {
                    if (claimDto != null && claimDto.ObjectId != null)
                    {
                        var role = await _roleManager.FindByIdAsync(claimDto.ObjectId.Value.ToString());
                        IdentityResult idnResult = null;
                        if (claimDto.IsSelected)
                        {
                            idnResult = await _roleManager.AddClaimAsync(role
                               , new Claim(CustomClaimTypes.Permissions, claimDto.ClaimValue));
                        }
                        else
                        {
                            var roleClaims = await _roleManager.GetClaimsAsync(role);
                            idnResult = await _roleManager.RemoveClaimAsync(role
                               , roleClaims.FirstOrDefault(rc => rc.Value == claimDto.ClaimValue));
                        } 

                        if (!idnResult.Succeeded)
                            response.Text = idnResult.Errors.FirstOrDefault().Description;
                    }
                }
                else if (claimDto.ObjectType == "user")
                {

                }
            }

            return response;
        }
    }
}
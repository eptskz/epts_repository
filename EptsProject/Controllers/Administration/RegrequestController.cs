﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBCore;
using DBCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EptsProject.Core;
using EptsProject.Models;
using System.Text.Json;
using Newtonsoft.Json;
using System.Security.Claims;
using EptsBL.Services.Interface;
using DBCore.DTOs;
using DBCore.DTOs.Administration;
using Microsoft.EntityFrameworkCore;
using EptsBL.Extensions;
using DBCore.Models.Administration;
using AutoMapper;
using DBCore.Models.Dictionaries;
using EptsProject.Helpers;
using DbLog;

namespace EptsProject.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class RegrequestController : MainController
    {
        RoleManager<AspNetRoles> _roleManager;
        ICertificateService _certificateService;
        ILogUnitOfWork _logUoW;
        public RegrequestController(ILogger<RegrequestController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , SignInManager<AspNetUsers> signInManager
            , RoleManager<AspNetRoles> roleManager
            , ICertificateService certificateService
            , IMapper mapper
            , ILogUnitOfWork logUoW) : base(logger, unitOfWork, userManager, signInManager)
        {
            _roleManager = roleManager;
            _certificateService = certificateService;
            _mapper = mapper;
            _logUoW = logUoW;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public async Task<JsonResponse> CreateAsync([FromBody] RegrequestDto requestModel)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                RegRequest request = this._mapper.Map<RegRequest>(requestModel);
                if (requestModel.UserType == "ul")
                {
                    request.Id = Guid.NewGuid();
                    request.Created = DateTime.Now;
                    request.Modified = DateTime.Now;
                    var newStatus = _UnitOfWork.DicStatusesRepository
                        .GetAsIQueryable(s => s.Code == DictionaryCodes.StatusCodes.New
                                && s.Group == DictionaryCodes.StatusCodes.Groups.RegRequest)
                        .FirstOrDefault();
                    if (newStatus != null)
                        request.StatusId = newStatus.Id;

                    bool isExistRequest = await _UnitOfWork.RegRequestRepository.GetAsIQueryable(rr => rr.OrganizationBin == request.OrganizationBin
                        && rr.UserName == request.UserName
                        && rr.Status.Code != DictionaryCodes.StatusCodes.Rejected).AnyAsync();

                    if (!isExistRequest)
                    {
                        bool isExistContract = _UnitOfWork.ContractRepository
                            .GetAsIQueryable(c => c.ClientOrg.Bin == request.OrganizationBin)
                            .Any();
                        request.IsExistContract = isExistContract;
                        await _UnitOfWork.RegRequestRepository.InsertAsync(request);
                        await _UnitOfWork.SaveAsync();
                    }
                }
                else
                {
                    AspNetUsers user = new AspNetUsers
                    {
                        UserName = request.UserName,
                        PhoneNumber = request.PhoneNumber,
                        Email = request.Email,
                        OrganizationId = null
                    };
                    IdentityResult identityResult = await _userManager.CreateAsync(user, requestModel.Password);

                    if (identityResult.Succeeded)
                    {
                        var userLog = new DbLog.Models.UserLog()
                        {
                            EventDate = DateTime.Now,
                            IpAddress = AppCustomContext.Current.GetRemoteIPAddress(true).ToString(),
                            UserName = User.Identity.Name,
                            Message = "Создание пользователя",
                            AdditionalInfo = JsonConvert.SerializeObject(user, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            })
                        };
                        await _logUoW.LogUserInfo(userLog);
                        _logger.LogInformation("Создание пользователя: {0}. Автор: {1}. IpAddress: {2}", userLog.AdditionalInfo, userLog.UserName, userLog.IpAddress);

                        var userRole = new UserRoleDto();
                        userRole.UserId = user.Id;

                        var exRole = _UnitOfWork.RolesRepository.GetAsIQueryable(r => r.Name == "externaluser").FirstOrDefault();
                        userRole.RoleId = exRole.Id;
                        await _UnitOfWork.UserRolesRepository.InsertAsync(new AspNetUserRoles()
                        {
                            UserId = userRole.UserId,
                            RoleId = userRole.RoleId,
                        });

                        var status = _UnitOfWork.DicStatusesRepository
                                .GetAsIQueryable(d => d.Code == DictionaryCodes.StatusCodes.New  
                                                    && d.Group == DictionaryCodes.StatusCodes.Groups.User)
                                .FirstOrDefault();

                        await _UnitOfWork.UserProfileRepository.InsertAsync(new UserProfile()
                        {
                            Id = user.Id,
                            CreateDate = DateTime.Now,
                            LastName = request.LastName,
                            FirstName = request.FirstName,
                            StatusId = status.Id
                        });
                        await _UnitOfWork.SaveAsync();
                    }
                    else
                    {
                        response.Status = "Error";
                        response.Text = identityResult.Errors.First().Description;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Regrequest CreateAsync");                
                response.Status = "Error";
                response.Text = e.Message;
            }
            return response;
        }

        [AllowAnonymous]
        [HttpGet("check-psw")]
        public async Task<JsonResponse> CheckPswAsync([FromQuery] string psw)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var isPswValid = false;
                foreach(var pv in this._userManager.PasswordValidators)
                {
                    isPswValid = isPswValid || (await pv.ValidateAsync(_userManager, null, psw)).Succeeded;
                }
                response.Data = isPswValid;

            }
            catch (Exception e)
            {
                _logger.LogError(e, "Regrequest CreateAsync");
                response.Status = "Error";
                response.Text = e.Message;
            }
            return response;
        }

        [HttpPost("update")]
        public async Task<JsonResponse> UpdateAsync([FromBody] RegrequestDto requestModel)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                RegRequest request = this._mapper.Map<RegRequest>(requestModel);

                request.Modified = DateTime.Now;
                var status = _UnitOfWork.DicStatusesRepository
                    .GetAsIQueryable(s => s.Code == requestModel.StatusCode
                            && s.Group == DictionaryCodes.StatusCodes.Groups.RegRequest)
                    .FirstOrDefault();
                if (status != null)
                {
                    request.StatusId = status.Id;
                    if (requestModel.StatusCode == DictionaryCodes.StatusCodes.Accepted)
                    {
                        response.Data = true;
                                                
                        var user = await _userManager.Users
                            .Include(u => u.UserProfile)
                            .FirstOrDefaultAsync(u => u.UserName == requestModel.UserName);

                        if (user != null)
                        {
                            await this.updateUserAsync(requestModel);
                        } 
                        else
                        {
                            await this.createUserAsync(requestModel);
                        }                        
                    }
                }

                _UnitOfWork.RegRequestRepository.Update(request);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CreateRole");
                response.Status = "Error";
                response.Text = e.Message;
            }
            return response;
        }

        [HttpPost("getList")]
        public async Task<JsonResponse> GetListAsync([FromBody] PagedSortRequestDto<RegrequestFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var q = _UnitOfWork.RegRequestRepository.GetAsIQueryable();
                if (!request.Filter.IsShowDeleted)
                {
                    q = q.Where(rr => rr.Deleted == null);
                }
                if (!string.IsNullOrEmpty(request.Filter.Search))
                {
                    q = q.Where(rr => rr.UserName.Contains(request.Filter.Search)
                        || rr.LastName.Contains(request.Filter.Search)
                        || rr.FirstName.Contains(request.Filter.Search)
                        || rr.OrganizationBin.Contains(request.Filter.Search)
                        || rr.OrganizationName.Contains(request.Filter.Search)
                    );
                }

                var total = await q.CountAsync();
                
                if (request.SortField != null && request.SortField.Length > 0)
                    q = q.OrderByCommonFields(request.SortField, request.Direction);
                else
                    q = q.OrderByDescending(d => d.Created);

                if (request.PageSize > 0)
                    q = q.Skip(request.Page * request.PageSize).Take(request.PageSize);

                var sql = q.ToSql();

                var entities = await q.Include(u => u.Status)
                    .ToListAsync();
                var outputData = _mapper.Map<ICollection<RegrequestDto>>(entities);
                foreach (var rr in outputData)
                {
                    if (rr.RoleId != null)
                    {
                        var role = await _roleManager.Roles.FirstOrDefaultAsync(r => r.Id == rr.RoleId);
                        rr.RoleName = role.Description;
                    }
                }

                response.Data = new
                {
                    Data = outputData,
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetListAsync");
            }
            return response;
        }

        private async Task createUserAsync(RegrequestDto request)
        {
            var existOrg = _UnitOfWork.OrganizationsRepository
                .GetAsIQueryable(org => org.Bin == request.OrganizationBin && org.Deleted == null)
                .FirstOrDefault();

            var userNewStatus =_UnitOfWork.DicStatusesRepository.GetAsIQueryable(ds => ds.Code == DictionaryCodes.StatusCodes.New
                && ds.Group == DictionaryCodes.StatusCodes.Groups.User).FirstOrDefault();

            if (existOrg == null)            
                throw new Exception("Организация не обнаружена");
            
            AspNetUsers user = new AspNetUsers
            {
                UserName = request.UserName
                , PhoneNumber = request.PhoneNumber
                , Email = request.Email
                , OrganizationId = existOrg.Id
            };
            IdentityResult identityResult = await _userManager.CreateAsync(user, request.Password);

            if (identityResult.Succeeded)
            {
                await _UnitOfWork.UserRolesRepository.InsertAsync(new AspNetUserRoles()
                {
                    UserId = user.Id,
                    RoleId = request.RoleId
                });

                await _UnitOfWork.UserProfileRepository.InsertAsync(new UserProfile()
                {
                    Id = user.Id,
                    CreateDate = DateTime.Now,
                    LastName = request.LastName,
                    FirstName = request.FirstName,
                    StatusId = userNewStatus.Id
                });
                await _UnitOfWork.SaveAsync();
            }
            else
            {
                throw new Exception(identityResult.Errors.First().Description);
            }
        }

        private async Task updateUserAsync(RegrequestDto request)
        {
            var existOrg = _UnitOfWork.OrganizationsRepository
                .GetAsIQueryable(org => org.Bin == request.OrganizationBin && org.Deleted == null)
                .FirstOrDefault();

            var userNewStatus = _UnitOfWork.DicStatusesRepository.GetAsIQueryable(ds => ds.Code == DictionaryCodes.StatusCodes.New
                 && ds.Group == DictionaryCodes.StatusCodes.Groups.User).FirstOrDefault();

            if (existOrg == null)
                throw new Exception("Организация не обнаружена");

            AspNetUsers user = await _userManager.Users
                            .Include(u => u.UserProfile)
                            .FirstOrDefaultAsync(u => u.UserName == request.UserName);
            user.PhoneNumber = request.PhoneNumber;
            user.Email = request.Email;
            user.OrganizationId = existOrg.Id;

            IdentityResult identityResult = await _userManager.UpdateAsync(user);

            if (identityResult.Succeeded)
            {                
                /*
                await _UnitOfWork.UserRolesRepository.InsertAsync(new AspNetUserRoles()
                {
                    UserId = user.Id,
                    RoleId = request.RoleId
                });
                */
                _UnitOfWork.UserProfileRepository.Update(new UserProfile()
                {
                    Id = user.Id,
                    CreateDate = DateTime.Now,
                    LastName = request.LastName,
                    FirstName = request.FirstName,
                    StatusId = userNewStatus.Id,
                    Deleted = null
                });
                await _UnitOfWork.SaveAsync();
            }
            else
            {
                throw new Exception(identityResult.Errors.First().Description);
            }
        }

    }
}

﻿using AutoMapper;
using DBCore;
using DBCore.Models;
using DBCore.Models.FileStorage;
using EptsBL.Services.Interface;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers.FileStorage
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class FileStorageController : ControllerBase
    {
        private readonly ILogger<FileStorageController> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<AspNetUsers> _userManager;
        private readonly IFileStorageService _fileStorage;
        private readonly IMapper _mapper;
        private readonly NLog.ILogger _nlogger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="userManager"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="fileStorage"></param>
        public FileStorageController(ILogger<FileStorageController> logger
            , UserManager<AspNetUsers> userManager
            , IUnitOfWork unitOfWork
            , IMapper mapper
            , IFileStorageService fileStorage)
        {
            this._unitOfWork = unitOfWork;
            this._logger = logger;
            this._userManager = userManager;
            this._fileStorage = fileStorage;
            this._mapper = mapper;

            this._nlogger = NLog.LogManager.GetCurrentClassLogger();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentType"></param>
        /// <returns></returns>
        [HttpGet("files/{documenttype}")]
        public async Task<ActionResult> GetFiles(string documentType, [FromQuery]string search)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            if (string.IsNullOrEmpty(documentType))
            {
                response.Status = "Error";
                response.Text = "Не указан тип документа";

                return Ok(response);
            }
            var q = _unitOfWork.FileStorageRepository
                .GetAsIQueryable(f => f.DocumentType.ToLower() == documentType.ToLower());

            if (!string.IsNullOrEmpty(search))
                q = q.Where(fs => fs.Title.ToLower().Contains(search)
                || fs.Title.ToLower().StartsWith(search)

                || fs.FileName.ToLower().StartsWith(search)
                || fs.FileName.ToLower().StartsWith(search)

                || fs.Description.ToLower().StartsWith(search)
                || fs.Description.ToLower().StartsWith(search));

            var total = await q.CountAsync();
            var fsEntities = await q.ToListAsync();
            response.Data = new
            {
                data = fsEntities,
                total = total
            };

            return Ok(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("file/{id}")]
        public async Task<ActionResult> GetFile(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            var q = _unitOfWork.FileStorageRepository
                .GetAsIQueryable(f => f.Id == id);

            var fs = await q.FirstOrDefaultAsync();
            response.Data = fs;

            return Ok(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileStorage"></param>
        /// <returns></returns>
        [HttpPost("file")]
        public async Task<ActionResult> UpdateFileInfo(FileStorageItemDto fileStorage)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };
            try
            {
                var fs = _mapper.Map<FileStorageItem>(fileStorage);
                var q = _unitOfWork.FileStorageRepository.Update(fs);
                await _unitOfWork.SaveAsync();
            } 
            catch (Exception e)
            {
                response.Status = "Error";
                response.Text = e.Message;
            }            

            return Ok(response);
        }


        /// <summary>
        /// удалить тикет
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("file")]
        public async Task<JsonResponse> DeletFile(Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var fsItem = _unitOfWork.FileStorageRepository.GetByID(id);
                await _fileStorage.RemoveAsync(fsItem.DocumentType, fsItem.SubPath + fsItem.FileName);
                var ticket = _unitOfWork.FileStorageRepository.Delete(id);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeletFile");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentType"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("filecontent/{documenttype}/{id}")]
        public async Task<ActionResult> GetFileContent(string documentType, Guid id)
        {
            if (string.IsNullOrEmpty(documentType))
                return BadRequest();

            String fileName = string.Empty;
            byte[] fileBytes = null;
            var mimeType = "application/octet-stream";

            if (documentType.ToLower() == "VehicleGeneralView".ToLower())
            {
                var generalView = await _unitOfWork.VehicleGeneralViewRepository
                    .GetAsIQueryable(g => g.Id == id)
                    .SingleOrDefaultAsync();

                if (generalView != null) 
                {
                    fileName = generalView.FileName;
                    fileBytes = generalView.VehiclePicture;
                }
            }
            else
            {
                var fsEntity = await _unitOfWork.FileStorageRepository
                    .GetAsIQueryable(f => f.Id == id)
                    .FirstOrDefaultAsync();
                if (fsEntity != null)
                {
                    fileName = fsEntity.FileName;
                    mimeType = fsEntity.MimeType;
                    fileBytes = await _fileStorage.GetAsync(fsEntity.DocumentType, fsEntity.SubPath + fsEntity.FileName);
                }
            }

            if (string.IsNullOrEmpty(fileName) && fileBytes == null)
                return NoContent();

            return new FileContentResult(fileBytes, mimeType)
            {
                FileDownloadName = fileName
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("filecontent")]
        public async Task<ActionResult> UploadFileContent([FromForm] UploadFileModel request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            if (string.IsNullOrEmpty(request.DocumentType))
            {
                response.Status = "Error";
                response.Text = "Не указан тип документа";
                return Ok(response);
            }
            if (string.IsNullOrEmpty(request.Title))
            {
                response.Status = "Error";
                response.Text = "Не указано имя документа";
                return Ok(response);
            }
            if (Request.Form.Files == null || Request.Form.Files.Count == 0)
            {
                response.Status = "Error";
                response.Text = "Отсуствуют прикрепленные документы";
                return Ok(response);
            }

            var formFiles = Request.Form.Files;
            foreach (var file in formFiles)
            {
                if (file.FileName.EndsWith(".exe") || file.FileName.EndsWith(".bat") || file.FileName.EndsWith(".sh"))
                {
                    _logger.LogInformation("Upload into bucket {0} file {1}. Unavailable due to extention", request.DocumentType, request.SubPath + file.FileName);
                    continue;
                }

                if (await _unitOfWork.FileStorageRepository.FileExistAsync(request.DocumentType, request.SubPath, file.FileName))
                {
                    _logger.LogInformation("Upload into bucket {0} file {1}. File already exist", request.DocumentType, request.SubPath + file.FileName);
                    continue;
                }

                _logger.LogInformation("Upload into bucket {0} file {1}. Starting...", request.DocumentType, request.SubPath + file.FileName);
                if (file.Length <= 0)
                {
                    _logger.LogInformation("Upload into bucket {0} file {1}. Failed length is less or equal 0", request.DocumentType, request.SubPath + file.FileName);
                    continue;
                }
                var storageId = Guid.NewGuid();
                try
                {
                    using (var stream = file.OpenReadStream())
                    {
                        await _fileStorage.AddAsync(request.DocumentType, request.SubPath + file.FileName, stream, file.ContentType);
                    }
                    
                    string mimeType = MimeTypes.GetMimeType(file.FileName);

                    var currentUser = await _userManager.GetUserAsync(User);
                    await _unitOfWork.FileStorageRepository.InsertAsync(new DBCore.Models.FileStorage.FileStorageItem()
                    {
                        Id = storageId,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        DocumentType = request.DocumentType,
                        Title = request.Title,
                        FileName = file.FileName,
                        SubPath = request.SubPath,
                        OwnerId = currentUser.Id,
                        MimeType = mimeType,
                        EntityId = request.DocumentId.HasValue
                                    ? request.DocumentId.ToString()
                                    : string.Empty,
                        Description = request.Description
                    });
                    await _unitOfWork.SaveAsync();
                    _logger.LogInformation("Upload into bucket {0} file {1}. Done", request.DocumentType, request.SubPath + file.FileName);
                } 
                catch(Exception e)
                {
                    _logger.LogError(e, "Upload into bucket {0} file {1}. Error", request.DocumentType, request.SubPath + file.FileName);
                    _nlogger.Error(e, "Upload into bucket {0} file {1}. Error", request.DocumentType, request.SubPath + file.FileName);
                    response.Status = "Error";
                    response.Text = "Ошибка при загрузке файла";
                }
                

                response.Data = new
                {
                    storageId
                };
            }

            return Ok(response);
        }

    }
}

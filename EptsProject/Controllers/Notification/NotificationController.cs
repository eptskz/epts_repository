﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.Models;
using DBCore.Models.FileStorage;
using DBCore.Models.Notifications;
using EptsBL.Extensions;
using EptsBL.Services.Interface;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers.FileStorage
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NotificationController : MainController
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="userManager"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="fileStorage"></param>
        public NotificationController(ILogger<FileStorageController> logger
            , UserManager<AspNetUsers> userManager
            , IUnitOfWork unitOfWork
            , IMapper mapper) : base(logger, unitOfWork, userManager, mapper) { }

        /// <summary>
        /// Получить уведомления
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("notifications")]
        public async Task<JsonResponse> GetNotifications([FromQuery] PagedSortRequestDto<NotificationFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var currentUser = await _userManager.GetUserAsync(User);

                var q = _UnitOfWork.NotificationRepository.GetAsIQueryable();

                if (request.Filter == null)
                {
                    q = q.Where(v => false);
                }
                else
                {
                    if (request.Filter.IsShowUnreadOnly)
                    {
                        q = q.Where(v => v.ReadDate == null);
                    }

                    q = q.Where(v => v.SendToUserId == currentUser.Id);

                    if (!string.IsNullOrEmpty(request.Filter.Search))
                    {
                        q = q.Where(v => v.Title.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.Title.ToLower().Contains(request.Filter.Search.ToLower())

                                        || v.Detail.ToLower().StartsWith(request.Filter.Search.ToLower())
                                        || v.Detail.ToLower().Contains(request.Filter.Search.ToLower())
                                    );
                    }
                }

                if (request.SortField != null && request.SortField.Length > 0)
                    q = q.OrderByCommonFields(request.SortField, request.Direction);
                else
                    q = q.OrderByDescending(v => v.CreateDate);

                var total = await q.CountAsync();
                var pageSize = request.PageSize == 0 ? total : request.PageSize;
                var notifications = await q.Skip(request.Page * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();
                
                foreach(var n in notifications)
                {
                    n.ReadDate = DateTime.Now;
                    _UnitOfWork.NotificationRepository.Update(n);
                }
                await _UnitOfWork.SaveAsync();

                response.Data = new
                {
                    Data = _mapper.Map<ICollection<NotificationDto>>(notifications),
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNotifications");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Получить контракт по ИД
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("notification/{id}")]
        public async Task<JsonResponse> GetNotification([FromRoute] Guid id)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var notification = await _UnitOfWork.NotificationRepository.GetAsIQueryable(v => v.Id == id)
                    .FirstOrDefaultAsync();
                response.Data = _mapper.Map<NotificationDto>(notification);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNotification");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        /// <summary>
        /// Обновить уведомления
        /// </summary>
        /// <param name="notificationDto"></param>
        /// <returns></returns>
        [HttpPut("notification")]
        public async Task<JsonResponse> UpdateNotification(NotificationDto notificationDto)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var notification = _mapper.Map<Notification>(notificationDto);
                _UnitOfWork.NotificationRepository.Update(notification);
                await _UnitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "UpdateNotification");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }
    }
}
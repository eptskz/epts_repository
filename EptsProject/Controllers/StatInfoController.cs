﻿using AutoMapper;
using DBCore;
using DBCore.DTOs.Public;
using DBCore.Models;
using DBCore.Models.Dictionaries;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StatInfoController : MainController
    {
        public StatInfoController(ILogger<StatInfoController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , IMapper mapper) : base(logger, unitOfWork, userManager, mapper) { }


        /// <summary>
        /// Получить стат инфу о ЭПТС
        /// </summary>
        /// <returns></returns>
        [HttpGet("public")]
        public async Task<JsonResponse> GetStatInfoPublic()
        {
            var response = new JsonResponse { Text = "", Action = Constants.ACTIONS.OK };
            try
            {
                StatInfoDto statInfoDto = new StatInfoDto();

                var q = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(v => v.Deleted == null);
                statInfoDto.DpassportOttsCount = await q.Where(v => v.DocKindNsi.Code == DictionaryCodes.DocumentTypeCodes.Otts30).CountAsync();
                statInfoDto.DpassportOtchCount = await q.Where(v => v.DocKindNsi.Code == DictionaryCodes.DocumentTypeCodes.Otch35).CountAsync();
                statInfoDto.DpassportSbktsCount = await q.Where(v => v.DocKindNsi.Code == DictionaryCodes.DocumentTypeCodes.Sbkts40).CountAsync();

                response.Data = statInfoDto;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetStatInfoPublic");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
    }
}

﻿using AutoMapper;
using DBCore;
using DBCore.DTOs;
using DBCore.DTOs.DigitalPassport;
using DBCore.Models;
using DBCore.Models.PassportDetails;
using DBCore.Permissions;
using EptsBL.Utils;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace EptsProject.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class DigitalPassportController : MainController
    {
        enum DocTypeEnum { EPTS=1, EPSHTS=2, EPSM=3}
        public DigitalPassportController(ILogger<DictionariesController> logger
            , IUnitOfWork unitOfWork
            , UserManager<AspNetUsers> userManager
            , IMapper mapper) : base(logger, unitOfWork, userManager, mapper)
        {
        }

        [HttpPost("GetMonitoringList")]
        public async Task<JsonResponse> GetMonitoringList([FromBody] PagedSortRequestDto<ExternalServiceFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var currentUser = GetUser();
                if (!User.HasClaim(CustomClaimTypes.Permissions, AdministrationPermissions.Monitoring.Read))
                {
                    throw new Exception("Данные запрещены");
                }
                var q = _UnitOfWork.ExternalInteractHistoryRepository
                    .GetAsIQueryable(v => v.Deleted == null);
                if (!string.IsNullOrEmpty(request.Filter.Service))
                {
                    q = q.Where(v => v.ServiceName == request.Filter.Service);
                }

                if (request.Filter.From.HasValue)
                {
                    q = q.Where(v => v.Created >= request.Filter.From.Value);
                }
                if (request.Filter.Till.HasValue)
                {
                    q = q.Where(v => v.Created <= request.Filter.Till.Value.AddDays(1));
                }

                
                var data = q.ToList()
                    .GroupBy(x => new { x.ServiceName, x.Created.Date })
                    //.ToDictionary(g => g.Key, g => g.ToList());
                    .Select(x => new
                    {
                        ServiceName = x.Key.ServiceName,
                        Created = x.Key.Date,
                        Total = x.Count(),
                        Success = x.ToList().Count(c=>c.StatusCode == "5"),
                        Failed = x.ToList().Count(c=>c.StatusCode == "-1"),
                    })
                    .OrderByDescending(v => v.Created)
                    .ToList();
                response.Data = new
                {
                    Data = data,
                    Total = data.Count()
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetMonitoringList");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        [HttpPost("GetReportList")]
        public async Task<JsonResponse> GetReportList([FromBody] PagedSortRequestDto<ReportFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var currentUser = GetUser();
                if (!User.HasClaim(CustomClaimTypes.Permissions, ReportPermissions.Reports.EptsView))
                {
                    throw new Exception("Данные запрещены");
                }
                var q = _UnitOfWork.DigitalPassportRepository
                    .GetAsIQueryable(v => v.Deleted == null);
                if (!string.IsNullOrEmpty(request.Filter.Region))
                {
                    q = q.Where(v => v.AspNetUser.Organization.JuridicalDistinctOrCity != null && v.AspNetUser.Organization.JRegionOrCity == request.Filter.Region);
                }

                if (!string.IsNullOrEmpty(request.Filter.Org))
                {
                    q = q.Where(v => v.AspNetUser.OrganizationId.ToString() == request.Filter.Org);
                }

                if (!string.IsNullOrEmpty(request.Filter.Kind))//1 -epts, 2- epshts, 3- epsm
                {

                    q = q.Where(v => v.DocType.ToString() == request.Filter.Kind);
                }

                if (!string.IsNullOrEmpty(request.Filter.Status))
                {
                    q = q.Where(v => v.StatusNsi.Code == request.Filter.Status);
                }

                if (!string.IsNullOrEmpty(request.Filter.RegKind))
                {
                    q = q.Where(v => v.VehicleEPassportBaseCode == request.Filter.RegKind);
                }

                if (request.Filter.From.HasValue)
                {
                    q = q.Where(v => v.Created >= request.Filter.From.Value);
                }
                if (request.Filter.Till.HasValue)
                {
                    q = q.Where(v => v.Created <= request.Filter.Till.Value.AddDays(1));
                }

                var total = await q.CountAsync();
                var jsonData = await q.Include(v => v.StatusNsi)
                    .Include(v => v.DocKindNsi)
                    .Include(v => v.VehicleMakeNameNsi)
                    .Include(v => v.VehicleTechCategoryCodeNsi)
                    //.Include(v => v.AspNetUser.Organization)
                    .Include(v => v.AspNetUser.Organization.JuridicalDistinctOrCity)
                    .Include(v => v.ComplienceDocument)
                    .OrderByDescending(v => v.Created)
                    .ToListAsync();
                var tableData = jsonData
                    .Skip(request.Page * request.PageSize)
                    .Take(request.PageSize).ToList();

                response.Data = new
                {
                    Data = new { tableData= tableData, jsonData= jsonData },
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetReportList");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        [HttpGet("GetDraft")]
        public async Task<JsonResponse> GetDraft(Guid signId)
        {
            var response = new JsonResponse { Text = "Не удалось найти", Action = Constants.ACTIONS.OK };
            try
            {
                var filterArray = new BsonArray();
                var filter = new BsonDocument("$and", filterArray);
                filterArray.Add(new BsonDocument("SignId", new BsonDocument("$eq", signId)));
                var currentUser = GetUser();
                var users = _UnitOfWork.UserRepository.GetAsIQueryable(x => x.OrganizationId == currentUser.OrganizationId).Include(x => x.Organization).ToList();
                filterArray.Add(new BsonDocument("AspNetUserId", new BsonDocument("$in", new BsonArray(users.Select(x => x.Id)))));
                var data = _UnitOfWork.DraftPassportDtoMongoRepository.GetAsync(filter).GetAwaiter().GetResult().FirstOrDefault();
                if (data != null)
                {
                    response.Status = "Ok";
                    response.Text = "";
                    response.Data = data;
                }
                
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetDraft");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        [HttpPost("GetDraftList")]
        public async Task<JsonResponse> GetDraftList([FromBody] PagedSortRequestDto<DocFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var filterArray = new BsonArray();
                var filter = new BsonDocument("$and", filterArray);
                var currentUser = GetUser();
                
                // get all users of the organization
                var users = _UnitOfWork.UserRepository.GetAsIQueryable(x=>x.OrganizationId == currentUser.OrganizationId).Include(x=>x.Organization).ToList();
                filterArray.Add(new BsonDocument("AspNetUserId", new BsonDocument("$in", new BsonArray(users.Select(x => x.Id)))));
                var q = _UnitOfWork.DraftPassportDtoMongoRepository.GetAsync(filter).GetAwaiter().GetResult();
                if (!string.IsNullOrEmpty(request.Filter.TypeCode))
                {
                    q = q.Where(v => v.DocType.ToString() == request.Filter.TypeCode).ToList();
                }

                var complinceDocs = await _UnitOfWork.ComplienceDocumentRepository.GetAsIQueryable(x => x.AuthorOrgId == currentUser.OrganizationId).ToListAsync();

                var nsiValues = await _UnitOfWork.NsiValuesRepository
                    .GetAsIQueryable(v => new List<string>() { "nsi012", "nsi013", "nsi025", "nsi113", "nsi014", "nsi021", "nsi065", "nsi046", "nsi015", "nsi016", "nsi017", "nsi067", "nsi034", "nsi022" }.Contains(v.Nsi.Code) && v.Deleted == null).Include(v => v.Nsi)
                    .ToListAsync();
                var orgName = users.FirstOrDefault()?.Organization.NameRu ?? "";
                q = q.Select(x => new RootPassportDto {
                    Id = x.Id,
                    SignId = x.SignId,
                    DocType = x.DocType,
                    Created = x.Created,
                    Modified = x.Modified,
                    DocKindCode = nsiValues.FirstOrDefault(v => v.Nsi.Code == "nsi012" && v.Code == x.DocKindCode)?.NameRu,
                    DocIdHandy = complinceDocs.FirstOrDefault(v=>v.Id == x.ComplienceDocumentId)?.DocNumber,//тут DocIdHandy используется только отображения номера документа (оттс, отш и т.д.)
                    VehicleIdentityNumberId = x.VehicleIdentityNumberId,
                    VehicleMakeName = nsiValues.FirstOrDefault(v => v.Nsi.Code == "nsi046" && v.Code == x.VehicleMakeName)?.NameRu,
                    VehicleCommercialName = x.VehicleCommercialName,
                    AuthorityName = orgName,
                }).ToList();

                var total = q.Count();
                var entities =  q
                    .Skip(request.Page * request.PageSize)
                    .Take(request.PageSize)
                    .OrderByDescending(v => v.Created)
                    .ToList();
                response.Data = new
                {
                    Data = entities,
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetDraftList");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        [HttpGet("GetView")]
        public async Task<JsonResponse> GetView(int id)
        {
            var response = new JsonResponse { Text = "Не удалось найти", Action = Constants.ACTIONS.OK };
            try
            {
                var doc = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.Id == id)
                   .Include(x => x.VehicleCategoryCodeNsi)
                   .Include(x => x.VehicleTechCategoryCodeNsi)
                   .Include(x => x.StatusNsi)
                   .Include(x => x.MoverEpsmNsi)
                   .Include(x => x.VehicleMakeNameNsi)
                   .Include(x => x.MemberOrganizations)
                   .Include(x => x.AssemblerOrganizations)
                   .Include(x => x.VehicleImages)
                   .Include(x => x.VehicleEngines)
                   .Include(x => x.EnginePlateDetail)
                   .Include(x => x.EnginePlateDetail.VehicleIdentLocations)
                   .Include(x => x.EnginePlateDetail.VehicleStructureCards)
                        .ThenInclude(card=>card.MeaningExplanations)
                   .Include(x => x.ManufacturerPlateDetail)
                   .Include(x => x.ManufacturerPlateDetail.VehicleStructureCards)
                        .ThenInclude(card => card.MeaningExplanations)
                   .Include(x => x.ManufacturerPlateDetail.VehicleIdentLocations)
                   .Include(x => x.ManufacturerPlateDetail.ManufacturerPlateLocations)
                   .Include(x => x.VehicleEcoClassCodeNsi)
                   .Include(x => x.VehicleBodyColourCodeNsi)
                   .Include(x => x.VehicleBodyColourCode2Nsi)
                   .Include(x => x.VehicleBodyColourCode3Nsi)
                   .Include(x => x.AspNetUser)
                   .FirstOrDefault();
                doc.VehicleImages = _UnitOfWork.VehicleImageRepository.GetAsIQueryable(x => x.DigitalPassportId == doc.Id).ToList();

                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Read))
                    throw new Exception("Нет данных!");
                //if (GetUser().OrganizationId != doc.AspNetUser.OrganizationId)
                //    throw new Exception("Нет данных!");
                var filter = new BsonDocument("DigitalPassportId", new BsonDocument("$eq", doc.Id));
                CharacteristicDetails character = _UnitOfWork.CharacteristicDetailsMongoRepository.GetAsync(filter).GetAwaiter().GetResult().FirstOrDefault();
                var data = _mapper.Map<RootPassportDto>(doc);
                if (doc.VehicleImages.Count() > 0)
                {
                    data.imagesTS = new List<ImagesTs>();
                    foreach (var item in doc.VehicleImages)
                    {
                        data.imagesTS.Add(new ImagesTs()
                        {
                            name = item.FileName,
                            image = "data:image/jpeg;base64," + Convert.ToBase64String(item.VehiclePicture)
                        });
                    }
                }
                
                data.CharacteristicDetails = character;
                if (data != null)
                {
                    response.Status = "Ok";
                    response.Text = "";
                    response.Data = data;
                }

            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetView");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        [HttpPost("GetList")]
        public async Task<JsonResponse> GetList([FromBody] PagedSortRequestDto<DocFilterDto> request)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var currentUser = GetUser();
                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Read))
                {
                    throw new Exception("Данные запрещены");
                }
                var q = _UnitOfWork.DigitalPassportRepository
                    .GetAsIQueryable(v => v.Deleted == null);
                if (!string.IsNullOrEmpty(request.Filter.TypeCode))
                {
                    q = q.Where(v => v.DocType.ToString() == request.Filter.TypeCode);
                }
                
                
                if (!GetUserRoles().Any(x => x == "КИР МИИР РК" || x == "administrator" || x== "Национальный оператор"))
                    q = q.Where(v => v.AspNetUser.OrganizationId == currentUser.OrganizationId);
                

                if (request.Filter.Search != null)
                {
                    q = q.Where(v => v.VehicleIdentityNumberId.ToLower().Contains(request.Filter.Search.ToLower()) 
                        || v.UniqueCode.ToLower().Contains(request.Filter.Search.ToLower()));
                }

                var total = await q.CountAsync();
                var entities = await q.Include(v => v.StatusNsi)
                    .Include(v => v.DocKindNsi)
                    .Include(v => v.VehicleMakeNameNsi)
                    .Include(v => v.AspNetUser.Organization)
                    .Include(v=>v.ComplienceDocument)
                    .Skip(request.Page * request.PageSize)
                    .Take(request.PageSize)
                    .OrderByDescending(v => v.Created)
                    .ToListAsync();
                response.Data = new
                {
                    Data = entities,
                    Total = total
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetList");
                response.Text = e.Message;
                response.Status = "Error";
            }

            return response;
        }

        [HttpPost("GetDigitalPassportCard")]
        public async Task<ActionResult> GetDigitalPassportCard([FromBody] string passportId)
        {
            //HttpContext.GetOwinContext().GetUserManager<UserManager>();
            // TODO удостовериться, что адрес возврата принадлежит приложению с помощью метода Url.IsLocalUrl()
            var response = new JsonResponse { Text = "Неправильный логин или пароль", Action = Constants.ACTIONS.OK };
            try
            {
                var currentUser = GetUser();
                var doc = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.Id.ToString() == passportId)
                    .Include(x=>x.VehicleCategoryCodeNsi)
                    .Include(x => x.VehicleTechCategoryCodeNsi)
                    .Include(x => x.StatusNsi)
                    .Include(x => x.VehicleMakeNameNsi)
                    .Include(x => x.VehicleEngines)
                    .Include(x => x.VehicleEcoClassCodeNsi)
                    .Include(x => x.VehicleBodyColourCodeNsi )
                    .Include(x => x.VehicleBodyColourCode2Nsi )
                    .Include(x => x.VehicleBodyColourCode3Nsi)
                    .Include(x => x.VehicleEngines)
                    .Include(x=> x.AspNetUser)
                    .FirstOrDefault();

                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Read))
                    throw new Exception("Данные запрещены");
                
                if (!GetUserRoles().Any(x => x == "КИР МИИР РК" || x == "administrator" || x == "Национальный оператор")
                    && doc.AspNetUser.OrganizationId != currentUser.OrganizationId)
                    throw new Exception("Данные запрещены");
                var filter = new BsonDocument("DigitalPassportId", new BsonDocument("$eq", doc.Id));
                CharacteristicDetails character = _UnitOfWork.CharacteristicDetailsMongoRepository.GetAsync(filter).GetAwaiter().GetResult().FirstOrDefault();
                var x = 613;
                var y = 280;
                var yStep = 34;
                var fileName = "ptstemplate.jpg";
                var filePath = Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "Contents"), "Images", fileName);
                MemoryStream memory = new MemoryStream();

                using (Bitmap bitmap = new Bitmap(filePath))
                {
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        Brush brush = new SolidBrush(Color.Black);
                        Font font = new Font("Roboto", 16, FontStyle.Bold, GraphicsUnit.Pixel);
                        SizeF textSize = new SizeF();

                        
                        graphics.DrawString(doc.UniqueCode, new Font("Roboto", 24, FontStyle.Bold, GraphicsUnit.Pixel), brush, new Point(150, 164));
                        graphics.DrawString(doc.StatusNsi.NameRu, new Font("Roboto", 18, FontStyle.Bold, GraphicsUnit.Pixel), brush, new Point(150, 203));
                        if (doc.StatusNsi.Code == "05")
                            graphics.DrawString(doc.Created.ToString("dd-MM-yyyy HH:mm:ss"), new Font("Roboto", 18, FontStyle.Bold, GraphicsUnit.Pixel), brush, new Point(292, 235));


                        //textSize = graphics.MeasureString(stringToBeAdded, font);
                        Point position = new Point(x, y);
                        graphics.DrawString(doc.VehicleIdentityNumberId, font, brush, position);

                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(character.VehicleCharacteristicsName==""?"Отсутствует": character.VehicleCharacteristicsName, font, brush, position);

                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(doc.VehicleMakeNameNsi?.NameRu, font, brush, position);

                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(doc.VehicleCommercialName == "" ? "Отсутствует": doc.VehicleCommercialName, font, brush, position);

                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(doc.VehicleCategoryCodeNsi == null ? "Отсутствует" : doc.VehicleCategoryCodeNsi?.NameRu, font, brush, position);

                        y += yStep*2;
                        position = new Point(x, y);
                        graphics.DrawString(doc.VehicleTechCategoryCodeNsi.NameRu, font, brush, position);

                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(doc.VehicleEngines.FirstOrDefault()?.VehicleEngineIdentityNumberId ?? "Отсутствует", font, brush, position);

                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(doc.VehicleFrameIdentityNumberId == "" ? "Отсутствует" : doc.VehicleFrameIdentityNumberId, font, brush, position);

                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(doc.VehicleBodyIdentityNumberId == "" ? "Отсутствует" : doc.VehicleBodyIdentityNumberId, font, brush, position);

                        var color = doc.VehicleBodyColourCodeNsi.NameRu;
                        if (doc.VehicleBodyColourCode2Nsi != null) color += ", " + doc.VehicleBodyColourCode2Nsi.NameRu;
                        if (doc.VehicleBodyColourCode3Nsi != null) color += ", " + doc.VehicleBodyColourCode3Nsi.NameRu;
                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(color, font, brush, position);

                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(doc.ManufactureYear, font, brush, position);

                        if (character.DvsEngine.Count > 0 && (character.DvsEngine[0].VehicleDVSComponentMakeName + character.DvsEngine[0].VehicleDVSComponentText).Length > 0)
                        {
                            var txt = character.DvsEngine[0].VehicleDVSComponentMakeName + ", " + character.DvsEngine[0].VehicleDVSComponentText;
                            if (txt.Length > 20)
                            {
                                y += yStep;
                                position = new Point(x, y);
                                graphics.DrawString(txt.Substring(0, txt.Length / 2), font, brush, position);

                                y += yStep;
                                position = new Point(x, y);
                                graphics.DrawString(txt.Substring(txt.Length / 2, txt.Length / 2), font, brush, position);
                            }
                            else
                            {
                                y += yStep;
                                position = new Point(x, y);
                                graphics.DrawString(character.DvsEngine[0].VehicleDVSComponentMakeName + ", " + character.DvsEngine[0].VehicleDVSComponentText, font, brush, position);
                                y += yStep;
                            }

                            y += yStep;
                            position = new Point(x, y);
                            graphics.DrawString(character.DvsEngine[0].EngineCapacityMeasure, font, brush, position);

                            y += yStep;
                            position = new Point(x, y);
                            graphics.DrawString(character.DvsEngine[0].EngineMaxPowerMeasure, font, brush, position);
                        } else
                        {
                            y += yStep;
                            position = new Point(x, y);
                            graphics.DrawString("Отсутствует", font, brush, position);
                            y += yStep * 3;
                        }
                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(doc.VehicleEcoClassCodeNsi == null ? "Отсутствует" : doc.VehicleEcoClassCodeNsi.NameRu, font, brush, position);

                        y += yStep;
                        position = new Point(x, y);
                        graphics.DrawString(character.VehicleMassMeasure1, font, brush, position);

                        y += yStep*2;
                        position = new Point(x, y);
                        graphics.DrawString(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), font, brush, position);



                        bitmap.Save(memory, ImageFormat.Jpeg);
                        memory.Position = 0;
                        return File(memory.ToArray(), "image/jpeg", fileName);
                    }
                }

            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось скачать выписку: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            return null;
        }


        [HttpPost("GetDigitalPassportCardTest")]
        public async Task<ActionResult> GetDigitalPassportCardTest([FromBody] string passportId)
        {
            //HttpContext.GetOwinContext().GetUserManager<UserManager>();
            // TODO удостовериться, что адрес возврата принадлежит приложению с помощью метода Url.IsLocalUrl()
            var response = new JsonResponse { Text = "Неправильный логин или пароль", Action = Constants.ACTIONS.OK };
            try
            {
                throw new Exception("StackOverFlow)");
                var respRop = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                    + "<soap:Header xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"/>"
                                    + "<soap:Body>"
                                    + "<ns3:SendMessageResponse xmlns:ns2=\"http://bip.bee.kz/SyncChannel/v10/Interfaces\" xmlns:ns3=\"http://bip.bee.kz/SyncChannel/v10/Types\">"
                                    + "<response><responseInfo><messageId>bdbb97a1-21b7-440b-ade7-a71e31e31b73</messageId>"
                                    + "<responseDate>2021-04-05T19:10:52.354Z</responseDate><status><code>0</code>"
                                    + "<message>Message has been processed successfully</message></status></responseInfo><responseData><data>"
                                    + "<HavePay>TRUE</HavePay>"
                                    + "<PaySum>972300</PaySum>"
                                    + "<PayDate>2020-09-02 11:05:02</PayDate>"
                                    + "<PayNumber>412113</PayNumber>"
                                    + "<CarCaterogy>cat-m1g</CarCaterogy>"
                                    + "<CarVolume>1690.00</CarVolume>"
                                    + "</data></responseData></response></ns3:SendMessageResponse></soap:Body></soap:Envelope>";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(respRop);
                XmlNodeList elemlist = xmlDoc.GetElementsByTagName("code");
                string result = elemlist[0].InnerXml;
                Console.WriteLine(result);
                if (result == "0")
                {
                    Console.WriteLine(xmlDoc.GetElementsByTagName("HavePay")[0].InnerXml);
                }
                var x = 560;
                var fileName = "ptscardTest.jpeg";
                var filePath = Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "Contents"), "Images", fileName);
                _logger.LogInformation($"filePath {filePath}");
                MemoryStream memory = new MemoryStream();

                using (Bitmap bitmap = new Bitmap(filePath))
                {
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        bitmap.Save(memory, ImageFormat.Jpeg);
                        memory.Position = 0;
                        return File(memory.ToArray(), "image/jpeg", fileName);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось скачать выписку: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            return null;
        }

        [HttpPost("GetRopTest")]
        public async Task<JsonResponse> GetRopTest([FromBody] string vin)
        {
            //HttpContext.GetOwinContext().GetUserManager<UserManager>();
            // TODO удостовериться, что адрес возврата принадлежит приложению с помощью метода Url.IsLocalUrl()
            var response = new JsonResponse { Text = "Тест не пройден", Action = Constants.ACTIONS.OK };
            throw new Exception("StackOverFlow)");
            try
            {
                var ropRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns3=\"http://bip.bee.kz/SyncChannel/v10/Types\"><soap:Body><ns3:SendMessage><request><requestInfo>" +
            "<messageId>bdbb97a1-21b7-440b-ade7-a71e31e31b73</messageId><serviceId>GetRecycleVehicle</serviceId><messageDate>2020-08-27T12:49:47</messageDate><sender>" +
            "<senderId>epts</senderId><password>Dam3oha23dk!</password></sender></requestInfo><requestData><data><method>PaymentUtilizationFee</method>" +
            $"<vin>{vin}</vin><iin>960610301846</iin><bin>000740000728</bin></data></requestData></request></ns3:SendMessage></soap:Body></soap:Envelope>";

                //curl - X POST - H 'Content-type: text/xml' - d @req1.xml http://192.168.39.30:19022/bip-external-sync/ --insecure
                var content = new StringContent(ropRequest, Encoding.UTF8, "text/xml");
                var url = "http://192.168.39.30:19022/bip-external-sync/";
                _logger.LogInformation($"Запрос в РОП по вин коду {vin}");
                using var client = new HttpClient();
                var responseROP = await client.PostAsync(url, content);

                string respRop = responseROP.Content.ReadAsStringAsync().Result;
                _logger.LogInformation(respRop);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(respRop);
                XmlNodeList elemlist = xmlDoc.GetElementsByTagName("code");
                string result = elemlist[0].InnerXml;
                if (result == "0" && xmlDoc.GetElementsByTagName("HavePay")?[0]?.InnerXml?.ToLower() == "true")
                {
                    response.Status = "Ok";
                    response.Text = respRop;
                    response.Data = "";
                    _logger.LogInformation($" {HttpContext.Request.Headers["X-Real-IP"]}");
                }
            }
            catch (Exception exc)
            {
                response.Text = exc.Message;
                _logger.LogInformation($"Запрос в РОП не прошел по причине {exc}");
            }
            return null;
        }

        [HttpGet("GetBatchUsedCount")]
        public async Task<JsonResponse> GetBatchUsedCount(string id)
        {
            var response = new JsonResponse { Text = "Не удалось найти", Action = Constants.ACTIONS.OK };
            var count = 0;
            try
            {
                count = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.ComplienceDocumentId.Value.ToString() == id 
                    && x.SignId != null 
                    && GetUser().OrganizationId == x.AspNetUser.OrganizationId)
                        .Count();
            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось найти ЭП по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            response.Status = "Ok";
            response.Text = "Ок";
            response.Data = count;
            _logger.LogInformation($" {HttpContext.Request.Headers["X-Real-IP"]}");            
            return response;
        }

        [HttpGet("GetDPassportByUniqCode")]
        public async Task<JsonResponse> GetDPassportByUniqCode(string ident)
        {
            var response = new JsonResponse { Text = "Не удалось найти", Action = Constants.ACTIONS.OK };
            try
            {
                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Read))
                    throw new Exception("Данные запрещены");

                var dp = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.UniqueCode == ident && x.SignId != null).FirstOrDefault();
                if (dp != null)
                {
                    response.Status = "Ok";
                    response.Text = "Ок";
                    response.Data = new { uniqueCode = dp.UniqueCode, creationDate = dp.Created, commercialName = dp.BaseVehicleCommercialName };
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось найти ЭП по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            
            _logger.LogInformation($" {HttpContext.Request.Headers["X-Real-IP"]}");
            return response;
        }
        
        [HttpGet("GetDPassportByVinCode")]
        public async Task<JsonResponse> GetDPassportByVinCode(string vinCode)
        {
            var response = new JsonResponse { Text = "Не удалось найти", Action = Constants.ACTIONS.OK };
            try
            {
                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Read))
                    throw new Exception("Данные запрещены");

                var dp = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.VehicleIdentityNumberId == vinCode && x.SignId != null).FirstOrDefault();
                if (dp != null)
                {
                    response.Status = "Ok";
                    response.Text = "Ок";
                    response.Data = new { uniqueCode = dp.UniqueCode, creationDate = dp.Created, commercialName = dp.BaseVehicleCommercialName };
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось найти ЭП по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }

            _logger.LogInformation($" {HttpContext.Request.Headers["X-Real-IP"]}");
            return response;
        }

        [HttpPost("SaveDraft")]
        public async Task<JsonResponse> SaveDraftDigitalPassport([FromBody] RootPassportDto data)
        {
            var response = new JsonResponse { Text = "Не удалось сохранить ЭП", Action = Constants.ACTIONS.OK };
            try
            {
                var f = User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Edit);
                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Edit))
                    throw new Exception("Данные запрещены");
                //data.DocType = 1;
                data.AspNetUserId = GetUser().Id;
                if (data.VehicleIdentityNumberId != "" && data.VehicleIdentityNumberId != null)
                {
                    var existDP = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.VehicleIdentityNumberId.ToLower() == data.VehicleIdentityNumberId.ToLower(), isAsNoTracking: true).FirstOrDefault();
                    if (existDP != null)
                    {
                        if (data.SignId != null && data.SignId != existDP.SignId)
                        {
                            response.Text = $"Ошибка уникальности Винкода - {data.VehicleIdentityNumberId}!";
                            throw new Exception(response.Text);
                        }
                    }
                }
                RootPassportDto save = null;
                var docPre = _UnitOfWork.DraftPassportDtoMongoRepository.GetAsync(new BsonDocument("SignId", new BsonDocument("$eq", data.SignId))).GetAwaiter().GetResult().FirstOrDefault();
                if (data.SignId == null && docPre == null)
                {
                    data.SignId = Guid.NewGuid();
                    data.Created = DateTime.Now;
                    save = await _UnitOfWork.DraftPassportDtoMongoRepository.CreateAsync(data);
                }
                else
                {//_UnitOfWork.CharacteristicDetailsMongoRepository.RemoveFirst(new BsonDocument("DigitalPassportId", new BsonDocument("$eq", doc.Id)));
                    data.Created = docPre.Created;
                    data.Modified = DateTime.Now;
                    _UnitOfWork.DraftPassportDtoMongoRepository.RemoveFirst(new BsonDocument("SignId", new BsonDocument("$eq", docPre.SignId)));
                    save = await _UnitOfWork.DraftPassportDtoMongoRepository.CreateAsync(data);
                }
                response.Status = "Ok";
                response.Text = "Сохранено";
                response.Data = save.SignId.ToString();
                _logger.LogInformation($" {HttpContext.Request.Headers["X-Real-IP"]}");
            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось сохранить ЭП по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            return response;
        }

        [HttpPost("Save")]
        public async Task<JsonResponse> SaveDigitalPassport([FromBody] RootPassportDto data)
        {
            var response = new JsonResponse { Text = "Не удалось сохранить ЭП", Action = Constants.ACTIONS.OK };
            try
            {
                var doc = _mapper.Map<DigitalPassportDocument>(data);
                doc.DocType = 1;
                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Edit))
                    throw new Exception("Данные запрещены");

                if (!GetUserRoles().Any(x => x == "КИР МИИР РК" || x == "administrator" || x == "Национальный оператор"))
                    doc.AspNetUserId = GetUser().Id;
                //nsi references
                var nsiValues = await _UnitOfWork.NsiValuesRepository
                        .GetAsIQueryable(v => new List<string>() { "nsi012", "nsi013", "nsi025", "nsi113", "nsi014", "nsi021", "nsi065", "nsi046", "nsi015", "nsi016", "nsi017", "nsi067", "nsi034", "nsi022" }.Contains(v.Nsi.Code) && v.Deleted == null).Include(v => v.Nsi)
                        .ToListAsync();
                doc.DocKindNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi012" && x.Code == doc.DocKindCode);
                doc.DocumentNotExistDescriptionNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi025" && x.Code == doc.DocumentNotExistDescription);
                doc.DocKindCodeHandyNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi113" && x.Code == doc.DocKindCodeHandy);
                doc.VehicleCategoryCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi014" && x.Code == doc.VehicleCategoryCode);
                doc.VehicleClassCategoryCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi013" && x.Code == doc.VehicleClassCategoryCode);
                
                doc.VehicleBodyColourCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi021" && x.Code == doc.VehicleBodyColourCode);
                doc.VehicleBodyColourCode2Nsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi021" && x.Code == doc.VehicleBodyColourCode2);
                doc.VehicleBodyColourCode3Nsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi021" && x.Code == doc.VehicleBodyColourCode3);
                doc.ManufactureMonthNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi065" && x.Code == doc.ManufactureMonth);
                doc.VehicleMakeNameNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi046" && x.Code == doc.VehicleMakeName);
                doc.BaseVehicleMakeNameNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi046" && x.Code == doc.BaseVehicleMakeName);
                doc.VehicleTechCategoryCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi015" && x.Code == doc.VehicleTechCategoryCode);
                doc.VehicleEcoClassCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi016" && x.Code == doc.VehicleEcoClassCode);
                doc.VehicleManufactureOptionNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi017" && x.Code == doc.VehicleManufactureOption);
                doc.ManufactureWithPrivilegeModeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi067" && x.Code == doc.ManufactureWithPrivilegeMode);
                //doc.AcceptCountryRegistrationNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi034" && x.Code == doc.AcceptCountryRegistration);
                //doc.AcceptRegionRegistration = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi034" && x.Code == doc.AcceptCountryRegistration);
                doc.VehicleMadeInCountryNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi034" && x.Code == doc.VehicleMadeInCountry);
                doc.StatusNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi022" && x.Code == "00");

                if (doc.VehicleIdentityNumberId != "" && doc.VehicleIdentityNumberId != null)
                {
                    var existDP = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.VehicleIdentityNumberId.ToLower() == doc.VehicleIdentityNumberId.ToLower(), isAsNoTracking: true).FirstOrDefault();
                    if (existDP != null)
                    {
                        if (data.SignId != null && data.SignId != existDP.SignId)
                        {
                            response.Text = $"Ошибка уникальности Винкода - {doc.VehicleIdentityNumberId}!";
                            throw new Exception(response.Text);
                        }
                    }
                }
                var save = false;
                if (data.SignId == null)
                {
                    doc.SignId = Guid.NewGuid();
                    doc.Created = DateTime.Now;
                    save = await _UnitOfWork.DigitalPassportRepository.InsertAsync(doc);
                    if (save)
                    {
                        using (var transaction = _UnitOfWork.BeginTransaction())
                        {
                            try
                            {
                                await _UnitOfWork.SaveAsync();
                                //save images
                                if (data.imagesTS.Count() > 0)
                                {
                                    foreach (var item in data.imagesTS)
                                    {
                                        var image = new VehicleImage();
                                        image.CreateDate = doc.Created;
                                        image.FileName = item.name;
                                        var base64Data = Regex.Match(item.image, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                                        image.VehiclePicture = Convert.FromBase64String(base64Data);
                                        image.DigitalPassportId = doc.Id;
                                        await _UnitOfWork.VehicleImageRepository.InsertAsync(image);
                                    }
                                }

                                data.CharacteristicDetails.DigitalPassportId = doc.Id;
                                var mongoSave = await _UnitOfWork.CharacteristicDetailsMongoRepository.CreateAsync(data.CharacteristicDetails);

                                save = _UnitOfWork.DigitalPassportRepository.Update(doc);
                                await _UnitOfWork.SaveAsync();
                                _UnitOfWork.Commit();
                            }
                            catch (Exception ex)
                            {
                                _UnitOfWork.Rollback();
                                _UnitOfWork.CharacteristicDetailsMongoRepository.RemoveFirst(new BsonDocument("DigitalPassportId", new BsonDocument("$eq", doc.Id)));
                                throw ex;
                            }
                        }
                    }
                } else
                {
                    doc.Modified = DateTime.Now;
                    var docPre = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.Deleted == null && x.SignId == data.SignId, isAsNoTracking:true).FirstOrDefault();
                    //_UnitOfWork.DigitalPassportRepository.Delete(docPre.Id);
                    doc.Id = docPre.Id;
                    save = _UnitOfWork.DigitalPassportRepository.Update(doc);
                    if (save)
                    {
                        using (var transaction = _UnitOfWork.BeginTransaction())
                        {
                            try
                            {
                                //await _UnitOfWork.SaveAsync();
                                //save images
                                if (data.imagesTS.Count() > 0)
                                {
                                    var imagesIDs = _UnitOfWork.VehicleImageRepository.GetAsIQueryable(x => x.DigitalPassportId == doc.Id).Select(x => x.Id).ToArray();
                                    _UnitOfWork.VehicleImageRepository.Delete(imagesIDs);
                                    await _UnitOfWork.SaveAsync();
                                    foreach (var item in data.imagesTS)
                                    {
                                        var image = new VehicleImage();
                                        image.CreateDate = doc.Created;
                                        image.FileName = item.name;
                                        var base64Data = Regex.Match(item.image, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                                        image.VehiclePicture = Convert.FromBase64String(base64Data);
                                        image.DigitalPassportId = doc.Id;
                                        await _UnitOfWork.VehicleImageRepository.InsertAsync(image);
                                    }
                                }
                                var characteristicId = _UnitOfWork.CharacteristicDetailsMongoRepository.GetAsync(new BsonDocument("DigitalPassportId", new BsonDocument("$eq", doc.Id))).GetAwaiter().GetResult().FirstOrDefault()?.Id;
                                data.CharacteristicDetails.DigitalPassportId = doc.Id;
                                _UnitOfWork.CharacteristicDetailsMongoRepository.UpdateById(characteristicId.Value.ToString(), data.CharacteristicDetails);

                                //save = _UnitOfWork.DigitalPassportRepository.Update(doc);
                                await _UnitOfWork.SaveAsync();
                                _UnitOfWork.Commit();
                            }
                            catch (Exception ex)
                            {
                                _UnitOfWork.Rollback();
                                _UnitOfWork.CharacteristicDetailsMongoRepository.RemoveFirst(new BsonDocument("DigitalPassportId", new BsonDocument("$eq", doc.Id)));
                                throw ex;
                            }
                        }
                    }
                }
                response.Status = "Ok";
                response.Text = "Сохранено";
                response.Data = doc.SignId.ToString();
                _logger.LogInformation($" {HttpContext.Request.Headers["X-Real-IP"]}");
            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось сохранить ЭП по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            return response;
        }

        //[HttpPost("Save")]
        //public async Task<JsonResponse> SaveDigitalPassport([FromBody] RootPassportDto data)
        //{
        //    var response = new JsonResponse { Text = "Не удалось сохранить ЭП", Action = Constants.ACTIONS.OK };
        //    try
        //    {
        //        var doc = _mapper.Map<DigitalPassportDocument>(data);
        //        doc.DocType = 1;

        //        doc.Created = DateTime.Now;
        //        doc.AspNetUserId = GetUser().Id;
        //        //nsi references
        //        var nsiValues = await _UnitOfWork.NsiValuesRepository
        //                .GetAsIQueryable(v => new List<string>(){"nsi012", "nsi025", "nsi113", "nsi014", "nsi021", "nsi065", "nsi046", "nsi015", "nsi016", "nsi017", "nsi067", "nsi034", "nsi022"}.Contains(v.Nsi.Code) && v.Deleted == null).Include(v => v.Nsi)
        //                .ToListAsync();
        //        doc.DocKindNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi012" && x.Code == doc.DocKindCode);
        //        doc.DocumentNotExistDescriptionNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi025" && x.Code == doc.DocumentNotExistDescription);
        //        doc.DocKindCodeHandyNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi113" && x.Code == doc.DocKindCodeHandy);
        //        doc.VehicleCategoryCodeNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi014" && x.Code == doc.VehicleCategoryCode);
        //        doc.VehicleBodyColourCodeNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi021" && x.Code == doc.VehicleBodyColourCode);
        //        doc.VehicleBodyColourCode2Nsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi021" && x.Code == doc.VehicleBodyColourCode2);
        //        doc.VehicleBodyColourCode3Nsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi021" && x.Code == doc.VehicleBodyColourCode3);
        //        doc.ManufactureMonthNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi065" && x.Code == doc.ManufactureMonth);
        //        doc.VehicleMakeNameNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi046" && x.Code == doc.VehicleMakeName);
        //        doc.BaseVehicleMakeNameNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi046" && x.Code == doc.BaseVehicleMakeName);
        //        doc.VehicleTechCategoryCodeNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi015" && x.Code == doc.VehicleTechCategoryCode);
        //        doc.VehicleEcoClassCodeNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi016" && x.Code == doc.VehicleEcoClassCode);
        //        doc.VehicleManufactureOptionNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi017" && x.Code == doc.VehicleManufactureOption);
        //        doc.ManufactureWithPrivilegeModeNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi067" && x.Code == doc.ManufactureWithPrivilegeMode);
        //        doc.AcceptCountryRegistrationNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi034" && x.Code == doc.AcceptCountryRegistration);
        //        //doc.AcceptRegionRegistration = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi034" && x.Code == doc.AcceptCountryRegistration);
        //        doc.VehicleMadeInCountryNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi034" && x.Code == doc.VehicleMadeInCountry);
        //        doc.StatusNsi = nsiValues.FirstOrDefault(x=>x.Nsi.Code == "nsi022" && x.Code == "00");

        //        if (doc.VehicleIdentityNumberId != "" && doc.VehicleIdentityNumberId != null)
        //        {
        //            var existDP = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.VehicleIdentityNumberId.ToLower() == doc.VehicleIdentityNumberId.ToLower()).FirstOrDefault();
        //            if (existDP != null)
        //            {
        //                response.Text = $"Ошибка уникальности Винкода - {doc.VehicleIdentityNumberId}!";
        //                throw new Exception(response.Text);
        //            }
        //        }
        //        var save = await _UnitOfWork.DigitalPassportRepository.InsertAsync(doc);
        //       if (save)
        //        {
        //            using (var transaction = _UnitOfWork.BeginTransaction())
        //            {
        //                try
        //                {
        //                    await _UnitOfWork.SaveAsync();
        //                    //save images
        //                    if (data.imagesTS.Count() > 0)
        //                    {
        //                        foreach (var item in data.imagesTS)
        //                        {
        //                            var image = new VehicleImage();
        //                            image.CreateDate = doc.Created;
        //                            image.FileName = item.name;
        //                            var base64Data = Regex.Match(item.image, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
        //                            image.VehiclePicture = Convert.FromBase64String(base64Data);
        //                            image.DigitalPassportId = doc.Id;
        //                            await _UnitOfWork.VehicleImageRepository.InsertAsync(image); 
        //                        }
        //                    }

        //                    data.CharacteristicDetails.DigitalPassportId = doc.Id;
        //                    var mongoSave = await _UnitOfWork.CharacteristicDetailsMongoRepository.CreateAsync(data.CharacteristicDetails);
        //                    var uniq = "13980";
        //                    uniq += data.VehicleEPassportBaseCode.Substring(1, 1);
        //                    var length = doc.Id.ToString().Length;

        //                    uniq += new String('0', (7 - doc.Id.ToString().Length));
        //                    uniq += doc.Id.ToString();
        //                    // some math
        //                    long mod = long.Parse(uniq) % 11;
        //                    if (mod == 10) uniq += "0";
        //                    else uniq += mod.ToString();
        //                    doc.UniqueCode = uniq;

        //                    save = _UnitOfWork.DigitalPassportRepository.Update(doc);
        //                    await _UnitOfWork.SaveAsync();
        //                    //throw new Exception("test");
        //                    _UnitOfWork.Commit();
        //                } catch (Exception ex)
        //                {
        //                    _UnitOfWork.Rollback();
        //                    _UnitOfWork.CharacteristicDetailsMongoRepository.RemoveFirst(new BsonDocument("DigitalPassportId", new BsonDocument("$eq", doc.Id)));
        //                    throw ex;
        //                }
        //                // anno
        //                if (doc.VehicleIdentityNumberId != "" && doc.VehicleIdentityNumberId != null)
        //                {
        //                    var dpHistory = new DigitalPassportStatusHistory { DigitalPassportId = doc.Id, Created = doc.Created };
        //                    // TODO ENUM FOR ServiceName
        //                    var dpInteractHistory = new ExternalInteractHistory { DigitalPassportId = doc.Id, Created = doc.Created, ServiceName="ROP" };
        //                    try
        //                    {
        //                        try
        //                        {
        //                            var ropRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns3=\"http://bip.bee.kz/SyncChannel/v10/Types\"><soap:Body><ns3:SendMessage><request><requestInfo>" +
        //                                    "<messageId>bdbb97a1-21b7-440b-ade7-a71e31e31b73</messageId><serviceId>GetRecycleVehicle</serviceId><messageDate>2020-08-27T12:49:47</messageDate><sender>" +
        //                                    "<senderId>epts</senderId><password>Dam3oha23dk!</password></sender></requestInfo><requestData><data><method>PaymentUtilizationFee</method>" +
        //                                    $"<vin>{doc.VehicleIdentityNumberId}</vin><iin>960610301846</iin><bin>000740000728</bin></data></requestData></request></ns3:SendMessage></soap:Body></soap:Envelope>";

        //                            //curl - X POST - H 'Content-type: text/xml' - d @req1.xml http://192.168.39.30:19022/bip-external-sync/ --insecure
        //                            var content = new StringContent(ropRequest, Encoding.UTF8, "text/xml");
        //                            var url = "http://192.168.39.30:19022/bip-external-sync/";
        //                            _logger.LogInformation($"Запрос в РОП по вин коду {doc.VehicleIdentityNumberId}");
        //                            using var client = new HttpClient();
        //                            var responseROP = await client.PostAsync(url, content);

        //                            string respRop = responseROP.Content.ReadAsStringAsync().Result;
        //                            _logger.LogInformation(respRop);
        //                            XmlDocument xmlDoc = new XmlDocument();
        //                            xmlDoc.LoadXml(respRop);
        //                            XmlNodeList elemlist = xmlDoc.GetElementsByTagName("code");
        //                            string resultCode = elemlist[0].InnerXml;

        //                            if (resultCode == "0" && xmlDoc.GetElementsByTagName("HavePay")?[0]?.InnerXml?.ToLower() == "true")
        //                            {
        //                                doc.StatusNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi022" && x.Code == "05");
        //                                dpHistory.Description = "Ответ РОП положительный";
        //                            }
        //                            else
        //                            {
        //                                dpHistory.Description = "Ответ РОП отрицательный";
        //                            }
        //                            dpInteractHistory.Description = respRop;
        //                            dpInteractHistory.StatusCode = resultCode;
        //                        }
        //                        catch (Exception exc)
        //                        {
        //                            _logger.LogInformation($"Запрос в РОП не прошел по причине {exc}");
        //                            dpHistory.StatusNsiId = doc.StatusNsi.Id;
        //                            dpHistory.Description = $"ROP: exc.Message";
        //                            dpInteractHistory.Description = exc.Message;
        //                            dpInteractHistory.StatusCode = "-1";
        //                        } finally
        //                        {
        //                            dpHistory.StatusNsiId = doc.StatusNsi.Id;
        //                            await _UnitOfWork.DigitalPassportStatusHistoryRepository.InsertAsync(dpHistory);
        //                            await _UnitOfWork.ExternalInteractHistoryRepository.InsertAsync(dpInteractHistory);
        //                            await _UnitOfWork.SaveAsync();
        //                        }

        //                    }
        //                    catch (Exception exc)
        //                    {
        //                        _logger.LogInformation($"Ошибка Сохранения в DigitalPassportStatusHistoryRepository: {exc}");
        //                    }

        //                }
        //            }
        //        }
        //        response.Status = "Ok";
        //        response.Text = "Сохранено";
        //        response.Data = doc.Id.ToString();
        //        _logger.LogInformation($" {HttpContext.Request.Headers["X-Real-IP"]}");
        //    } 
        //    catch (Exception e)
        //    {
        //        _logger.LogError($"Не удалось сохранить ЭП по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
        //    }
        //    return response;
        //}


        [HttpGet("GetDataToSign")]
        public async Task<JsonResponse> GetDataToSign(Guid signId)
        {
            var response = new JsonResponse { Text = "Не удалось взять данные для подписи ЭП", Action = Constants.ACTIONS.OK };
            try
            {
                _logger.LogInformation($"Digital Passport GetDataToSign Start DateTime = {DateTime.Now}, ip: { HttpContext.Request.Headers["X-Real-IP"]}");
                var data = _UnitOfWork.DraftPassportDtoMongoRepository.GetAsync(new BsonDocument("SignId", new BsonDocument("$eq", signId))).GetAwaiter().GetResult().FirstOrDefault();
                if (data == null) return response;
                //var passportDocDto = _mapper.Map<RootPassportDto>(data);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(RootPassportDto));

                using (var textWriter = new Utf8StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, data);
                    response.Data = new { dataToSign = textWriter.ToString() };
                }
                response.Status = "Ok";
                response.Text = "Данные для подписи";
                
            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось найти ЭП по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            return response;
        }

        
        [HttpPost("SaveSignData")]
        public async Task<JsonResponse> SaveSignData([FromBody] SignedDoc signedDoc)
        {
            var response = new JsonResponse
            {
                Text = "Не удалось подписать и сохранить",
                Action = Constants.ACTIONS.OK,
            };
            using (var transaction = _UnitOfWork.BeginTransaction())
            {
                try
                {
                    if (signedDoc.Id == Guid.Empty)
                    {
                        signedDoc.CreateDate = DateTime.Now;
                        var isSuccess = await _UnitOfWork.SignedDocRepository.InsertAsync(signedDoc);
                    }
                    else
                    {
                        signedDoc.ModifiedDate = DateTime.Now;
                        var isSuccess = _UnitOfWork.SignedDocRepository.Update(signedDoc);
                    }
                    //var doc = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(v => v.Deleted == null && v.SignId == signedDoc.DocId).AsNoTracking().FirstOrDefault();
                    var data = _UnitOfWork.DraftPassportDtoMongoRepository.GetAsync(new BsonDocument("SignId", new BsonDocument("$eq", signedDoc.DocId))).GetAwaiter().GetResult().FirstOrDefault();
                    var doc = _mapper.Map<DigitalPassportDocument>(data);
                    if (data == null) throw new Exception("Документ не найден!");

                    if (!await Save(data, doc, _UnitOfWork)) throw new Exception("Документ не сохранен!");
                    if (doc.UniqueCode == "" || doc.UniqueCode is null)
                    {
                        var uniq = "13980";
                        uniq += doc.VehicleEPassportBaseCode.Substring(1, 1);
                        var length = doc.Id.ToString().Length;

                        uniq += new String('0', (8 - doc.Id.ToString().Length));
                        uniq += doc.Id.ToString();
                        // some math
                        long mod = long.Parse(uniq) % 11;
                        if (mod == 10) uniq += "0";
                        else uniq += mod.ToString();
                        doc.UniqueCode = uniq;

                        _UnitOfWork.DigitalPassportRepository.Update(doc);
                    }
                    
                    //check for Kgd if base code means export ts
                    if (doc.VehicleEPassportBaseCode == "02" && doc.DocType != 3)
                    {
                        var kgdData = _UnitOfWork.KgdDataRepository.Get(x=>x.VinCode == doc.VehicleIdentityNumberId).FirstOrDefault();
                        if (kgdData != null)
                        {
                            doc.KgdDataId = kgdData.Id;
                        } else
                        {
                            doc.DeclineDate = DateTime.Now;
                            doc.DeclineDescription = "Отсутсвует оплата по данным КГД";
                        }
                    }
                    if (doc.VehicleEPassportBaseCode == "02" && doc.DocType == 3)
                    {
                        MshDatas mshData = null;
                        if (doc.VehicleIdentityNumberIndicator == "2")//{ key: 1, value: 'Идентификационный номер' },{ key: 2, value: 'Заводской номер' }
                            mshData = _UnitOfWork.MshDataRepository.Get(x => x.FactoryNumber == doc.VehicleIdentityNumberId).FirstOrDefault();
                        if (doc.VehicleIdentityNumberIndicator == "1")//{ key: 1, value: 'Идентификационный номер' },{ key: 2, value: 'Заводской номер' }
                            mshData = _UnitOfWork.MshDataRepository.Get(x => x.VinCode == doc.VehicleIdentityNumberId).FirstOrDefault();
                        if (mshData != null)
                        {
                            doc.MshDataId = mshData.Id;
                        }
                        else
                        {
                            doc.DeclineDate = DateTime.Now;
                            doc.DeclineDescription = "Отсутсвует оплата по данным МСХ";
                        }
                    }
                    // check for Rop
                    if (doc.VehicleIdentityNumberId != "" && doc.VehicleIdentityNumberId != null && doc.DeclineDescription == "")
                    {
                        var nsiValues = await _UnitOfWork.NsiValuesRepository
                            .GetAsIQueryable(v => v.Nsi.Code == "nsi022" && v.Deleted == null).Include(v => v.Nsi)
                            .ToListAsync();
                        var dpHistory = new DigitalPassportStatusHistory { DigitalPassportId = doc.Id, Created = doc.Created };
                        // TODO ENUM FOR ServiceName
                        var dpInteractHistory = new ExternalInteractHistory { DigitalPassportId = doc.Id, Created = doc.Created, ServiceName = "ROP" };
                        try
                        {
                            try
                            {
                                var ropRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns3=\"http://bip.bee.kz/SyncChannel/v10/Types\"><soap:Body><ns3:SendMessage><request><requestInfo>" +
                                        "<messageId>bdbb97a1-21b7-440b-ade7-a71e31e31b73</messageId><serviceId>GetRecycleVehicle</serviceId><messageDate>2020-08-27T12:49:47</messageDate><sender>" +
                                        "<senderId>epts</senderId><password>Dam3oha23dk!</password></sender></requestInfo><requestData><data><method>PaymentUtilizationFee</method>" +
                                        $"<vin>{doc.VehicleIdentityNumberId}</vin><iin>960610301846</iin><bin>000740000728</bin></data></requestData></request></ns3:SendMessage></soap:Body></soap:Envelope>";

                                //curl - X POST - H 'Content-type: text/xml' - d @req1.xml http://192.168.39.30:19022/bip-external-sync/ --insecure
                                var content = new StringContent(ropRequest, Encoding.UTF8, "text/xml");
                                var url = "http://192.168.39.30:19022/bip-external-sync/";
                                _logger.LogInformation($"Запрос в РОП по вин коду {doc.VehicleIdentityNumberId}");
                                using var client = new HttpClient();
                                var responseROP = await client.PostAsync(url, content);

                                string respRop = responseROP.Content.ReadAsStringAsync().Result;
                                _logger.LogInformation(respRop);
                                XmlDocument xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml(respRop);
                                XmlNodeList elemlist = xmlDoc.GetElementsByTagName("code");
                                string resultCode = elemlist[0].InnerXml;

                                if (resultCode == "0" && xmlDoc.GetElementsByTagName("HavePay")?[0]?.InnerXml?.ToLower() == "true")
                                {
                                    doc.StatusNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi022" && x.Code == "05");
                                    dpHistory.Description = "РОП оплачено";
                                    if (doc.BaseDocIndicator && doc.BaseVehicleEPassportId != "")
                                    {
                                        var oldDp = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(v => v.Deleted == null
                                            && v.UniqueCode == doc.BaseVehicleEPassportId
                                            && v.SignId != null).AsNoTracking().FirstOrDefault();
                                        if (oldDp == null) throw new Exception("Базовое ТС не найден");

                                        oldDp.StatusNsiId = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi022" && x.Code == "15").Id; // погашенный
                                        _UnitOfWork.DigitalPassportRepository.Update(oldDp);
                                    }
                                }
                                else
                                {
                                    dpHistory.Description = "РОП не оплачено";
                                    doc.DeclineDate = DateTime.Now;
                                    doc.DeclineDescription = "Ответ РОП отрицательный";
                                }
                                dpInteractHistory.Description = respRop;
                                dpInteractHistory.StatusCode = "5";
                            }
                            catch (Exception exc)
                            {
                                _logger.LogInformation($"Запрос в РОП не прошел по причине {exc}");
                                doc.DeclineDate = DateTime.Now;
                                doc.DeclineDescription = exc.Message != "Базовое ТС не найден"?"Нет доступа в РОП": "Базовое ТС не найден";
                                dpHistory.StatusNsiId = doc.StatusNsi.Id;
                                dpHistory.Description = $"ROP: {exc.Message}";
                                dpInteractHistory.Description = exc.Message;
                                dpInteractHistory.StatusCode = "-1";
                            }
                            finally
                            {
                                dpHistory.StatusNsiId = doc.StatusNsi.Id;
                                await _UnitOfWork.DigitalPassportStatusHistoryRepository.InsertAsync(dpHistory);
                                await _UnitOfWork.ExternalInteractHistoryRepository.InsertAsync(dpInteractHistory);
                            }

                        }
                        catch (Exception exc)
                        {
                            _logger.LogInformation($"Ошибка Сохранения в DigitalPassportStatusHistoryRepository: {exc}");
                            throw exc;
                        }

                    }
                    // удалить из монго черновик
                    _UnitOfWork.DraftPassportDtoMongoRepository.RemoveFirst(new BsonDocument("SignId", new BsonDocument("$eq", doc.SignId)));
                    await _UnitOfWork.SaveAsync();
                    response.Data = signedDoc;
                    _UnitOfWork.Commit();
                    response.Text = "Подписано и сохранено УСПЕШНО!";
                    response.Status = "Ok";
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Save SignedDoc");
                    _UnitOfWork.Rollback();
                    response.Text = e.Message;
                    response.Status = "Error";
                }
            }
            return response;
        }

        private async Task<bool> Save( RootPassportDto data, DigitalPassportDocument doc, IUnitOfWork _UnitOfWork)
        {
            //nsi references
            var nsiValues = await _UnitOfWork.NsiValuesRepository
                    .GetAsIQueryable(v => new List<string>() { "nsi012", "nsi013", "nsi025", "nsi113", "nsi014", "nsi021", "nsi031", "nsi065", "nsi046", "nsi015", "nsi016", "nsi017", "nsi067", "nsi034", "nsi022" }.Contains(v.Nsi.Code) && v.Deleted == null).Include(v => v.Nsi)
                    .ToListAsync();
            doc.DocKindNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi012" && x.Code == doc.DocKindCode);
            doc.DocumentNotExistDescriptionNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi025" && x.Code == doc.DocumentNotExistDescription);
            doc.DocKindCodeHandyNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi113" && x.Code == doc.DocKindCodeHandy);
            doc.VehicleCategoryCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi014" && x.Code == doc.VehicleCategoryCode);
            doc.VehicleClassCategoryCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi013" && x.Code == doc.VehicleClassCategoryCode);
            if (doc.MoverEpsm != "")
                doc.MoverEpsmNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi031" && x.Code == doc.MoverEpsm);

            doc.VehicleBodyColourCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi021" && x.Code == doc.VehicleBodyColourCode);
            doc.VehicleBodyColourCode2Nsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi021" && x.Code == doc.VehicleBodyColourCode2);
            doc.VehicleBodyColourCode3Nsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi021" && x.Code == doc.VehicleBodyColourCode3);
            doc.ManufactureMonthNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi065" && x.Code == doc.ManufactureMonth);
            doc.VehicleMakeNameNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi046" && x.Code == doc.VehicleMakeName);
            doc.BaseVehicleMakeNameNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi046" && x.Code == doc.BaseVehicleMakeName);
            doc.VehicleTechCategoryCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi015" && x.Code == doc.VehicleTechCategoryCode);
            doc.VehicleEcoClassCodeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi016" && x.Code == doc.VehicleEcoClassCode);
            doc.VehicleManufactureOptionNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi017" && x.Code == doc.VehicleManufactureOption);
            doc.ManufactureWithPrivilegeModeNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi067" && x.Code == doc.ManufactureWithPrivilegeMode);
            //doc.AcceptCountryRegistrationNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi034" && x.Code == doc.AcceptCountryRegistration);
            //doc.AcceptRegionRegistration = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi034" && x.Code == doc.AcceptCountryRegistration);
            doc.VehicleMadeInCountryNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi034" && x.Code == doc.VehicleMadeInCountry);
            doc.StatusNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi022" && x.Code == "00");
            doc.Created = DateTime.Now;
            if (doc.AssemblerOrganizations != null && doc.AssemblerOrganizations.Count == 1 && doc.AssemblerOrganizations.FirstOrDefault() == null)
                doc.AssemblerOrganizations = null;
            if (doc.MemberOrganizations != null && doc.MemberOrganizations.Count == 1 && doc.MemberOrganizations.FirstOrDefault() == null)
                doc.MemberOrganizations = null;
            var save = await _UnitOfWork.DigitalPassportRepository.InsertAsync(doc);
            if (save)
            {
                    try
                    {
                        await _UnitOfWork.SaveAsync();
                        //save images
                        if (data.imagesTS.Count() > 0)
                        {
                            foreach (var item in data.imagesTS)
                            {
                                var image = new VehicleImage();
                                image.CreateDate = doc.Created;
                                image.FileName = item.name;
                                var base64Data = Regex.Match(item.image, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                                image.VehiclePicture = Convert.FromBase64String(base64Data);
                                image.DigitalPassportId = doc.Id;
                                await _UnitOfWork.VehicleImageRepository.InsertAsync(image);
                            }
                        }

                        data.CharacteristicDetails.DigitalPassportId = doc.Id;
                        var mongoSave = await _UnitOfWork.CharacteristicDetailsMongoRepository.CreateAsync(data.CharacteristicDetails);

                        save = _UnitOfWork.DigitalPassportRepository.Update(doc);
                    }
                    catch (Exception ex)
                    {
                        _UnitOfWork.CharacteristicDetailsMongoRepository.RemoveFirst(new BsonDocument("DigitalPassportId", new BsonDocument("$eq", doc.Id)));
                        throw ex;
                    }
                
            }
            else return save;
            return true;
        }

        [HttpPost("CheckVinCodeExists")]
        public async Task<JsonResponse> CheckVinCodeExists([FromBody] string vin)
        {
            var response = new JsonResponse { Text = "Недопустимый винкод", Action = Constants.ACTIONS.OK };
            try
            {
                var existMvd = _UnitOfWork.MvdDataRepository.GetAsIQueryable(x => x.VinCode == vin).FirstOrDefault();
                var existDP = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.VehicleIdentityNumberId.ToLower() == vin.ToLower()).FirstOrDefault();
                response.Status = "Ok";
                if (vin != null && vin.Trim() != "" && existMvd == null && existDP == null)
                {
                    response.Text = "Проверка прошла успешно";
                    response.Data = "1";
                }                
                _logger.LogInformation($" {HttpContext.Request.Headers["X-Real-IP"]}");
            }
            catch (Exception e)
            {
                response.Text = "Не удалось проверить винкод";
                response.Data = "";
                _logger.LogError($"Не удалось проверить вин по базе мвд по причине: {HttpContext.Request.Headers["X-Real-IP"]}, {e}");
            }
            return response;
        }

        #region for Application
        
        [HttpGet("GetDigitalPassportListByKind")]
        public async Task<JsonResponse> GetDigitalPassportListByKind(string kind)
        {
            var response = new JsonResponse
            {
                Text = "Нет данных",
                Action = Constants.ACTIONS.OK,
            };
            try
            {
                var currentUser = GetUser();
                if (!User.HasClaim(CustomClaimTypes.Permissions, DocumentPermissions.EPassport.Edit))
                    throw new Exception("Данные запрещены");
                if (GetUserRoles().Any(x => x == "КИР МИИР РК" || x == "administrator" || x == "Национальный оператор"))
                {
                    throw new Exception("Данные запрещены");
                }
                var q = _UnitOfWork.DigitalPassportRepository
                    .GetAsIQueryable(v => v.Deleted == null && v.AspNetUser.OrganizationId == currentUser.OrganizationId && v.DocType.ToString() == kind);
                var entities = await q.Include(v => v.AspNetUser.Organization)
                    .Include(v => v.StatusNsi)
                    .OrderByDescending(v => v.Created)
                    .ToListAsync();
                response.Data = entities.Select(x => new { key = x.Id, value = x.UniqueCode, statusCode = x.StatusNsi.Code, statusName = x.StatusNsi.NameRu }).ToArray();
                response.Text = "";
                response.Status = "Ok";
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetDigitalPassportListByKind");
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }
        #endregion
    }
}

﻿using AutoMapper;
using DBCore;
using DBCore.Models;
using DbLog;
using EptsProject.Core;
using EptsProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Controllers.Reestrs
{
    /// <summary>
    /// Подписанные документы
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AppLogController : ControllerBase
    {
        private readonly ILogUnitOfWork _unitOfWork;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        public AppLogController(ILogUnitOfWork unitOfWork)
        {               
            _unitOfWork = unitOfWork;   
        }

  
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("getUserLogs")]
        public async Task<JsonResponse> GetUserLogs([FromQuery] int page, [FromQuery] int pageSize)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
               response.Data = await _unitOfWork.UserLogRepository.GetAsIQueryable()
                    .OrderByDescending(v => v.EventDate)
                    .Skip(page * pageSize)
                    .Take(pageSize)
                    .ToListAsync();
            }
            catch (Exception e)
            {
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("getAuditLogs")]
        public async Task<JsonResponse> GetAuditLogs([FromQuery] int page, [FromQuery] int pageSize)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                response.Data = await _unitOfWork.AuditLogRepository.GetAsIQueryable()
                     .OrderByDescending(v => v.ActionDate)
                     .Skip(page * pageSize)
                     .Take(pageSize)
                     .ToListAsync();
            }
            catch (Exception e)
            {
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="onlyExceptions"></param>
        /// <returns></returns>
        [HttpGet("getAppLogs")]
        public async Task<JsonResponse> GetAppLogs([FromQuery] int page, [FromQuery] int pageSize, [FromQuery] bool onlyExceptions)
        {
            var response = new JsonResponse
            {
                Status = "Ok",
                Text = "",
                Action = Constants.ACTIONS.OK,
                Data = null
            };

            try
            {
                var q = _unitOfWork.AppLogRepository.GetAsIQueryable();
                if (onlyExceptions)
                    q = q.Where(al => !string.IsNullOrEmpty(al.Exception));

                response.Data = await q
                     .OrderByDescending(v => v.LoggedDate)
                     .Skip(page * pageSize)
                     .Take(pageSize)
                     .ToListAsync();
            }
            catch (Exception e)
            {
                response.Text = e.Message;
                response.Status = "Error";
            }
            return response;
        }

    }
}

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using VueCliMiddleware;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using DBCore.Models;
using DBCore;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.CookiePolicy;
using EptsProject.Core;
using System.Threading.Tasks;
using EptsProject.Models;
using System.Runtime.Serialization.Formatters.Binary;
using AutoMapper;
using DbLog;
using EptsProject.Helpers;

namespace EptsProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// 
        public IConfiguration Configuration { get; }

        /// This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            // Add AddRazorPages if the app uses Razor Pages.
            services.AddRazorPages();

            // In production, the Vue files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "EPTS Service API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      Example: 'Bearer 12345abcdef'",
                    BearerFormat = "JWT",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                  {
                    {
                      new OpenApiSecurityScheme
                      {
                        Reference = new OpenApiReference
                          {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                          },
                          Scheme = "oauth2",
                          Name = "Bearer",
                          In = ParameterLocation.Header,

                        },
                        new List<string>()
                      }
                    });
                c.CustomOperationIds(e => $"{e.ActionDescriptor.RouteValues["controller"]}_{e.HttpMethod}");
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            // Mongo
            services.Configure<MongoDatabaseSettings>(
                Configuration.GetSection(nameof(MongoDatabaseSettings)));

            services.AddSingleton<IMongoDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<MongoDatabaseSettings>>().Value);

            // SQL
            services.Configure<SQLDatabaseSettings>(
                Configuration.GetSection(nameof(SQLDatabaseSettings)));

            services.AddSingleton<ISQLDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<SQLDatabaseSettings>>().Value);

            services.AddControllers().AddNewtonsoftJson();

            services.AddDbContext<EptsContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"),  b => b.MigrationsAssembly("DBCore")),
                ServiceLifetime.Transient);

            services.AddDbContext<LogContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("LogConnection"), b => b.MigrationsAssembly("DbLog")),
                ServiceLifetime.Transient);

            services.AddIdentity<AspNetUsers, AspNetRoles> (opts => {
                    opts.Password.RequiredLength = 8;   // ����������� �����
                    opts.Password.RequireNonAlphanumeric = true;   // ��������� �� �� ���������-�������� �������
                    opts.Password.RequireLowercase = true; // ��������� �� ������� � ������ ��������
                    opts.Password.RequireUppercase = true; // ��������� �� ������� � ������� ��������
                    opts.Password.RequireDigit = true; // ��������� �� �����
                    opts.User.RequireUniqueEmail = true;    // ���������� email
                    opts.User.AllowedUserNameCharacters = ".@#$%^&*abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; // ���������� �������

                    opts.Lockout.AllowedForNewUsers = true;
                    opts.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
                    opts.Lockout.MaxFailedAccessAttempts = 3;
                })
                .AddEntityFrameworkStores<EptsContext>()
                .AddDefaultTokenProviders();
            services.Configure<SecurityStampValidatorOptions>(options => options.ValidationInterval = TimeSpan.FromSeconds(10));
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .Services.ConfigureApplicationCookie(options =>
                {
                    options.SlidingExpiration = true;
                    options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                    options.LogoutPath = "/api/Account/ShouldLogOut";
                    options.LoginPath = "/api/Account/ShouldLogin";
                    options.AccessDeniedPath = "/api/Account/AccessDenied";
                    options.Events.OnRedirectToLogin = context =>
                    {
                        if (context.Request.Path.StartsWithSegments("/api")
                            && context.Response.StatusCode == StatusCodes.Status200OK)
                        {
                            context.Response.Clear();
                            string jr = "{\"status\":\"Ok\",\"text\":\"ShouldLogin\",\"action\":0,\"data\":{}}";
                            byte[] data = Encoding.UTF8.GetBytes(jr);
                            context.Response.ContentType = "application/json";
                            context.Response.StatusCode = StatusCodes.Status200OK;//.Status511NetworkAuthenticationRequired;
                            context.Response.Body.WriteAsync(data, 0, data.Length).Wait();
                            return Task.FromResult<object>(null);
                        }
                        context.Response.Redirect(context.RedirectUri);
                        return Task.FromResult<object>(null);
                    };
                });

            // Uinon work with all db's
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ILogUnitOfWork, LogUnitOfWork>();
            services.AddScoped<AntiForgeryAttribute>();

            services.AddAutoMapper(typeof(AspNetUsers).Assembly);
            services.AddHttpClient();

            

            registerModules(services);

            #region JWT settings
            //services.AddAuthentication(options =>
            //{
            //    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //    //options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
            //});
            // For jwt
            //services.AddAuthentication(option =>
            //{
            //    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            //}).AddJwtBearer(options =>
            //{
            //    options.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuer = true,
            //        ValidateAudience = true,
            //        ValidateLifetime = true,
            //        ValidateIssuerSigningKey = true,
            //        ValidIssuer = Configuration["JwtToken:Issuer"],
            //        ValidAudience = Configuration["JwtToken:Issuer"],
            //        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtToken:SecretKey"])), //Configuration["JwtToken:SecretKey"]
            //        ClockSkew = TimeSpan.FromMinutes(1)//�������� expiration �� 1 ������ �� ���������� �������
            //    };
            //});
            #endregion
        }

        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
                //app.UseHttpsRedirection();
            }
            app.UseDeveloperExceptionPage();
            app.UseCookiePolicy(new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.Strict,
                HttpOnly = HttpOnlyPolicy.Always,
                //Secure = CookieSecurePolicy.Always
            });

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseSwagger(c => { c.SerializeAsV2 = true; });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Remote Control Service API V1");
                //c.InjectJavascript("../Content/swagger-bearer-auth.js");
            });

            app.UseRouting();

            app.UseAuthentication();    // ����������� ��������������
            app.UseAuthorization();     // ������� ���� ����� UseRouting � UseEndpoints

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");

                if (env.IsDevelopment())
                {
                    endpoints.MapToVueCliProxy(
                        "{*path}",
                        new SpaOptions { SourcePath = "ClientApp" },
                        npmScript: "serve",
                        regex: "Compiled successfully");
                }

                // Add MapRazorPages if the app uses Razor Pages. Since Endpoint Routing includes support for many frameworks, adding Razor Pages is now opt -in.
                endpoints.MapRazorPages();
            });

            AppCustomContext.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";
            });

            migrate(app);
        }
    
        private void registerModules(IServiceCollection services)
        {
            new EptsBL.EptsBLModule().Register(services);
        }
    
        private void migrate(IApplicationBuilder app)
        {
            new SimpleSelfMigrator().Migrate(app);
            new LogSelfMigrator().Migrate(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Security.Cryptography;
using DBCore;
using Microsoft.Extensions.Logging;
using DBCore.Models;

namespace EptsProject.Core
{
    public class AntiForgeryManager
    {
        protected internal IUnitOfWork _UnitOfWork;
        protected internal readonly ILogger _logger;
        static internal Dictionary<string, List<string>> csrfSecurity = new Dictionary<string, List<string>>();
        int maxTokenCount = 2;
        public AntiForgeryManager(ILogger logger, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _UnitOfWork = unitOfWork;
        }

        public string CreateAppHash()
        {
            string token = "";
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                byte[] tokenData = new byte[32];
                rng.GetBytes(tokenData);

                token = Convert.ToBase64String(tokenData);
            }
            return token;
        }

        public bool SaveAppHash(string userid, string token)
        {
            var resp = false;
            try
            {
                if (!csrfSecurity.ContainsKey(userid)) csrfSecurity.Add(userid, new List<string>());
                var userTokenList = csrfSecurity.GetValueOrDefault(userid);
                if (userTokenList.Count >= maxTokenCount) userTokenList.RemoveAt(0);
                userTokenList.Add(token);
                Guid uId = Guid.Parse(userid);
                var tokens = _UnitOfWork.TokenCacheRepository.GetAsIQueryable().Where(x => x.UserId == uId).ToList();
                if (tokens.Count() >= maxTokenCount)
                {
                    _UnitOfWork.TokenCacheRepository.Delete(tokens.FirstOrDefault());
                }
                var tokenCache = new TokenCache() { UserId = uId, Token = token };
                var inserted = _UnitOfWork.TokenCacheRepository.Insert(tokenCache);
                _UnitOfWork.Save();
                resp = true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "SaveAppHash");
                //csrfSecurity.Remove;
            }
            return resp;
        }

        public static string RemoveAppHash(string username)
        {
            string hash = "";
            return hash;
        }

        public static string GetAppHash(string username)
        {
            string hash = "";
            return hash;
        }
    }
}

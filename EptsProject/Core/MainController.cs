﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DBCore;
using DBCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using System.IO;
using System.Reflection;
using AutoMapper;

namespace EptsProject.Core
{
    public abstract class MainController : ControllerBase
    {

        protected internal IUnitOfWork _UnitOfWork;
        protected internal readonly ILogger _logger;

        protected internal readonly UserManager<AspNetUsers> _userManager;
        protected internal readonly SignInManager<AspNetUsers> _signInManager;

        protected private IMapper _mapper;

        protected MainController(ILogger logger, IUnitOfWork unitOfWork, UserManager<AspNetUsers> userManager,
            SignInManager<AspNetUsers> signInManager)
        {
            _logger = logger;
            _UnitOfWork = unitOfWork;
            _userManager = userManager;
            _signInManager = signInManager;
            //_logger.LogDebug("123456789");
        }
        protected MainController(ILogger logger, IUnitOfWork unitOfWork, UserManager<AspNetUsers> userManager, IMapper mapper)
        {
            _logger = logger;
            _UnitOfWork = unitOfWork;
            _userManager = userManager; 
            _mapper = mapper;
        }

        protected AspNetUsers GetUser()
        {
            return _userManager.GetUserAsync(User).Result;
        }

        protected IList<string> GetUserRoles()
        {
            return _userManager.GetRolesAsync(GetUser()).Result;
        }

        protected string GetLogMessageFormat(string message)
        {
            if (User?.Identity?.Name != null)
                return $"{HttpContext.Request.Headers["X-Real-IP"]}, {User.Identity.Name}, {message}";
            else
                return $"{HttpContext.Request.Headers["X-Real-IP"]}, {message}";
        }
    }
}
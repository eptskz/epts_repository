﻿using DBCore;
using DBCore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EptsProject.Core
{
    public class AntiForgeryAttribute: ActionFilterAttribute
    {
        protected internal IUnitOfWork _UnitOfWork;
        
        public AntiForgeryAttribute(IUnitOfWork unitOfWork)
        {
            _UnitOfWork = unitOfWork;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var b = filterContext.HttpContext.User.Identity;
            var user = _UnitOfWork.UserRepository.GetAsIQueryable().Where(x=>x.UserName == filterContext.HttpContext.User.Identity.Name).FirstOrDefault();
            if (filterContext.HttpContext.Request.Headers.ContainsKey("Authorization") && user != null)
            {
                Microsoft.Extensions.Primitives.StringValues authHeader;
                if (filterContext.HttpContext.Request.Headers.TryGetValue("Authorization", out authHeader))
                {
                    var authVals = authHeader.ToString().Split(" ");
                    if (authVals.Length == 2 && authVals[0] == "Bearer" && authVals[1].Length > 10)
                    {
                        var tokens = _UnitOfWork.TokenCacheRepository.GetAsIQueryable().Where(x => x.UserId == user.Id && x.Token == authVals[1]).FirstOrDefault();
                        if (tokens ==  null) filterContext.Result = new RedirectResult("~/api/Account/ShouldLogin");
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult("~/api/Account/ShouldLogin");
                    }
                }else
                    {
                        filterContext.Result = new RedirectResult("~/api/Account/ShouldLogin");
                    }
            } else {
                filterContext.Result = new RedirectResult("~/api/Account/ShouldLogin");
            }
            base.OnActionExecuting(filterContext);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EptsProject.Core;

namespace EptsProject.Models
{
    [Serializable]
    public class JsonResponse
    {
        public string Status = "Error";
        public string Text = "Ошибка";
        public Constants.ACTIONS Action = 0;

        public object Data { get; set; }
    }
}

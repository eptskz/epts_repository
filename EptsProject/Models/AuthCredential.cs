﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Models
{
    public class AuthCredential
    {
        public string Token { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Models
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class UploadFileModel
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid? DocumentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DocumentType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SubPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IFormFile File { get; set; }
    }
}

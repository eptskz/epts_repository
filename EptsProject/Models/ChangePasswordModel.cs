﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EptsProject.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ChangePasswordModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Login { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OldPassword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ConfirmPassword { get; set; }
    }
}

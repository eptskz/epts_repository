-- Constraint: FK_VehicleCabins_CharacteristicsDetail_CharacteristicsDetailId

ALTER TABLE public."VehicleCabins" DROP CONSTRAINT "FK_VehicleCabins_CharacteristicsDetail_CharacteristicsDetailId";

ALTER TABLE public."VehicleCabins"
    ADD CONSTRAINT "FK_VehicleCabins_CharacteristicsDetail_CharacteristicsDetailId" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;


ALTER TABLE public."VehicleCarriageSpaceImplementations" DROP CONSTRAINT "FK_VehicleCarriageSpaceImplementations_CharacteristicsDetail_C~";

ALTER TABLE public."VehicleCarriageSpaceImplementations"
    ADD CONSTRAINT "FK_VehicleCarriageSpaceImplementations_CharacteristicsDetail_C~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	

ALTER TABLE public."VehicleEngineLayouts" DROP CONSTRAINT "FK_VehicleEngineLayouts_CharacteristicsDetail_CharacteristicsD~";

ALTER TABLE public."VehicleEngineLayouts"
    ADD CONSTRAINT "FK_VehicleEngineLayouts_CharacteristicsDetail_CharacteristicsD~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;


ALTER TABLE public."VehicleLengthMeasures" DROP CONSTRAINT "FK_VehicleLengthMeasures_CharacteristicsDetail_Characteristics~";

ALTER TABLE public."VehicleLengthMeasures"
    ADD CONSTRAINT "FK_VehicleLengthMeasures_CharacteristicsDetail_Characteristics~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehiclePassengerQuantities" DROP CONSTRAINT "FK_VehiclePassengerQuantities_CharacteristicsDetail_Characteri~";

ALTER TABLE public."VehiclePassengerQuantities"
    ADD CONSTRAINT "FK_VehiclePassengerQuantities_CharacteristicsDetail_Characteri~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleRunningGearDetails" DROP CONSTRAINT "FK_VehicleRunningGearDetails_CharacteristicsDetail_Characteris~";

ALTER TABLE public."VehicleRunningGearDetails"
    ADD CONSTRAINT "FK_VehicleRunningGearDetails_CharacteristicsDetail_Characteris~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleSeatDetails" DROP CONSTRAINT "FK_VehicleSeatDetails_CharacteristicsDetail_CharacteristicsDet~";

ALTER TABLE public."VehicleSeatDetails"
    ADD CONSTRAINT "FK_VehicleSeatDetails_CharacteristicsDetail_CharacteristicsDet~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."EngineDetails" DROP CONSTRAINT "FK_EngineDetails_CharacteristicsDetail_CharacteristicsDetailId";

ALTER TABLE public."EngineDetails"
    ADD CONSTRAINT "FK_EngineDetails_CharacteristicsDetail_CharacteristicsDetailId" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleHeightMeasures" DROP CONSTRAINT "FK_VehicleHeightMeasures_CharacteristicsDetail_Characteristics~";

ALTER TABLE public."VehicleHeightMeasures"
    ADD CONSTRAINT "FK_VehicleHeightMeasures_CharacteristicsDetail_Characteristics~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleLayoutPatterns" DROP CONSTRAINT "FK_VehicleLayoutPatterns_CharacteristicsDetail_Characteristics~";

ALTER TABLE public."VehicleLayoutPatterns"
    ADD CONSTRAINT "FK_VehicleLayoutPatterns_CharacteristicsDetail_Characteristics~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleWheelbaseMeasures" DROP CONSTRAINT "FK_VehicleWheelbaseMeasures_CharacteristicsDetail_Characterist~";

ALTER TABLE public."VehicleWheelbaseMeasures"
    ADD CONSTRAINT "FK_VehicleWheelbaseMeasures_CharacteristicsDetail_Characterist~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleWidthMeasures" DROP CONSTRAINT "FK_VehicleWidthMeasures_CharacteristicsDetail_CharacteristicsD~";

ALTER TABLE public."VehicleWidthMeasures"
    ADD CONSTRAINT "FK_VehicleWidthMeasures_CharacteristicsDetail_CharacteristicsD~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."TransmissionTypes" DROP CONSTRAINT "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicsDeta~";

ALTER TABLE public."TransmissionTypes"
    ADD CONSTRAINT "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicsDeta~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleAxleDetails" DROP CONSTRAINT "FK_VehicleAxleDetails_CharacteristicsDetail_CharacteristicsDet~";

ALTER TABLE public."VehicleAxleDetails"
    ADD CONSTRAINT "FK_VehicleAxleDetails_CharacteristicsDetail_CharacteristicsDet~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleBodyworkDetails" DROP CONSTRAINT "FK_VehicleBodyworkDetails_CharacteristicsDetail_Characteristic~";

ALTER TABLE public."VehicleBodyworkDetails"
    ADD CONSTRAINT "FK_VehicleBodyworkDetails_CharacteristicsDetail_Characteristic~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleBrakingSystemDetails" DROP CONSTRAINT "FK_VehicleBrakingSystemDetails_CharacteristicsDetail_Character~";

ALTER TABLE public."VehicleBrakingSystemDetails"
    ADD CONSTRAINT "FK_VehicleBrakingSystemDetails_CharacteristicsDetail_Character~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleClutchDetails" DROP CONSTRAINT "FK_VehicleClutchDetails_CharacteristicsDetail_CharacteristicsD~";

ALTER TABLE public."VehicleClutchDetails"
    ADD CONSTRAINT "FK_VehicleClutchDetails_CharacteristicsDetail_CharacteristicsD~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleEquipmentInfos" DROP CONSTRAINT "FK_VehicleEquipmentInfos_CharacteristicsDetail_Characteristics~";

ALTER TABLE public."VehicleEquipmentInfos"
    ADD CONSTRAINT "FK_VehicleEquipmentInfos_CharacteristicsDetail_Characteristics~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleSteeringDetails" DROP CONSTRAINT "FK_VehicleSteeringDetails_CharacteristicsDetail_Characteristic~";

ALTER TABLE public."VehicleSteeringDetails"
    ADD CONSTRAINT "FK_VehicleSteeringDetails_CharacteristicsDetail_Characteristic~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleSuspensionDetails" DROP CONSTRAINT "FK_VehicleSuspensionDetails_CharacteristicsDetail_Characterist~";

ALTER TABLE public."VehicleSuspensionDetails"
    ADD CONSTRAINT "FK_VehicleSuspensionDetails_CharacteristicsDetail_Characterist~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleTyreKindInfos" DROP CONSTRAINT "FK_VehicleTyreKindInfos_CharacteristicsDetail_CharacteristicsD~";

ALTER TABLE public."VehicleTyreKindInfos"
    ADD CONSTRAINT "FK_VehicleTyreKindInfos_CharacteristicsDetail_CharacteristicsD~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleWeightMeasures" DROP CONSTRAINT "FK_VehicleWeightMeasures_CharacteristicsDetail_Characteristics~";

ALTER TABLE public."VehicleWeightMeasures"
    ADD CONSTRAINT "FK_VehicleWeightMeasures_CharacteristicsDetail_Characteristics~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleFrames" DROP CONSTRAINT "FK_VehicleFrames_CharacteristicsDetail_CharacteristicsDetailId";

ALTER TABLE public."VehicleFrames"
    ADD CONSTRAINT "FK_VehicleFrames_CharacteristicsDetail_CharacteristicsDetailId" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehiclePurposes" DROP CONSTRAINT "FK_VehiclePurposes_CharacteristicsDetail_CharacteristicsDetail~";

ALTER TABLE public."VehiclePurposes"
    ADD CONSTRAINT "FK_VehiclePurposes_CharacteristicsDetail_CharacteristicsDetail~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."VehicleTrunkVolumeMeasures" DROP CONSTRAINT "FK_VehicleTrunkVolumeMeasures_CharacteristicsDetail_Characteri~";

ALTER TABLE public."VehicleTrunkVolumeMeasures"
    ADD CONSTRAINT "FK_VehicleTrunkVolumeMeasures_CharacteristicsDetail_Characteri~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."BrakedTrailerWeightMeasures" DROP CONSTRAINT "FK_BrakedTrailerWeightMeasures_CharacteristicsDetail_Character~";

ALTER TABLE public."BrakedTrailerWeightMeasures"
    ADD CONSTRAINT "FK_BrakedTrailerWeightMeasures_CharacteristicsDetail_Character~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."UnbrakedTrailerWeightMeasures" DROP CONSTRAINT "FK_UnbrakedTrailerWeightMeasures_CharacteristicsDetail_Charact~";

ALTER TABLE public."UnbrakedTrailerWeightMeasures"
    ADD CONSTRAINT "FK_UnbrakedTrailerWeightMeasures_CharacteristicsDetail_Charact~" FOREIGN KEY ("CharacteristicsDetailId")
    REFERENCES public."CharacteristicsDetail" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."TransmissionUnitDetails" DROP CONSTRAINT "FK_TransmissionUnitDetails_TransmissionTypes_TransmissionTypeId";

ALTER TABLE public."TransmissionUnitDetails"
    ADD CONSTRAINT "FK_TransmissionUnitDetails_TransmissionTypes_TransmissionTypeId" FOREIGN KEY ("TransmissionTypeId")
    REFERENCES public."TransmissionTypes" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
	
ALTER TABLE public."AspNetUsers" DROP CONSTRAINT "FK_AspNetUsers_Organizations_OrganizationId";

ALTER TABLE public."AspNetUsers"
    ADD CONSTRAINT "FK_AspNetUsers_Organizations_OrganizationId" FOREIGN KEY ("OrganizationId")
    REFERENCES public."Organizations" ("Id") MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE RESTRICT;
UPDATE public."NsiValues"
	SET 
	"PowerWheelQuantity"= split_part("Code", 'x', 1)::INTEGER
	, "WheelQuantity"= split_part("Code", 'x', 2)::INTEGER
WHERE "NsiId" in (select "Id" from "Nsis" where "Code" = 'nsi045');
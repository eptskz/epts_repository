﻿using DBCore;
using DBCore.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NLog;
using NLog.Config;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RopJobScheduler
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            LogManager.Configuration = new XmlLoggingConfiguration($"{AppContext.BaseDirectory}NLog.config");
            logger.Info("Start job");
            IConfiguration Config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
            IMongoDatabaseSettings mongoDatabaseSettings = new MongoDatabaseSettings{ ConnectionString = Config.GetSection(nameof(MongoDatabaseSettings))["ConnectionString"], DatabaseName = Config.GetSection(nameof(MongoDatabaseSettings))["DatabaseName"] };
            ISQLDatabaseSettings sqlDatabaseSettings = new SQLDatabaseSettings { ConnectionString = Config.GetSection(nameof(SQLDatabaseSettings))["ConnectionString"], DatabaseName = Config.GetSection(nameof(SQLDatabaseSettings))["DatabaseName"] };
            UnitOfWork unitOfWork = new UnitOfWork(mongoDatabaseSettings, sqlDatabaseSettings, null);
            //var dp = unitOfWork.DigitalPassportRepository.GetAsIQueryable(x => x.Deleted == null).ToListAsync().Result;
            var checkPassport = new CheckPassport(logger, unitOfWork);
            checkPassport.Check().Wait();
        }
    }

    class CheckPassport
    {
        IUnitOfWork _UnitOfWork;
        Logger _logger;
        public CheckPassport(Logger logger, IUnitOfWork unitOfWork)
        {
            _UnitOfWork = unitOfWork;
            _logger = logger;
        }
        public async Task Check()
        {
            var nsiValues = await _UnitOfWork.NsiValuesRepository
                            .GetAsIQueryable(v => v.Nsi.Code == "nsi022" && v.Deleted == null).Include(v => v.Nsi)
                            .ToListAsync();
            var passports = await _UnitOfWork.DigitalPassportRepository
                            .GetAsIQueryable(v => v.StatusNsi.Code == "00" &&
                                (v.VehicleEPassportBaseCode != "02"
                                || (v.VehicleEPassportBaseCode == "02" && (v.DocType == 1 || v.DocType == 2) && v.KgdDataId != null)
                                || (v.VehicleEPassportBaseCode == "02" && v.DocType == 3 && v.MshDataId != null)
                                )
                            && v.Deleted == null).Include(v => v.StatusNsi)
                            .ToListAsync();
            Console.WriteLine(passports.Count);
            _logger.Info($"Count of Passport to ROP check is {passports.Count}");
            foreach (var doc in passports)
            {
                using (var transaction = _UnitOfWork.BeginTransaction())
                {
                    try
                    {
                        if (doc.VehicleIdentityNumberId != "" && doc.VehicleIdentityNumberId != null)
                        {
                            var dpHistory = new DigitalPassportStatusHistory { DigitalPassportId = doc.Id, Created = doc.Created };
                            // TODO ENUM FOR ServiceName
                            var dpInteractHistory = new ExternalInteractHistory { DigitalPassportId = doc.Id, Created = doc.Created, ServiceName = "ROP" };

                            try
                            {
                                var ropRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns3=\"http://bip.bee.kz/SyncChannel/v10/Types\"><soap:Body><ns3:SendMessage><request><requestInfo>" +
                                        $"<messageId>{Guid.NewGuid()}</messageId><serviceId>GetRecycleVehicle</serviceId><messageDate>{DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")}</messageDate><sender>" +
                                        "<senderId>epts</senderId><password>Dam3oha23dk!</password></sender></requestInfo><requestData><data><method>PaymentUtilizationFee</method>" +
                                        $"<vin>{doc.VehicleIdentityNumberId}</vin><iin>960610301846</iin><bin>000740000728</bin></data></requestData></request></ns3:SendMessage></soap:Body></soap:Envelope>";
                                //2020-08-27T12:49:47
                                //curl - X POST - H 'Content-type: text/xml' - d @req1.xml http://192.168.39.30:19022/bip-external-sync/ --insecure
                                var content = new StringContent(ropRequest, Encoding.UTF8, "text/xml");
                                var url = "http://192.168.39.30:19022/bip-external-sync/";
                                _logger.Info($"Запрос в РОП по вин коду {doc.VehicleIdentityNumberId}");
                                using var client = new HttpClient();
                                var responseROP = await client.PostAsync(url, content);

                                string respRop = responseROP.Content.ReadAsStringAsync().Result;
                                _logger.Info(respRop);
                                XmlDocument xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml(respRop);
                                XmlNodeList elemlist = xmlDoc.GetElementsByTagName("code");
                                string resultCode = elemlist[0].InnerXml;

                                if (resultCode == "0" && xmlDoc.GetElementsByTagName("HavePay")?[0]?.InnerXml?.ToLower() == "true")
                                {
                                    doc.StatusNsi = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi022" && x.Code == "05");
                                    dpHistory.Description = "Ответ РОП положительный";
                                    if (doc.BaseDocIndicator && doc.BaseVehicleEPassportId != "")
                                    {
                                        var oldDp = _UnitOfWork.DigitalPassportRepository.GetAsIQueryable(v => v.Deleted == null
                                            && v.UniqueCode == doc.BaseVehicleEPassportId
                                            && v.SignId != null).AsNoTracking().FirstOrDefault();
                                        if (oldDp == null) throw new Exception("Базовое ТС не найден");

                                        oldDp.StatusNsiId = nsiValues.FirstOrDefault(x => x.Nsi.Code == "nsi022" && x.Code == "15").Id ; // погашенный
                                        _UnitOfWork.DigitalPassportRepository.Update(oldDp);
                                    }
                                }
                                else
                                {
                                    dpHistory.Description = "Ответ РОП отрицательный";
                                    doc.DeclineDate = DateTime.Now;
                                    doc.DeclineDescription = "Ответ РОП отрицательный";
                                }
                                dpInteractHistory.Description = respRop;
                                dpInteractHistory.StatusCode = "5";
                            }
                            catch (Exception exc)
                            {
                                _logger.Error($"Запрос в РОП не прошел по причине {exc}");
                                doc.DeclineDate = DateTime.Now;
                                doc.DeclineDescription = exc.Message != "Базовое ТС не найден" ? "Нет доступа в РОП" : "Базовое ТС не найден";
                                dpHistory.StatusNsiId = doc.StatusNsi.Id;
                                dpHistory.Description = $"ROP: {exc.Message}";
                                dpInteractHistory.Description = exc.Message;
                                dpInteractHistory.StatusCode = "-1";
                            }
                            finally
                            {
                                dpHistory.StatusNsiId = doc.StatusNsi.Id;
                                await _UnitOfWork.DigitalPassportStatusHistoryRepository.InsertAsync(dpHistory);
                                await _UnitOfWork.ExternalInteractHistoryRepository.InsertAsync(dpInteractHistory);
                            }
                            await _UnitOfWork.SaveAsync();
                            _UnitOfWork.Commit();
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e, "Not Saved Rop ");
                        _UnitOfWork.Rollback();
                    }
                }
            }
        }
    }
}

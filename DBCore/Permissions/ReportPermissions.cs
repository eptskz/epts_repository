﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DBCore.Permissions
{
    [Description("Отчеты")]
    public class ReportPermissions
    {
        public const string ReportPolicy = "ReportPolicy";

        [Description("Отчеты")]
        public class Reports
        {
            [Description("ОТТС/ОТШ/СБКТС")]
            public const string CdView = "reports.cdreport.view";

            [Description("Общий отчет по ЭП")]
            public const string EptsView = "reports.epts.view";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DBCore.Permissions
{
    [Description("Внешний пользователь")]
    public class ExternalUserPermissions
    {
        public const string RegistryPolicy = "ExternalUserPolicy";

        [Description("Внешний пользователь")]
        public class ExternalUser
        {
            [Description("Право на чтения всех функций внешнего пользователя")]
            public const string Read = "external.user.read";

            [Description("Право на редактирование всех функций внешнего пользователя")]
            public const string Edit = "external.user.edit";

            [Description("Право на удаление всех функций внешнего пользователя")]
            public const string Delete = "external.user.delete";
        }       
    }
}
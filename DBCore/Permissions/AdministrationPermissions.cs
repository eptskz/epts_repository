﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DBCore.Permissions
{
    [Description("Администрирование")]
    public class AdministrationPermissions
    {
        public const string AdministrationPolicy = "AdministrationPolicy";

        [Description("Запросы на регистрацию")]
        public class RegRequest
        {
            [Description("Запросы на регистрацию Просмотр")]
            public const string Read = "administration.regrequest.read";

            [Description("Запросы на регистрацию Редактировать")]
            public const string Edit = "administration.regrequest.edit";

            [Description("Запросы на регистрацию Удалить")]
            public const string Delete = "administration.regrequest.delete";
        }

        [Description("Пользователи")]
        public class User
        {
            [Description("Пользователи Просмотр")]
            public const string Read = "administration.user.read";
            
            [Description("Пользователи Редактировать")]
            public const string Edit = "administration.user.edit";

            [Description("Пользователи Удалить")]
            public const string Delete = "administration.user.delete";
        }

        [Description("Роли")]
        public class Role
        {
            [Description("Роли Просмотр")]
            public const string Read = "administration.role.read";
            
            [Description("Роли Редактировать")]
            public const string Edit = "administration.role.edit";

            [Description("Роли Удалить")]
            public const string Delete = "administration.role.delete";
        }

        [Description("Обращения")]
        public class SupportTicket
        {
            [Description("Обращения Просмотр")]
            public const string Read = "administration.support.read";

            [Description("Обращения Обработка")]
            public const string Processing = "administration.support.processing";

            [Description("Обращения Удалить")]
            public const string Delete = "administration.support.delete";
        }

        [Description("Договоры")]
        public class Contract
        {
            [Description("Договоры Просмотр")]
            public const string Read = "administration.contract.read";

            [Description("Договоры Обработка")]
            public const string Processing = "administration.contract.edit";

            [Description("Договоры Удалить")]
            public const string Delete = "administration.contract.delete";
        }

        [Description("Инструкции")]
        public class Instructions
        {
            [Description("Инструкции Просмотр")]
            public const string Read = "administration.instruction.read";

            [Description("Инструкции Редактирование")]
            public const string Processing = "administration.instruction.edit";

            [Description("Инструкции Удалить")]
            public const string Delete = "administration.instruction.delete";
        }

        [Description("Справочники")]
        public class Dictionaries
        {
            [Description("Справочники Просмотр")]
            public const string Read = "administration.dictionary.read";

            [Description("Справочники Редактирование")]
            public const string Processing = "administration.dictionary.edit";

            [Description("Справочники Удалить")]
            public const string Delete = "administration.dictionary.delete";
        }

        [Description("Новости")]
        public class News
        {
            [Description("Новости Просмотр")]
            public const string Read = "administration.news.read";

            [Description("Новости Редактирование")]
            public const string Processing = "administration.news.edit";

            [Description("Новости Удалить")]
            public const string Delete = "administration.news.delete";
        }

        [Description("Мониторинг")]
        public class Monitoring
        {
            [Description("Мониторинг Просмотр")]
            public const string Read = "administration.monitoring.read";
        }
    }
}

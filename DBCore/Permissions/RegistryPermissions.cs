﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DBCore.Permissions
{
    [Description("Реестры")]
    public class RegistryPermissions
    {
        public const string RegistryPolicy = "RegistryPolicy";

        [Description("Реестр Уполномоченные органы (организации)")]
        public class Kiru
        {
            [Description("Уполномоченные органы (организации) Просмотр")]
            public const string Read = "registry.kiru.read";
            
            [Description("Уполномоченные органы (организации) Редактировать")]
            public const string Edit = "registry.kiru.edit";

            [Description("Уполномоченные органы (организации) Удалить")]
            public const string Delete = "registry.kiru.delete";
        }

        [Description("Реестр Организации-изготовители транспортных средств (шасси)")]
        public class Kirm
        {
            [Description("Организации-изготовители транспортных средств (шасси) Просмотр")]
            public const string Read = "registry.kirm.read";
            
            [Description("Организации-изготовители транспортных средств (шасси) Редактировать")]
            public const string Edit = "registry.kirm.edit";

            [Description("Организации-изготовители транспортных средств (шасси) Удалить")]
            public const string Delete = "registry.kirm.delete";
        }

        [Description("Реестр организаций СЭП")]
        public class General
        {
            [Description("Реестр организаций СЭП Просмотр")]
            public const string Read = "registry.general.read";

            [Description("Реестр организаций СЭП Редактировать")]
            public const string Edit = "registry.general.edit";

            [Description("Реестр организаций СЭП Удалить")]
            public const string Delete = "registry.general.delete";
        }

        [Description("Учет оплаты услуг")]
        public class Invoice
        {
            [Description("Учет оплаты услуг Просмотр")]
            public const string Read = "registry.invoice.read";

            [Description("Учет оплаты услуг Редактировать")]
            public const string Edit = "registry.invoice.edit";

            [Description("Учет оплаты услуг Удалить")]
            public const string Delete = "registry.invoice.delete";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DBCore.Permissions
{
    [Description("Документы")]
    public class DocumentPermissions
    {
        public const string DocumentPolicy = "DocumentPolicy";

        [Description("ОТТС")]
        public class OTTS
        {
            [Description("ОТТС Просмотр")]
            public const string Read = "document.otts.read";
            
            [Description("ОТТС Редактировать")]
            public const string Edit = "document.otts.edit";

            [Description("ОТТС Удалить")]
            public const string Delete = "document.otts.delete";

            [Description("ОТТС Подтверждение")]
            public const string Approval = "document.otts.approval";
        }

        [Description("ОТШ")]
        public class OTCH
        {
            [Description("ОТШ Просмотр")]
            public const string Read = "document.otch.read";
            
            [Description("ОТШ Редактировать")]
            public const string Edit = "document.otch.edit";

            [Description("ОТШ Удалить")]
            public const string Delete = "document.otch.delete";

            [Description("ОТШ Подтверждение")]
            public const string Approval = "document.otch.approval";
        }

        [Description("СБКТС")]
        public class SBKTS
        {
            [Description("СБКТС Просмотр")]
            public const string Read = "document.sbkts.read";

            [Description("СБКТС Редактировать")]
            public const string Edit = "document.sbkts.edit";

            [Description("СБКТС Удалить")]
            public const string Delete = "document.sbkts.delete";

            [Description("СБКТС Подтверждение")]
            public const string Approval = "document.sbkts.approval";
        }

        [Description("Оформление ЭП")]
        public class EPassport
        {
            [Description("ЭП Просмотр")]
            public const string Read = "document.epts.read";

            [Description("ЭП Редактировать")]
            public const string Edit = "document.epts.edit";

            [Description("ЭП Удалить")]
            public const string Delete = "document.epts.delete";
        }
    }
}

﻿using DBCore.DTOs.Characteristics;
using DBCore.DTOs.Complience;
using DBCore.DTOs.Complience.Vehicle;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs
{
    public class ComplienceDocumenReestrtDto
    {
        public Guid? Id { get; set; }

        public string DocNumber { get; set; }
        public string CommercialName { get; set; }
        public string CategoryName { get; set; }
        public string Type { get; set; }
        public string BrandName { get; set; }
        public DateTime? DocDate { get; set; }
        public DateTime? DocStartDate { get; set; }
        public DateTime? DocEndDate { get; set; }

        public DateTime? Deleted { get; set; }

        public int? DocTypeId { get; set; }
        public string DocTypeCode { get; set; }
        public string DocTypeName { get; set; }

        public int? AuthorityId { get; set; }
        public string AuthorityName { get; set; }

       
        public int? ManufacturerId { get; set; }
        public string ManufacturerCode { get; set; }
        public string ManufacturerName { get; set; }

        public int? ApplicantId { get; set; }
        public string ApplicantCode { get; set; }
        public string ApplicantName { get; set; }
/*
        public int? RepresentativeManufacturerId { get; set; }
        public string RepresentativeManufacturerCode { get; set; }
        public string RepresentativeManufacturerName { get; set; }
*/
        public List<RepresentativeManufacturerDto> RepresentativeManufacturers { get; set; }

/*
        public int? AssemblyPlantId { get; set; }
        public string AssemblyPlantCode { get; set; }
        public string AssemblyPlantName { get; set; }
*/
        public List<AssemblyPlantDto> AssemblyPlants { get; set; }
/*
        public int? AssemblyKitSupplierId { get; set; }
        public string AssemblyKitSupplierCode { get; set; }
        public string AssemblyKitSupplierNames { get; set; }
*/
        public List<AssemblyKitSupplierDto> AssemblyKitSuppliers { get; set; }

        public int? StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }

        public int? CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public int DocDistributionSign { get; set; } = 0;
        public int? BatchCount { get; set; }

        public int? BatchSmallSign { get; set; } = 0;
    }
}

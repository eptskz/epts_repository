﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    public class VehicleSeatDetailDto
    {
        public int Id { get; set; }
        public int? CharacteristicId { get; set; }

        public int SeatQuantity { get; set; }
        public string SeatDescription { get; set; }

        public List<VehicleSeatRawDetailDto> VehicleSeatRawDetails { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Устройство накопления энергии
    /// </summary>
    public class PowerStorageDeviceDetailDto
    {
        public int Id { get; set; }

        public int? CharacteristicId { get; set; }
        public int? ElectricalMachineDetailId { get; set; }        

        public int PowerStorageDeviceTypeId { get; set; }        
        public string PowerStorageDeviceTypeCode { get; set; }
        public string PowerStorageDeviceTypeName { get; set; }

        public string PowerStorageDeviceDescription { get; set; }
        public string PowerStorageDeviceLocation { get; set; }
                
        public double? VehicleRangeMeasure { get; set; }
        public int VehicleRangeUnitId { get; set; }
        public string VehicleRangeUnitName { get; set; }

        public double? PowerStorageDeviceVoltageMeasure { get; set; }
        public int PowerStorageDeviceVoltageMeasureUnitId { get; set; }
        public string PowerStorageDeviceVoltageMeasureUnitCode { get; set; }
        public string PowerStorageDeviceVoltageMeasureUnitName { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.DTOs.Complience;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    public class CharacteristicsDetailDto
    {
        public int? Id { get; set; }
        public Guid? VehicleTypeDetailId { get; set; }

        public List<VehicleRunningGearDetailDto> VehicleRunningGearDetails { get;set; }
        public List<VehicleBodyworkDetailDto> VehicleBodyworkDetails { get;set; }
        public List<VehicleSeatDetailDto> VehicleSeatDetails { get;set; }

        public List<VehicleLayoutPatternDto> VehicleLayoutPatterns { get; set; }
        public List<VehicleEngineLayoutDto> VehicleEngineLayouts { get; set; }
        public List<VehicleCarriageSpaceImplementationDto> VehicleCarriageSpaceImplementations { get; set; }

        /// <summary>
        ///  Специализированное транспортное средство
        /// </summary>
        public int? IsSpecializedVehicle { get; set; }

        public List<VehicleCabinDto> VehicleCabins { get; set; }
        public List<VehiclePurposeDto> VehiclePurposes { get; set; }
        public List<VehicleFrameDto> VehicleFrames { get; set; }
        public List<VehiclePassengerQuantityDto> VehiclePassengerQuantities { get; set; }
        public List<VehicleTrunkVolumeMeasureDto> VehicleTrunkVolumeMeasures { get; set; }

        /// <summary>
        /// Дополнительные параметры для контейнеровоза
        /// Признак "Переменная высота"
        /// </summary>
        public int? IsVariableHeight { get; set; }
        public List<VehicleLengthMeasureDto> LengthRanges { get; set; }
        public List<VehicleWidthMeasureDto> WidthRanges { get; set; }
        public List<VehicleHeightMeasureDto> HeightRanges { get; set; }
        public List<VehicleMaxHeightMeasureDto> MaxHeightRanges { get; set; }
        public List<VehicleLoadingHeightMeasureDto> LoadingHeightRanges { get; set; }
        public List<VehicleWheelbaseMeasureDto> WheelbaseMeasureRanges { get; set; }

        public List<VehicleAxleDetailDto> VehicleAxleDetails { get; set; }

        public int NotVehicleTrailerIndicator { get; set; } = 0;
        public List<VehicleWeightMeasureDto> VehicleWeightMeasures { get; set; }        
        public List<UnbrakedTrailerWeightMeasureDto> UnbrakedTrailerWeightMeasures { get; set; }
        public List<BrakedTrailerWeightMeasureDto> BrakedTrailerWeightMeasures { get; set; }
        public List<VehicleHitchLoadMeasureDto> VehicleHitchLoadMeasures { get; set; }

        /// <summary>
        /// Двигатель 
        /// </summary>
        public int NotEngineDetailIndicator { get; set; } = 0;
        public int? EngineTypeId { get; set; }
        public string EngineTypeCode { get; set; }
        public string EngineTypeName { get; set; }
        public string VehicleHybridDesignText { get; set; }
        public List<VehicleHybridDesignDto> VehicleHybridDesigns { get; set; }
        public List<EngineDetailDto> EngineDetails { get; set; }

        public int NotElectricalMachineDetailIndicator { get; set; }
        public List<ElectricalMachineDetailDto> ElectricalMachineDetails { get; set; }

        /// <summary>
        /// Устройство накопления энергии
        /// </summary>
        public int? NotPowerStorageDeviceDetailIndicator { get; set; }
        public virtual List<PowerStorageDeviceDetailDto> PowerStorageDeviceDetails { get; set; }


        /// <summary>
        /// Сцепление
        /// </summary>
        public int? NotVehicleClutchDetailIndicator { get; set; }
        public List<VehicleClutchDetailDto> VehicleClutchDetails { get; set; }


        /// <summary>
        /// Трансмиссия
        /// </summary>
        public int? NotVehicleTransmissionDetailIndicator { get; set; }
        public List<TransmissionTypeDto> VehicleTransmissionTypes { get; set; }

        /// <summary>
        /// Подвеска
        /// </summary>
        public List<VehicleSuspensionDetailDto> VehicleSuspensionDetails { get; set; }


        /// <summary>
        /// Рулевое управление
        /// </summary>
        public int? NotVehicleSteeringDetailIndicator { get; set; }
        public string VehicleSteeringDetailDescription { get; set; }
        public List<VehicleSteeringDescriptionDto> VehicleSteeringDescriptions { get; set; }
        public List<VehicleSteeringDetailDto> VehicleSteeringDetails { get; set; }


        /// <summary>
        /// Тормозные системы
        /// </summary>
        public int? NotVehicleBrakingSystemDetailIndicator { get; set; }
        public List<VehicleBrakingSystemDetailDto> VehicleBrakingSystemDetails { get; set; }

        /// <summary>
        /// Информация о шинах
        /// </summary>
        public List<VehicleTyreKindInfoDto> VehicleTyreKindInfos { get; set; }

        /// <summary>
        /// Описание оборудования транспортного средства
        /// </summary>
        public List<VehicleEquipmentInfoDto> VehicleEquipmentInfos { get; set; }
    }
}

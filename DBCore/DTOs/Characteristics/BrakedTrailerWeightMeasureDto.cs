﻿using DBCore.Enums;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Масса прицепа c тормозной системы
    /// </summary>
    public class BrakedTrailerWeightMeasureDto
    {
        public int Id { get; set; }
                
        public double WeightMeasure { get; set; }
        public double? WeightMeasureMax { get; set; }
        public int UnitId { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        
        public int CharacteristicId { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

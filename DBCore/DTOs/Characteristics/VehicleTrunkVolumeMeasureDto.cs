﻿using DBCore.Enums;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// общий объем багажных отделений транспортного средства
    /// Заполнение обязательно, если тег « Код технической категории транспортного средства» 
    /// (trsdo:VehicleTechCategoryCode) = M3/M3G и поле Класс = III.
    /// </summary>
    public class VehicleTrunkVolumeMeasureDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }

        public double TrunkVolumeMeasure { get; set; }

        public int UnitId { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

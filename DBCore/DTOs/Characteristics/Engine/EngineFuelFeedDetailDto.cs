﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle.Characteristics.Engine
{
    public class EngineFuelFeedDetailDto
    {
        public int Id { get; set; }
        public int EngineDetailId { get; set; }

        public int FuelFeedId { get; set; }
        public string FuelFeedCode { get; set; }
        public string FuelFeedName { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Максимальный крутящий момент
    /// </summary>
    public class EngineTorqueDetailDto
    {
        public int Id { get; set; }
        public int EngineDetailId { get; set; }

        /// <summary>
        /// Максимальный крутящий момент
        /// </summary>
        public double EngineMaxTorqueMeasure { get; set; }
        public int? EngineMaxTorqueMeasureUnitId { get; set; }
        public string EngineMaxTorqueMeasureUnitCode { get; set; }
        public string EngineMaxTorqueMeasureUnitName { get; set; }


        /// <summary>
        /// Скорость вращения коленчатого вала (подраздел Максимальногоскорости крутящего момента)
        /// </summary>
        public double EngineMaxTorqueMeasureShaftRotationFrequencyMinMeasure { get; set; }
        public double? EngineMaxTorqueMeasureShaftRotationFrequencyMaxMeasure { get; set; }
        public int? EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId { get; set; }
        public string EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitCode { get; set; }
        public string EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitName { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

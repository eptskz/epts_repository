﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle.Characteristics.Engine
{
    public  class VehicleFuelKindDto
    {
        public int Id { get; set; }
        public int EngineDetailId { get; set; }

        public int FuelKindId { get; set; }
        public string FuelKindCode { get; set; }
        public string FuelKindName { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

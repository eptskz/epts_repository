﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Максимальная мощность двигателя
    /// </summary>
    public class EnginePowerDetailDto
    {
        public int Id { get; set; }
        public int EngineDetailId { get; set; }

        /// <summary>
        /// максимальная мощность двигателя
        /// </summary>
        public double EngineMaxPowerMeasure { get; set; }
        public int? EngineMaxPowerMeasureUnitId { get; set; }
        public string EngineMaxPowerMeasureUnitCode { get; set; }
        public string EngineMaxPowerMeasureUnitName { get; set; }


        /// <summary>
        /// Скорость вращения коленчатого вала (подраздел Максимальной мощности двигателя)
        /// </summary>
        public double EngineMaxPowerShaftRotationFrequencyMinMeasure { get; set; }
        public double? EngineMaxPowerShaftRotationFrequencyMaxMeasure { get; set; }
        public int? EngineMaxPowerShaftRotationFrequencyMeasureUnitId { get; set; }
        public string EngineMaxPowerShaftRotationFrequencyMeasureUnitCode { get; set; }
        public string EngineMaxPowerShaftRotationFrequencyMeasureUnitName { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using DBCore.Models.Vehicle.Characteristics.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// информация о двигателе внутреннего сгорания транспортного средства (шасси транспортного средства), приводном двигателе самоходной машины и других видов техники
    /// </summary>
    public class EngineDetailDto
    {
        public int Id { get; set; }

        public int CharacteristicsDetailId { get; set; }

        /// <summary>
        /// наименование марки двигателя внутреннего сгорания
        /// </summary>
        public string VehicleComponentMakeName { get; set; }

        /// <summary>
        /// описание конструктивных особенностей (типа) двигателя внутреннего сгорания, характеризующее принцип его действия
        /// </summary>
        public string VehicleComponentText { get; set; }

        /// <summary>
        /// количество цилиндров двигателя внутреннего сгорания
        /// </summary>
        public int EngineCylinderQuantity { get; set; }

        /// <summary>
        /// описание расположения цилиндров двигателя внутреннего сгорания
        /// </summary>
        public int EngineCylinderArrangementId { get; set; }
        public string EngineCylinderArrangementCode { get; set; }
        public string EngineCylinderArrangementName { get; set; }

        /// <summary>
        /// рабочий объем цилиндров двигателя внутреннего сгорания
        /// </summary>
        public double EngineCapacityMeasure { get; set; }
        public int? EngineCapacityMeasureUnitId { get; set; }
        public string EngineCapacityMeasureUnitCode { get; set; }
        public string EngineCapacityMeasureUnitName { get; set; }

        /// <summary>
        /// степень сжатия двигателя внутреннего сгорания
        /// </summary>
        public string EngineCompressionRate { get; set; }


        /// <summary>
        /// максимальная мощность двигателя
        /// </summary>

        public List<EnginePowerDetailDto> EnginePowerDetails { get; set; }

        /// <summary>
        /// Максимальный крутящий момент
        /// </summary>

        public List<EngineTorqueDetailDto> EngineTorqueDetails { get; set; }

        /// <summary>
        /// топлива транспортного средства NSI_030
        /// </summary>
        public int NotVehicleFuelIndicator { get; set; } = 0;
        public List<VehicleFuelKindDto> VehicleFuelKinds { get; set; }


        /// <summary>
        /// Система питания NSI_056
        /// </summary>
        public int NotFuelFeedDetailIndicator { get; set; } = 0;        
        public List<EngineFuelFeedDetailDto> EngineFuelFeedDetails { get; set; }

        /// <summary>
        /// Блок управления
        /// </summary>
        public virtual List<ECUModelDetailDto> ECUModelDetails { get; set; }        

        /// <summary>
        /// Система зажигания NSI_055
        /// </summary>
        public int NotVehicleIgnitionDetailIndicator { get; set; } = 0;
        public List<VehicleIgnitionDetailDto> VehicleIgnitionDetails { get; set; }

        /// <summary>
        /// Система выпуска и нейтрализации отработавших газов NSI_055
        /// </summary>
        public int NotExhaustDetailIndicator { get; set; } = 0;
        public List<ExhaustDetailDto> ExhaustDetails { get; set; }


        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Исполнение загрузочного пространства
    /// (Для категорий N, O) Код технической категории транспортного средства» 
    /// (trsdo:VehicleTechCategoryCode) = любая категория N или О 
    /// </summary>
        
    public class VehicleCarriageSpaceImplementationDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        
        public string CarriageSpaceImplementation { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    ///  Колея передних/задних колес 
    /// </summary>

    public class VehicleAxleDetailDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }

        /// <summary>
        /// Порядковый номер оси транспортного средства
        /// </summary>
        public int AxleOrdinal { get; set; }

        /// <summary>
        /// Признак ведущей оси
        /// </summary>
        public int? DrivingAxleIndicator { get; set; }

        /// <summary>
        ///  Величина колеи оси
        /// </summary>
        public double AxleSweptPathMeasure { get; set; }
        public double? AxleSweptPathMeasureMax { get; set; }

        public int? AxleSweptPathMeasureUnitId { get; set; }
        public string AxleSweptPathMeasureUnitCode { get; set; }
        public string AxleSweptPathMeasureUnitName { get; set; }

        /// <summary>
        ///  Технически максимальная масса приходящаяся на ось
        /// </summary>
        public double? TechnicallyPermissibleMaxWeightOnAxleMeasure { get; set; }
        public double? TechnicallyPermissibleMaxWeightOnAxleMeasureMax { get; set; }

        public int? TechnicallyPermissibleMaxWeightOnAxleUnitId { get; set; }
        public string TechnicallyPermissibleMaxWeightOnAxleUnitCode { get; set; }
        public string TechnicallyPermissibleMaxWeightOnAxleUnitName { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

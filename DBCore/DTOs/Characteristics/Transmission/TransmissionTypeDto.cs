﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// описание типа трансмиссии транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// NSI_058
    /// </summary>
    public class TransmissionTypeDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }

        public int TransTypeId { get; set; }
        public string TransTypeCode { get; set; }
        public string TransTypeName { get; set; }

        public virtual List<TransmissionUnitDetailDto> TransmissionUnitDetails { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

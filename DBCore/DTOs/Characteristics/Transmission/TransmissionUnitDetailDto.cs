﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// информация об узле трансмиссии транспортного средства (шасси транспортного средства)
    /// </summary>
    public class TransmissionUnitDetailDto
    {
        public int Id { get; set; }

        public int TransTypeId { get; set; }
        public int TransmissionTypeId { get; set; }
        public string TransmissionTypeName { get; set; }

        /// <summary>
        /// NSI_018
        /// вид узла транспортного средства (шасси транспортного средства, самоходной машины и других видов техники
        /// </summary>
        public int UnitKindId { get; set; }
        public string UnitKindCode { get; set; }
        public string UnitKindName { get; set; }

        /// <summary>
        /// Бесступенчатая коробка передач
        /// Отображается только для КОРОБКИ ПЕРЕДАЧ
        /// </summary>
        public int ContinuouslyVariableTransmissionIndicator { get; set; } = 0;

        /// <summary>
        /// Распределение по осям для множественной Главной передачи
        /// Отображается только для ГЛАВНОЙ ПЕРЕДАЧИ
        /// NSI_026
        /// </summary>
        public int? AxisDistributionId { get; set; }
        public string AxisDistributionCode { get; set; }
        public string AxisDistributionName { get; set; }

        /// <summary>
        /// наименование марки узла трансмиссии
        /// </summary>
        public string TransmissionUnitMakeName { get; set; }
        public string TransmissionUnitDescription { get; set; }

        /// <summary>
        /// описание конструктивных особенностей (типа) узла трансмиссии транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
        /// </summary>
        ///<code>
        /// NSI_053
        /// </code>
        public int? TransmissionBoxTypeId { get; set; }
        public string TransmissionBoxTypeCode { get; set; }
        public string TransmissionBoxTypeName { get; set; }

        ///<code>
        /// NSI_051
        /// </code>
        public int? MainGearTypeId { get; set; }
        public string MainGearTypeCode { get; set; }
        public string MainGearTypeName { get; set; }

        public string TransferCaseType { get; set; }


        /// <summary>
        /// количество передач узла трансмиссии транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
        /// </summary>
        public int? TransmissionUnitGearQuantity { get; set; }


        public List<TransmissionUnitGearDto> TransmissionUnitGears { get; set; }


        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

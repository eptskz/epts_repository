﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// информация о передаче узла трансмиссии транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    public class TransmissionUnitGearDto
    {
        public int Id { get; set; }

        public int TransmissionUnitId { get; set; }

        /// <summary>
        /// NSI_080
        /// передачи узла трансмиссии техники
        /// </summary>
        public int? TransmissionUnitGearValueId { get; set; }
        public string TransmissionUnitGearValueCode { get; set; }
        public string TransmissionUnitGearValueName { get; set; }

        /// <summary>
        /// NSI_081
        /// передачи узла трансмиссии техники
        /// </summary>
        public int? TransmissionUnitGearTypeId { get; set; }
        public string TransmissionUnitGearTypeCode { get; set; }
        public string TransmissionUnitGearTypeName { get; set; }

        /// <summary>
        /// передаточное число передачи узла трансмиссии (для бесступенчатой коробки передач – нижняя граница диапазона)
        /// </summary>
        public double? TransmissionUnitGearRate { get; set; }
        /// <summary>
        /// передаточное число передачи узла трансмиссии (для бесступенчатой коробки передач – верхняя граница диапазона)
        /// </summary>
        public double? TransmissionUnitGearRateMax { get; set; }

        /// <summary>
        /// признак, определяющий передачу заднего хода: 1 - передача заднего хода; 0 - передача переднего хода
        /// </summary>
        public int TransmissionUnitReverseGearIndicator { get; set; } = 0;

        public ObjectStateEnum State { get; set; }
    }
}

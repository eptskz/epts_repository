﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Complience
{
    public class VehicleSteeringDescriptionDto : IObjectState
    {
        public int? Id { get; set; }
        public int CharacteristicId { get; set; }
        public string Description { get; set; }
        public ObjectStateEnum State { get; set; }
    }
}

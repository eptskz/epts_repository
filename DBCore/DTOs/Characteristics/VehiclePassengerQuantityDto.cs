﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// пассажировместимость транспортного средства (самоходной машины и других видов техники) 
    /// при максимальной разрешенной массе
    /// </summary>

    public class VehiclePassengerQuantityDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }

        public int PassengerQuantity { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

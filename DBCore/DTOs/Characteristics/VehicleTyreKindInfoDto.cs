﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// информация о рулевом управлении транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    public class VehicleTyreKindInfoDto
    {
        public int Id { get; set; }

        public int CharacteristicsDetailId { get; set; }

        /// <summary>
        /// Обозначение размера
        /// </summary>
        public string VehicleTyreKindSize { get; set; }

        /// <summary>
        /// Индекс несущей способности для максимально допустимой нагрузки 
        /// </summary>
        public string VehicleTyreKindIndex { get; set; }

        /// <summary>
        /// Скоростная категория
        /// </summary>
        public int? VehicleTyreKindSpeedId { get; set; }
        public string VehicleTyreKindSpeedCode { get; set; }
        public string VehicleTyreKindSpeedName { get; set; }

        /// <summary>
        /// Запасная шина для временного использования
        /// </summary>
        public int IsSupplementVehicleTyre { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

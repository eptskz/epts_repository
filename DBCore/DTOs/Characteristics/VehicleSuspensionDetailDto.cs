﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// информация о подвеске транспортного средства (шасси транспортного средства, самоходной машины и других видов техники
    /// </summary>
    public class VehicleSuspensionDetailDto
    {
        public int Id { get; set; }

        public int CharacteristicsDetailId { get; set; }

        /// <summary>
        /// Вид подвески
        /// </summary>
        public int VehicleSuspensionKindId { get; set; }        
        public string VehicleSuspensionKindCode { get; set; }
        public string VehicleSuspensionKindName { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string VehicleSuspensionDescription { get; set; }


        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

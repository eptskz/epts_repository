﻿using DBCore.Enums;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Технически допустимая максимальная нагрузка на опорно сцепное устройство
    /// </summary>
    public class VehicleHitchLoadMeasureDto
    {
        public int Id { get; set; }
                
        public double HitchLoadMeasure { get; set; }
        public double? HitchLoadMeasureMax { get; set; }

        public int UnitId { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        
        public int CharacteristicId { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

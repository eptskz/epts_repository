﻿using DBCore.Enums;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Масса
    /// </summary>
    public class VehicleWeightMeasureDto
    {
        public int Id { get; set; }

        public double Weight { get; set; }
        public double? WeightMax { get; set; }

        public int CharacteristicId { get; set; }

        public int UnitId { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }

        /// <summary>
        /// Вид массы
        /// </summary>
        public int MassTypeId { get; set; }
        public string MassTypeName { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

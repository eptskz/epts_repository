﻿using DBCore.Enums;
using DBCore.Models.Dictionaries;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    public class VehicleHeightMeasureDto
    {
        public int Id { get; set; }

        public double Height { get; set; }
        public double? HeightMax { get; set; }

        public int CharacteristicId { get; set; }

        public int UnitId { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}
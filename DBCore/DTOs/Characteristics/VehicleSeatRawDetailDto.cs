﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    public class VehicleSeatRawDetailDto
    {
        public int Id { get; set; }
        public int VehicleSeatDetailId { get; set; }

        public int SeatRawOrdinal { get; set; }
        public int SeatRawQuantity { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

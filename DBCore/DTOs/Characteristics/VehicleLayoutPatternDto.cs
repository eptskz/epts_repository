﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Схема компоновки транспортного средства
    /// </summary>
    public class VehicleLayoutPatternDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }

        /// <summary>
        /// Описание схемы компоновки транспортного средства
        /// Заполнение обязательно, если тег « Код технической категории транспортного средства» (trsdo:VehicleTechCategoryCode) ≠ O1 - O4
        /// </summary>
        /// <example>
        /// переднеприводная
        /// </example>
        /// <code>
        /// Nsi050LayoutPattern
        /// </code>
        public int LayoutPatternId { get; set; }
        public string LayoutPatternCode { get; set; }
        public string LayoutPatternName { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    public class VehicleBodyworkDetailDto
    {
        public int Id { get; set; }
        public int? CharacteristicId { get; set; }

        public int? DoorQuantity { get; set; }

        public int? VehicleBodyWorkTypeId { get; set; }
        public string VehicleBodyWorkTypeCode { get; set; }
        public string VehicleBodyWorkTypeName { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

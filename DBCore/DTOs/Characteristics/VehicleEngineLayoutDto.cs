﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Расположение двигателя
    /// </summary>
    public class VehicleEngineLayoutDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        
        /// <summary> 
        /// Расположение двигателя
        /// </summary>
        /// <example>
        /// переднее поперечное
        /// </example>
        /// <code>
        /// Nsi047EngineLocation
        /// </code>
        public int EngineLocationId { get; set; }
        public string EngineLocationCode{ get; set; }
        public string EngineLocationName { get; set; }
                
        public ObjectStateEnum State { get; set; }
    }
}

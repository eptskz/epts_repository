﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    ///  Описание рамы транспортного средства
    /// </summary>
    
    public class VehicleFrameDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public string FrameText { get; set; }
                
        public ObjectStateEnum State { get; set; }
    }
}

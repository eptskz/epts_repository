﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Система зажигания NSI_055
    /// </summary>
    public class VehicleIgnitionDetailDto
    {
        public int Id { get; set; }

        public int EngineDetailId { get; set; }

        public int VehicleIgnitionTypeId { get; set; }
        public string VehicleIgnitionTypeCode { get; set; }
        public string VehicleIgnitionTypeName { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

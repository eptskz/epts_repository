﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    public class ECUModelDetailDto
    {
        public int Id { get; set; }

        public int EngineDetailId { get; set; }

        public string EcuModel { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    ///  назначение
    /// </summary>

    public class VehiclePurposeDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }

        public int PurposeId { get; set; }
        public string PurposeCode { get; set; }
        public string PurposeName { get; set; }

        public int? OtherInfoId { get; set; }
        public string OtherInfoCode { get; set; }
        public string OtherInfoName { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

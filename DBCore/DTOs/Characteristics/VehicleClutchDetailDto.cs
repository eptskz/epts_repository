﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Сцепление
    /// </summary>
    public class VehicleClutchDetailDto
    {
        public int Id { get; set; }

        public int CharacteristicsDetailId { get; set; }

        /// <summary>
        /// Марка
        /// </summary>
        public string VehicleClutchMakeName { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public string VehicleClutchDescription { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

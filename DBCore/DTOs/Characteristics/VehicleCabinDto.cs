﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Кабина
    /// Отображается и заполняется в случае, если Код технической категории транспортного средства» (trsdo:VehicleTechCategoryCode) = N
    /// </summary>
    public class VehicleCabinDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }

        public string CabinDescription { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

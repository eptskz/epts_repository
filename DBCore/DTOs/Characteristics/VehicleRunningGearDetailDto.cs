﻿using DBCore.Enums;
using DBCore.Models.Vehicle.Characteristics;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    public class VehicleRunningGearDetailDto
    {
        public int Id { get; set; }
        public int? CharacteristicId { get; set; }

        public int? VehicleWheelFormulaId { get; set; }
        public string VehicleWheelFormulaCode { get; set; }
        public string VehicleWheelFormulaName { get; set; }

        public List<PoweredWheelLocationDto> PoweredWheelLocations { get; set; }
        
        public int? VehicleAxleQuantity { get; set; }

        public int? VehicleWheelQuantity { get; set; }
        public int? PowerWheelQuantity { get; set; }

        public string VehicleWheelLocation { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

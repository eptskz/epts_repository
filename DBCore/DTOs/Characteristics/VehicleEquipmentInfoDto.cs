﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// Описание оборудования транспортного средства
    /// </summary>
    public class VehicleEquipmentInfoDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }

        /// <summary>
        /// Описание оборудования транспортного средства
        /// </summary>
        public string VehicleEquipmentText { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

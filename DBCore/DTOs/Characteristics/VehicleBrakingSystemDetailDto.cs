﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// информация о тормозной системе транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    public class VehicleBrakingSystemDetailDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }

        /// <summary>
        /// Наименование элемента тормозной системы 
        /// NSI_029
        /// </summary>
        public int VehicleBrakingSystemKindId { get; set; }        
        public string VehicleBrakingSystemKindCode { get; set; }
        public string VehicleBrakingSystemKindName { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string VehicleBrakingSystemDescription { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    public class PoweredWheelLocationDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Заполняется в случае, если «Код технической категории транспортного средства» (trsdo:VehicleTechCategoryCode) ≠ О
        /// </summary>
        public int WheelLocationId { get; set; }
        public string WheelLocationCode { get; set; }
        public string WheelLocationName { get; set; }


        /// <summary>
        /// Количество и расположение колес (ОТТС)
        /// </summary>
        public int VehicleRunningGearDetailId { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

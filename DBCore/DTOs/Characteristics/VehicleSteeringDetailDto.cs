﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Characteristics
{
    /// <summary>
    /// информация о рулевом управлении транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    public class VehicleSteeringDetailDto
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }


        public int? SteeringWheelPositionId { get; set; }
        public string SteeringWheelPositionCode { get; set; }
        public string SteeringWheelPositionName { get; set; }

        /// <summary>
        /// Тип рулевого
        /// </summary>
        public string VehicleSteeringType { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string VehicleSteeringDescription { get; set; }


        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

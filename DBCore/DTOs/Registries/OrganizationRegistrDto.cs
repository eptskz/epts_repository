﻿using DBCore.DTOs.Dictionary;
using DBCore.DTOs.Registries;
using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs
{
    public class OrganizationRegistrDto
    {
        public int Id { get; set; }

        public string Bin { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string OrganizationTypeNames { get; set; }
        
        public string JuridicalPostalIndex { get; set; }
        public string JuridicalAddressName { get; set; }
        

        public string FactPostalIndex { get; set; }
        public string FactAddressName { get; set; }

        public string Phones { get; set; }
        public string Emails { get; set; }
        public string Faxes { get; set; }

        public int? StatusId { get; set; }
        public string StatusName { get; set; }
        public int LegalFormId { get; set; }
        public string LegalFormName { get; set; }
        public int CountryId { get; set; }
        public int CountryName { get; set; }

        public string HeadFullName { get; set; }

        public DateTime? Deleted { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}
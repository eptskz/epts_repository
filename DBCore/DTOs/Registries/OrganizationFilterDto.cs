﻿using DBCore.Enums.Organizations;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Registries
{
    public class OrganizationFilterDto
    {
        public string RegisterTypeStr { get; set; } 
        public string Search { get; set; } 
        public int? ParentId { get; set; }
        public string OrgTypeCode { get; set; }
        public bool ShowDeleted { get; set; }
    }
}

﻿using DBCore.DTOs.Dictionary;
using DBCore.DTOs.Registries;
using DBCore.Enums;
using DBCore.Enums.Organizations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs
{
    public class OrganizationDto
    {
        public int Id { get; set; }

        // public string Code { get; set; }
        public string Bin { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public List<OrganizationTypeDto> OrganizationTypes { get; set; }
        
        public string JuridicalPostalIndex { get; set; }
        public string JuridicalAddress { get; set; }

        public int? JuridicalRegionOrCityId { get; set; }
        public DictionaryDto JuridicalRegionOrCity { get; set; }
        public string JRegionOrCity { get; set; }

        public int? JuridicalDistinctOrCityId { get; set; }
        public DictionaryDto JuridicalDistinctOrCity { get; set; }
        public string JDistinctOrCity { get; set; }

        public int? JuridicalOkrugOrCityId { get; set; }
        public DictionaryDto JuridicalOkrugOrCity { get; set; }
        public string JOkrugOrCity { get; set; }

        public int? JuridicalVillageId { get; set; }
        public DictionaryDto JuridicalVillage { get; set; }
        public string JVillage { get; set; }


        public string JStreetName { get; set; }
        public string JBuildingNumber { get; set; }
        public string JRoomNumber { get; set; }


        public string FactPostalIndex { get; set; }
        public string FactAddress { get; set; }

        public int? FactCountryId { get; set; }
        public DictionaryDto FactCountry { get; set; }

        public int? FactRegionOrCityId { get; set; }
        public DictionaryDto FactRegionOrCity { get; set; }
        public string FRegionOrCity { get; set; }

        public int? FactDistinctOrCityId { get; set; }
        public DictionaryDto FactDistinctOrCity { get; set; }
        public string FDistinctOrCity { get; set; }

        public int? FactOkrugOrCityId { get; set; }
        public DictionaryDto FactOkrugOrCity { get; set; }
        public string FOkrugOrCity { get; set; }

        public int? FactVillageId { get; set; }
        public DictionaryDto FactVillage { get; set; }
        public string FVillage { get; set; }

        public string FStreetName { get; set; }
        public string FBuildingNumber { get; set; }
        public string FRoomNumber { get; set; }

        public List<CommunicationInfoDto> CommunicationInfos { get; set; }

        public int? StatusId { get; set; }
        public int? LegalFormId { get; set; }
        public int CountryId { get; set; }

        public string HeadLastName { get; set; }
        public string HeadFirstName { get; set; }
        public string HeadSecondName { get; set; }

        public RegisterTypeEnum RegisterType { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }

        public int? ParentId { get; set; }
        public List<DictionaryObjectModel> OrganizationDigPassportTypes { get; set; }

        public bool NotOrganizationDocumentIndicator { get; set; }

        public List<OrgDocumentDto> OrganizationDocuments { get; set; }

        public DateTime? Created { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Registries.Invoice
{
    public class InvoiceDto
    {
        public int Id { get; set; }
        public string Number { get; set; }

        public string[] PeriodMonths { get; set; }

        /// <summary>
        /// Количество оформлений
        /// </summary>
        public int DigitalPassportCount { get; set; }

        /// <summary>
        /// Изменения в ЭП (шт)
        /// </summary>
        public int ChangeDigitalPassportCount { get; set; }

        /// <summary>
        /// Тариф
        /// </summary>
        public double Rate { get; set; }

        /// <summary>
        /// Сумма к оплате
        /// </summary>
        public double? PayableAmount { get; set; }

        /// <summary>
        /// Оплаченная сумма
        /// </summary>
        public double? PaidAmount { get; set; }

        /// <summary>
        /// Дата и время оплаты
        /// </summary>
        public string PaidDateTime { get; set; }

        /// <summary>
        /// Пеня
        /// </summary>
        public double? Fine { get; set; }

        /// <summary>
        /// Срок начисления
        /// </summary>
        public int? AccrualTermFine { get; set; }

        /// <summary>
        /// Сумма к оплате Пени
        /// </summary>
        public double? PayableFine { get; set; }

        /// <summary>
        /// Оплаченная сумма
        /// </summary>
        public double? PaidFine { get; set; }

        /// <summary>
        /// Дата и время оплаты пени
        /// </summary>
        public string PaidFineDateTime { get; set; }

        /// <summary>
        /// Сальдо
        /// </summary>
        public double? Balance { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }

        public Guid ContractId { get; set; }
        public string ContractNumber { get; set; }
        public DateTime ContractDate { get; set; }
        public string AccountNumber { get; set; }
        public DateTime? AccountDate { get; set; }

        public string OrganizationName { get; set; }
        public string OrganizationBin { get; set; }

        public int? StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }

        public Guid? AuthorId { get; set; }
        public string AuthorName { get; set; }

        public string Description { get; set; }
    }
}
﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Registries
{
    public class OrgDocumentDto
    {
        public int Id { get; set; }

        public string OrgDocType { get; set; }

        public int? DocTypeId { get; set; }
        public string DocTypeCode { get; set; }
        public string DocTypeName { get; set; }

        public string DocSeries { get; set; }
        public string DocNumber { get; set; }
        public string DocName { get; set; }
        public DateTime? DocDate { get; set; }
        public DateTime? DocValidDateStart { get; set; }
        public DateTime? DocValidDateEnd { get; set; }

        public string TnVedTcCode { get; set; }
        public string AgreementOrgName { get; set; }
        public double? AgreementQuotaSize { get; set; }

        public int? TechRegulationSubjectQuantity { get; set; }
        public int? TechRegulationSubjectTypeId { get; set; }
        public string TechRegulationSubjectTypeCode { get; set; }
        public string TechRegulationSubjectTypeName { get; set; }

        public int? TechRegulationSubjectUnitId { get; set; }
        public string TechRegulationSubjectUnitCode { get; set; }
        public string TechRegulationSubjectUnitName { get; set; }

        public int? IssueCountryId { get; set; }
        public string IssueCountryCode { get; set; }
        public string IssueCountryName { get; set; }

        public int OrganizationId { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

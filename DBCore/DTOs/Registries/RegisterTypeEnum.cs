﻿namespace DBCore.Enums.Organizations
{    
    public enum RegisterTypeEnum
    {        
        RegisterGeneral,
        RegisterKiru,
        RegisterKirm,
    }
}

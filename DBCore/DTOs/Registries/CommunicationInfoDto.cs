﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Registries
{
    public class CommunicationInfoDto
    {
        public int Id { get; set; }

        public int? CommunicationTypeId { get; set; }
        public string CommunicationTypeCode { get; set; }
        public string CommunicationTypeName { get; set; }
        public string CommunicationValue { get; set; }
        public int OrganizationId { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Registries
{
    public class OrganizationRefDto
    {
        public int ParentId { get; set; }
        public int ChildId { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

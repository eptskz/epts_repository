﻿using DBCore.Enums;

namespace DBCore.DTOs
{
    public class PagedSortRequestDto<T> where T: class
    {
        private int page { get; set; }
        public virtual int Page {
            get
            {
                if (page - 1 <= 0) return 0;
                return page - 1;
            }
            set { this.page = value; } 
        }
        public virtual int PageSize { get; set; }
        public virtual string[] SortField { get; set; }
        public virtual SortDirectionEnum[] Direction { get; set; }

        public T Filter { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Notifications
{
    public class NotificationFilterDto
    {
        public string CurrentUserId { get; set; }
        public string Search { get; set; }
        public bool IsShowDeleted { get; set; }
        public bool IsShowUnreadOnly { get; set; }
    }
}
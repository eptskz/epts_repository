﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Notifications
{
    public class NotificationDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }

        public Guid SendToUserId { get; set; }
        public string SendToUserName { get; set; }
        public DateTime CreateDate { get; set; }

        public DateTime? ReadDate { get; set; }

        public string Author { get; set; }
    }
}
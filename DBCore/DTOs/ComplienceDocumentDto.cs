﻿using DBCore.DTOs.Complience;
using DBCore.DTOs.Complience.Vehicle;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs
{
    /// <summary>
    /// ОТТС ОТШ СБКТС
    /// </summary>
    public class ComplienceDocumentDto
    {
        public Guid? Id { get; set; }

        /// <summary>
        /// DocId
        /// </summary>
        public string DocNumber { get; set; }

        /// <summary>
        /// DocCreationDate
        /// </summary>
        public DateTime? DocDate { get; set; }        
        public DateTime? DocStartDate { get; set; }

        /// <summary>
        /// DocValidityDate
        /// </summary>
        public DateTime? DocEndDate { get; set; }

        public int? DocTypeId { get; set; }
        public string DocTypeCode { get; set; }
        /// <summary>
        /// DocKindName
        /// </summary>
        public string DocTypeName { get; set; }

        public int? AuthorityId { get; set; }
        public string AuthorityCode { get; set; }
        public string AuthorityName { get; set; }

        public bool NotManufacturerIndicator { get; set; }
        public int? ManufacturerId { get; set; }
        public string ManufacturerCode { get; set; }
        public string ManufacturerName { get; set; }

        public bool NotApplicantOrgIndicator { get; set; }
        public int? ApplicantId { get; set; }
        public string ApplicantCode { get; set; }
        public string ApplicantName { get; set; }

        public bool NotRepresentativeManufacturerIndicator { get; set; }
/*
        public int? RepresentativeManufacturerId { get; set; }
        public string RepresentativeManufacturerCode { get; set; }
        public string RepresentativeManufacturerName { get; set; }
*/
        public List<RepresentativeManufacturerDto> RepresentativeManufacturers { get; set; }

        public bool NotAssemblyPlantIndicator { get; set; }
/*
        public int? AssemblyPlantId { get; set; }
        public string AssemblyPlantCode { get; set; }
        public string AssemblyPlantName { get; set; }
*/
        public List<AssemblyPlantDto> AssemblyPlants { get; set; }

        public bool NotAssemblyKitSupplierIndicator { get; set; }
/*
        public int? AssemblyKitSupplierId { get; set; }
        public string AssemblyKitSupplierCode { get; set; }
        public string AssemblyKitSupplierNames { get; set; }
*/

        public List<AssemblyKitSupplierDto> AssemblyKitSuppliers { get; set; }

        public int? StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }

        public int? CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public int DocDistributionSign { get; set; } = 0;
        public int? BatchCount { get; set; }

        public int? BatchSmallSign { get; set; } = 0;

        public List<BlankNumberDto> BlankNumbers { get; set; }

        public List<VinNumberDto> VinNumbers { get; set; }
        public VehicleLabelingDetailDto VehicleLabelingDetail { get; set; }
        public VehicleTypeDetailDto VehicleTypeDetail { get; set; }


        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }

        public int? AuthorOrgId { get; set; }
    }
}

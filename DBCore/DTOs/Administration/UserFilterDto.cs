﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration
{
    public class UserFilterDto : GeneralFilterDto
    {
        public string Type { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration
{
    public class ObjectClaimDto
    {
        public int Id { get; set; } = 0;
        public Guid? ObjectId { get; set; }
        public string Description { get; set; }        
        public string ClaimValue { get; set; }
        
        public string ObjectType { get; set; }
        public bool IsSelected { get; set; }
    }
}

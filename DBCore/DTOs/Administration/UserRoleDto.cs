﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration
{
    public class UserRoleDto
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

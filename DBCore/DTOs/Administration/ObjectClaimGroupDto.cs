﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration
{
    public class ObjectClaimGroupDto
    {
        public string ClaimGroupName { get; set; }
        public List<ObjectClaimDto> Permissions { get; set; } = new List<ObjectClaimDto>();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration
{
    public class RegrequestDto
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }

        public string OrganizationName { get; set; }
        public string OrganizationBin { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public string SecondName { get; set; }
        public string Certificate { get; set; }
        
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string UserType { get; set; }
        public bool isAccept { get; set; }
        public bool IsExistContract { get; set; }
    }
}

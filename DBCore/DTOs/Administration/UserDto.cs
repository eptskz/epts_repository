﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }        
        public string Password { get; set; }
        public string OldPassword { get; set; }

        public int? OrganizationId { get; set; }
        public string OrganizationName { get; set; }

        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }

        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string FullName { get; set; }
        
        public UserRoleDto[] UserRoles { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration
{
    public class NewsDto
    {
        public Guid Id { get; set; }

        public string TitleRu { get; set; }
        public string TitleKz { get; set; }
        public string TitleEn { get; set; }

        public string ContentRu { get; set; }
        public string ContentKz { get; set; }
        public string ContentEn { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }

        public Guid AuthorId { get; set; }
        public string AuthorName { get; set; }
    }
}

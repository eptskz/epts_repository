﻿using DBCore.Models.Administration.Support;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration
{
    public class SupportFilterDto
    {
        public string Search { get; set; }
        public bool IsShowDeleted { get; set; }
        public TicketCategoryEnum? Category { get; set; }
    }
}

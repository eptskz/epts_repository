﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration.Support
{
    public class SupportTicketCommentDto
    {
        public int Id { get; set; }
        public string Comment { get; set; }

        public DateTime CreateDate { get; set; }
        
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }

        public Guid TicketId { get; set; }
    }
}

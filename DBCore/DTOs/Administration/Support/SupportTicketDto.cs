﻿using DBCore.Models.Administration.Support;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration.Support
{
    public class SupportTicketDto
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public DateTime CreateDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string AuthorId { get; set; }
        public string AuthorName { get; set; }

        public TicketCategoryEnum Category { get; set; }

        public int? StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }

        public List<SupportTicketCommentDto> Comments { get; set; }
    }
}

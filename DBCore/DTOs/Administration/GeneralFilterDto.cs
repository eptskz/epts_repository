﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration
{
    public class GeneralFilterDto
    {
        public string Search { get; set; }
        public bool IsShowDeleted { get; set; }
    }
}

﻿using DBCore.Models.Administration.Support;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Administration.Support
{
    public class ContractDto
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public DateTime ContractDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public string Description { get; set; }

        public string AccountNumber { get; set; }
        public DateTime? AccountDate { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }

        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }

        public int ClientOrgId { get; set; }
        public string ClientOrgBin { get; set; }
        public string ClientOrgName { get; set; }

        public string ClientOrgPhone { get; set; }
        public string ClientOrgEmail { get; set; }
        public string ClientOrgAddress { get; set; }

        public Guid AuthorId { get; set; }
        public string AuthorName { get; set; }

        public string Reason { get; set; }
    }
}

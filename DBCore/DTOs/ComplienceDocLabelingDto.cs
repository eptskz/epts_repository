﻿using DBCore.DTOs.Characteristics;
using DBCore.DTOs.Complience;
using DBCore.DTOs.Complience.Vehicle;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs
{
    public class ComplienceDocLabelingDto
    {
        public Guid? Id { get; set; }

        public string DocNumber { get; set; }
        
        public int? DocTypeId { get; set; }
        public string DocTypeCode { get; set; }
        public string DocTypeName { get; set; }
        public List<VinNumberDto> VinNumbers { get; set; }

        public VehicleLabelingDetailDto VehicleLabelingDetail { get; set; }
    }
}

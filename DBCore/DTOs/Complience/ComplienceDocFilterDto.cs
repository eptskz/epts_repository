﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs
{
    public class ComplienceDocFilterDto
    {
        public string TypeCode { get; set; }
        public string StatusCode { get; set; }
        public bool ShowDeleted { get; set; }
    }
}

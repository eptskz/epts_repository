﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Complience
{
    public class RepresentativeManufacturerDto
    {
        public int Id { get; set; }
        public Guid DocumentId { get; set; }
        public string Document { get; set; }
        public int OrganizationId { get; set; }
        public string Organization { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

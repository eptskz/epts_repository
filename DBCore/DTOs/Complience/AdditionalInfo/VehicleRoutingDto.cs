﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{   
    public class VehicleRoutingDto
    {
        public int Id { get; set; }
        public string VehicleRoutingText { get; set; }

        public Guid VehicleTypeDetailId { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

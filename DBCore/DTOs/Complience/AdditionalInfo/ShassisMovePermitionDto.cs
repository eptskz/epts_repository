﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class ShassisMovePermitionDto
    {
        public int Id { get; set; }
        public string ShassisMovePermitionText { get; set; }

        public Guid VehicleTypeDetailId { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

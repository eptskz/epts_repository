﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class VehicleLabelLocationDto
    {
        public int Id { get; set; }

        public string LocationText { get; set; }

        public int VehicleLabelingDetailId { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

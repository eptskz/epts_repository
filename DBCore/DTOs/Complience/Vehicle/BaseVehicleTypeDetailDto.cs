﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class BaseVehicleTypeDetailDto
    {
        public int Id { get; set; }
        public Guid? VehicleTypeDetailId { get; set; }

        public string CommercialName { get; set; }
        public string Type { get; set; }
        
        public int? MakeId { get; set; }
        public string MakeCode { get; set; }
        public string MakeName { get; set; }
        public string Modification { get; set; }
        public string Vin { get; set; }

        // public string PtsNumber { get; set; }
        // public string EptsNumber { get; set; }
        // public DateTime? EptsDate { get; set; }
        public ObjectStateEnum State { get; set; }
    }
}

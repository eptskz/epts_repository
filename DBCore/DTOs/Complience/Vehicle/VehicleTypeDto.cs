﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class VehicleTypeDto
    {
        public int Id { get; set; }
        public Guid VehicleTypeDetailId { get; set; }
        public string TypeValue { get; set; }
        public ObjectStateEnum State { get; set; }
    }
}
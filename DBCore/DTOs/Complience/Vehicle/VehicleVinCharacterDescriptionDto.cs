﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class VehicleVinCharacterDescriptionDto
    {
        public int Id { get; set; }

        public string IdCharacterValue { get; set; }
        public string IdCharacterValueText { get; set; }

        public int VinCharacterDetailId { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}
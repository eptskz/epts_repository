﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class VehicleTypeDicDto
    {
        public int Id { get; set; }
        public Guid? VehicleTypeDetailId { get; set; }
        public int? DicId { get; set; }
        public string DicCode { get; set; }
        public string DicName{ get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.DTOs.Characteristics;
using DBCore.Models.Vehicle;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class VehicleTypeDetailDto
    {
        public Guid? Id { get; set; }

        public Guid? DocumentId { get; set; }

        public int NotVehicleCommercialNameIndicator { get; set; } = 0;
        public string CommercialName { get; set; }
        public List<VehicleCommercialNameDto> CommercialNames { get; set; }
        
        public string Type { get; set; }        
        public List<VehicleTypeDto> Types { get; set; }

        public string YearIssue { get; set; }

        public int NotVehicleMakeNameIndicator { get; set; } = 0;
        public List<VehicleTypeDicDto> Makes { get; set; }

        public int NotVehicleTechCategoryIndicator { get; set; } = 0;
        public List<VehicleTypeDicDto> TechCategories { get; set; }

        public int NotVehicleClassCategoryIndicator { get; set; } = 0;
        public List<VehicleTypeDicDto> ClassCategories { get; set; }

        public int NotVehicleEcoClassCategoryIndicator { get; set; } = 0;
        public List<VehicleTypeDicDto> EcoClasses { get; set; }

        public int NotVehicleChassisDesignIndicator { get; set; } = 0;        
        public List<VehicleTypeDicDto> ChassisDesigns { get; set; }

        public int NotVehicleModificationIndicator { get; set; } = 0;
        
        /// <summary>
        /// VehicleTypeVariantId
        /// </summary>
        public List<ModificationDto> Modifications { get; set; }

        public int NotBaseVehicleTypeDetailIndicator { get; set; } = 0;
        public List<BaseVehicleTypeDetailDto> BaseVehicleTypeDetails { get; set; }


        public CharacteristicsDetailDto CharacteristicsDetail { get; set; }

        /// <summary>
        /// Описание ограничений использования транспортного средства
        /// </summary>
        public List<VehicleUseRestrictionDto> VehicleUseRestrictions { get; set; }

        /// <summary>
        /// Возможность передвижения шасси своим ходом по дорогам общего пользования
        /// </summary>
        public List<ShassisMovePermitionDto> ShassisMovePermitions { get; set; }

        /// <summary>
        /// Возможность использования в качестве маршрутного транспортного средства
        /// Может быть передано только для категорий по ТР ТС = М2, М3, М2G, M3G
        /// </summary>
        public List<VehicleRoutingDto> VehicleRoutings { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        public List<VehicleNoteDto> VehicleNotes { get; set; }
    }
}

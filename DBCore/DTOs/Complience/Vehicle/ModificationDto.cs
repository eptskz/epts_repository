﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class ModificationDto
    {
        public int Id { get; set; }
        public Guid? VehicleTypeDetailId { get; set; }
        public string Name { get; set; }
        public ObjectStateEnum State { get; set; }
    }
}
﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class VehicleLabelingDetailDto
    {
        public int Id { get; set; }

        public int NotManufacturerPlateIndicatorSign { get; set; }
        public string VehicleComponentLocationText { get; set; }
        public List<VehicleLabelLocationDto> VehicleLabelLocations { get; set; }
        
        public string VehicleIdentificationNumberLocationText { get; set; }
        public List<VehicleLabelLocationDto> VehicleIdentificationNumberLocations { get; set; }
        
        public string EngineIdentificationNumberLocationText { get; set; }

        public string Vin { get; set; }

        public Guid DocumentId { get; set; }

        public List<VehicleVinCharacterDetailDto> VinCharacterDetails { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

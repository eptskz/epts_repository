﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Complience.Vehicle
{
    public class VehicleVinCharacterDetailDto
    {
        public int Id { get; set; }
        public int IdCharacterStartingOrdinal { get; set; }
        public int IdCharacterEndingOrdinal { get; set; }
        public string IdCharacterText { get; set; }

        public int? DataTypeId { get; set; }
        public string DataTypeCode { get; set; }
        public string DataTypeName { get; set; }

        public bool IsAlpha { get; set; }
        public bool IsDigit { get; set; }

        public bool IsIncludeChar { get; set; }
        public string IncludeCharStr { get; set; }

        public bool IsExcludeChar { get; set; }
        public string ExcludeCharStr { get; set; }


        public int VehicleLabelingDetailId { get; set; }

        public List<VehicleVinCharacterDescriptionDto> VinCharacterDescriptions { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

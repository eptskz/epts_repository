﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace DBCore.DTOs.Complience
{
    public class VinNumberDto : IObjectState
    {
        public int? Id { get; set; }
        public Guid? DocumentId { get; set; }
        public string Number { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

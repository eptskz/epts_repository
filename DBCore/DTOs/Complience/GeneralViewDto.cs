﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Complience
{
    public class GeneralViewDto
    {
        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }

        public string VehiclePicture { get; set; }
        public string FileName { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

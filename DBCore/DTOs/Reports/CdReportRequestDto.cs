﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Reports
{
    public class CdReportRequestDto
    {
        public List<int> OrganizationIds { get; set; }
        public List<int> DocumentTypeIds { get; set; }
        public List<int> StatusIds { get; set; }

        public string DocumentNumber { get; set; }
        
        public DateTime? DateStartFrom { get; set; }
        public DateTime? DateStartTo { get; set; }

        public DateTime? DateEndFrom { get; set; }
        public DateTime? DateEndTo { get; set; }
    }
}

﻿using DBCore.DTOs.Characteristics;
using DBCore.DTOs.Complience;
using DBCore.DTOs.Complience.Vehicle;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs
{
    public class ComplienceDocumentOrgDto
    {
        public Guid? Id { get; set; }

        public string DocNumber { get; set; }
        
        public int? DocTypeId { get; set; }
        public string DocTypeCode { get; set; }
        public string DocTypeName { get; set; }

        public int? AuthorityId { get; set; }
        public OrganizationDto Authority { get; set; }


        public bool NotManufacturerIndicator { get; set; }
        public int? ManufacturerId { get; set; }
        public OrganizationDto Manufacturer { get; set; }


        public bool NotApplicantOrgIndicator { get; set; }
        public int? ApplicantId { get; set; }
        public OrganizationDto Applicant { get; set; }


        public bool NotRepresentativeManufacturerIndicator { get; set; }
 /*
        public int? RepresentativeManufacturerId { get; set; }
        public OrganizationDto RepresentativeManufacturer { get; set; }
*/
        public List<RepresentativeManufacturerDto> RepresentativeManufacturers { get; set; }

        public bool NotAssemblyPlantIndicator { get; set; }
/*
        public int? AssemblyPlantId { get; set; }
        public OrganizationDto AssemblyPlant { get; set; }
*/
        public List<AssemblyPlantDto> AssemblyPlants { get; set; }

        public bool NotAssemblyKitSupplierIndicator { get; set; }
/*
        public int? AssemblyKitSupplierId { get; set; }
        public OrganizationDto AssemblyKitSupplier { get; set; }
*/
        public List<AssemblyKitSupplierDto> AssemblyKitSuppliers { get; set; }

    }
}

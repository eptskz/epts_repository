﻿using DBCore.DTOs.DigitalPassport;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs
{
    public class ChangeApplicationDto
    {
        public MongoDB.Bson.ObjectId Id { get; set; }
        public Guid? SignId { get; set; }
        public Guid? ExecuteSignId { get; set; }
        public DateTime? DeclineDate { get; set; }
        public string DeclineDescription { get; set; }
        public Guid? UserCreatedId { get; set; }
        public Guid? UserExecutorId { get; set; }

        public bool IsFieldChange { get; set; }
        public string ChangeStatusNsiCode { get; set; }
        public string ChangeStatusReason { get; set; }

        public string StatusNsiCode { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public string Comment { get; set; }

        public List<ChangeApplicationFileDto> Files { get; set; }
        public List<ChangeApplicationFieldDto> Fields { get; set; }
        public int DigitalPassportId { get; set; }
        public RootPassportDto DigitalPassport { get; set; }
    }

    public class ChangeApplicationFieldDto
    {
        public string Section { get; set; }
        public string Field { get; set; }
        public string PreValue { get; set; }
        public string PostValue { get; set; }
        public string Reason { get; set; }
    }

    public class ChangeApplicationFileDto
    {
        public string FileObject { get; set; }
        public string FileName { get; set; }
    }
}

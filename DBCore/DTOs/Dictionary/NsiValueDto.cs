﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Dictionary
{
    public class NsiValueDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string NameRu { get; set; }
        public string NameKz { get; set; }
        public string NameEn { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }

        public int NsiId { get; set; }
        public string NsiCode { get; set; }
        public string NsiName { get; set; }

        public int? WheelQuantity { get; set; }
        public int? PowerWheelQuantity { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs
{
    public class DictionaryFilterDto
    {
        public string Search { get; set; }
    }
}

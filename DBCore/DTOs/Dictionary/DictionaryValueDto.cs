﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Dictionary
{
    public class DictionaryValueDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string NameRu { get; set; }
        public string NameKz { get; set; }
        public string NameEn { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Deleted { get; set; }
        public int DictionaryId { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

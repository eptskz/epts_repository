﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.DTOs.Dictionary
{
    public class OrganizationTypeDto
    {
        public int Id { get; set; }
        public int OrgTypeId { get; set; }
        public string OrgTypeCode { get; set; }
        public string OrgTypeName { get; set; }
        public int OrganizationId { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

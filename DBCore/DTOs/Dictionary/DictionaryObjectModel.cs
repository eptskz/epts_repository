﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Dictionary
{
    public class DictionaryObjectModel
    {
        public int Id { get; set; }
        public int DicId { get; set; }
        public int ObjectId { get; set; }
        public string ObjectStrId { get; set; }

        public ObjectStateEnum State { get; set; }
    }
}

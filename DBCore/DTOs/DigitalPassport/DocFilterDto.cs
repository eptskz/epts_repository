﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs
{
    public class DocFilterDto
    {
        public string TypeCode { get; set; }
        public string Search { get; set; }
    }
}

﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.DigitalPassport
{
    public class RootPassportDto
    {
        public MongoDB.Bson.ObjectId Id { get; set; }
        public int DocType { get; set; }
        public Guid? AspNetUserId { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public Guid? SignId { get; set; }
        public string UniqueCode { get; set; }
        public string VehicleEPassportBaseCode { get; set; }
        public string VehicleEPassportBaseDescription { get; set; }
        public string DocKindCode { get; set; }
        public Guid? ComplienceDocumentId { get; set; }
        public DateTime? DocDateFrom { get; set; }
        public DateTime? DocDateTo { get; set; }
        public string AuthorityName { get; set; }
        public string DocumentNotExistDescription { get; set; }
        public string DocKindCodeHandy { get; set; }
        public string DocNameHandy { get; set; }
        public string DocAuthorHandy { get; set; }
        public string DocIdHandy { get; set; }
        public DateTime? DocDateHandy { get; set; }
        public string DocPageCountHandy { get; set; }
        public bool documentNotExists { get; set; }
        public string VehicleIdentityNumberIndicator { get; set; }
        public string VehicleIdentityNumberId { get; set; }
        public List<VehicleEngineIdList> VehicleEngineIdList { get; set; }
        public bool NotVehicleIdentityNumberIndicator { get; set; }
        public string VehicleCategoryCode { get; set; }
        public bool NotVehicleEngineIdentityNumberIndicator { get; set; }
        public string VehicleFrameIdentityNumberId { get; set; }
        public bool NotVehicleFrameIdentityNumberIndicator { get; set; }
        public string VehicleBodyIdentityNumberId { get; set; }
        public bool NotVehicleBodyIdentityNumberIndicator { get; set; }
        public string VehicleEmergencyCallIdentityNumberId { get; set; }
        public bool NotVehicleEmergencyCallIdentityNumberIndicator { get; set; }
        public bool NotGearNumberIndicator { get; set; }
        public string GearNumber { get; set; }
        public bool NotBaseAxleNumberIndicator { get; set; }
        public string BaseAxleNumber { get; set; }
        public string MoverEpsm { get; set; }
        public bool BodyMultiColourIndicator { get; set; }
        public string VehicleBodyColourCode { get; set; }
        public string VehicleBodyColourCode2 { get; set; }
        public string VehicleBodyColourCode3 { get; set; }
        public string VehicleBodyColourName { get; set; }
        public string ManufactureYear { get; set; }
        public string ManufactureMonth { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DocValidityDate { get; set; }
        public DateTime? DocCreationDate { get; set; }
        public string VehicleMakeName { get; set; }
        public bool NotVehicleMakeNameIndicator { get; set; }
        public string VehicleCommercialName { get; set; }
        public bool NotVehicleCommercialNameIndicator { get; set; }
        public string VehicleTypeId { get; set; }
        public string VehicleTypeVariantId { get; set; }
        public bool notVehicleModificationIndicator { get; set; }
        public string VehicleTechCategoryCode { get; set; }
        public string VehicleEcoClassCode { get; set; }
        public bool NotVehicleEcoClassCodeIndicator { get; set; }
        public string VehicleManufactureOption { get; set; }
        public bool NotBaseVehicleIndicator { get; set; }
        public string BaseVehicleMakeName { get; set; }
        public string BaseVehicleTypeId { get; set; }
        public string BaseVehicleCommercialName { get; set; }
        public string BaseVehicleTypeVariantId { get; set; }
        public bool BaseDocIndicator { get; set; }
        public string BaseVehicleEPassportId { get; set; }
        public DateTime? BaseDocCreationDate { get; set; }
        public bool NotManufacturerIndicator { get; set; }
        public int ManufactureOrganizationId { get; set; }
        public bool NotMemberOrganizationIndicator { get; set; }
        public List<MemberOrganizationList> MemberOrganizationList { get; set; }
        public bool NotAssemblerOrganizationIndicator { get; set; }
        public List<AssemblerOrganizationList> AssemblerOrganizationList { get; set; }
        public ManufacturerPlateDetails ManufacturerPlateDetails { get; set; }
        public EnginePlateDetails EnginePlateDetails { get; set; }
        public CharacteristicDetails CharacteristicDetails { get; set; }
        public List<ImagesTs> imagesTS { get; set; }

        public string SbktsDocId { get; set; }
        public DateTime? SbktsStartDate { get; set; }
        public DateTime? SbktsDocValidityDate { get; set; }
        public string CertifiedMemberFullName { get; set; }
        public bool VehicleMovementPermitIndicator { get; set; }
        public string ShassisMovePermitionText { get; set; }
        public string AllRoadMoveAbilityLimit { get; set; }
        public string MinibusAbility { get; set; }
        public string OtherInformation { get; set; }
        public string ManufactureWithPrivilegeMode { get; set; }
        public string AcceptTerritoryRegistration { get; set; }

        public bool NotSateliteNavigationIndicator { get; set; }
        public string SateliteNavigationDescription { get; set; }
        public bool NotVehicleOfJobControllIndicator { get; set; }
        public string VehicleOfJobControllDescription { get; set; }
        public string VehicleMadeInCountry { get; set; }
        public string DigitalPassportRegisterBaseDescription { get; set; }
        public string ManufacturerInformation { get; set; }
    }

    public class AcceptCountryAndRegion
    {
        public string AcceptCountryRegistration { get; set; }
        public string AcceptRegionRegistration { get; set; }
    }

    public class VehicleEngineIdList
    {
        public string VehicleEngineIdentityNumberId { get; set; }
    }

    public class MemberOrganizationList
    {
        public bool NotMemberIndicator { get; set; }
        public int MemberOrganizationId { get; set; }
    }

    public class AssemblerOrganizationList
    {
        public bool NotAssemblerIndicator { get; set; }
        public int AssemblerOrganizationId { get; set; }
    }

    public class MeaningExplanationList
    {
        public string Explanation { get; set; }
        public string Meaning { get; set; }
    }

    public class VehicleStructureCardList
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Description { get; set; }
        public List<MeaningExplanationList> MeaningExplanationList { get; set; }
    }

    public class ManufacturerPlateDetails
    {
        public bool NotManufacturerPlateLocationIndicator { get; set; }
        public List<ManufacturerPlateLocationList> ManufacturerPlateLocationList { get; set; }
        public List<VehicleIdentLocationList> VehicleIdentLocationList { get; set; }
        public List<VehicleStructureCardList> VehicleStructureCardList { get; set; }
    }

    public class VehicleIdentLocationList
    {
        public string VehicleIdentLocation { get; set; }
    }

    public class ManufacturerPlateLocationList
    {
        public string ManufacturerPlateLocation { get; set; }
    }

    public class EnginePlateDetails
    {
        public List<VehicleIdentLocationList> VehicleIdentLocationList { get; set; }
        public List<VehicleStructureCardList> VehicleStructureCardList { get; set; }
    }

    public class SeatRawAndQuantityList
    {
        public string SeatQuantity { get; set; }
        public string SeatRawOrdinal { get; set; }
    }

    public class PoweredWheelLocationList
    {
        public string PoweredWheelLocation { get; set; }
    }

    public class VehicleAxlesList
    {
        public string VehicleAxleOrdinal { get; set; }
        public bool DrivingAxleIndicator { get; set; }
        public string VehicleTechnicallyPermissibleMaxWeightOnAxleMeasure { get; set; }
        public bool VehicleTechnicallyPermissibleMaxWeightOnAxleMeasureRange { get; set; }
        public string VehicleTechnicallyPermissibleMaxWeightOnAxleMeasureMin { get; set; }
        public string VehicleTechnicallyPermissibleMaxWeightOnAxleMeasureMax { get; set; }
        public string VehicleTechnicallyPermissibleMaxWeightOnAxleMeasureUnitCode { get; set; }
        public string VehicleAxleSweptPathMeasure { get; set; }
        public bool VehicleAxleSweptPathMeasureRange { get; set; }
        public string VehicleAxleSweptPathMeasureUnitCode { get; set; }
    }

    public class VehiclePowerStorageDeviceList
    {
        public string VehiclePowerStorageDeviceComponentText { get; set; }
        public string VehiclePowerStorageDeviceComponentLocationText { get; set; }
        public string VehicleRangeMeasure { get; set; }
        public string VehicleRangeMeasureUnitCode { get; set; }
        public string PowerStorageDeviceVoltageMeasure { get; set; }
        public string PowerStorageDeviceVoltageMeasureUnitCode { get; set; }
    }

    public class VehicleTransmissionList
    {
        public string VehicleTransmissionText { get; set; }
        public string VehicleTransmissionDescription { get; set; }
    }

    public class TransmissionUnitGearList
    {
        public string TransmissionUnitGearName { get; set; }
        public string TransmissionUnitGearType { get; set; }
        public bool TransmissionUnitGearRange { get; set; }
        public string TransmissionUnitGearRate { get; set; }
        public string TransmissionUnitGearRateMax { get; set; }
        public string TransmissionUnitGearRateMin { get; set; }
    }

    public class GearBoxList
    {
        public string VehicleGearComponentMakeName { get; set; }
        public string VehicleGearComponentText { get; set; }
        public bool NotVehicleGearIndicator { get; set; }
        public bool TransmissionUnitReverseGearIndicator { get; set; }
        public List<TransmissionUnitGearList> TransmissionUnitGearList { get; set; }
    }

    public class TransferGearBoxList
    {
        public string VehicleGearComponentMakeName { get; set; }
        public string VehicleGearComponentText { get; set; }
        public List<TransmissionUnitGearList> TransmissionUnitGearList { get; set; }
    }

    public class MainGearBoxList
    {
        public string VehicleGearComponentMakeName { get; set; }
        public string AxisDistribution { get; set; }
        public string VehicleGearComponentText { get; set; }
        public List<TransmissionUnitGearList> TransmissionUnitGearList { get; set; }
    }

    public class VehicleSuspensionList
    {
        public string VehicleSuspensionKindCode { get; set; }
        public string VehicleComponentText { get; set; }
    }

    public class VehicleBrakingSystemList
    {
        public string VehicleBrakingSystemKindCode { get; set; }
        public string VehicleComponentText { get; set; }
    }

    public class VehicleTyreList
    {
        public string VehicleTyreKindSize { get; set; }
        public string VehicleTyreKindIndex { get; set; }
        public string VehicleTyreKindSpeed { get; set; }
        public bool IsSupplementVehicleTyre { get; set; }
    }

    public class VehicleEquipmentList
    {
        public string VehicleEquipmentText { get; set; }
    }

    [Serializable]
    public class VehicleWheelbaseMeasureList
    {
        public string VehicleWheelbaseMeasure { get; set; }
        public bool VehicleWheelbaseMeasureRange { get; set; }
        public string VehicleWheelbaseMeasureMin { get; set; }
        public string VehicleWheelbaseMeasureMax { get; set; }
        public string VehicleWheelbaseMeasureUnitCode { get; set; }
    }

    [Serializable]
    public class VehicleHitchLoadMeasureList
    {
        public string VehicleHitchLoadMeasure { get; set; }
        public bool VehicleHitchLoadMeasureRange { get; set; }
        public string VehicleHitchLoadMeasureMin { get; set; }
        public string VehicleHitchLoadMeasureMax { get; set; }
        public string VehicleHitchLoadMeasureUnitCode { get; set; }
    }

    [Serializable]
    public class CharacteristicDetails
    {
        public MongoDB.Bson.ObjectId Id { get; set; }
        public int DigitalPassportId { get; set; }
        public string VehicleWheelLocationQuantity { get; set; }
        public string VehicleWheelAxelQuantity { get; set; }
        public string VehicleWheelQuantity { get; set; }
        public string VehicleWheelsFormula { get; set; }
        public List<PoweredWheelLocationList> PoweredWheelLocationList { get; set; }
        public string VehicleComponentText { get; set; }
        public string VehicleDoorQuantity { get; set; }
        public string SeatQuantity { get; set; }
        public string SeatDescription { get; set; }
        public List<SeatRawAndQuantityList> SeatRawAndQuantityList { get; set; }
        public string VehicleLayoutPatternText { get; set; }
        public string VehicleComponentLocationText { get; set; }
        public List<EpsmVehicleComponentLocationText> EpsmVehicleComponentLocationText { get; set; }
        public string VehicleCarriageSpaceImplementationText { get; set; }
        public string VehiclePassengerQuantity { get; set; }
        public string VehicleTrunkVolumeMeasure { get; set; }
        public string measurementUnitCode { get; set; }
        public string VehicleComponentText2 { get; set; }
        public string VehicleFrameText { get; set; }
        public bool VehicleSpecialText { get; set; }
        public string VehicleCharacteristicsName { get; set; }
        public string VehicleCharacteristicsNameOther { get; set; }
        public string LengthMeasure { get; set; }
        public bool LengthMeasureRange { get; set; }
        public string LengthMeasureMin { get; set; }
        public string LengthMeasureMax { get; set; }
        public string LengthMeasureUnitCode { get; set; }
        public string WidthMeasure { get; set; }
        public bool WidthMeasureRange { get; set; }
        public string WidthMeasureMin { get; set; }
        public string WidthMeasureMax { get; set; }
        public string WidthMeasureUnitCode { get; set; }
        public string HeightMeasure { get; set; }
        public bool HeightMeasureRange { get; set; }
        public string HeightMeasureUnitCode { get; set; }
        public bool HeightMeasureAdditionalIndicator { get; set; }
        public string HeightShipMeasure { get; set; }
        public bool HeightShipMeasureRange { get; set; }
        public string HeightShipMeasureMin { get; set; }
        public string HeightShipMeasureMax { get; set; }
        public string HeightShipMeasureUnitCode { get; set; }
        public string MaxHeightMeasure { get; set; }
        public bool MaxHeightMeasureRange { get; set; }
        public string MaxHeightMeasureMin { get; set; }
        public string MaxHeightMeasureMax { get; set; }
        public string MaxHeightMeasureUnitCode { get; set; }
        public List<VehicleWheelbaseMeasureList> VehicleWheelbaseMeasureList { get; set; }
        public List<VehicleAxlesList> VehicleAxlesList { get; set; }
        public bool EpsmReverseDriveIndicator { get; set; }
        public string EpsmClearance { get; set; }
        public string EpsmDescription { get; set; }
        public string EpsmVoltageRate { get; set; }
        public string EpsmMaxSpeed { get; set; }
        public string EpsmTrailerMaxMass { get; set; }
        public string EpsmExpluationMass { get; set; }
        public string EpsmAxleMaxMass { get; set; }
        public string EpsmAxleVerticalMaxMass { get; set; }
        public string EpsmUsefulLoad { get; set; }
        public string EpsmTotalMaxMass { get; set; }


        public string VehicleMassMeasure { get; set; }
        public bool VehicleMassMeasureRange { get; set; }
        public string VehicleMassMeasureMin { get; set; }
        public string VehicleMassMeasureMax { get; set; }
        public string VehicleMassMeasureUnitCode { get; set; }
        public string VehicleMassMeasure1 { get; set; }
        public bool VehicleMassMeasure1Range { get; set; }
        public string VehicleMassMeasure1Min { get; set; }
        public string VehicleMassMeasure1Max { get; set; }
        public string VehicleMassMeasure1UnitCode { get; set; }
        public string VehicleMassMeasure2 { get; set; }
        public bool VehicleMassMeasure2Range { get; set; }
        public string VehicleMassMeasure2Min { get; set; }
        public string VehicleMassMeasure2Max { get; set; }
        public string VehicleMassMeasure2UnitCode { get; set; }
        public bool NotVehicleTrailerIndicator { get; set; }
        public string VehicleMaxUnbrakedTrailerWeightMeasure { get; set; }
        public bool VehicleMaxUnbrakedTrailerWeightMeasureRange { get; set; }
        public string VehicleMaxUnbrakedTrailerWeightMeasureMin { get; set; }
        public string VehicleMaxUnbrakedTrailerWeightMeasureMax { get; set; }
        public string VehicleMaxUnbrakedTrailerWeightMeasureUnitCode { get; set; }
        public string VehicleMaxBrakedTrailerWeightMeasure { get; set; }
        public bool VehicleMaxBrakedTrailerWeightMeasureRange { get; set; }
        public string VehicleMaxBrakedTrailerWeightMeasureMin { get; set; }
        public string VehicleMaxBrakedTrailerWeightMeasureMax { get; set; }
        public string VehicleMaxBrakedTrailerWeightMeasureUnitCode { get; set; }
        public List<VehicleHitchLoadMeasureList> VehicleHitchLoadMeasureList { get; set; }
        public bool NotVehicleEngineIndicator { get; set; }
        public string VehicleEngineType { get; set; }
        public string VehicleHybridDesignText { get; set; }

        public List<DvsEngine> DvsEngine { get; set; }
        public bool NotElectricalEngineIndicator { get; set; }

        public List<ElectricalEngine> ElectricalEngine { get; set; }
        public bool NotElectricalMachineIndicator { get; set; }
        public List<ElectricalMachine> ElectricalMachine { get; set; }
        public List<EpsmOtherEngine> EpsmOtherEngine { get; set; }
        
        public bool NotVehicleFuelIndicator { get; set; }
        public List<VehicleFuel> VehicleFuelKindCodeList { get; set; }
        public bool NotVehicleSupplySystemIndicator { get; set; }
        public string VehicleSupplySystemComponentText { get; set; }
        public bool NotECUModelCodeIndicator { get; set; }
        public string ECUModelCode { get; set; }
        public bool NotPowerStorageDeviceIndicator { get; set; }
        public List<VehiclePowerStorageDeviceList> VehiclePowerStorageDeviceList { get; set; }
        public bool NotVehicleIgnitionIndicator { get; set; }
        public string VehicleIgnitionComponentText { get; set; }
        public bool NotVehicleExhaustIndicator { get; set; }
        public string VehicleExhaustComponentText { get; set; }
        public bool NotVehicleTransmissionIndicator { get; set; }
        public List<VehicleTransmissionList> VehicleTransmissionList { get; set; }
        public List<GearBoxList> GearBoxList { get; set; }
        public List<TransferGearBoxList> TransferGearBoxList { get; set; }
        public List<MainGearBoxList> MainGearBoxList { get; set; }
        public bool NotVehicleClutchDetailIndicator { get; set; }
        public string VehicleClutchComponentMakeName { get; set; }
        public string VehicleClutchComponentText { get; set; }
        public bool NotVehicleSuspensionDetailIndicator { get; set; }
        public List<VehicleSuspensionList> VehicleSuspensionList { get; set; }
        public bool NotVehicleSteeringDetailIndicator { get; set; }
        public string VehicleSteeringDetailComponentText { get; set; }
        public string SteeringWheelPositionCode { get; set; }
        public string VehicleSteeringComponentLocationText { get; set; }
        public bool NotVehicleBrakingSystemDetailIndicator { get; set; }
        public List<VehicleBrakingSystemList> VehicleBrakingSystemList { get; set; }
        public List<VehicleTyreList> VehicleTyreList { get; set; }
        public List<VehicleEquipmentList> VehicleEquipmentList { get; set; }
        public List<AcceptCountryAndRegion> AcceptCountryAndRegion { get; set; }
    }

    public class ImagesTs
    {
        public string name { get; set; }
        public string image { get; set; }
    }

    public class ElectricalEngine
    {
        public string ElectricalEngineKindCode { get; set; }
        public string ElectricalEngineMakeName { get; set; }
        public string ElectricalEngineModelCode { get; set; }
        public string ElectricalEngineVoltageMeasure { get; set; }
        public string ElectricalEngineVoltageMeasureUnitCode { get; set; }
        public string ElectricEnginePowerMeasure { get; set; }
        public string ElectricEnginePowerMeasureUnitCode { get; set; }
        public string ElectricEngineVehicleComponentText { get; set; }
    }

    public class EpsmOtherEngine
    {
        public string MakeName { get; set; }
        public string TradeMark { get; set; }
        public string EngineType { get; set; }
    }

    public class VehicleFuel
    {
        public string VehicleFuelKindCode { get; set; }
    }

    public class ElectricalMachine
    {
        public string ElectricalMachineKindCode { get; set; }
        public string ElectricalMachineMakeName { get; set; }
        public string ElectricalMachineModelCode { get; set; }
        public string ElectricalMachineVoltageMeasure { get; set; }
        public string ElectricalMachineVoltageMeasureUnitCode { get; set; }
        public string ElectricMachinePowerMeasure { get; set; }
        public string ElectricMachinePowerMeasureUnitCode { get; set; }
        public string ElectricMachineVehicleComponentText { get; set; }
    }

    public class DvsEngine
    {
        public string pk { get; set; }
        public string VehicleDVSComponentMakeName { get; set; }
        public string VehicleDVSComponentText { get; set; }
        public string EngineCylinderQuantity { get; set; }
        public string EngineCompressionRate { get; set; }
        public string EngineCapacityMeasure { get; set; }
        public string EngineCapacityMeasureUnitCode { get; set; }
        public string EngineCylinderArrangementText { get; set; }
        public string EngineMaxPowerMeasure { get; set; }
        public string EngineMaxPowerMeasureUnitCode { get; set; }
        public string PowerVehicleShaftRotationFrequencyMinMeasure { get; set; }
        public string PowerVehicleShaftRotationFrequencyMaxMeasure { get; set; }
        public string PowerVehicleShaftRotationFrequencyMeasureUnitCode { get; set; }
        public string EngineMaxTorqueMeasure { get; set; }
        public string EngineMaxTorqueMeasureUnitCode { get; set; }
        public string VehicleShaftRotationFrequencyMinMeasure { get; set; }
        public string VehicleShaftRotationFrequencyMaxMeasure { get; set; }
        public string VehicleShaftRotationFrequencyMeasureUnitCode { get; set; }

        public List<EpsmShaftChoicePower> EpsmShaftChoicePower { get; set; }
        public string EpsmComponentDescription { get; set; }
        public string EpsmComponentLocationDescription { get; set; }
        public string EpsmShaftSpeed { get; set; }
        public string EpsmTransmitionUnitGearRatio { get; set; }

    }
    public class EpsmShaftChoicePower
    {
        public string Power { get; set; }

    }

    public class EpsmVehicleComponentLocationText
    {
        public string VehicleComponentLocationText { get; set; }
    }
}

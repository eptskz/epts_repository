﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.DigitalPassport
{
    public class ExternalServiceFilterDto
    {
        public string Service { get; set; }
        public DateTime? From { get; set; }
        public DateTime? Till { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.DigitalPassport
{
    public class ReportFilterDto
    {
        public string Region { get; set; }
        public string Org { get; set; }
        public string Kind { get; set; }
        public string Status { get; set; }
        public string RegKind { get; set; }
        public DateTime? From { get; set; }
        public DateTime? Till { get; set; }
    }
}

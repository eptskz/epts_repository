﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Documents
{
    public class ProcessRequestDto
    {
        public Guid DocId { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }
}

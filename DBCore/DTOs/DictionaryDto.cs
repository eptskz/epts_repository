﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs
{
    public class DictionaryDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NsiCode { get; set; }

    }
}

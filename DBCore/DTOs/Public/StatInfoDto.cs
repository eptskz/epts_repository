﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.DTOs.Public
{
    public class StatInfoDto
    {
        public int DpassportOttsCount { get; set; }
        public int DpassportOtchCount { get; set; }
        public int DpassportSbktsCount { get; set; }
    }
}

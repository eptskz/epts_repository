﻿namespace DBCore.Enums
{
    public enum SortDirectionEnum
    {
        Ascending,
        Descending
    }
}

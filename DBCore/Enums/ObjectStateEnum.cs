﻿namespace DBCore.Enums
{    
    public enum ObjectStateEnum
    {
        Unchanged,
        Added,
        Modified,
        Deleted
    }
}

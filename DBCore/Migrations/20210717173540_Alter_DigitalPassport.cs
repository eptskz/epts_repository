﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class Alter_DigitalPassport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocId",
                table: "DigitalPassportDocument");

            migrationBuilder.AlterColumn<Guid>(
                name: "SignId",
                table: "DigitalPassportDocument",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<bool>(
                name: "NotVehicleEcoClassCodeIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NotVehicleEcoClassCodeIndicator",
                table: "DigitalPassportDocument");

            migrationBuilder.AlterColumn<Guid>(
                name: "SignId",
                table: "DigitalPassportDocument",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocId",
                table: "DigitalPassportDocument",
                type: "text",
                nullable: true);
        }
    }
}

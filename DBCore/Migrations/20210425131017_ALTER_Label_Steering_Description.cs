﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class ALTER_Label_Steering_Description : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VehicleIdentificationNumberLocations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LocationText = table.Column<string>(nullable: true),
                    VehicleLabelingDetailId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleIdentificationNumberLocations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleIdentificationNumberLocations_VehicleLabelingDetail_~",
                        column: x => x.VehicleLabelingDetailId,
                        principalTable: "VehicleLabelingDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleLabelLocations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LocationText = table.Column<string>(nullable: true),
                    VehicleLabelingDetailId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleLabelLocations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleLabelLocations_VehicleLabelingDetail_VehicleLabeling~",
                        column: x => x.VehicleLabelingDetailId,
                        principalTable: "VehicleLabelingDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleSteeringDescriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleSteeringDescriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleSteeringDescriptions_CharacteristicsDetail_Character~",
                        column: x => x.CharacteristicId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleIdentificationNumberLocations_VehicleLabelingDetailId",
                table: "VehicleIdentificationNumberLocations",
                column: "VehicleLabelingDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLabelLocations_VehicleLabelingDetailId",
                table: "VehicleLabelLocations",
                column: "VehicleLabelingDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSteeringDescriptions_CharacteristicId",
                table: "VehicleSteeringDescriptions",
                column: "CharacteristicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleIdentificationNumberLocations");

            migrationBuilder.DropTable(
                name: "VehicleLabelLocations");

            migrationBuilder.DropTable(
                name: "VehicleSteeringDescriptions");
        }
    }
}

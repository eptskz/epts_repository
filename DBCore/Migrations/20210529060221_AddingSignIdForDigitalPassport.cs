﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class AddingSignIdForDigitalPassport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_DicKatos_AcceptRegionRegistrationId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_AcceptRegionRegistrationId",
                table: "DigitalPassportDocument");

            migrationBuilder.AddColumn<Guid>(
                name: "SignId",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SignId",
                table: "DigitalPassportDocument");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_AcceptRegionRegistrationId",
                table: "DigitalPassportDocument",
                column: "AcceptRegionRegistrationId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_DicKatos_AcceptRegionRegistrationId",
                table: "DigitalPassportDocument",
                column: "AcceptRegionRegistrationId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

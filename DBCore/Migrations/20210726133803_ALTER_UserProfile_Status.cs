﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_UserProfile_Status : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "UserProfiles",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserProfiles_StatusId",
                table: "UserProfiles",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserProfiles_DicStatuses_StatusId",
                table: "UserProfiles",
                column: "StatusId",
                principalTable: "DicStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserProfiles_DicStatuses_StatusId",
                table: "UserProfiles");

            migrationBuilder.DropIndex(
                name: "IX_UserProfiles_StatusId",
                table: "UserProfiles");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "UserProfiles");
        }
    }
}

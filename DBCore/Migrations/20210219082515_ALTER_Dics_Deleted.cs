﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Dics_Deleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComplienceDocuments_DicDocTypes_DocTypeId",
                table: "ComplienceDocuments");

            migrationBuilder.AddColumn<DateTime>(
                name: "Deleted",
                table: "DicStatuses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Deleted",
                table: "DicDocTypes",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ComplienceDocuments_DicDocTypes_DocTypeId",
                table: "ComplienceDocuments",
                column: "DocTypeId",
                principalTable: "DicDocTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComplienceDocuments_DicDocTypes_DocTypeId",
                table: "ComplienceDocuments");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "DicStatuses");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "DicDocTypes");

            migrationBuilder.AddForeignKey(
                name: "FK_ComplienceDocuments_DicDocTypes_DocTypeId",
                table: "ComplienceDocuments",
                column: "DocTypeId",
                principalTable: "DicDocTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

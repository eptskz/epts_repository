﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Organization_ExtendAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FDistinctOrCity",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FOkrugOrCity",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FRegionOrCity",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FVillage",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JDistinctOrCity",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JOkrugOrCity",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JRegionOrCity",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JVillage",
                table: "Organizations",
                nullable: true);

            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AF' WHERE \"NsiId\" = 28 AND \"Code\"='004';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AL' WHERE \"NsiId\" = 28 AND \"Code\"='008';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AQ' WHERE \"NsiId\" = 28 AND \"Code\"='010';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='DZ' WHERE \"NsiId\" = 28 AND \"Code\"='012';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AS' WHERE \"NsiId\" = 28 AND \"Code\"='016';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AD' WHERE \"NsiId\" = 28 AND \"Code\"='020';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AO' WHERE \"NsiId\" = 28 AND \"Code\"='024';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AG' WHERE \"NsiId\" = 28 AND \"Code\"='028';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AZ' WHERE \"NsiId\" = 28 AND \"Code\"='031';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AR' WHERE \"NsiId\" = 28 AND \"Code\"='032';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AU' WHERE \"NsiId\" = 28 AND \"Code\"='036';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AT' WHERE \"NsiId\" = 28 AND \"Code\"='040';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BS' WHERE \"NsiId\" = 28 AND \"Code\"='044';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BH' WHERE \"NsiId\" = 28 AND \"Code\"='048';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BD' WHERE \"NsiId\" = 28 AND \"Code\"='050';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AM' WHERE \"NsiId\" = 28 AND \"Code\"='051';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BB' WHERE \"NsiId\" = 28 AND \"Code\"='052';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BE' WHERE \"NsiId\" = 28 AND \"Code\"='056';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BM' WHERE \"NsiId\" = 28 AND \"Code\"='060';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BT' WHERE \"NsiId\" = 28 AND \"Code\"='064';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BO' WHERE \"NsiId\" = 28 AND \"Code\"='068';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BA' WHERE \"NsiId\" = 28 AND \"Code\"='070';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BW' WHERE \"NsiId\" = 28 AND \"Code\"='072';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BV' WHERE \"NsiId\" = 28 AND \"Code\"='074';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BR' WHERE \"NsiId\" = 28 AND \"Code\"='076';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BZ' WHERE \"NsiId\" = 28 AND \"Code\"='084';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='IO' WHERE \"NsiId\" = 28 AND \"Code\"='086';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SB' WHERE \"NsiId\" = 28 AND \"Code\"='090';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='VG' WHERE \"NsiId\" = 28 AND \"Code\"='092';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BN' WHERE \"NsiId\" = 28 AND \"Code\"='096';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BG' WHERE \"NsiId\" = 28 AND \"Code\"='100';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MM' WHERE \"NsiId\" = 28 AND \"Code\"='104';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BI' WHERE \"NsiId\" = 28 AND \"Code\"='108';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BY' WHERE \"NsiId\" = 28 AND \"Code\"='112';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CM' WHERE \"NsiId\" = 28 AND \"Code\"='120';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CA' WHERE \"NsiId\" = 28 AND \"Code\"='124';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KY' WHERE \"NsiId\" = 28 AND \"Code\"='136';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TD' WHERE \"NsiId\" = 28 AND \"Code\"='148';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CL' WHERE \"NsiId\" = 28 AND \"Code\"='152';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CN' WHERE \"NsiId\" = 28 AND \"Code\"='156';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TW' WHERE \"NsiId\" = 28 AND \"Code\"='158';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CX' WHERE \"NsiId\" = 28 AND \"Code\"='162';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CC' WHERE \"NsiId\" = 28 AND \"Code\"='166';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CO' WHERE \"NsiId\" = 28 AND \"Code\"='170';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KM' WHERE \"NsiId\" = 28 AND \"Code\"='174';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='YT' WHERE \"NsiId\" = 28 AND \"Code\"='175';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CG' WHERE \"NsiId\" = 28 AND \"Code\"='178';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CD' WHERE \"NsiId\" = 28 AND \"Code\"='180';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CK' WHERE \"NsiId\" = 28 AND \"Code\"='184';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CR' WHERE \"NsiId\" = 28 AND \"Code\"='188';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='HR' WHERE \"NsiId\" = 28 AND \"Code\"='191';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CU' WHERE \"NsiId\" = 28 AND \"Code\"='192';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CY' WHERE \"NsiId\" = 28 AND \"Code\"='196';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CZ' WHERE \"NsiId\" = 28 AND \"Code\"='203';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='DK' WHERE \"NsiId\" = 28 AND \"Code\"='208';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='DM' WHERE \"NsiId\" = 28 AND \"Code\"='212';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='DO' WHERE \"NsiId\" = 28 AND \"Code\"='214';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SV' WHERE \"NsiId\" = 28 AND \"Code\"='222';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GQ' WHERE \"NsiId\" = 28 AND \"Code\"='226';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='ET' WHERE \"NsiId\" = 28 AND \"Code\"='231';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='ER' WHERE \"NsiId\" = 28 AND \"Code\"='232';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='EE' WHERE \"NsiId\" = 28 AND \"Code\"='233';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='FO' WHERE \"NsiId\" = 28 AND \"Code\"='234';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='FK' WHERE \"NsiId\" = 28 AND \"Code\"='238';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GS' WHERE \"NsiId\" = 28 AND \"Code\"='239';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='FJ' WHERE \"NsiId\" = 28 AND \"Code\"='242';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='FI' WHERE \"NsiId\" = 28 AND \"Code\"='246';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AX' WHERE \"NsiId\" = 28 AND \"Code\"='248';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='FR' WHERE \"NsiId\" = 28 AND \"Code\"='250';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GF' WHERE \"NsiId\" = 28 AND \"Code\"='254';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PF' WHERE \"NsiId\" = 28 AND \"Code\"='258';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TF' WHERE \"NsiId\" = 28 AND \"Code\"='260';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GA' WHERE \"NsiId\" = 28 AND \"Code\"='266';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GE' WHERE \"NsiId\" = 28 AND \"Code\"='268';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GM' WHERE \"NsiId\" = 28 AND \"Code\"='270';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PS' WHERE \"NsiId\" = 28 AND \"Code\"='275';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='DE' WHERE \"NsiId\" = 28 AND \"Code\"='276';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GH' WHERE \"NsiId\" = 28 AND \"Code\"='288';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GI' WHERE \"NsiId\" = 28 AND \"Code\"='292';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KI' WHERE \"NsiId\" = 28 AND \"Code\"='296';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GR' WHERE \"NsiId\" = 28 AND \"Code\"='300';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GL' WHERE \"NsiId\" = 28 AND \"Code\"='304';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GD' WHERE \"NsiId\" = 28 AND \"Code\"='308';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GP' WHERE \"NsiId\" = 28 AND \"Code\"='312';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GU' WHERE \"NsiId\" = 28 AND \"Code\"='316';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GT' WHERE \"NsiId\" = 28 AND \"Code\"='320';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GN' WHERE \"NsiId\" = 28 AND \"Code\"='324';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GY' WHERE \"NsiId\" = 28 AND \"Code\"='328';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='HT' WHERE \"NsiId\" = 28 AND \"Code\"='332';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='HM' WHERE \"NsiId\" = 28 AND \"Code\"='334';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='VA' WHERE \"NsiId\" = 28 AND \"Code\"='336';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='HN' WHERE \"NsiId\" = 28 AND \"Code\"='340';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='HK' WHERE \"NsiId\" = 28 AND \"Code\"='344';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='HU' WHERE \"NsiId\" = 28 AND \"Code\"='348';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='IS' WHERE \"NsiId\" = 28 AND \"Code\"='352';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='IN' WHERE \"NsiId\" = 28 AND \"Code\"='356';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='ID' WHERE \"NsiId\" = 28 AND \"Code\"='360';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='IR' WHERE \"NsiId\" = 28 AND \"Code\"='364';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='IQ' WHERE \"NsiId\" = 28 AND \"Code\"='368';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='IE' WHERE \"NsiId\" = 28 AND \"Code\"='372';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='IL' WHERE \"NsiId\" = 28 AND \"Code\"='376';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='IT' WHERE \"NsiId\" = 28 AND \"Code\"='380';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='JM' WHERE \"NsiId\" = 28 AND \"Code\"='388';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='JP' WHERE \"NsiId\" = 28 AND \"Code\"='392';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KZ' WHERE \"NsiId\" = 28 AND \"Code\"='398';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='JO' WHERE \"NsiId\" = 28 AND \"Code\"='400';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KE' WHERE \"NsiId\" = 28 AND \"Code\"='404';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KP' WHERE \"NsiId\" = 28 AND \"Code\"='408';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KR' WHERE \"NsiId\" = 28 AND \"Code\"='410';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KW' WHERE \"NsiId\" = 28 AND \"Code\"='414';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KG' WHERE \"NsiId\" = 28 AND \"Code\"='417';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LA' WHERE \"NsiId\" = 28 AND \"Code\"='418';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LB' WHERE \"NsiId\" = 28 AND \"Code\"='422';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LS' WHERE \"NsiId\" = 28 AND \"Code\"='426';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LV' WHERE \"NsiId\" = 28 AND \"Code\"='428';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LR' WHERE \"NsiId\" = 28 AND \"Code\"='430';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LY' WHERE \"NsiId\" = 28 AND \"Code\"='434';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LI' WHERE \"NsiId\" = 28 AND \"Code\"='438';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LT' WHERE \"NsiId\" = 28 AND \"Code\"='440';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LU' WHERE \"NsiId\" = 28 AND \"Code\"='442';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MO' WHERE \"NsiId\" = 28 AND \"Code\"='446';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MW' WHERE \"NsiId\" = 28 AND \"Code\"='454';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MV' WHERE \"NsiId\" = 28 AND \"Code\"='462';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='ML' WHERE \"NsiId\" = 28 AND \"Code\"='466';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MT' WHERE \"NsiId\" = 28 AND \"Code\"='470';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MQ' WHERE \"NsiId\" = 28 AND \"Code\"='474';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MR' WHERE \"NsiId\" = 28 AND \"Code\"='478';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MU' WHERE \"NsiId\" = 28 AND \"Code\"='480';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MX' WHERE \"NsiId\" = 28 AND \"Code\"='484';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MI' WHERE \"NsiId\" = 28 AND \"Code\"='488';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MC' WHERE \"NsiId\" = 28 AND \"Code\"='492';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MN' WHERE \"NsiId\" = 28 AND \"Code\"='496';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MD' WHERE \"NsiId\" = 28 AND \"Code\"='498';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='ME' WHERE \"NsiId\" = 28 AND \"Code\"='499';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MS' WHERE \"NsiId\" = 28 AND \"Code\"='500';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MA' WHERE \"NsiId\" = 28 AND \"Code\"='504';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='МZ' WHERE \"NsiId\" = 28 AND \"Code\"='508';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='OM' WHERE \"NsiId\" = 28 AND \"Code\"='512';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NA' WHERE \"NsiId\" = 28 AND \"Code\"='516';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NR' WHERE \"NsiId\" = 28 AND \"Code\"='520';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NP' WHERE \"NsiId\" = 28 AND \"Code\"='524';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NL' WHERE \"NsiId\" = 28 AND \"Code\"='528';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CW' WHERE \"NsiId\" = 28 AND \"Code\"='531';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AW' WHERE \"NsiId\" = 28 AND \"Code\"='533';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SX' WHERE \"NsiId\" = 28 AND \"Code\"='534';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BQ' WHERE \"NsiId\" = 28 AND \"Code\"='535';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NC' WHERE \"NsiId\" = 28 AND \"Code\"='540';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NZ' WHERE \"NsiId\" = 28 AND \"Code\"='554';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NI' WHERE \"NsiId\" = 28 AND \"Code\"='558';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NE' WHERE \"NsiId\" = 28 AND \"Code\"='562';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NG' WHERE \"NsiId\" = 28 AND \"Code\"='566';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NU' WHERE \"NsiId\" = 28 AND \"Code\"='570';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NF' WHERE \"NsiId\" = 28 AND \"Code\"='574';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='NO' WHERE \"NsiId\" = 28 AND \"Code\"='578';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MP' WHERE \"NsiId\" = 28 AND \"Code\"='580';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='UM' WHERE \"NsiId\" = 28 AND \"Code\"='581';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='FM' WHERE \"NsiId\" = 28 AND \"Code\"='583';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MH' WHERE \"NsiId\" = 28 AND \"Code\"='584';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PW' WHERE \"NsiId\" = 28 AND \"Code\"='585';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PK' WHERE \"NsiId\" = 28 AND \"Code\"='586';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PA' WHERE \"NsiId\" = 28 AND \"Code\"='591';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PG' WHERE \"NsiId\" = 28 AND \"Code\"='598';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PY' WHERE \"NsiId\" = 28 AND \"Code\"='600';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PE' WHERE \"NsiId\" = 28 AND \"Code\"='604';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PH' WHERE \"NsiId\" = 28 AND \"Code\"='608';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PN' WHERE \"NsiId\" = 28 AND \"Code\"='612';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PL' WHERE \"NsiId\" = 28 AND \"Code\"='616';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PT' WHERE \"NsiId\" = 28 AND \"Code\"='620';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GW' WHERE \"NsiId\" = 28 AND \"Code\"='624';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TL' WHERE \"NsiId\" = 28 AND \"Code\"='626';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PR' WHERE \"NsiId\" = 28 AND \"Code\"='630';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='QA' WHERE \"NsiId\" = 28 AND \"Code\"='634';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='RE' WHERE \"NsiId\" = 28 AND \"Code\"='638';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='RO' WHERE \"NsiId\" = 28 AND \"Code\"='642';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='RU' WHERE \"NsiId\" = 28 AND \"Code\"='643';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='RW' WHERE \"NsiId\" = 28 AND \"Code\"='646';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='BL' WHERE \"NsiId\" = 28 AND \"Code\"='652';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SH' WHERE \"NsiId\" = 28 AND \"Code\"='654';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='KN' WHERE \"NsiId\" = 28 AND \"Code\"='659';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AI' WHERE \"NsiId\" = 28 AND \"Code\"='660';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='LC' WHERE \"NsiId\" = 28 AND \"Code\"='662';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MF' WHERE \"NsiId\" = 28 AND \"Code\"='663';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='PM' WHERE \"NsiId\" = 28 AND \"Code\"='666';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='VC' WHERE \"NsiId\" = 28 AND \"Code\"='670';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SM' WHERE \"NsiId\" = 28 AND \"Code\"='674';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='ST' WHERE \"NsiId\" = 28 AND \"Code\"='678';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SN' WHERE \"NsiId\" = 28 AND \"Code\"='686';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='RS' WHERE \"NsiId\" = 28 AND \"Code\"='688';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SC' WHERE \"NsiId\" = 28 AND \"Code\"='690';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SL' WHERE \"NsiId\" = 28 AND \"Code\"='694';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SG' WHERE \"NsiId\" = 28 AND \"Code\"='702';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SK' WHERE \"NsiId\" = 28 AND \"Code\"='703';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SI' WHERE \"NsiId\" = 28 AND \"Code\"='705';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SO' WHERE \"NsiId\" = 28 AND \"Code\"='706';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='ZA' WHERE \"NsiId\" = 28 AND \"Code\"='710';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='ES' WHERE \"NsiId\" = 28 AND \"Code\"='724';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SS' WHERE \"NsiId\" = 28 AND \"Code\"='728';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SD' WHERE \"NsiId\" = 28 AND \"Code\"='729';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='EH' WHERE \"NsiId\" = 28 AND \"Code\"='732';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SR' WHERE \"NsiId\" = 28 AND \"Code\"='740';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SJ' WHERE \"NsiId\" = 28 AND \"Code\"='744';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SZ' WHERE \"NsiId\" = 28 AND \"Code\"='748';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SE' WHERE \"NsiId\" = 28 AND \"Code\"='752';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='CH' WHERE \"NsiId\" = 28 AND \"Code\"='756';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='SY' WHERE \"NsiId\" = 28 AND \"Code\"='760';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TJ' WHERE \"NsiId\" = 28 AND \"Code\"='762';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TH' WHERE \"NsiId\" = 28 AND \"Code\"='764';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TG' WHERE \"NsiId\" = 28 AND \"Code\"='768';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TK' WHERE \"NsiId\" = 28 AND \"Code\"='772';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TO' WHERE \"NsiId\" = 28 AND \"Code\"='776';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TT' WHERE \"NsiId\" = 28 AND \"Code\"='780';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AE' WHERE \"NsiId\" = 28 AND \"Code\"='784';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TN' WHERE \"NsiId\" = 28 AND \"Code\"='788';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TR' WHERE \"NsiId\" = 28 AND \"Code\"='792';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TM' WHERE \"NsiId\" = 28 AND \"Code\"='795';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TC' WHERE \"NsiId\" = 28 AND \"Code\"='796';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='TV' WHERE \"NsiId\" = 28 AND \"Code\"='798';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='UG' WHERE \"NsiId\" = 28 AND \"Code\"='800';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='UA' WHERE \"NsiId\" = 28 AND \"Code\"='804';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='MK' WHERE \"NsiId\" = 28 AND \"Code\"='807';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='EG' WHERE \"NsiId\" = 28 AND \"Code\"='818';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GB' WHERE \"NsiId\" = 28 AND \"Code\"='826';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='GG' WHERE \"NsiId\" = 28 AND \"Code\"='831';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='JE' WHERE \"NsiId\" = 28 AND \"Code\"='832';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='IM' WHERE \"NsiId\" = 28 AND \"Code\"='833';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='US' WHERE \"NsiId\" = 28 AND \"Code\"='840';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='VI' WHERE \"NsiId\" = 28 AND \"Code\"='850';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='UY' WHERE \"NsiId\" = 28 AND \"Code\"='858';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='UZ' WHERE \"NsiId\" = 28 AND \"Code\"='860';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='VE' WHERE \"NsiId\" = 28 AND \"Code\"='862';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='WF' WHERE \"NsiId\" = 28 AND \"Code\"='876';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='WS' WHERE \"NsiId\" = 28 AND \"Code\"='882';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='YE' WHERE \"NsiId\" = 28 AND \"Code\"='887';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='ZM' WHERE \"NsiId\" = 28 AND \"Code\"='894';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='AB' WHERE \"NsiId\" = 28 AND \"Code\"='895';");
            migrationBuilder.Sql("UPDATE public.\"NsiValues\" SET \"Code\"='OS' WHERE \"NsiId\" = 28 AND \"Code\"='896';");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FDistinctOrCity",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FOkrugOrCity",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FRegionOrCity",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FVillage",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JDistinctOrCity",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JOkrugOrCity",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JRegionOrCity",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JVillage",
                table: "Organizations");
        }
    }
}

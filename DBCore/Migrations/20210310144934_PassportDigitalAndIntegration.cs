﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class PassportDigitalAndIntegration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EncumbranceTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EncumbranceTypes", x => x.Id);
                    table.UniqueConstraint("Unique_EncumbranceTypes_code", x=>x.Code);
                });
            migrationBuilder.CreateTable(
                name: "KgdDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OperationType = table.Column<string>(nullable: true),
                    OperationDate = table.Column<DateTime>(nullable: false),
                    ManufactureCountry = table.Column<string>(nullable: true),
                    ImportExportCountry = table.Column<string>(nullable: true),
                    CustomPoint = table.Column<string>(nullable: true),
                    Mode = table.Column<string>(nullable: true),
                    ImporterCategory = table.Column<short>(nullable: false),
                    ImporterIinBin = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    Patronymic = table.Column<string>(nullable: true),
                    VinExist = table.Column<short>(nullable: false),
                    VinCode = table.Column<string>(nullable: true),
                    ChassisExist = table.Column<short>(nullable: false),
                    ChassisNumber = table.Column<string>(nullable: true),
                    KuzovExist = table.Column<short>(nullable: false),
                    KuzovNumber = table.Column<string>(nullable: true),
                    SelfMoveKuzov = table.Column<short>(nullable: false),
                    SelfMoveKuzovNumber = table.Column<string>(nullable: true),
                    UnitMeter = table.Column<string>(nullable: true),
                    TsMassType = table.Column<string>(nullable: true),
                    TsMass = table.Column<double>(nullable: true),
                    TnvedCode = table.Column<string>(nullable: true),
                    DeclarationExist = table.Column<short>(nullable: false),
                    DeclarationSerieNumber = table.Column<string>(nullable: true),
                    CustomReceiptExist = table.Column<short>(nullable: false),
                    ReceiptSerieNumber = table.Column<string>(nullable: true),
                    CustomLimitExist = table.Column<short>(nullable: false),
                    CustomLimitDescription = table.Column<string>(nullable: true),
                    CustomLimitBegin = table.Column<DateTime>(nullable: true),
                    CustomLimitEnd = table.Column<DateTime>(nullable: true),
                    MessageDate = table.Column<DateTime>(nullable: false),
                    MessageId = table.Column<string>(nullable: true),
                    SessionId = table.Column<string>(nullable: true),
                    CorrelationId = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: true),
                    CustomDocName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KgdDatas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MvdDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OperationType = table.Column<string>(nullable: true),
                    OperationDate = table.Column<DateTime>(nullable: false),
                    DocDescriptionBase = table.Column<string>(nullable: true),
                    RegisterRemoveReason = table.Column<string>(nullable: true),
                    RegionCode = table.Column<string>(nullable: true),
                    RegionName = table.Column<string>(nullable: true),
                    DigitalPassportId = table.Column<string>(nullable: true),
                    Grnz = table.Column<string>(nullable: true),
                    SrtsNumber = table.Column<string>(nullable: true),
                    VinCode = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: true),
                    Odometer = table.Column<int>(nullable: true),
                    SignTsChange = table.Column<short>(nullable: false),
                    DescriptionTsChange = table.Column<string>(nullable: true),
                    OwnerCategory = table.Column<short>(nullable: false),
                    OwnerIinBin = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    Patronymic = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: true),
                    OwnerDocType = table.Column<string>(nullable: true),
                    OwnerDocNumber = table.Column<string>(nullable: true),
                    OwnerDocDate = table.Column<DateTime>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    TsLimitDescription = table.Column<string>(nullable: true),
                    AllRoadMoveAbility = table.Column<string>(nullable: true),
                    MinibusAbility = table.Column<string>(nullable: true),
                    MessageDate = table.Column<DateTime>(nullable: false),
                    MessageId = table.Column<string>(nullable: true),
                    SessionId = table.Column<string>(nullable: true),
                    CorrelationId = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MvdDatas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DigitalPassport",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EDocId = table.Column<Guid>(nullable: false),
                    VehicleEPassportKindCode = table.Column<string>(nullable: true),
                    VehicleEPassportId = table.Column<string>(nullable: true),
                    VehicleEPassportStatusCode = table.Column<string>(nullable: true),
                    EventDate = table.Column<DateTime>(nullable: false),
                    VehicleEPassportBaseCode = table.Column<string>(nullable: true),
                    VehicleIdInfoDetails = table.Column<string>(nullable: true),
                    VehicleIdDetails = table.Column<string>(nullable: true),
                    VehicleIdentityNumberId = table.Column<string>(nullable: true),
                    NotVehicleIdentityNumberIndicator = table.Column<byte>(nullable: false),
                    VehicleEngineIdDetails = table.Column<string>(nullable: true),
                    VehicleEngineIdentityNumberId = table.Column<string>(nullable: true),
                    NotVehicleEngineIdentityNumberIndicator = table.Column<byte>(nullable: false),
                    VehicleFrameIdDetails = table.Column<string>(nullable: true),
                    VehicleFrameIdentityNumberId = table.Column<string>(nullable: true),
                    NotVehicleFrameIdentityNumberIndicator = table.Column<byte>(nullable: false),
                    VehicleBodyIdDetails = table.Column<string>(nullable: true),
                    VehicleBodyIdentityNumberId = table.Column<string>(nullable: true),
                    NotVehicleBodyIdentityNumberIndicator = table.Column<byte>(nullable: false),
                    VehicleEmergencyCallDeviceIdDetails = table.Column<string>(nullable: true),
                    VehicleEmergencyCallIdentityNumberId = table.Column<string>(nullable: true),
                    NotVehicleEmergencyCallIdentityNumberIndicator = table.Column<byte>(nullable: false),
                    VehicleCategoryCode = table.Column<string>(nullable: true),
                    VehicleBodyColourCode = table.Column<string>(nullable: true),
                    BodyMultiColourIndicator = table.Column<byte>(nullable: false),
                    VehicleBodyColourName = table.Column<string>(nullable: true),
                    ManufactureYear = table.Column<int>(nullable: false),
                    ManufactureMonth = table.Column<string>(nullable: true),
                    DocExistIndicator = table.Column<byte>(nullable: false),
                    DocId = table.Column<string>(nullable: true),
                    HandyVehicleEPassportId = table.Column<string>(nullable: true),
                    DocCreationDate = table.Column<DateTime>(nullable: false),
                    EngineIdentificationNumberLocation = table.Column<string>(nullable: true),
                    EngineIdCharacterDetails = table.Column<string>(nullable: true),
                    IdCharacterStartingOrdinal = table.Column<byte>(nullable: false),
                    IdCharacterQuantity = table.Column<byte>(nullable: false),
                    IdCharacterText = table.Column<string>(nullable: true),
                    IdCharacterValueDetails = table.Column<string>(nullable: true),
                    IdCharacterValueCode = table.Column<string>(nullable: true),
                    IdCharacterValueText = table.Column<string>(nullable: true),
                    PreferentialManufacturingModeText = table.Column<string>(nullable: true),
                    RegistrationTerritory = table.Column<string>(nullable: true),
                    RegistrationTerritoryIndicator = table.Column<byte>(nullable: false),
                    PreferentialManufacturingCountryCode = table.Column<string>(nullable: true),
                    PreferentialManufacturingRegionCode = table.Column<byte>(nullable: false),
                    VehicleSatelliteNavigationIdDetails = table.Column<string>(nullable: true),
                    SatelliteNumberId = table.Column<string>(nullable: true),
                    SatelliteIndicator = table.Column<byte>(nullable: false),
                    VehicleTachographIdDetails = table.Column<string>(nullable: true),
                    TachographNumberId = table.Column<string>(nullable: true),
                    TachographIndicator = table.Column<byte>(nullable: false),
                    VehicleEPassportBaseDetails = table.Column<string>(nullable: true),
                    VehicleImportCountryCode = table.Column<string>(nullable: true),
                    UnifiedCountryCode = table.Column<string>(nullable: true),
                    BusinessEntityName = table.Column<string>(nullable: true),
                    BusinessEntityId = table.Column<string>(nullable: true),
                    BusinessEntityIdKindId = table.Column<string>(nullable: true),
                    CommonDocCreationDate = table.Column<DateTime>(nullable: false),
                    VehicleEPassportRegistrationReasonCode = table.Column<int>(nullable: false),
                    OwnerDocDetails = table.Column<string>(nullable: true),
                    NoSafetyUnifiedCountryCode = table.Column<string>(nullable: true),
                    NoSafetyDocKindCode = table.Column<string>(nullable: true),
                    NoSafetyDocKindName = table.Column<string>(nullable: true),
                    NoSafetyDocName = table.Column<string>(nullable: true),
                    NoSafetyDocId = table.Column<string>(nullable: true),
                    NoSafetyDocCreationDate = table.Column<DateTime>(nullable: false),
                    NoSafetyAuthorityName = table.Column<string>(nullable: true),
                    NoSafetyPageQuantity = table.Column<short>(nullable: false),
                    NoteText = table.Column<string>(nullable: true),
                    KgdDataId = table.Column<int>(nullable: false),
                    MvdDataId = table.Column<int>(nullable: false),
                    ComplienceDocumentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalPassport", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalPassport_ComplienceDocuments_ComplienceDocumentId",
                        column: x => x.ComplienceDocumentId,
                        principalTable: "ComplienceDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalPassport_KgdDatas_KgdDataId",
                        column: x => x.KgdDataId,
                        principalTable: "KgdDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalPassport_MvdDatas_MvdDataId",
                        column: x => x.MvdDataId,
                        principalTable: "MvdDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Encumbrances",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MvdDataId = table.Column<int>(nullable: true),
                    EncumbranceIndicator = table.Column<short>(nullable: true),
                    EncumbranceTypeCode = table.Column<string>(nullable: true),
                    EncumbranceDate = table.Column<DateTime>(nullable: true),
                    EncumbranceOrgName = table.Column<string>(nullable: true),
                    EncumbranceDocName = table.Column<string>(nullable: true),
                    EncumbranceDocDate = table.Column<DateTime>(nullable: true),
                    EncumbranceBegin = table.Column<DateTime>(nullable: true),
                    EncumbranceEnd = table.Column<DateTime>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Encumbrances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Encumbrances_EncumbranceTypes_EncumbranceTypeNavigationId",
                        column: x => x.EncumbranceTypeCode,
                        principalTable: "EncumbranceTypes",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Encumbrances_MvdDatas_MvdDataId",
                        column: x => x.MvdDataId,
                        principalTable: "MvdDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassport_ComplienceDocumentId",
                table: "DigitalPassport",
                column: "ComplienceDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassport_KgdDataId",
                table: "DigitalPassport",
                column: "KgdDataId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassport_MvdDataId",
                table: "DigitalPassport",
                column: "MvdDataId");

            migrationBuilder.CreateIndex(
                name: "IX_Encumbrances_EncumbranceTypeNavigationId",
                table: "Encumbrances",
                column: "EncumbranceTypeCode");

            migrationBuilder.CreateIndex(
                name: "IX_Encumbrances_MvdDataId",
                table: "Encumbrances",
                column: "MvdDataId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DigitalPassport");

            migrationBuilder.DropTable(
                name: "Encumbrances");

            migrationBuilder.DropTable(
                name: "KgdDatas");

            migrationBuilder.DropTable(
                name: "EncumbranceTypes");

            migrationBuilder.DropTable(
                name: "MvdDatas");
        }
    }
}

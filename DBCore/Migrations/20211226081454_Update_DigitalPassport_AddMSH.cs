﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class Update_DigitalPassport_AddMSH : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExternalInteractHistory_DigitalPassportDocument_DigitalPass~",
                table: "ExternalInteractHistory");

            migrationBuilder.AlterColumn<int>(
                name: "DigitalPassportId",
                table: "ExternalInteractHistory",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "MshDataId",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_MshDataId",
                table: "DigitalPassportDocument",
                column: "MshDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_MshDatas_MshDataId",
                table: "DigitalPassportDocument",
                column: "MshDataId",
                principalTable: "MshDatas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExternalInteractHistory_DigitalPassportDocument_DigitalPass~",
                table: "ExternalInteractHistory",
                column: "DigitalPassportId",
                principalTable: "DigitalPassportDocument",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_MshDatas_MshDataId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropForeignKey(
                name: "FK_ExternalInteractHistory_DigitalPassportDocument_DigitalPass~",
                table: "ExternalInteractHistory");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_MshDataId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "MshDataId",
                table: "DigitalPassportDocument");

            migrationBuilder.AlterColumn<int>(
                name: "DigitalPassportId",
                table: "ExternalInteractHistory",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ExternalInteractHistory_DigitalPassportDocument_DigitalPass~",
                table: "ExternalInteractHistory",
                column: "DigitalPassportId",
                principalTable: "DigitalPassportDocument",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

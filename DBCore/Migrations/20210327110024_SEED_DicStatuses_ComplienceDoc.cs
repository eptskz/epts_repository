﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DBCore.Migrations
{
    public partial class SEED_DicStatuses_ComplienceDoc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleVinCharacterDetail_NsiValues_DataTypeId",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.AlterColumn<int>(
                name: "DataTypeId",
                table: "VehicleVinCharacterDetail",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "WheelQuantity",
                table: "NsiValues",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "PowerWheelQuantity",
                table: "NsiValues",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleVinCharacterDetail_NsiValues_DataTypeId",
                table: "VehicleVinCharacterDetail",
                column: "DataTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "consideration", "На рассмотрении", "На рассмотрении", "On consideration", DateTime.Now, "compliencedoc" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "active", "Действующий", "Действующий", "Active", DateTime.Now, "compliencedoc" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "rejected", "Отклоненный", "Отклоненный", "Rejected", DateTime.Now, "compliencedoc" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "withdrawn", "Отозванный", "Отозванный", "Withdrawn", DateTime.Now, "compliencedoc" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "overdue", "Просроченные", "Отозванный", "Overdue", DateTime.Now, "compliencedoc" }
            );

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleVinCharacterDetail_NsiValues_DataTypeId",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.AlterColumn<int>(
                name: "DataTypeId",
                table: "VehicleVinCharacterDetail",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "WheelQuantity",
                table: "NsiValues",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PowerWheelQuantity",
                table: "NsiValues",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleVinCharacterDetail_NsiValues_DataTypeId",
                table: "VehicleVinCharacterDetail",
                column: "DataTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_PowerStorageDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PowerStorageDeviceDetail_ElectricalMachineDetails_Electrica~",
                table: "PowerStorageDeviceDetail");

            migrationBuilder.AlterColumn<int>(
                name: "ElectricalMachineDetailId",
                table: "PowerStorageDeviceDetail",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicId",
                table: "PowerStorageDeviceDetail",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NotPowerStorageDeviceDetailIndicator",
                table: "CharacteristicsDetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_PowerStorageDeviceDetail_CharacteristicId",
                table: "PowerStorageDeviceDetail",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_PowerStorageDeviceDetail_CharacteristicsDetail_Characterist~",
                table: "PowerStorageDeviceDetail",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PowerStorageDeviceDetail_ElectricalMachineDetails_Electrica~",
                table: "PowerStorageDeviceDetail",
                column: "ElectricalMachineDetailId",
                principalTable: "ElectricalMachineDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PowerStorageDeviceDetail_CharacteristicsDetail_Characterist~",
                table: "PowerStorageDeviceDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_PowerStorageDeviceDetail_ElectricalMachineDetails_Electrica~",
                table: "PowerStorageDeviceDetail");

            migrationBuilder.DropIndex(
                name: "IX_PowerStorageDeviceDetail_CharacteristicId",
                table: "PowerStorageDeviceDetail");

            migrationBuilder.DropColumn(
                name: "CharacteristicId",
                table: "PowerStorageDeviceDetail");

            migrationBuilder.DropColumn(
                name: "NotPowerStorageDeviceDetailIndicator",
                table: "CharacteristicsDetail");

            migrationBuilder.AlterColumn<int>(
                name: "ElectricalMachineDetailId",
                table: "PowerStorageDeviceDetail",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PowerStorageDeviceDetail_ElectricalMachineDetails_Electrica~",
                table: "PowerStorageDeviceDetail",
                column: "ElectricalMachineDetailId",
                principalTable: "ElectricalMachineDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

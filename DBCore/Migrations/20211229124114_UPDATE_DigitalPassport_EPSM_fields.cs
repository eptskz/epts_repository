﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class UPDATE_DigitalPassport_EPSM_fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BaseAxleNumber",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GearNumber",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MoverEpsm",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MoverEpsmNsiId",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NotBaseAxleNumberIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NotGearNumberIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_MoverEpsmNsiId",
                table: "DigitalPassportDocument",
                column: "MoverEpsmNsiId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_NsiValues_MoverEpsmNsiId",
                table: "DigitalPassportDocument",
                column: "MoverEpsmNsiId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_NsiValues_MoverEpsmNsiId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_MoverEpsmNsiId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "BaseAxleNumber",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "GearNumber",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "MoverEpsm",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "MoverEpsmNsiId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "NotBaseAxleNumberIndicator",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "NotGearNumberIndicator",
                table: "DigitalPassportDocument");
        }
    }
}

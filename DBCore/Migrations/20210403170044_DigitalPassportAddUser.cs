﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class DigitalPassportAddUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AspNetUserId",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_AspNetUserId",
                table: "DigitalPassportDocument",
                column: "AspNetUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_AspNetUsers_AspNetUserId",
                table: "DigitalPassportDocument",
                column: "AspNetUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_AspNetUsers_AspNetUserId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_AspNetUserId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "AspNetUserId",
                table: "DigitalPassportDocument");
        }
    }
}

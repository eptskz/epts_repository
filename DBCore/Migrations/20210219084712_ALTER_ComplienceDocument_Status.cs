﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_ComplienceDocument_Status : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "ComplienceDocuments",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_StatusId",
                table: "ComplienceDocuments",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_ComplienceDocuments_DicStatuses_StatusId",
                table: "ComplienceDocuments",
                column: "StatusId",
                principalTable: "DicStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComplienceDocuments_DicStatuses_StatusId",
                table: "ComplienceDocuments");

            migrationBuilder.DropIndex(
                name: "IX_ComplienceDocuments_StatusId",
                table: "ComplienceDocuments");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "ComplienceDocuments");
        }
    }
}

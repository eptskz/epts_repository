﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_FileStorageItems_add_title : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "FileStorageItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Title",
                table: "FileStorageItems");
        }
    }
}

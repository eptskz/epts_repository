﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehiclePurpose_OtherInfoId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehiclePurposes_NsiValues_OtherInfoId",
                table: "VehiclePurposes");

            migrationBuilder.AlterColumn<int>(
                name: "OtherInfoId",
                table: "VehiclePurposes",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_VehiclePurposes_NsiValues_OtherInfoId",
                table: "VehiclePurposes",
                column: "OtherInfoId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehiclePurposes_NsiValues_OtherInfoId",
                table: "VehiclePurposes");

            migrationBuilder.AlterColumn<int>(
                name: "OtherInfoId",
                table: "VehiclePurposes",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehiclePurposes_NsiValues_OtherInfoId",
                table: "VehiclePurposes",
                column: "OtherInfoId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ChangeApplicationFile_FIle_Set_String : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "FileObject",
                table: "ChangeApplicationFile",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "bytea",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "FileObject",
                table: "ChangeApplicationFile",
                type: "bytea",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

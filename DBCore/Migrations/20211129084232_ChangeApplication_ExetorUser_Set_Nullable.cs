﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ChangeApplication_ExetorUser_Set_Nullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChangeApplication_AspNetUsers_UserExecutorId",
                table: "ChangeApplication");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserExecutorId",
                table: "ChangeApplication",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddForeignKey(
                name: "FK_ChangeApplication_AspNetUsers_UserExecutorId",
                table: "ChangeApplication",
                column: "UserExecutorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChangeApplication_AspNetUsers_UserExecutorId",
                table: "ChangeApplication");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserExecutorId",
                table: "ChangeApplication",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ChangeApplication_AspNetUsers_UserExecutorId",
                table: "ChangeApplication",
                column: "UserExecutorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class CREATE_BlankNumbers_VinNumbers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BatchSmallSign",
                table: "ComplienceDocuments",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BlankNumbers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DocumentId = table.Column<Guid>(nullable: false),
                    Number = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlankNumbers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlankNumbers_ComplienceDocuments_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "ComplienceDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VinNumbers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DocumentId = table.Column<Guid>(nullable: false),
                    Number = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VinNumbers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VinNumbers_ComplienceDocuments_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "ComplienceDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlankNumbers_DocumentId",
                table: "BlankNumbers",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_VinNumbers_DocumentId",
                table: "VinNumbers",
                column: "DocumentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlankNumbers");

            migrationBuilder.DropTable(
                name: "VinNumbers");

            migrationBuilder.DropColumn(
                name: "BatchSmallSign",
                table: "ComplienceDocuments");
        }
    }
}

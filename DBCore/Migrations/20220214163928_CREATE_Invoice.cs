﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using System;

namespace DBCore.Migrations
{
    public partial class CREATE_Invoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Number = table.Column<string>(nullable: true),
                    PeriodMonths = table.Column<string[]>(type: "text[]", nullable: true),
                    DigitalPassportCount = table.Column<int>(nullable: false),
                    ChangeDigitalPassportCount = table.Column<int>(nullable: false),
                    Rate = table.Column<double>(nullable: false),
                    PayableAmount = table.Column<double>(nullable: true),
                    PaidAmount = table.Column<double>(nullable: true),
                    PaidDateTime = table.Column<DateTime>(nullable: true),
                    Fine = table.Column<double>(nullable: true),
                    AccrualTermFine = table.Column<int>(nullable: true),
                    PayableFine = table.Column<double>(nullable: true),
                    PaidFine = table.Column<double>(nullable: true),
                    PaidFineDateTime = table.Column<DateTime>(nullable: true),
                    Balance = table.Column<double>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<DateTime>(nullable: true),
                    ContractId = table.Column<Guid>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invoices_Contracts_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contracts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Invoices_DicStatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "DicStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_ContractId",
                table: "Invoices",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_StatusId",
                table: "Invoices",
                column: "StatusId");

            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "new", "Черновик", "Черновик", "Draft", DateTime.Now, "invoice" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "active", "Ожидает оплаты", "Ожидает оплаты", "Awaiting payment", DateTime.Now, "invoice" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "paid", "Оплачен", "Оплачен", "Paid", DateTime.Now, "invoice" }
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Invoices");
        }
    }
}

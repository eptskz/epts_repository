﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class CREATE_VehicleTypeDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VehicleTypeDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    NotVehicleCommercialNameIndicator = table.Column<int>(nullable: false),
                    CommercialName = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    DocumentId = table.Column<Guid>(nullable: false),
                    NotVehicleMakeNameIndicator = table.Column<int>(nullable: false),
                    NotVehicleTechCategoryIndicator = table.Column<int>(nullable: false),
                    NotVehicleClassCategoryIndicator = table.Column<int>(nullable: false),
                    NotVehicleEcoClassCategoryIndicator = table.Column<int>(nullable: false),
                    NotVehicleChassisDesignIndicator = table.Column<int>(nullable: false),
                    NoteText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypeDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTypeDetails_ComplienceDocuments_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "ComplienceDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTypeChassisDesigns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ChassisDesignId = table.Column<int>(nullable: false),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypeChassisDesigns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTypeChassisDesigns_NsiValues_ChassisDesignId",
                        column: x => x.ChassisDesignId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleTypeChassisDesigns_VehicleTypeDetails_VehicleTypeDet~",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTypeClassCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClassCategoryId = table.Column<int>(nullable: false),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypeClassCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTypeClassCategories_NsiValues_ClassCategoryId",
                        column: x => x.ClassCategoryId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleTypeClassCategories_VehicleTypeDetails_VehicleTypeDe~",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTypeEcoClasses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EcoClassId = table.Column<int>(nullable: false),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypeEcoClasses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTypeEcoClasses_NsiValues_EcoClassId",
                        column: x => x.EcoClassId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleTypeEcoClasses_VehicleTypeDetails_VehicleTypeDetailId",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTypeMakes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false),
                    MakeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypeMakes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTypeMakes_NsiValues_MakeId",
                        column: x => x.MakeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleTypeMakes_VehicleTypeDetails_VehicleTypeDetailId",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTypeModifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypeModifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTypeModifications_VehicleTypeDetails_VehicleTypeDeta~",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTypeTechCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CategoryId = table.Column<int>(nullable: false),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypeTechCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTypeTechCategories_NsiValues_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleTypeTechCategories_VehicleTypeDetails_VehicleTypeDet~",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeChassisDesigns_ChassisDesignId",
                table: "VehicleTypeChassisDesigns",
                column: "ChassisDesignId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeChassisDesigns_VehicleTypeDetailId",
                table: "VehicleTypeChassisDesigns",
                column: "VehicleTypeDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeClassCategories_ClassCategoryId",
                table: "VehicleTypeClassCategories",
                column: "ClassCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeClassCategories_VehicleTypeDetailId",
                table: "VehicleTypeClassCategories",
                column: "VehicleTypeDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeDetails_DocumentId",
                table: "VehicleTypeDetails",
                column: "DocumentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeEcoClasses_EcoClassId",
                table: "VehicleTypeEcoClasses",
                column: "EcoClassId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeEcoClasses_VehicleTypeDetailId",
                table: "VehicleTypeEcoClasses",
                column: "VehicleTypeDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeMakes_MakeId",
                table: "VehicleTypeMakes",
                column: "MakeId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeMakes_VehicleTypeDetailId",
                table: "VehicleTypeMakes",
                column: "VehicleTypeDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeModifications_VehicleTypeDetailId",
                table: "VehicleTypeModifications",
                column: "VehicleTypeDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeTechCategories_CategoryId",
                table: "VehicleTypeTechCategories",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypeTechCategories_VehicleTypeDetailId",
                table: "VehicleTypeTechCategories",
                column: "VehicleTypeDetailId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleTypeChassisDesigns");

            migrationBuilder.DropTable(
                name: "VehicleTypeClassCategories");

            migrationBuilder.DropTable(
                name: "VehicleTypeEcoClasses");

            migrationBuilder.DropTable(
                name: "VehicleTypeMakes");

            migrationBuilder.DropTable(
                name: "VehicleTypeModifications");

            migrationBuilder.DropTable(
                name: "VehicleTypeTechCategories");

            migrationBuilder.DropTable(
                name: "VehicleTypeDetails");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class DigitalPassportAddIndicators : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "NotManufacturerPlateLocationIndicator",
                table: "ManufacturerPlateDetail",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "BaseVehicleCommercialName",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BaseVehicleMakeName",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BaseVehicleTypeId",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BaseVehicleTypeVariantId",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NotBaseVehicleIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NotSateliteNavigationIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NotVehicleCommercialNameIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NotVehicleMakeNameIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NotVehicleOfJobControllIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "VehicleMovementPermitIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "notVehicleModificationIndicator",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NotManufacturerPlateLocationIndicator",
                table: "ManufacturerPlateDetail");

            migrationBuilder.DropColumn(
                name: "BaseVehicleCommercialName",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "BaseVehicleMakeName",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "BaseVehicleTypeId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "BaseVehicleTypeVariantId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "NotBaseVehicleIndicator",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "NotSateliteNavigationIndicator",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "NotVehicleCommercialNameIndicator",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "NotVehicleMakeNameIndicator",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "NotVehicleOfJobControllIndicator",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "VehicleMovementPermitIndicator",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "notVehicleModificationIndicator",
                table: "DigitalPassportDocument");
        }
    }
}

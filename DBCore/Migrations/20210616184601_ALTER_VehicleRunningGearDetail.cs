﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleRunningGearDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_PoweredWheelLocationId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleRunningGearDetails_PoweredWheelLocationId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.DropColumn(
                name: "PoweredWheelLocationId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.CreateTable(
                name: "PoweredWheelLocation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    WheelLocationId = table.Column<int>(nullable: false),
                    VehicleRunningGearDetailId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PoweredWheelLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PoweredWheelLocation_VehicleRunningGearDetails_VehicleRunni~",
                        column: x => x.VehicleRunningGearDetailId,
                        principalTable: "VehicleRunningGearDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PoweredWheelLocation_NsiValues_WheelLocationId",
                        column: x => x.WheelLocationId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PoweredWheelLocation_VehicleRunningGearDetailId",
                table: "PoweredWheelLocation",
                column: "VehicleRunningGearDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_PoweredWheelLocation_WheelLocationId",
                table: "PoweredWheelLocation",
                column: "WheelLocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PoweredWheelLocation");

            migrationBuilder.AddColumn<int>(
                name: "PoweredWheelLocationId",
                table: "VehicleRunningGearDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRunningGearDetails_PoweredWheelLocationId",
                table: "VehicleRunningGearDetails",
                column: "PoweredWheelLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_PoweredWheelLocationId",
                table: "VehicleRunningGearDetails",
                column: "PoweredWheelLocationId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Organization_FactCountryId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FactCountryId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_FactCountryId",
                table: "Organizations",
                column: "FactCountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_NsiValues_FactCountryId",
                table: "Organizations",
                column: "FactCountryId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_NsiValues_FactCountryId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_FactCountryId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FactCountryId",
                table: "Organizations");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleTyreKind_CharacteristicsDetailId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleTyreKindInfos_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleTyreKindInfos");

            migrationBuilder.DropColumn(
                name: "CharacteristicId",
                table: "VehicleTyreKindInfos");

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleTyreKindInfos",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleTyreKindInfos_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleTyreKindInfos",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleTyreKindInfos_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleTyreKindInfos");

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleTyreKindInfos",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicId",
                table: "VehicleTyreKindInfos",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleTyreKindInfos_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleTyreKindInfos",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

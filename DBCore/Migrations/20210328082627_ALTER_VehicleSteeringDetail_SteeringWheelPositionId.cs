﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleSteeringDetail_SteeringWheelPositionId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SteeringWheelPositionId",
                table: "VehicleSteeringDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSteeringDetails_SteeringWheelPositionId",
                table: "VehicleSteeringDetails",
                column: "SteeringWheelPositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleSteeringDetails_NsiValues_SteeringWheelPositionId",
                table: "VehicleSteeringDetails",
                column: "SteeringWheelPositionId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleSteeringDetails_NsiValues_SteeringWheelPositionId",
                table: "VehicleSteeringDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleSteeringDetails_SteeringWheelPositionId",
                table: "VehicleSteeringDetails");

            migrationBuilder.DropColumn(
                name: "SteeringWheelPositionId",
                table: "VehicleSteeringDetails");
        }
    }
}

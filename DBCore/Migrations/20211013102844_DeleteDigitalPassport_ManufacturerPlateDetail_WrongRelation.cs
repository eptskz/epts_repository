﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class DeleteDigitalPassport_ManufacturerPlateDetail_WrongRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_NsiValues_AcceptCountryRegistration~",
                table: "DigitalPassportDocument");

            migrationBuilder.DropForeignKey(
                name: "FK_ManufacturerPlateDetail_DigitalPassportDocument_DigitalPass~",
                table: "ManufacturerPlateDetail");

            migrationBuilder.DropIndex(
                name: "IX_ManufacturerPlateDetail_DigitalPassportId",
                table: "ManufacturerPlateDetail");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_AcceptCountryRegistrationNsiId",
                table: "DigitalPassportDocument");


            migrationBuilder.DropColumn(
                name: "DigitalPassportId",
                table: "ManufacturerPlateDetail");

            migrationBuilder.DropColumn(
                name: "AcceptCountryRegistration",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "AcceptCountryRegistrationNsiId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "AcceptRegionRegistrationId",
                table: "DigitalPassportDocument");


            migrationBuilder.AlterColumn<int>(
                name: "ManufacturerPlateDetailId",
                table: "DigitalPassportDocument",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EnginePlateDetailId",
                table: "DigitalPassportDocument",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_ManufacturerPlateDetailId",
                table: "DigitalPassportDocument",
                column: "ManufacturerPlateDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_ManufacturerPlateDetail_Manufacture~",
                table: "DigitalPassportDocument",
                column: "ManufacturerPlateDetailId",
                principalTable: "ManufacturerPlateDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_ManufacturerPlateDetail_Manufacture~",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_ManufacturerPlateDetailId",
                table: "DigitalPassportDocument");

            migrationBuilder.AddColumn<int>(
                name: "DigitalPassportId",
                table: "ManufacturerPlateDetail",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "ManufacturerPlateDetailId",
                table: "DigitalPassportDocument",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EnginePlateDetailId",
                table: "DigitalPassportDocument",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "AcceptCountryRegistration",
                table: "DigitalPassportDocument",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AcceptCountryRegistrationNsiId",
                table: "DigitalPassportDocument",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AcceptRegionRegistrationId",
                table: "DigitalPassportDocument",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ManufacturerPlateDetail_DigitalPassportId",
                table: "ManufacturerPlateDetail",
                column: "DigitalPassportId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_AcceptCountryRegistrationNsiId",
                table: "DigitalPassportDocument",
                column: "AcceptCountryRegistrationNsiId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_NsiValues_AcceptCountryRegistration~",
                table: "DigitalPassportDocument",
                column: "AcceptCountryRegistrationNsiId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ManufacturerPlateDetail_DigitalPassportDocument_DigitalPass~",
                table: "ManufacturerPlateDetail",
                column: "DigitalPassportId",
                principalTable: "DigitalPassportDocument",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            
        }
    }
}

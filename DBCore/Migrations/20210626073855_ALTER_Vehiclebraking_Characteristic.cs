﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Vehiclebraking_Characteristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update public.\"VehicleBrakingSystemDetails\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.Sql("update public.\"VehicleSteeringDetails\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleBrakingSystemDetails_CharacteristicsDetail_Character~",
                table: "VehicleBrakingSystemDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleSteeringDetails_CharacteristicsDetail_Characteristic~",
                table: "VehicleSteeringDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleSteeringDetails_CharacteristicsDetailId",
                table: "VehicleSteeringDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleBrakingSystemDetails_CharacteristicsDetailId",
                table: "VehicleBrakingSystemDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleSteeringDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleBrakingSystemDetails");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSteeringDetails_CharacteristicId",
                table: "VehicleSteeringDetails",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleBrakingSystemDetails_CharacteristicId",
                table: "VehicleBrakingSystemDetails",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleBrakingSystemDetails_CharacteristicsDetail_Character~",
                table: "VehicleBrakingSystemDetails",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleSteeringDetails_CharacteristicsDetail_Characteristic~",
                table: "VehicleSteeringDetails",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleBrakingSystemDetails_CharacteristicsDetail_Character~",
                table: "VehicleBrakingSystemDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleSteeringDetails_CharacteristicsDetail_Characteristic~",
                table: "VehicleSteeringDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleSteeringDetails_CharacteristicId",
                table: "VehicleSteeringDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleBrakingSystemDetails_CharacteristicId",
                table: "VehicleBrakingSystemDetails");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleSteeringDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleBrakingSystemDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSteeringDetails_CharacteristicsDetailId",
                table: "VehicleSteeringDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleBrakingSystemDetails_CharacteristicsDetailId",
                table: "VehicleBrakingSystemDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleBrakingSystemDetails_CharacteristicsDetail_Character~",
                table: "VehicleBrakingSystemDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleSteeringDetails_CharacteristicsDetail_Characteristic~",
                table: "VehicleSteeringDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

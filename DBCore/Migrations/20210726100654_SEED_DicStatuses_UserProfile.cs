﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class SEED_DicStatuses_UserProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string[] columns = new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" };

            migrationBuilder.InsertData("DicStatuses"
                , columns
                , new object[] { "new", "Новый", "Новый", "Новый", DateTime.Now, "user" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , columns
                , new object[] { "active", "Действующий", "Действующий", "Действующий", DateTime.Now, "user" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , columns
                , new object[] { "locked", "Заблокирован", "Заблокирован", "Заблокирован", DateTime.Now, "user" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , columns
                , new object[] { "deleted", "Удален", "Удален", "Удален", DateTime.Now, "user" }
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}

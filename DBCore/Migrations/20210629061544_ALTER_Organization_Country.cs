﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Organization_Country : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LegalFormId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_CountryId",
                table: "Organizations",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_LegalFormId",
                table: "Organizations",
                column: "LegalFormId");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_NsiValues_CountryId",
                table: "Organizations",
                column: "CountryId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_NsiValues_LegalFormId",
                table: "Organizations",
                column: "LegalFormId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_NsiValues_CountryId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_NsiValues_LegalFormId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_CountryId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_LegalFormId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "LegalFormId",
                table: "Organizations");
        }
    }
}

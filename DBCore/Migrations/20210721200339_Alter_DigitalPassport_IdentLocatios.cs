﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class Alter_DigitalPassport_IdentLocatios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ManufacturerPlateLocation",
                table: "ManufacturerPlateDetail");

            migrationBuilder.DropColumn(
                name: "VehicleIdentLocation",
                table: "ManufacturerPlateDetail");

            migrationBuilder.DropColumn(
                name: "VehicleIdentStructure",
                table: "ManufacturerPlateDetail");

            migrationBuilder.AddColumn<int>(
                name: "ManufacturerPlateDetailId",
                table: "VehicleIdentLocation",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ManufacturerPlateLocation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Location = table.Column<string>(nullable: true),
                    ManufacturerPlateDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManufacturerPlateLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ManufacturerPlateLocation_ManufacturerPlateDetail_Manufactu~",
                        column: x => x.ManufacturerPlateDetailId,
                        principalTable: "ManufacturerPlateDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleIdentLocation_ManufacturerPlateDetailId",
                table: "VehicleIdentLocation",
                column: "ManufacturerPlateDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_ManufacturerPlateLocation_ManufacturerPlateDetailId",
                table: "ManufacturerPlateLocation",
                column: "ManufacturerPlateDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleIdentLocation_ManufacturerPlateDetail_ManufacturerPl~",
                table: "VehicleIdentLocation",
                column: "ManufacturerPlateDetailId",
                principalTable: "ManufacturerPlateDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleIdentLocation_ManufacturerPlateDetail_ManufacturerPl~",
                table: "VehicleIdentLocation");

            migrationBuilder.DropTable(
                name: "ManufacturerPlateLocation");

            migrationBuilder.DropIndex(
                name: "IX_VehicleIdentLocation_ManufacturerPlateDetailId",
                table: "VehicleIdentLocation");

            migrationBuilder.DropColumn(
                name: "ManufacturerPlateDetailId",
                table: "VehicleIdentLocation");

            migrationBuilder.AddColumn<string>(
                name: "ManufacturerPlateLocation",
                table: "ManufacturerPlateDetail",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VehicleIdentLocation",
                table: "ManufacturerPlateDetail",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VehicleIdentStructure",
                table: "ManufacturerPlateDetail",
                type: "text",
                nullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleEquipmentInfo_Characteristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update public.\"VehicleEquipmentInfos\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleEquipmentInfos_CharacteristicsDetail_Characteristics~",
                table: "VehicleEquipmentInfos");

            migrationBuilder.DropIndex(
                name: "IX_VehicleEquipmentInfos_CharacteristicsDetailId",
                table: "VehicleEquipmentInfos");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleEquipmentInfos");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEquipmentInfos_CharacteristicId",
                table: "VehicleEquipmentInfos",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleEquipmentInfos_CharacteristicsDetail_CharacteristicId",
                table: "VehicleEquipmentInfos",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleEquipmentInfos_CharacteristicsDetail_CharacteristicId",
                table: "VehicleEquipmentInfos");

            migrationBuilder.DropIndex(
                name: "IX_VehicleEquipmentInfos_CharacteristicId",
                table: "VehicleEquipmentInfos");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleEquipmentInfos",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEquipmentInfos_CharacteristicsDetailId",
                table: "VehicleEquipmentInfos",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleEquipmentInfos_CharacteristicsDetail_Characteristics~",
                table: "VehicleEquipmentInfos",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

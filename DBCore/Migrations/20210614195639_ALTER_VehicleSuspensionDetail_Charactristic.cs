﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleSuspensionDetail_Charactristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleSuspensionDetails_CharacteristicsDetail_Characterist~",
                table: "VehicleSuspensionDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicId",
                table: "VehicleSuspensionDetails");

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleSuspensionDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleSuspensionDetails_CharacteristicsDetail_Characterist~",
                table: "VehicleSuspensionDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleSuspensionDetails_CharacteristicsDetail_Characterist~",
                table: "VehicleSuspensionDetails");

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleSuspensionDetails",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicId",
                table: "VehicleSuspensionDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleSuspensionDetails_CharacteristicsDetail_Characterist~",
                table: "VehicleSuspensionDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

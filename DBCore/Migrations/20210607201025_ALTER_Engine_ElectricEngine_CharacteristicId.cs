﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Engine_ElectricEngine_CharacteristicId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComplienceDocuments_Organizations_AuthorOrgId",
                table: "ComplienceDocuments");

            migrationBuilder.DropForeignKey(
                name: "FK_ElectricalMachineDetails_CharacteristicsDetail_Characterist~",
                table: "ElectricalMachineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_CharacteristicsDetail_CharacteristicsDetailId",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicId",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicId",
                table: "ElectricalMachineDetails");

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "EngineDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "ElectricalMachineDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AuthorOrgId",
                table: "ComplienceDocuments",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_ComplienceDocuments_Organizations_AuthorOrgId",
                table: "ComplienceDocuments",
                column: "AuthorOrgId",
                principalTable: "Organizations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ElectricalMachineDetails_CharacteristicsDetail_Characterist~",
                table: "ElectricalMachineDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_CharacteristicsDetail_CharacteristicsDetailId",
                table: "EngineDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComplienceDocuments_Organizations_AuthorOrgId",
                table: "ComplienceDocuments");

            migrationBuilder.DropForeignKey(
                name: "FK_ElectricalMachineDetails_CharacteristicsDetail_Characterist~",
                table: "ElectricalMachineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_CharacteristicsDetail_CharacteristicsDetailId",
                table: "EngineDetails");

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "EngineDetails",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicId",
                table: "EngineDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "ElectricalMachineDetails",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicId",
                table: "ElectricalMachineDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "AuthorOrgId",
                table: "ComplienceDocuments",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ComplienceDocuments_Organizations_AuthorOrgId",
                table: "ComplienceDocuments",
                column: "AuthorOrgId",
                principalTable: "Organizations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ElectricalMachineDetails_CharacteristicsDetail_Characterist~",
                table: "ElectricalMachineDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_CharacteristicsDetail_CharacteristicsDetailId",
                table: "EngineDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class CREATE_BaseVehicleTypeDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NotBaseVehicleTypeDetailIndicator",
                table: "VehicleTypeDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "BaseVehicleTypeDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CommercialName = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false),
                    MakeId = table.Column<int>(nullable: true),
                    Modification = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseVehicleTypeDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseVehicleTypeDetails_NsiValues_MakeId",
                        column: x => x.MakeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseVehicleTypeDetails_VehicleTypeDetails_VehicleTypeDetail~",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaseVehicleTypeDetails_MakeId",
                table: "BaseVehicleTypeDetails",
                column: "MakeId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseVehicleTypeDetails_VehicleTypeDetailId",
                table: "BaseVehicleTypeDetails",
                column: "VehicleTypeDetailId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BaseVehicleTypeDetails");

            migrationBuilder.DropColumn(
                name: "NotBaseVehicleTypeDetailIndicator",
                table: "VehicleTypeDetails");
        }
    }
}

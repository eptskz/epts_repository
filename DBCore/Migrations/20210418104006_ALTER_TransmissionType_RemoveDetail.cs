﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_TransmissionType_RemoveDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicsDeta~",
                table: "TransmissionTypes");

            migrationBuilder.DropIndex(
                name: "IX_TransmissionTypes_CharacteristicsDetailId",
                table: "TransmissionTypes");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "TransmissionTypes");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsId",
                table: "TransmissionTypes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionTypes_CharacteristicsId",
                table: "TransmissionTypes",
                column: "CharacteristicsId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicsId",
                table: "TransmissionTypes",
                column: "CharacteristicsId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicsId",
                table: "TransmissionTypes");

            migrationBuilder.DropIndex(
                name: "IX_TransmissionTypes_CharacteristicsId",
                table: "TransmissionTypes");

            migrationBuilder.DropColumn(
                name: "CharacteristicsId",
                table: "TransmissionTypes");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "TransmissionTypes",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionTypes_CharacteristicsDetailId",
                table: "TransmissionTypes",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicsDeta~",
                table: "TransmissionTypes",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class ALTER_Document_Engine : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_FuelFeedDetailId",
                table: "EngineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_VehicleFuelKindId",
                table: "EngineDetails");

            migrationBuilder.DropIndex(
                name: "IX_EngineDetails_FuelFeedDetailId",
                table: "EngineDetails");

            migrationBuilder.DropIndex(
                name: "IX_EngineDetails_VehicleFuelKindId",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "FuelFeedDetailId",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "VehicleFuelKindId",
                table: "EngineDetails");

            migrationBuilder.AlterColumn<string>(
                name: "EngineCompressionRate",
                table: "EngineDetails",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "double precision");

            migrationBuilder.AddColumn<int>(
                name: "AuthorOrgId",
                table: "ComplienceDocuments",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "EngineFuelFeedDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EngineDetailId = table.Column<int>(nullable: false),
                    FuelFeedId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EngineFuelFeedDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EngineFuelFeedDetail_EngineDetails_EngineDetailId",
                        column: x => x.EngineDetailId,
                        principalTable: "EngineDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineFuelFeedDetail_NsiValues_FuelFeedId",
                        column: x => x.FuelFeedId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleFuelKind",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EngineDetailId = table.Column<int>(nullable: false),
                    FuelKindId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleFuelKind", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleFuelKind_EngineDetails_EngineDetailId",
                        column: x => x.EngineDetailId,
                        principalTable: "EngineDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleFuelKind_NsiValues_FuelKindId",
                        column: x => x.FuelKindId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_AuthorOrgId",
                table: "ComplienceDocuments",
                column: "AuthorOrgId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineFuelFeedDetail_EngineDetailId",
                table: "EngineFuelFeedDetail",
                column: "EngineDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineFuelFeedDetail_FuelFeedId",
                table: "EngineFuelFeedDetail",
                column: "FuelFeedId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleFuelKind_EngineDetailId",
                table: "VehicleFuelKind",
                column: "EngineDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleFuelKind_FuelKindId",
                table: "VehicleFuelKind",
                column: "FuelKindId");

            migrationBuilder.AddForeignKey(
                name: "FK_ComplienceDocuments_Organizations_AuthorOrgId",
                table: "ComplienceDocuments",
                column: "AuthorOrgId",
                principalTable: "Organizations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComplienceDocuments_Organizations_AuthorOrgId",
                table: "ComplienceDocuments");

            migrationBuilder.DropTable(
                name: "EngineFuelFeedDetail");

            migrationBuilder.DropTable(
                name: "VehicleFuelKind");

            migrationBuilder.DropIndex(
                name: "IX_ComplienceDocuments_AuthorOrgId",
                table: "ComplienceDocuments");

            migrationBuilder.DropColumn(
                name: "AuthorOrgId",
                table: "ComplienceDocuments");

            migrationBuilder.AlterColumn<double>(
                name: "EngineCompressionRate",
                table: "EngineDetails",
                type: "double precision",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FuelFeedDetailId",
                table: "EngineDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VehicleFuelKindId",
                table: "EngineDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_FuelFeedDetailId",
                table: "EngineDetails",
                column: "FuelFeedDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_VehicleFuelKindId",
                table: "EngineDetails",
                column: "VehicleFuelKindId");

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_FuelFeedDetailId",
                table: "EngineDetails",
                column: "FuelFeedDetailId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_VehicleFuelKindId",
                table: "EngineDetails",
                column: "VehicleFuelKindId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

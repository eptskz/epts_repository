﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class CREATE_VehicleAdditionalData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ShassisMovePermitions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ShassisMovePermitionText = table.Column<string>(nullable: true),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShassisMovePermitions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShassisMovePermitions_VehicleTypeDetails_VehicleTypeDetailId",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleNotes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NoteText = table.Column<string>(nullable: true),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleNotes_VehicleTypeDetails_VehicleTypeDetailId",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleRoutings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    VehicleRoutingText = table.Column<string>(nullable: true),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleRoutings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleRoutings_VehicleTypeDetails_VehicleTypeDetailId",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleUseRestrictions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    VehicleUseRestrictionText = table.Column<string>(nullable: true),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleUseRestrictions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleUseRestrictions_VehicleTypeDetails_VehicleTypeDetail~",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ShassisMovePermitions_VehicleTypeDetailId",
                table: "ShassisMovePermitions",
                column: "VehicleTypeDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleNotes_VehicleTypeDetailId",
                table: "VehicleNotes",
                column: "VehicleTypeDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRoutings_VehicleTypeDetailId",
                table: "VehicleRoutings",
                column: "VehicleTypeDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleUseRestrictions_VehicleTypeDetailId",
                table: "VehicleUseRestrictions",
                column: "VehicleTypeDetailId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShassisMovePermitions");

            migrationBuilder.DropTable(
                name: "VehicleNotes");

            migrationBuilder.DropTable(
                name: "VehicleRoutings");

            migrationBuilder.DropTable(
                name: "VehicleUseRestrictions");
        }
    }
}

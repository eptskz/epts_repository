﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class CREATE_Contracts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contracts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Number = table.Column<string>(nullable: false),
                    ContractDate = table.Column<DateTime>(nullable: false),
                    ContractEndDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<DateTime>(nullable: true),
                    StatusId = table.Column<int>(nullable: false),
                    ClientOrgId = table.Column<int>(nullable: false),
                    ClientOrgPhone = table.Column<string>(nullable: true),
                    ClientOrgEmail = table.Column<string>(nullable: true),
                    ClientOrgAddress = table.Column<string>(nullable: true),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contracts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contracts_Organizations_ClientOrgId",
                        column: x => x.ClientOrgId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contracts_DicStatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "DicStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_ClientOrgId",
                table: "Contracts",
                column: "ClientOrgId");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_StatusId",
                table: "Contracts",
                column: "StatusId");


            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "new", "Новый", "Новый", "New", DateTime.Now, "contract" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "active", "Действующий", "Действующий", "Active", DateTime.Now, "contract" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "closed", "Завершен", "Завершен", "Сlosed", DateTime.Now, "contract" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "closed", "Завершен", "Завершен", "Сlosed", DateTime.Now, "contract" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "locked", "Заблокирован", "Заблокирован", "Locked", DateTime.Now, "contract" }
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "Contracts");

            migrationBuilder.DeleteData("DicStatuses", "Group", "contract");
        }
    }
}

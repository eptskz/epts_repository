﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleTyreKindInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleTyreKindInfos_NsiValues_VehicleTyreKindSpeedId",
                table: "VehicleTyreKindInfos");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleTyreKindSpeedId",
                table: "VehicleTyreKindInfos",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleTyreKindInfos_NsiValues_VehicleTyreKindSpeedId",
                table: "VehicleTyreKindInfos",
                column: "VehicleTyreKindSpeedId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleTyreKindInfos_NsiValues_VehicleTyreKindSpeedId",
                table: "VehicleTyreKindInfos");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleTyreKindSpeedId",
                table: "VehicleTyreKindInfos",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleTyreKindInfos_NsiValues_VehicleTyreKindSpeedId",
                table: "VehicleTyreKindInfos",
                column: "VehicleTyreKindSpeedId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_NsiValue_WheelData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PowerWheelQuantity",
                table: "NsiValues",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WheelQuantity",
                table: "NsiValues",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PowerWheelQuantity",
                table: "NsiValues");

            migrationBuilder.DropColumn(
                name: "WheelQuantity",
                table: "NsiValues");
        }
    }
}

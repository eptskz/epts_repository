﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleAxleDetail_Characteristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update public.\"VehicleAxleDetails\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleAxleDetails_CharacteristicsDetail_CharacteristicsDet~",
                table: "VehicleAxleDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleAxleDetails_CharacteristicsDetailId",
                table: "VehicleAxleDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleAxleDetails");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleAxleDetails_CharacteristicId",
                table: "VehicleAxleDetails",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleAxleDetails_CharacteristicsDetail_CharacteristicId",
                table: "VehicleAxleDetails",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleAxleDetails_CharacteristicsDetail_CharacteristicId",
                table: "VehicleAxleDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleAxleDetails_CharacteristicId",
                table: "VehicleAxleDetails");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleAxleDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleAxleDetails_CharacteristicsDetailId",
                table: "VehicleAxleDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleAxleDetails_CharacteristicsDetail_CharacteristicsDet~",
                table: "VehicleAxleDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class DeleteDigitalPassport_EnginePlateDetail_WrongRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EnginePlateDetail_DigitalPassportDocument_DigitalPassportId",
                table: "EnginePlateDetail");

            migrationBuilder.DropIndex(
                name: "IX_EnginePlateDetail_DigitalPassportId",
                table: "EnginePlateDetail");

            migrationBuilder.DropColumn(
                name: "DigitalPassportId",
                table: "EnginePlateDetail");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_EnginePlateDetailId",
                table: "DigitalPassportDocument",
                column: "EnginePlateDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_EnginePlateDetail_EnginePlateDetail~",
                table: "DigitalPassportDocument",
                column: "EnginePlateDetailId",
                principalTable: "EnginePlateDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_EnginePlateDetail_EnginePlateDetail~",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_EnginePlateDetailId",
                table: "DigitalPassportDocument");

            migrationBuilder.AddColumn<int>(
                name: "DigitalPassportId",
                table: "EnginePlateDetail",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_EnginePlateDetail_DigitalPassportId",
                table: "EnginePlateDetail",
                column: "DigitalPassportId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_EnginePlateDetail_DigitalPassportDocument_DigitalPassportId",
                table: "EnginePlateDetail",
                column: "DigitalPassportId",
                principalTable: "DigitalPassportDocument",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

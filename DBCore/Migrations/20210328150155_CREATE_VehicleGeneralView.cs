﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class CREATE_VehicleGeneralView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VehicleGeneralViews",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DocumentId = table.Column<Guid>(nullable: false),
                    VehiclePicture = table.Column<byte[]>(nullable: true),
                    OrginalFileName = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false, defaultValueSql: "NOW()"),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleGeneralViews", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleGeneralViews_ComplienceDocuments_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "ComplienceDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleGeneralViews_DocumentId",
                table: "VehicleGeneralViews",
                column: "DocumentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleGeneralViews");
        }
    }
}

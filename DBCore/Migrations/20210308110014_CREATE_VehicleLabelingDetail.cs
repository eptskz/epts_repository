﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class CREATE_VehicleLabelingDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "AspNetUserClaims1");

            //migrationBuilder.DropTable(
            //    name: "DicKatos");

            migrationBuilder.CreateTable(
                name: "VehicleLabelingDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NotManufacturerPlateIndicatorSign = table.Column<int>(nullable: false),
                    VehicleComponentLocationText = table.Column<string>(nullable: true),
                    VehicleIdentificationNumberLocationText = table.Column<string>(nullable: true),
                    EngineIdentificationNumberLocationText = table.Column<string>(nullable: true),
                    Vin = table.Column<string>(nullable: true),
                    DocumentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleLabelingDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleLabelingDetail_ComplienceDocuments_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "ComplienceDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleVinCharacterDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    IdCharacterStartingOrdinal = table.Column<int>(nullable: false),
                    IdCharacterEndingOrdinal = table.Column<int>(nullable: false),
                    IdCharacterText = table.Column<string>(nullable: true),
                    VehicleLabelingDetailId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleVinCharacterDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleVinCharacterDetail_VehicleLabelingDetail_VehicleLabe~",
                        column: x => x.VehicleLabelingDetailId,
                        principalTable: "VehicleLabelingDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleVinCharacterDescription",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    IdCharacterValue = table.Column<string>(nullable: true),
                    IdCharacterValueText = table.Column<string>(nullable: true),
                    VinCharacterDetailId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleVinCharacterDescription", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleVinCharacterDescription_VehicleVinCharacterDetail_Vi~",
                        column: x => x.VinCharacterDetailId,
                        principalTable: "VehicleVinCharacterDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLabelingDetail_DocumentId",
                table: "VehicleLabelingDetail",
                column: "DocumentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleVinCharacterDescription_VinCharacterDetailId",
                table: "VehicleVinCharacterDescription",
                column: "VinCharacterDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleVinCharacterDetail_VehicleLabelingDetailId",
                table: "VehicleVinCharacterDetail",
                column: "VehicleLabelingDetailId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleVinCharacterDescription");

            migrationBuilder.DropTable(
                name: "VehicleVinCharacterDetail");

            migrationBuilder.DropTable(
                name: "VehicleLabelingDetail");

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims1",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims1", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims1_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DicKatos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Code = table.Column<string>(type: "text", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Deleted = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Lat = table.Column<double>(type: "double precision", nullable: true),
                    Lng = table.Column<double>(type: "double precision", nullable: true),
                    Modified = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    NameEn = table.Column<string>(type: "text", nullable: true),
                    NameKz = table.Column<string>(type: "text", nullable: true),
                    NameRu = table.Column<string>(type: "text", nullable: true),
                    ParentId = table.Column<int>(type: "integer", nullable: true),
                    TerritoryType = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DicKatos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DicKatos_DicKatos_ParentId",
                        column: x => x.ParentId,
                        principalTable: "DicKatos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims1_UserId",
                table: "AspNetUserClaims1",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_DicKatos_ParentId",
                table: "DicKatos",
                column: "ParentId");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_OrganizationDocuments_DocTypeId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrganizationDocuments_NsiValues_DocTypeId",
                table: "OrganizationDocuments");

            migrationBuilder.DropForeignKey(
                name: "FK_OrganizationDocuments_NsiValues_IssueCountryId",
                table: "OrganizationDocuments");

            migrationBuilder.AlterColumn<int>(
                name: "IssueCountryId",
                table: "OrganizationDocuments",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "DocTypeId",
                table: "OrganizationDocuments",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_OrganizationDocuments_NsiValues_DocTypeId",
                table: "OrganizationDocuments",
                column: "DocTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrganizationDocuments_NsiValues_IssueCountryId",
                table: "OrganizationDocuments",
                column: "IssueCountryId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrganizationDocuments_NsiValues_DocTypeId",
                table: "OrganizationDocuments");

            migrationBuilder.DropForeignKey(
                name: "FK_OrganizationDocuments_NsiValues_IssueCountryId",
                table: "OrganizationDocuments");

            migrationBuilder.AlterColumn<int>(
                name: "IssueCountryId",
                table: "OrganizationDocuments",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DocTypeId",
                table: "OrganizationDocuments",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_OrganizationDocuments_NsiValues_DocTypeId",
                table: "OrganizationDocuments",
                column: "DocTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrganizationDocuments_NsiValues_IssueCountryId",
                table: "OrganizationDocuments",
                column: "IssueCountryId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

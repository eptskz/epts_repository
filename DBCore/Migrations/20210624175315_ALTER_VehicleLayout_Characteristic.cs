﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleLayout_Characteristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update public.\"VehicleCabins\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.Sql("update public.\"VehicleCarriageSpaceImplementations\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.Sql("update public.\"VehicleEngineLayouts\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.Sql("update public.\"VehicleFrames\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.Sql("update public.\"VehicleLayoutPatterns\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.Sql("update public.\"VehiclePassengerQuantities\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.Sql("update public.\"VehiclePurposes\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            migrationBuilder.Sql("update public.\"VehicleTrunkVolumeMeasures\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");


            migrationBuilder.DropForeignKey(
                name: "FK_VehicleCabins_CharacteristicsDetail_CharacteristicsDetailId",
                table: "VehicleCabins");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleCarriageSpaceImplementations_CharacteristicsDetail_C~",
                table: "VehicleCarriageSpaceImplementations");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleEngineLayouts_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleEngineLayouts");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleFrames_CharacteristicsDetail_CharacteristicsDetailId",
                table: "VehicleFrames");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleLayoutPatterns_CharacteristicsDetail_Characteristics~",
                table: "VehicleLayoutPatterns");

            migrationBuilder.DropForeignKey(
                name: "FK_VehiclePassengerQuantities_CharacteristicsDetail_Characteri~",
                table: "VehiclePassengerQuantities");

            migrationBuilder.DropForeignKey(
                name: "FK_VehiclePurposes_CharacteristicsDetail_CharacteristicsDetail~",
                table: "VehiclePurposes");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleTrunkVolumeMeasures_CharacteristicsDetail_Characteri~",
                table: "VehicleTrunkVolumeMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleTrunkVolumeMeasures_CharacteristicsDetailId",
                table: "VehicleTrunkVolumeMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehiclePurposes_CharacteristicsDetailId",
                table: "VehiclePurposes");

            migrationBuilder.DropIndex(
                name: "IX_VehiclePassengerQuantities_CharacteristicsDetailId",
                table: "VehiclePassengerQuantities");

            migrationBuilder.DropIndex(
                name: "IX_VehicleLayoutPatterns_CharacteristicsDetailId",
                table: "VehicleLayoutPatterns");

            migrationBuilder.DropIndex(
                name: "IX_VehicleFrames_CharacteristicsDetailId",
                table: "VehicleFrames");

            migrationBuilder.DropIndex(
                name: "IX_VehicleEngineLayouts_CharacteristicsDetailId",
                table: "VehicleEngineLayouts");

            migrationBuilder.DropIndex(
                name: "IX_VehicleCarriageSpaceImplementations_CharacteristicsDetailId",
                table: "VehicleCarriageSpaceImplementations");

            migrationBuilder.DropIndex(
                name: "IX_VehicleCabins_CharacteristicsDetailId",
                table: "VehicleCabins");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleTrunkVolumeMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehiclePurposes");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehiclePassengerQuantities");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleLayoutPatterns");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleFrames");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleEngineLayouts");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleCarriageSpaceImplementations");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleCabins");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTrunkVolumeMeasures_CharacteristicId",
                table: "VehicleTrunkVolumeMeasures",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePurposes_CharacteristicId",
                table: "VehiclePurposes",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePassengerQuantities_CharacteristicId",
                table: "VehiclePassengerQuantities",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLayoutPatterns_CharacteristicId",
                table: "VehicleLayoutPatterns",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleFrames_CharacteristicId",
                table: "VehicleFrames",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEngineLayouts_CharacteristicId",
                table: "VehicleEngineLayouts",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleCarriageSpaceImplementations_CharacteristicId",
                table: "VehicleCarriageSpaceImplementations",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleCabins_CharacteristicId",
                table: "VehicleCabins",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleCabins_CharacteristicsDetail_CharacteristicId",
                table: "VehicleCabins",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleCarriageSpaceImplementations_CharacteristicsDetail_C~",
                table: "VehicleCarriageSpaceImplementations",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleEngineLayouts_CharacteristicsDetail_CharacteristicId",
                table: "VehicleEngineLayouts",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleFrames_CharacteristicsDetail_CharacteristicId",
                table: "VehicleFrames",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleLayoutPatterns_CharacteristicsDetail_CharacteristicId",
                table: "VehicleLayoutPatterns",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehiclePassengerQuantities_CharacteristicsDetail_Characteri~",
                table: "VehiclePassengerQuantities",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehiclePurposes_CharacteristicsDetail_CharacteristicId",
                table: "VehiclePurposes",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleTrunkVolumeMeasures_CharacteristicsDetail_Characteri~",
                table: "VehicleTrunkVolumeMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleCabins_CharacteristicsDetail_CharacteristicId",
                table: "VehicleCabins");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleCarriageSpaceImplementations_CharacteristicsDetail_C~",
                table: "VehicleCarriageSpaceImplementations");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleEngineLayouts_CharacteristicsDetail_CharacteristicId",
                table: "VehicleEngineLayouts");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleFrames_CharacteristicsDetail_CharacteristicId",
                table: "VehicleFrames");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleLayoutPatterns_CharacteristicsDetail_CharacteristicId",
                table: "VehicleLayoutPatterns");

            migrationBuilder.DropForeignKey(
                name: "FK_VehiclePassengerQuantities_CharacteristicsDetail_Characteri~",
                table: "VehiclePassengerQuantities");

            migrationBuilder.DropForeignKey(
                name: "FK_VehiclePurposes_CharacteristicsDetail_CharacteristicId",
                table: "VehiclePurposes");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleTrunkVolumeMeasures_CharacteristicsDetail_Characteri~",
                table: "VehicleTrunkVolumeMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleTrunkVolumeMeasures_CharacteristicId",
                table: "VehicleTrunkVolumeMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehiclePurposes_CharacteristicId",
                table: "VehiclePurposes");

            migrationBuilder.DropIndex(
                name: "IX_VehiclePassengerQuantities_CharacteristicId",
                table: "VehiclePassengerQuantities");

            migrationBuilder.DropIndex(
                name: "IX_VehicleLayoutPatterns_CharacteristicId",
                table: "VehicleLayoutPatterns");

            migrationBuilder.DropIndex(
                name: "IX_VehicleFrames_CharacteristicId",
                table: "VehicleFrames");

            migrationBuilder.DropIndex(
                name: "IX_VehicleEngineLayouts_CharacteristicId",
                table: "VehicleEngineLayouts");

            migrationBuilder.DropIndex(
                name: "IX_VehicleCarriageSpaceImplementations_CharacteristicId",
                table: "VehicleCarriageSpaceImplementations");

            migrationBuilder.DropIndex(
                name: "IX_VehicleCabins_CharacteristicId",
                table: "VehicleCabins");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleTrunkVolumeMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehiclePurposes",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehiclePassengerQuantities",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleLayoutPatterns",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleFrames",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleEngineLayouts",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleCarriageSpaceImplementations",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleCabins",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTrunkVolumeMeasures_CharacteristicsDetailId",
                table: "VehicleTrunkVolumeMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePurposes_CharacteristicsDetailId",
                table: "VehiclePurposes",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePassengerQuantities_CharacteristicsDetailId",
                table: "VehiclePassengerQuantities",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLayoutPatterns_CharacteristicsDetailId",
                table: "VehicleLayoutPatterns",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleFrames_CharacteristicsDetailId",
                table: "VehicleFrames",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEngineLayouts_CharacteristicsDetailId",
                table: "VehicleEngineLayouts",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleCarriageSpaceImplementations_CharacteristicsDetailId",
                table: "VehicleCarriageSpaceImplementations",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleCabins_CharacteristicsDetailId",
                table: "VehicleCabins",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleCabins_CharacteristicsDetail_CharacteristicsDetailId",
                table: "VehicleCabins",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleCarriageSpaceImplementations_CharacteristicsDetail_C~",
                table: "VehicleCarriageSpaceImplementations",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleEngineLayouts_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleEngineLayouts",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleFrames_CharacteristicsDetail_CharacteristicsDetailId",
                table: "VehicleFrames",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleLayoutPatterns_CharacteristicsDetail_Characteristics~",
                table: "VehicleLayoutPatterns",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehiclePassengerQuantities_CharacteristicsDetail_Characteri~",
                table: "VehiclePassengerQuantities",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehiclePurposes_CharacteristicsDetail_CharacteristicsDetail~",
                table: "VehiclePurposes",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleTrunkVolumeMeasures_CharacteristicsDetail_Characteri~",
                table: "VehicleTrunkVolumeMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

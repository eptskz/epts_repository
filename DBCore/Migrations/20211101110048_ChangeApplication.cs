﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class ChangeApplication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_EnginePlateDetail_EnginePlateDetail~",
                table: "DigitalPassportDocument");

            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_ManufacturerPlateDetail_Manufacture~",
                table: "DigitalPassportDocument");

            migrationBuilder.AlterColumn<int>(
                name: "ManufacturerPlateDetailId",
                table: "DigitalPassportDocument",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "EnginePlateDetailId",
                table: "DigitalPassportDocument",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.CreateTable(
                name: "ChangeApplication",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SignId = table.Column<Guid>(nullable: true),
                    DeclineDate = table.Column<DateTime>(nullable: true),
                    DeclineDescription = table.Column<string>(nullable: true),
                    UserCreatedId = table.Column<Guid>(nullable: false),
                    UserExecutorId = table.Column<Guid>(nullable: false),
                    IsFieldChange = table.Column<bool>(nullable: false),
                    ChangeStatusNsiId = table.Column<int>(nullable: true),
                    ChangeStatusReason = table.Column<string>(nullable: true),
                    StatusNsiId = table.Column<int>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<DateTime>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    DigitalPassportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChangeApplication", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChangeApplication_NsiValues_ChangeStatusNsiId",
                        column: x => x.ChangeStatusNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChangeApplication_DigitalPassportDocument_DigitalPassportId",
                        column: x => x.DigitalPassportId,
                        principalTable: "DigitalPassportDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChangeApplication_NsiValues_StatusNsiId",
                        column: x => x.StatusNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChangeApplication_AspNetUsers_UserCreatedId",
                        column: x => x.UserCreatedId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChangeApplication_AspNetUsers_UserExecutorId",
                        column: x => x.UserExecutorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChangeApplicationField",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Section = table.Column<string>(nullable: true),
                    Field = table.Column<string>(nullable: true),
                    PreValue = table.Column<string>(nullable: true),
                    PostValue = table.Column<string>(nullable: true),
                    Reason = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ChangeApplicationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChangeApplicationField", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChangeApplicationField_ChangeApplication_ChangeApplicationId",
                        column: x => x.ChangeApplicationId,
                        principalTable: "ChangeApplication",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChangeApplicationFile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FileObject = table.Column<byte[]>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ChangeApplicationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChangeApplicationFile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChangeApplicationFile_ChangeApplication_ChangeApplicationId",
                        column: x => x.ChangeApplicationId,
                        principalTable: "ChangeApplication",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChangeApplication_ChangeStatusNsiId",
                table: "ChangeApplication",
                column: "ChangeStatusNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeApplication_DigitalPassportId",
                table: "ChangeApplication",
                column: "DigitalPassportId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeApplication_StatusNsiId",
                table: "ChangeApplication",
                column: "StatusNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeApplication_UserCreatedId",
                table: "ChangeApplication",
                column: "UserCreatedId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeApplication_UserExecutorId",
                table: "ChangeApplication",
                column: "UserExecutorId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeApplicationField_ChangeApplicationId",
                table: "ChangeApplicationField",
                column: "ChangeApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeApplicationFile_ChangeApplicationId",
                table: "ChangeApplicationFile",
                column: "ChangeApplicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_EnginePlateDetail_EnginePlateDetail~",
                table: "DigitalPassportDocument",
                column: "EnginePlateDetailId",
                principalTable: "EnginePlateDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_ManufacturerPlateDetail_Manufacture~",
                table: "DigitalPassportDocument",
                column: "ManufacturerPlateDetailId",
                principalTable: "ManufacturerPlateDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_EnginePlateDetail_EnginePlateDetail~",
                table: "DigitalPassportDocument");

            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_ManufacturerPlateDetail_Manufacture~",
                table: "DigitalPassportDocument");

            migrationBuilder.DropTable(
                name: "ChangeApplicationField");

            migrationBuilder.DropTable(
                name: "ChangeApplicationFile");

            migrationBuilder.DropTable(
                name: "ChangeApplication");

            migrationBuilder.AlterColumn<int>(
                name: "ManufacturerPlateDetailId",
                table: "DigitalPassportDocument",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EnginePlateDetailId",
                table: "DigitalPassportDocument",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_EnginePlateDetail_EnginePlateDetail~",
                table: "DigitalPassportDocument",
                column: "EnginePlateDetailId",
                principalTable: "EnginePlateDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_ManufacturerPlateDetail_Manufacture~",
                table: "DigitalPassportDocument",
                column: "ManufacturerPlateDetailId",
                principalTable: "ManufacturerPlateDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

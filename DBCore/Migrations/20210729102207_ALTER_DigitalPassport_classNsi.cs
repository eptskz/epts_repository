﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_DigitalPassport_classNsi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "VehicleClassCategoryCodeNsiId",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleClassCategoryCodeNsiId",
                table: "DigitalPassportDocument",
                column: "VehicleClassCategoryCodeNsiId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_NsiValues_VehicleClassCategoryCodeN~",
                table: "DigitalPassportDocument",
                column: "VehicleClassCategoryCodeNsiId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_NsiValues_VehicleClassCategoryCodeN~",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_VehicleClassCategoryCodeNsiId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "VehicleClassCategoryCodeNsiId",
                table: "DigitalPassportDocument");
        }
    }
}

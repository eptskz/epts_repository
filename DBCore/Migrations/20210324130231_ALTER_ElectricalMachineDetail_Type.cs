﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_ElectricalMachineDetail_Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ElectricalMachineTypeId",
                table: "ElectricalMachineDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ElectricalMachineDetails_ElectricalMachineTypeId",
                table: "ElectricalMachineDetails",
                column: "ElectricalMachineTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineTypeId",
                table: "ElectricalMachineDetails",
                column: "ElectricalMachineTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineTypeId",
                table: "ElectricalMachineDetails");

            migrationBuilder.DropIndex(
                name: "IX_ElectricalMachineDetails_ElectricalMachineTypeId",
                table: "ElectricalMachineDetails");

            migrationBuilder.DropColumn(
                name: "ElectricalMachineTypeId",
                table: "ElectricalMachineDetails");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Engine_Transmission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineKindId",
                table: "ElectricalMachineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineTypeId",
                table: "ElectricalMachineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_FuelFeedDetailId",
                table: "EngineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_VehicleFuelKindId",
                table: "EngineDetails");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleFuelKindId",
                table: "EngineDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "FuelFeedDetailId",
                table: "EngineDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "ElectricalMachineTypeId",
                table: "ElectricalMachineDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "ElectricalMachineKindId",
                table: "ElectricalMachineDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineKindId",
                table: "ElectricalMachineDetails",
                column: "ElectricalMachineKindId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineTypeId",
                table: "ElectricalMachineDetails",
                column: "ElectricalMachineTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_FuelFeedDetailId",
                table: "EngineDetails",
                column: "FuelFeedDetailId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_VehicleFuelKindId",
                table: "EngineDetails",
                column: "VehicleFuelKindId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineKindId",
                table: "ElectricalMachineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineTypeId",
                table: "ElectricalMachineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_FuelFeedDetailId",
                table: "EngineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_VehicleFuelKindId",
                table: "EngineDetails");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleFuelKindId",
                table: "EngineDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FuelFeedDetailId",
                table: "EngineDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ElectricalMachineTypeId",
                table: "ElectricalMachineDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ElectricalMachineKindId",
                table: "ElectricalMachineDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineKindId",
                table: "ElectricalMachineDetails",
                column: "ElectricalMachineKindId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineTypeId",
                table: "ElectricalMachineDetails",
                column: "ElectricalMachineTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_FuelFeedDetailId",
                table: "EngineDetails",
                column: "FuelFeedDetailId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_VehicleFuelKindId",
                table: "EngineDetails",
                column: "VehicleFuelKindId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

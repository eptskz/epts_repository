﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class ALTER_ComplienceDoc_Add_YearIssue_Vin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "YearIssue",
                table: "VehicleTypeDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Vin",
                table: "BaseVehicleTypeDetails",
                nullable: true);

            migrationBuilder.InsertData("DicDocTypes"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created" }
                , new object[] { "sbkts", "СБКТС", "СБКТС", "СБКТС", DateTime.Now }
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "YearIssue",
                table: "VehicleTypeDetails");

            migrationBuilder.DropColumn(
                name: "Vin",
                table: "BaseVehicleTypeDetails");
        }
    }
}

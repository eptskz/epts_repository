﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_TransmissionUnitGearDetail_TransmissionUnitGearValueId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_NsiValues_CountryId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_NsiValues_LegalFormId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearV~",
                table: "TransmissionUnitGearDetails");

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionUnitGearValueId",
                table: "TransmissionUnitGearDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "LegalFormId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_NsiValues_CountryId",
                table: "Organizations",
                column: "CountryId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_NsiValues_LegalFormId",
                table: "Organizations",
                column: "LegalFormId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearV~",
                table: "TransmissionUnitGearDetails",
                column: "TransmissionUnitGearValueId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_NsiValues_CountryId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_NsiValues_LegalFormId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearV~",
                table: "TransmissionUnitGearDetails");

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionUnitGearValueId",
                table: "TransmissionUnitGearDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LegalFormId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_NsiValues_CountryId",
                table: "Organizations",
                column: "CountryId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_NsiValues_LegalFormId",
                table: "Organizations",
                column: "LegalFormId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearV~",
                table: "TransmissionUnitGearDetails",
                column: "TransmissionUnitGearValueId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleWeightMeasure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleWeightMeasures_CharacteristicsDetail_Characteristics~",
                table: "VehicleWeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleWeightMeasures_CharacteristicsDetailId",
                table: "VehicleWeightMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleWeightMeasures");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWeightMeasures_CharacteristicId",
                table: "VehicleWeightMeasures",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleWeightMeasures_CharacteristicsDetail_CharacteristicId",
                table: "VehicleWeightMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleWeightMeasures_CharacteristicsDetail_CharacteristicId",
                table: "VehicleWeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleWeightMeasures_CharacteristicId",
                table: "VehicleWeightMeasures");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleWeightMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWeightMeasures_CharacteristicsDetailId",
                table: "VehicleWeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleWeightMeasures_CharacteristicsDetail_Characteristics~",
                table: "VehicleWeightMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

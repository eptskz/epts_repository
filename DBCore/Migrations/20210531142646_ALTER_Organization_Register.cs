﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class ALTER_Organization_Register : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RegisterType",
                table: "Organizations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DocSignDate",
                table: "ComplienceDocuments",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDocSign",
                table: "ComplienceDocuments",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CommunicationInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CommunicationTypeId = table.Column<int>(nullable: false),
                    CommunicationValue = table.Column<string>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommunicationInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommunicationInfo_NsiValues_CommunicationTypeId",
                        column: x => x.CommunicationTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CommunicationInfo_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommunicationInfo_CommunicationTypeId",
                table: "CommunicationInfo",
                column: "CommunicationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CommunicationInfo_OrganizationId",
                table: "CommunicationInfo",
                column: "OrganizationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommunicationInfo");

            migrationBuilder.DropColumn(
                name: "RegisterType",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "DocSignDate",
                table: "ComplienceDocuments");

            migrationBuilder.DropColumn(
                name: "IsDocSign",
                table: "ComplienceDocuments");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleDimension_Characteristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BrakedTrailerWeightMeasures_CharacteristicsDetail_Character~",
                table: "BrakedTrailerWeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_UnbrakedTrailerWeightMeasures_CharacteristicsDetail_Charact~",
                table: "UnbrakedTrailerWeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHeightMeasures_CharacteristicsDetail_Characteristics~",
                table: "VehicleHeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHitchLoadMeasure_CharacteristicsDetail_Characteristi~",
                table: "VehicleHitchLoadMeasure");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleLengthMeasures_CharacteristicsDetail_Characteristics~",
                table: "VehicleLengthMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleLoadingHeightMeasures_CharacteristicsDetail_Characte~",
                table: "VehicleLoadingHeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleMaxHeightMeasures_CharacteristicsDetail_Characterist~",
                table: "VehicleMaxHeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleWheelbaseMeasures_CharacteristicsDetail_Characterist~",
                table: "VehicleWheelbaseMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleWidthMeasures_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleWidthMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleWidthMeasures_CharacteristicsDetailId",
                table: "VehicleWidthMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleWheelbaseMeasures_CharacteristicsDetailId",
                table: "VehicleWheelbaseMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleMaxHeightMeasures_CharacteristicsDetailId",
                table: "VehicleMaxHeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleLoadingHeightMeasures_CharacteristicsDetailId",
                table: "VehicleLoadingHeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleLengthMeasures_CharacteristicsDetailId",
                table: "VehicleLengthMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHitchLoadMeasure_CharacteristicsDetailId",
                table: "VehicleHitchLoadMeasure");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHeightMeasures_CharacteristicsDetailId",
                table: "VehicleHeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_UnbrakedTrailerWeightMeasures_CharacteristicsDetailId",
                table: "UnbrakedTrailerWeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_BrakedTrailerWeightMeasures_CharacteristicsDetailId",
                table: "BrakedTrailerWeightMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleWidthMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleWheelbaseMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleMaxHeightMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleLoadingHeightMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleLengthMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleHitchLoadMeasure");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleHeightMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "UnbrakedTrailerWeightMeasures");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "BrakedTrailerWeightMeasures");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWidthMeasures_CharacteristicId",
                table: "VehicleWidthMeasures",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWheelbaseMeasures_CharacteristicId",
                table: "VehicleWheelbaseMeasures",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleMaxHeightMeasures_CharacteristicId",
                table: "VehicleMaxHeightMeasures",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLoadingHeightMeasures_CharacteristicId",
                table: "VehicleLoadingHeightMeasures",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLengthMeasures_CharacteristicId",
                table: "VehicleLengthMeasures",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHitchLoadMeasure_CharacteristicId",
                table: "VehicleHitchLoadMeasure",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHeightMeasures_CharacteristicId",
                table: "VehicleHeightMeasures",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_UnbrakedTrailerWeightMeasures_CharacteristicId",
                table: "UnbrakedTrailerWeightMeasures",
                column: "CharacteristicId");

            migrationBuilder.CreateIndex(
                name: "IX_BrakedTrailerWeightMeasures_CharacteristicId",
                table: "BrakedTrailerWeightMeasures",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_BrakedTrailerWeightMeasures_CharacteristicsDetail_Character~",
                table: "BrakedTrailerWeightMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UnbrakedTrailerWeightMeasures_CharacteristicsDetail_Charact~",
                table: "UnbrakedTrailerWeightMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHeightMeasures_CharacteristicsDetail_CharacteristicId",
                table: "VehicleHeightMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHitchLoadMeasure_CharacteristicsDetail_Characteristi~",
                table: "VehicleHitchLoadMeasure",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleLengthMeasures_CharacteristicsDetail_CharacteristicId",
                table: "VehicleLengthMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleLoadingHeightMeasures_CharacteristicsDetail_Characte~",
                table: "VehicleLoadingHeightMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleMaxHeightMeasures_CharacteristicsDetail_Characterist~",
                table: "VehicleMaxHeightMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleWheelbaseMeasures_CharacteristicsDetail_Characterist~",
                table: "VehicleWheelbaseMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleWidthMeasures_CharacteristicsDetail_CharacteristicId",
                table: "VehicleWidthMeasures",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BrakedTrailerWeightMeasures_CharacteristicsDetail_Character~",
                table: "BrakedTrailerWeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_UnbrakedTrailerWeightMeasures_CharacteristicsDetail_Charact~",
                table: "UnbrakedTrailerWeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHeightMeasures_CharacteristicsDetail_CharacteristicId",
                table: "VehicleHeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHitchLoadMeasure_CharacteristicsDetail_Characteristi~",
                table: "VehicleHitchLoadMeasure");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleLengthMeasures_CharacteristicsDetail_CharacteristicId",
                table: "VehicleLengthMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleLoadingHeightMeasures_CharacteristicsDetail_Characte~",
                table: "VehicleLoadingHeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleMaxHeightMeasures_CharacteristicsDetail_Characterist~",
                table: "VehicleMaxHeightMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleWheelbaseMeasures_CharacteristicsDetail_Characterist~",
                table: "VehicleWheelbaseMeasures");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleWidthMeasures_CharacteristicsDetail_CharacteristicId",
                table: "VehicleWidthMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleWidthMeasures_CharacteristicId",
                table: "VehicleWidthMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleWheelbaseMeasures_CharacteristicId",
                table: "VehicleWheelbaseMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleMaxHeightMeasures_CharacteristicId",
                table: "VehicleMaxHeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleLoadingHeightMeasures_CharacteristicId",
                table: "VehicleLoadingHeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleLengthMeasures_CharacteristicId",
                table: "VehicleLengthMeasures");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHitchLoadMeasure_CharacteristicId",
                table: "VehicleHitchLoadMeasure");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHeightMeasures_CharacteristicId",
                table: "VehicleHeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_UnbrakedTrailerWeightMeasures_CharacteristicId",
                table: "UnbrakedTrailerWeightMeasures");

            migrationBuilder.DropIndex(
                name: "IX_BrakedTrailerWeightMeasures_CharacteristicId",
                table: "BrakedTrailerWeightMeasures");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleWidthMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleWheelbaseMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleMaxHeightMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleLoadingHeightMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleLengthMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleHitchLoadMeasure",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleHeightMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "UnbrakedTrailerWeightMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "BrakedTrailerWeightMeasures",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWidthMeasures_CharacteristicsDetailId",
                table: "VehicleWidthMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWheelbaseMeasures_CharacteristicsDetailId",
                table: "VehicleWheelbaseMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleMaxHeightMeasures_CharacteristicsDetailId",
                table: "VehicleMaxHeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLoadingHeightMeasures_CharacteristicsDetailId",
                table: "VehicleLoadingHeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLengthMeasures_CharacteristicsDetailId",
                table: "VehicleLengthMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHitchLoadMeasure_CharacteristicsDetailId",
                table: "VehicleHitchLoadMeasure",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHeightMeasures_CharacteristicsDetailId",
                table: "VehicleHeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_UnbrakedTrailerWeightMeasures_CharacteristicsDetailId",
                table: "UnbrakedTrailerWeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_BrakedTrailerWeightMeasures_CharacteristicsDetailId",
                table: "BrakedTrailerWeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_BrakedTrailerWeightMeasures_CharacteristicsDetail_Character~",
                table: "BrakedTrailerWeightMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UnbrakedTrailerWeightMeasures_CharacteristicsDetail_Charact~",
                table: "UnbrakedTrailerWeightMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHeightMeasures_CharacteristicsDetail_Characteristics~",
                table: "VehicleHeightMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHitchLoadMeasure_CharacteristicsDetail_Characteristi~",
                table: "VehicleHitchLoadMeasure",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleLengthMeasures_CharacteristicsDetail_Characteristics~",
                table: "VehicleLengthMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleLoadingHeightMeasures_CharacteristicsDetail_Characte~",
                table: "VehicleLoadingHeightMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleMaxHeightMeasures_CharacteristicsDetail_Characterist~",
                table: "VehicleMaxHeightMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleWheelbaseMeasures_CharacteristicsDetail_Characterist~",
                table: "VehicleWheelbaseMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleWidthMeasures_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleWidthMeasures",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class CREATE_VehicleDetail_ComNameAndType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VehicleCommercialNames",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CommercialName = table.Column<string>(nullable: true),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleCommercialNames", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleCommercialNames_VehicleTypeDetails_VehicleTypeDetail~",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TypeValue = table.Column<string>(nullable: true),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTypes_VehicleTypeDetails_VehicleTypeDetailId",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleCommercialNames_VehicleTypeDetailId",
                table: "VehicleCommercialNames",
                column: "VehicleTypeDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTypes_VehicleTypeDetailId",
                table: "VehicleTypes",
                column: "VehicleTypeDetailId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleCommercialNames");

            migrationBuilder.DropTable(
                name: "VehicleTypes");
        }
    }
}

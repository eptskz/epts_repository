﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class ALTER_ComplienceDocument_Characteristics : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NoteText",
                table: "VehicleTypeDetails");

            migrationBuilder.AddColumn<bool>(
                name: "NotApplicantOrgIndicator",
                table: "ComplienceDocuments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NotAssemblyKitSupplierIndicator",
                table: "ComplienceDocuments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NotAssemblyPlantOrgIndicator",
                table: "ComplienceDocuments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NotManufacturerOrgIndicator",
                table: "ComplienceDocuments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NotRepresentativeManufacturerOrgIndicator",
                table: "ComplienceDocuments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "CharacteristicsDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    VehicleTypeDetailId = table.Column<Guid>(nullable: false),
                    IsVariableHeight = table.Column<int>(nullable: false),
                    NotVehicleTrailerIndicator = table.Column<int>(nullable: false),
                    NotEngineDetailIndicator = table.Column<int>(nullable: false),
                    EngineTypeId = table.Column<int>(nullable: true),
                    VehicleHybridDesignText = table.Column<string>(nullable: true),
                    NotElectricalMachineDetailIndicator = table.Column<int>(nullable: false),
                    NotVehicleClutchDetailIndicator = table.Column<int>(nullable: false),
                    NotVehicleTransmissionDetailIndicator = table.Column<int>(nullable: false),
                    NotVehicleSteeringDetailIndicator = table.Column<int>(nullable: false),
                    VehicleSteeringDetailDescription = table.Column<string>(nullable: true),
                    NotVehicleBrakingSystemDetailIndicator = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacteristicsDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharacteristicsDetail_NsiValues_EngineTypeId",
                        column: x => x.EngineTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacteristicsDetail_VehicleTypeDetails_VehicleTypeDetailId",
                        column: x => x.VehicleTypeDetailId,
                        principalTable: "VehicleTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BrakedTrailerWeightMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    WeightMeasure = table.Column<double>(nullable: false),
                    WeightMeasureMax = table.Column<double>(nullable: true),
                    UnitId = table.Column<int>(nullable: false),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrakedTrailerWeightMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BrakedTrailerWeightMeasures_CharacteristicsDetail_Character~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BrakedTrailerWeightMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ElectricalMachineDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    ElectricalMachineKindId = table.Column<int>(nullable: false),
                    VehicleComponentMakeName = table.Column<string>(nullable: true),
                    VehicleComponentText = table.Column<string>(nullable: true),
                    ElectricMotorPowerMeasure = table.Column<double>(nullable: false),
                    ElectricMotorPowerMeasureUnitId = table.Column<int>(nullable: false),
                    ElectricalMachineVoltageMeasure = table.Column<double>(nullable: false),
                    ElectricalMachineVoltageMeasureUnitId = table.Column<int>(nullable: false),
                    NotPowerStorageDeviceDetailIndicator = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElectricalMachineDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ElectricalMachineDetails_CharacteristicsDetail_Characterist~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ElectricalMachineDetails_NsiValues_ElectricMotorPowerMeasur~",
                        column: x => x.ElectricMotorPowerMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineKindId",
                        column: x => x.ElectricalMachineKindId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ElectricalMachineDetails_NsiValues_ElectricalMachineVoltage~",
                        column: x => x.ElectricalMachineVoltageMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EngineDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    VehicleComponentMakeName = table.Column<string>(nullable: true),
                    VehicleComponentText = table.Column<string>(nullable: true),
                    EngineCylinderQuantity = table.Column<int>(nullable: false),
                    EngineCylinderArrangementId = table.Column<int>(nullable: false),
                    EngineCapacityMeasure = table.Column<double>(nullable: false),
                    EngineCapacityMeasureUnitId = table.Column<int>(nullable: false),
                    EngineCompressionRate = table.Column<double>(nullable: false),
                    EngineMaxPowerMeasure = table.Column<double>(nullable: false),
                    EngineMaxPowerMeasureUnitId = table.Column<int>(nullable: false),
                    EngineMaxPowerShaftRotationFrequencyMinMeasure = table.Column<double>(nullable: false),
                    EngineMaxPowerShaftRotationFrequencyMaxMeasure = table.Column<double>(nullable: true),
                    EngineMaxPowerShaftRotationFrequencyMeasureUnitId = table.Column<int>(nullable: false),
                    EngineMaxTorqueMeasure = table.Column<double>(nullable: false),
                    EngineMaxTorqueMeasureUnitId = table.Column<int>(nullable: false),
                    EngineMaxTorqueMeasureShaftRotationFrequencyMinMeasure = table.Column<double>(nullable: false),
                    EngineMaxTorqueMeasureShaftRotationFrequencyMaxMeasure = table.Column<double>(nullable: true),
                    EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId = table.Column<int>(nullable: false),
                    NotVehicleFuelIndicator = table.Column<int>(nullable: false),
                    VehicleFuelKindId = table.Column<int>(nullable: false),
                    NotFuelFeedDetailIndicator = table.Column<int>(nullable: false),
                    FuelFeedDetailId = table.Column<int>(nullable: false),
                    NotVehicleIgnitionDetailIndicator = table.Column<int>(nullable: false),
                    NotExhaustDetailIndicator = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EngineDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EngineDetails_CharacteristicsDetail_CharacteristicsDetailId",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EngineDetails_NsiValues_EngineCapacityMeasureUnitId",
                        column: x => x.EngineCapacityMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineDetails_NsiValues_EngineCylinderArrangementId",
                        column: x => x.EngineCylinderArrangementId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineDetails_NsiValues_EngineMaxPowerMeasureUnitId",
                        column: x => x.EngineMaxPowerMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineDetails_NsiValues_EngineMaxPowerShaftRotationFrequenc~",
                        column: x => x.EngineMaxPowerShaftRotationFrequencyMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineDetails_NsiValues_EngineMaxTorqueMeasureShaftRotation~",
                        column: x => x.EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineDetails_NsiValues_EngineMaxTorqueMeasureUnitId",
                        column: x => x.EngineMaxTorqueMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineDetails_NsiValues_FuelFeedDetailId",
                        column: x => x.FuelFeedDetailId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineDetails_NsiValues_VehicleFuelKindId",
                        column: x => x.VehicleFuelKindId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransmissionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    TransTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransmissionTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicsDeta~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransmissionTypes_NsiValues_TransTypeId",
                        column: x => x.TransTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UnbrakedTrailerWeightMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    WeightMeasure = table.Column<double>(nullable: false),
                    WeightMeasureMax = table.Column<double>(nullable: true),
                    UnitId = table.Column<int>(nullable: false),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnbrakedTrailerWeightMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UnbrakedTrailerWeightMeasures_CharacteristicsDetail_Charact~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UnbrakedTrailerWeightMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleAxleDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AxleOrdinal = table.Column<int>(nullable: false),
                    DrivingAxleIndicator = table.Column<int>(nullable: true),
                    AxleSweptPathMeasure = table.Column<double>(nullable: false),
                    AxleSweptPathMeasureMax = table.Column<double>(nullable: true),
                    TechnicallyPermissibleMaxWeightOnAxleMeasure = table.Column<double>(nullable: false),
                    TechnicallyPermissibleMaxWeightOnAxleMeasureMax = table.Column<double>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleAxleDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleAxleDetails_CharacteristicsDetail_CharacteristicsDet~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleBodyworkDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DoorQuantity = table.Column<int>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    VehicleBodyWorkTypeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleBodyworkDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleBodyworkDetails_CharacteristicsDetail_Characteristic~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleBodyworkDetails_NsiValues_VehicleBodyWorkTypeId",
                        column: x => x.VehicleBodyWorkTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleBrakingSystemDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    VehicleBrakingSystemKindId = table.Column<int>(nullable: false),
                    VehicleBrakingSystemDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleBrakingSystemDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleBrakingSystemDetails_CharacteristicsDetail_Character~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleBrakingSystemDetails_NsiValues_VehicleBrakingSystemK~",
                        column: x => x.VehicleBrakingSystemKindId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleCabins",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    CarriageSpaceImplementation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleCabins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleCabins_CharacteristicsDetail_CharacteristicsDetailId",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleCarriageSpaceImplementations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    CarriageSpaceImplementation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleCarriageSpaceImplementations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleCarriageSpaceImplementations_CharacteristicsDetail_C~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleClutchDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    VehicleClutchMakeName = table.Column<string>(nullable: true),
                    VehicleClutchDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleClutchDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleClutchDetails_CharacteristicsDetail_CharacteristicsD~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleEngineLayouts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    EngineLocationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleEngineLayouts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleEngineLayouts_CharacteristicsDetail_CharacteristicsD~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleEngineLayouts_NsiValues_EngineLocationId",
                        column: x => x.EngineLocationId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleEquipmentInfos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    VehicleEquipmentText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleEquipmentInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleEquipmentInfos_CharacteristicsDetail_Characteristics~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleFrames",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    FrameText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleFrames", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleFrames_CharacteristicsDetail_CharacteristicsDetailId",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleHeightMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Height = table.Column<double>(nullable: false),
                    HeightMax = table.Column<double>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    UnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleHeightMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleHeightMeasures_CharacteristicsDetail_Characteristics~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleHeightMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleHitchLoadMeasure",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    HitchLoadMeasure = table.Column<double>(nullable: false),
                    HitchLoadMeasureMax = table.Column<double>(nullable: true),
                    UnitId = table.Column<int>(nullable: false),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleHitchLoadMeasure", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleHitchLoadMeasure_CharacteristicsDetail_Characteristi~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleHitchLoadMeasure_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleLayoutPatterns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    LayoutPatternId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleLayoutPatterns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleLayoutPatterns_CharacteristicsDetail_Characteristics~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleLayoutPatterns_NsiValues_LayoutPatternId",
                        column: x => x.LayoutPatternId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleLengthMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Length = table.Column<double>(nullable: false),
                    LengthMax = table.Column<double>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    UnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleLengthMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleLengthMeasures_CharacteristicsDetail_Characteristics~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleLengthMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleLoadingHeightMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LoadingHeight = table.Column<double>(nullable: false),
                    LoadingHeightMax = table.Column<double>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    UnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleLoadingHeightMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleLoadingHeightMeasures_CharacteristicsDetail_Characte~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleLoadingHeightMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleMaxHeightMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MaxHeightMeasure = table.Column<double>(nullable: false),
                    MaxHeightMeasureMax = table.Column<double>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    UnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleMaxHeightMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleMaxHeightMeasures_CharacteristicsDetail_Characterist~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleMaxHeightMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehiclePassengerQuantities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    PassengerQuantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehiclePassengerQuantities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehiclePassengerQuantities_CharacteristicsDetail_Characteri~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehiclePurposes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    PurposeId = table.Column<int>(nullable: false),
                    OtherInfoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehiclePurposes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehiclePurposes_CharacteristicsDetail_CharacteristicsDetail~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehiclePurposes_NsiValues_OtherInfoId",
                        column: x => x.OtherInfoId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehiclePurposes_NsiValues_PurposeId",
                        column: x => x.PurposeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleRunningGearDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    VehicleWheelFormulaId = table.Column<int>(nullable: true),
                    PoweredWheelLocationId = table.Column<int>(nullable: true),
                    VehicleAxleQuantity = table.Column<int>(nullable: true),
                    VehicleWheelQuantity = table.Column<int>(nullable: true),
                    VehicleWheelLocation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleRunningGearDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleRunningGearDetails_CharacteristicsDetail_Characteris~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleRunningGearDetails_NsiValues_PoweredWheelLocationId",
                        column: x => x.PoweredWheelLocationId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleRunningGearDetails_NsiValues_VehicleWheelFormulaId",
                        column: x => x.VehicleWheelFormulaId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleSeatDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SeatQuantity = table.Column<int>(nullable: false),
                    SeatDescription = table.Column<string>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleSeatDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleSeatDetails_CharacteristicsDetail_CharacteristicsDet~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleSteeringDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    VehicleSteeringType = table.Column<string>(nullable: true),
                    VehicleSteeringDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleSteeringDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleSteeringDetails_CharacteristicsDetail_Characteristic~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleSuspensionDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    VehicleSuspensionKindId = table.Column<int>(nullable: false),
                    VehicleSuspensionDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleSuspensionDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleSuspensionDetails_CharacteristicsDetail_Characterist~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleSuspensionDetails_NsiValues_VehicleSuspensionKindId",
                        column: x => x.VehicleSuspensionKindId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTrunkVolumeMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    TrunkVolumeMeasure = table.Column<double>(nullable: false),
                    UnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTrunkVolumeMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTrunkVolumeMeasures_CharacteristicsDetail_Characteri~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleTrunkVolumeMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTyreKindInfos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    VehicleTyreKindSize = table.Column<string>(nullable: true),
                    VehicleTyreKindIndex = table.Column<string>(nullable: true),
                    VehicleTyreKindSpeedId = table.Column<int>(nullable: false),
                    IsSupplementVehicleTyre = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTyreKindInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTyreKindInfos_CharacteristicsDetail_CharacteristicsD~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleTyreKindInfos_NsiValues_VehicleTyreKindSpeedId",
                        column: x => x.VehicleTyreKindSpeedId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleWeightMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<double>(nullable: false),
                    WeightMax = table.Column<double>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    UnitId = table.Column<int>(nullable: false),
                    MassTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleWeightMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleWeightMeasures_CharacteristicsDetail_Characteristics~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleWeightMeasures_NsiValues_MassTypeId",
                        column: x => x.MassTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleWeightMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleWheelbaseMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Wheelbase = table.Column<double>(nullable: false),
                    WheelbaseMax = table.Column<double>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    UnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleWheelbaseMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleWheelbaseMeasures_CharacteristicsDetail_Characterist~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleWheelbaseMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleWidthMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Width = table.Column<double>(nullable: false),
                    WidthMax = table.Column<double>(nullable: true),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    UnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleWidthMeasures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleWidthMeasures_CharacteristicsDetail_CharacteristicsD~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleWidthMeasures_NsiValues_UnitId",
                        column: x => x.UnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PowerStorageDeviceDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ElectricalMachineDetailId = table.Column<int>(nullable: false),
                    PowerStorageDeviceTypeId = table.Column<int>(nullable: false),
                    PowerStorageDeviceDescription = table.Column<string>(nullable: true),
                    PowerStorageDeviceLocation = table.Column<string>(nullable: true),
                    VehicleRangeMeasure = table.Column<double>(nullable: false),
                    VehicleRangeUnitId = table.Column<int>(nullable: false),
                    PowerStorageDeviceVoltageMeasure = table.Column<double>(nullable: false),
                    PowerStorageDeviceVoltageMeasureUnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PowerStorageDeviceDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PowerStorageDeviceDetail_ElectricalMachineDetails_Electrica~",
                        column: x => x.ElectricalMachineDetailId,
                        principalTable: "ElectricalMachineDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PowerStorageDeviceDetail_NsiValues_PowerStorageDeviceTypeId",
                        column: x => x.PowerStorageDeviceTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PowerStorageDeviceDetail_NsiValues_PowerStorageDeviceVoltag~",
                        column: x => x.PowerStorageDeviceVoltageMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PowerStorageDeviceDetail_NsiValues_VehicleRangeUnitId",
                        column: x => x.VehicleRangeUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ECUModelDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EngineDetailId = table.Column<int>(nullable: false),
                    EcuModel = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ECUModelDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ECUModelDetails_EngineDetails_EngineDetailId",
                        column: x => x.EngineDetailId,
                        principalTable: "EngineDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExhaustDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EngineDetailId = table.Column<int>(nullable: false),
                    ExhaustDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExhaustDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExhaustDetails_EngineDetails_EngineDetailId",
                        column: x => x.EngineDetailId,
                        principalTable: "EngineDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleIgnitionDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EngineDetailId = table.Column<int>(nullable: false),
                    VehicleIgnitionTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleIgnitionDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleIgnitionDetails_EngineDetails_EngineDetailId",
                        column: x => x.EngineDetailId,
                        principalTable: "EngineDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleIgnitionDetails_NsiValues_VehicleIgnitionTypeId",
                        column: x => x.VehicleIgnitionTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransmissionUnitDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CharacteristicId = table.Column<int>(nullable: false),
                    CharacteristicsDetailId = table.Column<int>(nullable: true),
                    UnitKindId = table.Column<int>(nullable: false),
                    ContinuouslyVariableTransmissionIndicator = table.Column<int>(nullable: false),
                    AxisDistributionId = table.Column<int>(nullable: false),
                    TransmissionUnitMakeName = table.Column<string>(nullable: true),
                    TransmissionUnitDescription = table.Column<string>(nullable: true),
                    TransmissionBoxTypeId = table.Column<int>(nullable: false),
                    MainGearTypeId = table.Column<int>(nullable: false),
                    TransferCaseType = table.Column<string>(nullable: true),
                    TransmissionUnitGearQuantity = table.Column<int>(nullable: false),
                    TransmissionTypeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransmissionUnitDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransmissionUnitDetails_NsiValues_AxisDistributionId",
                        column: x => x.AxisDistributionId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransmissionUnitDetails_CharacteristicsDetail_Characteristi~",
                        column: x => x.CharacteristicsDetailId,
                        principalTable: "CharacteristicsDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransmissionUnitDetails_NsiValues_MainGearTypeId",
                        column: x => x.MainGearTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransmissionUnitDetails_NsiValues_TransmissionBoxTypeId",
                        column: x => x.TransmissionBoxTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransmissionUnitDetails_TransmissionTypes_TransmissionTypeId",
                        column: x => x.TransmissionTypeId,
                        principalTable: "TransmissionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransmissionUnitDetails_NsiValues_UnitKindId",
                        column: x => x.UnitKindId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleSeatRawDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SeatRawOrdinal = table.Column<int>(nullable: false),
                    SeatRawQuantity = table.Column<int>(nullable: false),
                    VehicleSeatDetailId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleSeatRawDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleSeatRawDetails_VehicleSeatDetails_VehicleSeatDetailId",
                        column: x => x.VehicleSeatDetailId,
                        principalTable: "VehicleSeatDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransmissionUnitGearDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TransmissionUnitId = table.Column<int>(nullable: false),
                    TransmissionUnitGearValueId = table.Column<int>(nullable: false),
                    TransmissionUnitGearTypeId = table.Column<int>(nullable: false),
                    TransmissionUnitGearRate = table.Column<int>(nullable: false),
                    TransmissionUnitGearRateMax = table.Column<int>(nullable: false),
                    TransmissionUnitReverseGearIndicator = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransmissionUnitGearDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearT~",
                        column: x => x.TransmissionUnitGearTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearV~",
                        column: x => x.TransmissionUnitGearValueId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransmissionUnitGearDetails_TransmissionUnitDetails_Transmi~",
                        column: x => x.TransmissionUnitId,
                        principalTable: "TransmissionUnitDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BrakedTrailerWeightMeasures_CharacteristicsDetailId",
                table: "BrakedTrailerWeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_BrakedTrailerWeightMeasures_UnitId",
                table: "BrakedTrailerWeightMeasures",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacteristicsDetail_EngineTypeId",
                table: "CharacteristicsDetail",
                column: "EngineTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacteristicsDetail_VehicleTypeDetailId",
                table: "CharacteristicsDetail",
                column: "VehicleTypeDetailId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ECUModelDetails_EngineDetailId",
                table: "ECUModelDetails",
                column: "EngineDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_ElectricalMachineDetails_CharacteristicsDetailId",
                table: "ElectricalMachineDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_ElectricalMachineDetails_ElectricMotorPowerMeasureUnitId",
                table: "ElectricalMachineDetails",
                column: "ElectricMotorPowerMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_ElectricalMachineDetails_ElectricalMachineKindId",
                table: "ElectricalMachineDetails",
                column: "ElectricalMachineKindId");

            migrationBuilder.CreateIndex(
                name: "IX_ElectricalMachineDetails_ElectricalMachineVoltageMeasureUni~",
                table: "ElectricalMachineDetails",
                column: "ElectricalMachineVoltageMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_CharacteristicsDetailId",
                table: "EngineDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineCapacityMeasureUnitId",
                table: "EngineDetails",
                column: "EngineCapacityMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineCylinderArrangementId",
                table: "EngineDetails",
                column: "EngineCylinderArrangementId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineMaxPowerMeasureUnitId",
                table: "EngineDetails",
                column: "EngineMaxPowerMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineMaxPowerShaftRotationFrequencyMeasureUn~",
                table: "EngineDetails",
                column: "EngineMaxPowerShaftRotationFrequencyMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineMaxTorqueMeasureShaftRotationFrequencyM~",
                table: "EngineDetails",
                column: "EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineMaxTorqueMeasureUnitId",
                table: "EngineDetails",
                column: "EngineMaxTorqueMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_FuelFeedDetailId",
                table: "EngineDetails",
                column: "FuelFeedDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_VehicleFuelKindId",
                table: "EngineDetails",
                column: "VehicleFuelKindId");

            migrationBuilder.CreateIndex(
                name: "IX_ExhaustDetails_EngineDetailId",
                table: "ExhaustDetails",
                column: "EngineDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_PowerStorageDeviceDetail_ElectricalMachineDetailId",
                table: "PowerStorageDeviceDetail",
                column: "ElectricalMachineDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_PowerStorageDeviceDetail_PowerStorageDeviceTypeId",
                table: "PowerStorageDeviceDetail",
                column: "PowerStorageDeviceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PowerStorageDeviceDetail_PowerStorageDeviceVoltageMeasureUn~",
                table: "PowerStorageDeviceDetail",
                column: "PowerStorageDeviceVoltageMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_PowerStorageDeviceDetail_VehicleRangeUnitId",
                table: "PowerStorageDeviceDetail",
                column: "VehicleRangeUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionTypes_CharacteristicsDetailId",
                table: "TransmissionTypes",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionTypes_TransTypeId",
                table: "TransmissionTypes",
                column: "TransTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitDetails_AxisDistributionId",
                table: "TransmissionUnitDetails",
                column: "AxisDistributionId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitDetails_CharacteristicsDetailId",
                table: "TransmissionUnitDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitDetails_MainGearTypeId",
                table: "TransmissionUnitDetails",
                column: "MainGearTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitDetails_TransmissionBoxTypeId",
                table: "TransmissionUnitDetails",
                column: "TransmissionBoxTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitDetails_TransmissionTypeId",
                table: "TransmissionUnitDetails",
                column: "TransmissionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitDetails_UnitKindId",
                table: "TransmissionUnitDetails",
                column: "UnitKindId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitGearDetails_TransmissionUnitGearTypeId",
                table: "TransmissionUnitGearDetails",
                column: "TransmissionUnitGearTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitGearDetails_TransmissionUnitGearValueId",
                table: "TransmissionUnitGearDetails",
                column: "TransmissionUnitGearValueId");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitGearDetails_TransmissionUnitId",
                table: "TransmissionUnitGearDetails",
                column: "TransmissionUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_UnbrakedTrailerWeightMeasures_CharacteristicsDetailId",
                table: "UnbrakedTrailerWeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_UnbrakedTrailerWeightMeasures_UnitId",
                table: "UnbrakedTrailerWeightMeasures",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleAxleDetails_CharacteristicsDetailId",
                table: "VehicleAxleDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleBodyworkDetails_CharacteristicsDetailId",
                table: "VehicleBodyworkDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleBodyworkDetails_VehicleBodyWorkTypeId",
                table: "VehicleBodyworkDetails",
                column: "VehicleBodyWorkTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleBrakingSystemDetails_CharacteristicsDetailId",
                table: "VehicleBrakingSystemDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleBrakingSystemDetails_VehicleBrakingSystemKindId",
                table: "VehicleBrakingSystemDetails",
                column: "VehicleBrakingSystemKindId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleCabins_CharacteristicsDetailId",
                table: "VehicleCabins",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleCarriageSpaceImplementations_CharacteristicsDetailId",
                table: "VehicleCarriageSpaceImplementations",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleClutchDetails_CharacteristicsDetailId",
                table: "VehicleClutchDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEngineLayouts_CharacteristicsDetailId",
                table: "VehicleEngineLayouts",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEngineLayouts_EngineLocationId",
                table: "VehicleEngineLayouts",
                column: "EngineLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEquipmentInfos_CharacteristicsDetailId",
                table: "VehicleEquipmentInfos",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleFrames_CharacteristicsDetailId",
                table: "VehicleFrames",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHeightMeasures_CharacteristicsDetailId",
                table: "VehicleHeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHeightMeasures_UnitId",
                table: "VehicleHeightMeasures",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHitchLoadMeasure_CharacteristicsDetailId",
                table: "VehicleHitchLoadMeasure",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHitchLoadMeasure_UnitId",
                table: "VehicleHitchLoadMeasure",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleIgnitionDetails_EngineDetailId",
                table: "VehicleIgnitionDetails",
                column: "EngineDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleIgnitionDetails_VehicleIgnitionTypeId",
                table: "VehicleIgnitionDetails",
                column: "VehicleIgnitionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLayoutPatterns_CharacteristicsDetailId",
                table: "VehicleLayoutPatterns",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLayoutPatterns_LayoutPatternId",
                table: "VehicleLayoutPatterns",
                column: "LayoutPatternId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLengthMeasures_CharacteristicsDetailId",
                table: "VehicleLengthMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLengthMeasures_UnitId",
                table: "VehicleLengthMeasures",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLoadingHeightMeasures_CharacteristicsDetailId",
                table: "VehicleLoadingHeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleLoadingHeightMeasures_UnitId",
                table: "VehicleLoadingHeightMeasures",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleMaxHeightMeasures_CharacteristicsDetailId",
                table: "VehicleMaxHeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleMaxHeightMeasures_UnitId",
                table: "VehicleMaxHeightMeasures",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePassengerQuantities_CharacteristicsDetailId",
                table: "VehiclePassengerQuantities",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePurposes_CharacteristicsDetailId",
                table: "VehiclePurposes",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePurposes_OtherInfoId",
                table: "VehiclePurposes",
                column: "OtherInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePurposes_PurposeId",
                table: "VehiclePurposes",
                column: "PurposeId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRunningGearDetails_CharacteristicsDetailId",
                table: "VehicleRunningGearDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRunningGearDetails_PoweredWheelLocationId",
                table: "VehicleRunningGearDetails",
                column: "PoweredWheelLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRunningGearDetails_VehicleWheelFormulaId",
                table: "VehicleRunningGearDetails",
                column: "VehicleWheelFormulaId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSeatDetails_CharacteristicsDetailId",
                table: "VehicleSeatDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSeatRawDetails_VehicleSeatDetailId",
                table: "VehicleSeatRawDetails",
                column: "VehicleSeatDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSteeringDetails_CharacteristicsDetailId",
                table: "VehicleSteeringDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSuspensionDetails_CharacteristicsDetailId",
                table: "VehicleSuspensionDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSuspensionDetails_VehicleSuspensionKindId",
                table: "VehicleSuspensionDetails",
                column: "VehicleSuspensionKindId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTrunkVolumeMeasures_CharacteristicsDetailId",
                table: "VehicleTrunkVolumeMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTrunkVolumeMeasures_UnitId",
                table: "VehicleTrunkVolumeMeasures",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTyreKindInfos_CharacteristicsDetailId",
                table: "VehicleTyreKindInfos",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTyreKindInfos_VehicleTyreKindSpeedId",
                table: "VehicleTyreKindInfos",
                column: "VehicleTyreKindSpeedId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWeightMeasures_CharacteristicsDetailId",
                table: "VehicleWeightMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWeightMeasures_MassTypeId",
                table: "VehicleWeightMeasures",
                column: "MassTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWeightMeasures_UnitId",
                table: "VehicleWeightMeasures",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWheelbaseMeasures_CharacteristicsDetailId",
                table: "VehicleWheelbaseMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWheelbaseMeasures_UnitId",
                table: "VehicleWheelbaseMeasures",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWidthMeasures_CharacteristicsDetailId",
                table: "VehicleWidthMeasures",
                column: "CharacteristicsDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleWidthMeasures_UnitId",
                table: "VehicleWidthMeasures",
                column: "UnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropTable(
                name: "BrakedTrailerWeightMeasures");

            migrationBuilder.DropTable(
                name: "ECUModelDetails");

            migrationBuilder.DropTable(
                name: "ExhaustDetails");

            migrationBuilder.DropTable(
                name: "PowerStorageDeviceDetail");

            migrationBuilder.DropTable(
                name: "TransmissionUnitGearDetails");

            migrationBuilder.DropTable(
                name: "UnbrakedTrailerWeightMeasures");

            migrationBuilder.DropTable(
                name: "VehicleAxleDetails");

            migrationBuilder.DropTable(
                name: "VehicleBodyworkDetails");

            migrationBuilder.DropTable(
                name: "VehicleBrakingSystemDetails");

            migrationBuilder.DropTable(
                name: "VehicleCabins");

            migrationBuilder.DropTable(
                name: "VehicleCarriageSpaceImplementations");

            migrationBuilder.DropTable(
                name: "VehicleClutchDetails");

            migrationBuilder.DropTable(
                name: "VehicleEngineLayouts");

            migrationBuilder.DropTable(
                name: "VehicleEquipmentInfos");

            migrationBuilder.DropTable(
                name: "VehicleFrames");

            migrationBuilder.DropTable(
                name: "VehicleHeightMeasures");

            migrationBuilder.DropTable(
                name: "VehicleHitchLoadMeasure");

            migrationBuilder.DropTable(
                name: "VehicleIgnitionDetails");

            migrationBuilder.DropTable(
                name: "VehicleLayoutPatterns");

            migrationBuilder.DropTable(
                name: "VehicleLengthMeasures");

            migrationBuilder.DropTable(
                name: "VehicleLoadingHeightMeasures");

            migrationBuilder.DropTable(
                name: "VehicleMaxHeightMeasures");

            migrationBuilder.DropTable(
                name: "VehiclePassengerQuantities");

            migrationBuilder.DropTable(
                name: "VehiclePurposes");

            migrationBuilder.DropTable(
                name: "VehicleRunningGearDetails");

            migrationBuilder.DropTable(
                name: "VehicleSeatRawDetails");

            migrationBuilder.DropTable(
                name: "VehicleSteeringDetails");

            migrationBuilder.DropTable(
                name: "VehicleSuspensionDetails");

            migrationBuilder.DropTable(
                name: "VehicleTrunkVolumeMeasures");

            migrationBuilder.DropTable(
                name: "VehicleTyreKindInfos");

            migrationBuilder.DropTable(
                name: "VehicleWeightMeasures");

            migrationBuilder.DropTable(
                name: "VehicleWheelbaseMeasures");

            migrationBuilder.DropTable(
                name: "VehicleWidthMeasures");

            migrationBuilder.DropTable(
                name: "ElectricalMachineDetails");

            migrationBuilder.DropTable(
                name: "TransmissionUnitDetails");

            migrationBuilder.DropTable(
                name: "EngineDetails");

            migrationBuilder.DropTable(
                name: "VehicleSeatDetails");

            migrationBuilder.DropTable(
                name: "TransmissionTypes");

            migrationBuilder.DropTable(
                name: "CharacteristicsDetail");

            migrationBuilder.DropColumn(
                name: "NotApplicantOrgIndicator",
                table: "ComplienceDocuments");

            migrationBuilder.DropColumn(
                name: "NotAssemblyKitSupplierIndicator",
                table: "ComplienceDocuments");

            migrationBuilder.DropColumn(
                name: "NotAssemblyPlantOrgIndicator",
                table: "ComplienceDocuments");

            migrationBuilder.DropColumn(
                name: "NotManufacturerOrgIndicator",
                table: "ComplienceDocuments");

            migrationBuilder.DropColumn(
                name: "NotRepresentativeManufacturerOrgIndicator",
                table: "ComplienceDocuments");

            migrationBuilder.AddColumn<string>(
                name: "NoteText",
                table: "VehicleTypeDetails",
                type: "text",
                nullable: true);
        }
    }
}

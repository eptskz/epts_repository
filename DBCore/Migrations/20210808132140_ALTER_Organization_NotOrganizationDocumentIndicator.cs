﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Organization_NotOrganizationDocumentIndicator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "NotOrganizationDocumentIndicator",
                table: "Organizations",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NotOrganizationDocumentIndicator",
                table: "Organizations");
        }
    }
}

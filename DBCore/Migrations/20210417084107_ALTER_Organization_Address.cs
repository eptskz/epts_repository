﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Organization_Address : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FBuildingNumber",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FRoomNumber",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FStreetName",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JBuildingNumber",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JRoomNumber",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JStreetName",
                table: "Organizations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FBuildingNumber",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FRoomNumber",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FStreetName",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JBuildingNumber",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JRoomNumber",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JStreetName",
                table: "Organizations");
        }
    }
}

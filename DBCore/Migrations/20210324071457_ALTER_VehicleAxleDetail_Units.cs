﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleAxleDetail_Units : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AxleSweptPathMeasureUnitId",
                table: "VehicleAxleDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TechnicallyPermissibleMaxWeightOnAxleUnitId",
                table: "VehicleAxleDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleAxleDetails_AxleSweptPathMeasureUnitId",
                table: "VehicleAxleDetails",
                column: "AxleSweptPathMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleAxleDetails_TechnicallyPermissibleMaxWeightOnAxleUni~",
                table: "VehicleAxleDetails",
                column: "TechnicallyPermissibleMaxWeightOnAxleUnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleAxleDetails_NsiValues_AxleSweptPathMeasureUnitId",
                table: "VehicleAxleDetails",
                column: "AxleSweptPathMeasureUnitId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleAxleDetails_NsiValues_TechnicallyPermissibleMaxWeigh~",
                table: "VehicleAxleDetails",
                column: "TechnicallyPermissibleMaxWeightOnAxleUnitId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleAxleDetails_NsiValues_AxleSweptPathMeasureUnitId",
                table: "VehicleAxleDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleAxleDetails_NsiValues_TechnicallyPermissibleMaxWeigh~",
                table: "VehicleAxleDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleAxleDetails_AxleSweptPathMeasureUnitId",
                table: "VehicleAxleDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleAxleDetails_TechnicallyPermissibleMaxWeightOnAxleUni~",
                table: "VehicleAxleDetails");

            migrationBuilder.DropColumn(
                name: "AxleSweptPathMeasureUnitId",
                table: "VehicleAxleDetails");

            migrationBuilder.DropColumn(
                name: "TechnicallyPermissibleMaxWeightOnAxleUnitId",
                table: "VehicleAxleDetails");
        }
    }
}

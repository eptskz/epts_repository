﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VahicleCabin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CharacteristicsDetail_NsiValues_EngineTypeId",
                table: "CharacteristicsDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_PoweredWheelLocationId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_VehicleWheelFormulaId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.DropColumn(
                name: "CarriageSpaceImplementation",
                table: "VehicleCabins");

            migrationBuilder.AddColumn<int>(
                name: "NotVehicleModificationIndicator",
                table: "VehicleTypeDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "VehicleWheelQuantity",
                table: "VehicleRunningGearDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleWheelFormulaId",
                table: "VehicleRunningGearDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleAxleQuantity",
                table: "VehicleRunningGearDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "PoweredWheelLocationId",
                table: "VehicleRunningGearDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<string>(
                name: "CabinDescription",
                table: "VehicleCabins",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EngineTypeId",
                table: "CharacteristicsDetail",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_CharacteristicsDetail_NsiValues_EngineTypeId",
                table: "CharacteristicsDetail",
                column: "EngineTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_PoweredWheelLocationId",
                table: "VehicleRunningGearDetails",
                column: "PoweredWheelLocationId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_VehicleWheelFormulaId",
                table: "VehicleRunningGearDetails",
                column: "VehicleWheelFormulaId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CharacteristicsDetail_NsiValues_EngineTypeId",
                table: "CharacteristicsDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_PoweredWheelLocationId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_VehicleWheelFormulaId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.DropColumn(
                name: "NotVehicleModificationIndicator",
                table: "VehicleTypeDetails");

            migrationBuilder.DropColumn(
                name: "CabinDescription",
                table: "VehicleCabins");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleWheelQuantity",
                table: "VehicleRunningGearDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "VehicleWheelFormulaId",
                table: "VehicleRunningGearDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "VehicleAxleQuantity",
                table: "VehicleRunningGearDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PoweredWheelLocationId",
                table: "VehicleRunningGearDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CarriageSpaceImplementation",
                table: "VehicleCabins",
                type: "text",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EngineTypeId",
                table: "CharacteristicsDetail",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CharacteristicsDetail_NsiValues_EngineTypeId",
                table: "CharacteristicsDetail",
                column: "EngineTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_PoweredWheelLocationId",
                table: "VehicleRunningGearDetails",
                column: "PoweredWheelLocationId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleRunningGearDetails_NsiValues_VehicleWheelFormulaId",
                table: "VehicleRunningGearDetails",
                column: "VehicleWheelFormulaId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Transmission_TypeUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitDetails_CharacteristicsDetail_Characteristi~",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitDetails_TransmissionTypes_TransmissionTypeId",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropIndex(
                name: "IX_TransmissionUnitDetails_CharacteristicsDetailId",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicId",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "TransmissionUnitDetails");

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionTypeId",
                table: "TransmissionUnitDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitDetails_TransmissionTypes_TransmissionTypeId",
                table: "TransmissionUnitDetails",
                column: "TransmissionTypeId",
                principalTable: "TransmissionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitDetails_TransmissionTypes_TransmissionTypeId",
                table: "TransmissionUnitDetails");

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionTypeId",
                table: "TransmissionUnitDetails",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicId",
                table: "TransmissionUnitDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "TransmissionUnitDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionUnitDetails_CharacteristicsDetailId",
                table: "TransmissionUnitDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitDetails_CharacteristicsDetail_Characteristi~",
                table: "TransmissionUnitDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitDetails_TransmissionTypes_TransmissionTypeId",
                table: "TransmissionUnitDetails",
                column: "TransmissionTypeId",
                principalTable: "TransmissionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class DigitalPassportAdditional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeclineDate",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeclineDescription",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Deleted",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DocType",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "Modified",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_StatusId",
                table: "DigitalPassportDocument",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_DicStatuses_StatusId",
                table: "DigitalPassportDocument",
                column: "StatusId",
                principalTable: "DicStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_DicStatuses_StatusId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_StatusId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "DeclineDate",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "DeclineDescription",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "DocType",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "Modified",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "DigitalPassportDocument");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_ComplienceDocument_DocEndDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocStartEnd",
                table: "ComplienceDocuments");

            migrationBuilder.AddColumn<DateTime>(
                name: "DocEndDate",
                table: "ComplienceDocuments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocEndDate",
                table: "ComplienceDocuments");

            migrationBuilder.AddColumn<DateTime>(
                name: "DocStartEnd",
                table: "ComplienceDocuments",
                type: "timestamp without time zone",
                nullable: true);

        }
    }
}

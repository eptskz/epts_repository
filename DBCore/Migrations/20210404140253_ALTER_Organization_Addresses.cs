﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class ALTER_Organization_Addresses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicOrganizationTypes_OrganizationTypeId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_Regions_RegionId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_OrganizationTypeId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_RegionId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "OrganizationTypeId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "RegionId",
                table: "Organizations");

            migrationBuilder.AddColumn<int>(
                name: "FactDistinctOrCityId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FactOkrugOrCityId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FactPostalIndex",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FactRegionOrCityId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FactVillageId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JuridicalDistinctOrCityId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JuridicalOkrugOrCityId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JuridicalPostalIndex",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JuridicalRegionOrCityId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JuridicalVillageId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortNameEn",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortNameKz",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortNameRu",
                table: "Organizations",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "OrganizationType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OrganizationId = table.Column<int>(nullable: false),
                    OrgTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganizationType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrganizationType_DicOrganizationTypes_OrgTypeId",
                        column: x => x.OrgTypeId,
                        principalTable: "DicOrganizationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrganizationType_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_FactDistinctOrCityId",
                table: "Organizations",
                column: "FactDistinctOrCityId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_FactOkrugOrCityId",
                table: "Organizations",
                column: "FactOkrugOrCityId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_FactRegionOrCityId",
                table: "Organizations",
                column: "FactRegionOrCityId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_FactVillageId",
                table: "Organizations",
                column: "FactVillageId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_JuridicalDistinctOrCityId",
                table: "Organizations",
                column: "JuridicalDistinctOrCityId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_JuridicalOkrugOrCityId",
                table: "Organizations",
                column: "JuridicalOkrugOrCityId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_JuridicalRegionOrCityId",
                table: "Organizations",
                column: "JuridicalRegionOrCityId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_JuridicalVillageId",
                table: "Organizations",
                column: "JuridicalVillageId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationType_OrgTypeId",
                table: "OrganizationType",
                column: "OrgTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationType_OrganizationId",
                table: "OrganizationType",
                column: "OrganizationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactDistinctOrCityId",
                table: "Organizations",
                column: "FactDistinctOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactOkrugOrCityId",
                table: "Organizations",
                column: "FactOkrugOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactRegionOrCityId",
                table: "Organizations",
                column: "FactRegionOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactVillageId",
                table: "Organizations",
                column: "FactVillageId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalDistinctOrCityId",
                table: "Organizations",
                column: "JuridicalDistinctOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalOkrugOrCityId",
                table: "Organizations",
                column: "JuridicalOkrugOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalRegionOrCityId",
                table: "Organizations",
                column: "JuridicalRegionOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalVillageId",
                table: "Organizations",
                column: "JuridicalVillageId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactVillageId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalVillageId",
                table: "Organizations");

            migrationBuilder.DropTable(
                name: "OrganizationType");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_FactDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_FactOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_FactRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_FactVillageId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_JuridicalDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_JuridicalOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_JuridicalRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_JuridicalVillageId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FactDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FactOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FactPostalIndex",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FactRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "FactVillageId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JuridicalDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JuridicalOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JuridicalPostalIndex",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JuridicalRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "JuridicalVillageId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "ShortNameEn",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "ShortNameKz",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "ShortNameRu",
                table: "Organizations");

            migrationBuilder.AddColumn<int>(
                name: "OrganizationTypeId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RegionId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_OrganizationTypeId",
                table: "Organizations",
                column: "OrganizationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_RegionId",
                table: "Organizations",
                column: "RegionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicOrganizationTypes_OrganizationTypeId",
                table: "Organizations",
                column: "OrganizationTypeId",
                principalTable: "DicOrganizationTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_Regions_RegionId",
                table: "Organizations",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

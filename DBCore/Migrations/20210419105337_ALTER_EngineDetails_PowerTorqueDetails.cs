﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class ALTER_EngineDetails_PowerTorqueDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnginePowerDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EngineDetailId = table.Column<int>(nullable: false),
                    EngineMaxPowerMeasure = table.Column<double>(nullable: false),
                    EngineMaxPowerMeasureUnitId = table.Column<int>(nullable: false),
                    EngineMaxPowerShaftRotationFrequencyMinMeasure = table.Column<double>(nullable: false),
                    EngineMaxPowerShaftRotationFrequencyMaxMeasure = table.Column<double>(nullable: true),
                    EngineMaxPowerShaftRotationFrequencyMeasureUnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnginePowerDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnginePowerDetails_EngineDetails_EngineDetailId",
                        column: x => x.EngineDetailId,
                        principalTable: "EngineDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EnginePowerDetails_NsiValues_EngineMaxPowerMeasureUnitId",
                        column: x => x.EngineMaxPowerMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EnginePowerDetails_NsiValues_EngineMaxPowerShaftRotationFre~",
                        column: x => x.EngineMaxPowerShaftRotationFrequencyMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EngineTorqueDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EngineDetailId = table.Column<int>(nullable: false),
                    EngineMaxTorqueMeasure = table.Column<double>(nullable: false),
                    EngineMaxTorqueMeasureUnitId = table.Column<int>(nullable: false),
                    EngineMaxTorqueMeasureShaftRotationFrequencyMinMeasure = table.Column<double>(nullable: false),
                    EngineMaxTorqueMeasureShaftRotationFrequencyMaxMeasure = table.Column<double>(nullable: true),
                    EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EngineTorqueDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EngineTorqueDetails_EngineDetails_EngineDetailId",
                        column: x => x.EngineDetailId,
                        principalTable: "EngineDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineTorqueDetails_NsiValues_EngineMaxTorqueMeasureShaftRo~",
                        column: x => x.EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EngineTorqueDetails_NsiValues_EngineMaxTorqueMeasureUnitId",
                        column: x => x.EngineMaxTorqueMeasureUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EnginePowerDetails_EngineDetailId",
                table: "EnginePowerDetails",
                column: "EngineDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_EnginePowerDetails_EngineMaxPowerMeasureUnitId",
                table: "EnginePowerDetails",
                column: "EngineMaxPowerMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EnginePowerDetails_EngineMaxPowerShaftRotationFrequencyMeas~",
                table: "EnginePowerDetails",
                column: "EngineMaxPowerShaftRotationFrequencyMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineTorqueDetails_EngineDetailId",
                table: "EngineTorqueDetails",
                column: "EngineDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineTorqueDetails_EngineMaxTorqueMeasureShaftRotationFreq~",
                table: "EngineTorqueDetails",
                column: "EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineTorqueDetails_EngineMaxTorqueMeasureUnitId",
                table: "EngineTorqueDetails",
                column: "EngineMaxTorqueMeasureUnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EnginePowerDetails");

            migrationBuilder.DropTable(
                name: "EngineTorqueDetails");
        }
    }
}

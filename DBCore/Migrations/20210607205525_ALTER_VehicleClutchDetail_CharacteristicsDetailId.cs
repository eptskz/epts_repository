﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleClutchDetail_CharacteristicsDetailId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleClutchDetails_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleClutchDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicId",
                table: "VehicleClutchDetails");

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleClutchDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleClutchDetails_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleClutchDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleClutchDetails_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleClutchDetails");

            migrationBuilder.AlterColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleClutchDetails",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicId",
                table: "VehicleClutchDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleClutchDetails_CharacteristicsDetail_CharacteristicsD~",
                table: "VehicleClutchDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleLabelingDetail_DataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DataTypeId",
                table: "VehicleVinCharacterDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExcludeCharStr",
                table: "VehicleVinCharacterDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IncludeCharStr",
                table: "VehicleVinCharacterDetail",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsAlpha",
                table: "VehicleVinCharacterDetail",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDigit",
                table: "VehicleVinCharacterDetail",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsExcludeChar",
                table: "VehicleVinCharacterDetail",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsIncludeChar",
                table: "VehicleVinCharacterDetail",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleVinCharacterDetail_DataTypeId",
                table: "VehicleVinCharacterDetail",
                column: "DataTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleVinCharacterDetail_NsiValues_DataTypeId",
                table: "VehicleVinCharacterDetail",
                column: "DataTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleVinCharacterDetail_NsiValues_DataTypeId",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.DropIndex(
                name: "IX_VehicleVinCharacterDetail_DataTypeId",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.DropColumn(
                name: "DataTypeId",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.DropColumn(
                name: "ExcludeCharStr",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.DropColumn(
                name: "IncludeCharStr",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.DropColumn(
                name: "IsAlpha",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.DropColumn(
                name: "IsDigit",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.DropColumn(
                name: "IsExcludeChar",
                table: "VehicleVinCharacterDetail");

            migrationBuilder.DropColumn(
                name: "IsIncludeChar",
                table: "VehicleVinCharacterDetail");
        }
    }
}

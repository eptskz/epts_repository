﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class PassportHistoryAndExternalInteractHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
               name: "AssemblerOrganization");

            migrationBuilder.DropTable(
                name: "MeaningExplanation");

            migrationBuilder.DropTable(
                name: "MemberOrganization");

            migrationBuilder.DropTable(
                name: "VehicleEngine");

            migrationBuilder.DropTable(
                name: "VehicleIdentLocation");

            migrationBuilder.DropTable(
                name: "VehicleImage");

            migrationBuilder.DropTable(
                name: "VehicleStructureCard");

            migrationBuilder.DropTable(
                name: "EnginePlateDetail");

            migrationBuilder.DropTable(
                name: "ManufacturerPlateDetail");

            migrationBuilder.DropTable(
                name: "DigitalPassportDocument");

            migrationBuilder.CreateTable(
                name: "DigitalPassportDocument",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UniqueCode = table.Column<string>(nullable: true),
                    VehicleEPassportBaseCode = table.Column<string>(nullable: true),
                    VehicleEPassportBaseDescription = table.Column<string>(nullable: true),
                    DocKindCode = table.Column<string>(nullable: true),
                    DocDateFrom = table.Column<DateTime>(nullable: true),
                    DocDateTo = table.Column<DateTime>(nullable: true),
                    DocId = table.Column<string>(nullable: true),
                    AuthorityName = table.Column<string>(nullable: true),
                    DocumentNotExistDescription = table.Column<string>(nullable: true),
                    DocKindCodeHandy = table.Column<string>(nullable: true),
                    DocNameHandy = table.Column<string>(nullable: true),
                    DocAuthorHandy = table.Column<string>(nullable: true),
                    DocIdHandy = table.Column<string>(nullable: true),
                    DocDateHandy = table.Column<DateTime>(nullable: true),
                    DocPageCountHandy = table.Column<string>(nullable: true),
                    documentNotExists = table.Column<bool>(nullable: false),
                    VehicleIdentityNumberIndicator = table.Column<string>(nullable: true),
                    VehicleIdentityNumberId = table.Column<string>(nullable: true),
                    NotVehicleIdentityNumberIndicator = table.Column<bool>(nullable: false),
                    VehicleCategoryCode = table.Column<string>(nullable: true),
                    NotVehicleEngineIdentityNumberIndicator = table.Column<bool>(nullable: false),
                    VehicleFrameIdentityNumberId = table.Column<string>(nullable: true),
                    NotVehicleFrameIdentityNumberIndicator = table.Column<bool>(nullable: false),
                    VehicleBodyIdentityNumberId = table.Column<string>(nullable: true),
                    NotVehicleBodyIdentityNumberIndicator = table.Column<bool>(nullable: false),
                    VehicleEmergencyCallIdentityNumberId = table.Column<string>(nullable: true),
                    NotVehicleEmergencyCallIdentityNumberIndicator = table.Column<bool>(nullable: false),
                    BodyMultiColourIndicator = table.Column<bool>(nullable: false),
                    VehicleBodyColourCode = table.Column<string>(nullable: true),
                    VehicleBodyColourCode2 = table.Column<string>(nullable: true),
                    VehicleBodyColourCode3 = table.Column<string>(nullable: true),
                    VehicleBodyColourName = table.Column<string>(nullable: true),
                    ManufactureYear = table.Column<string>(nullable: true),
                    ManufactureMonth = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    DocValidityDate = table.Column<DateTime>(nullable: true),
                    DocCreationDate = table.Column<DateTime>(nullable: true),
                    VehicleMakeName = table.Column<string>(nullable: true),
                    NotVehicleMakeNameIndicator = table.Column<bool>(nullable: false),
                    VehicleCommercialName = table.Column<string>(nullable: true),
                    NotVehicleCommercialNameIndicator = table.Column<bool>(nullable: false),
                    VehicleTypeId = table.Column<string>(nullable: true),
                    VehicleTypeVariantId = table.Column<string>(nullable: true),
                    notVehicleModificationIndicator = table.Column<bool>(nullable: false),
                    VehicleTechCategoryCode = table.Column<string>(nullable: true),
                    VehicleEcoClassCode = table.Column<string>(nullable: true),
                    VehicleManufactureOption = table.Column<string>(nullable: true),
                    NotBaseVehicleIndicator = table.Column<bool>(nullable: false),
                    BaseVehicleMakeName = table.Column<string>(nullable: true),
                    BaseVehicleTypeId = table.Column<string>(nullable: true),
                    BaseVehicleCommercialName = table.Column<string>(nullable: true),
                    BaseVehicleTypeVariantId = table.Column<string>(nullable: true),
                    BaseDocIndicator = table.Column<string>(nullable: true),
                    BaseVehicleEPassportId = table.Column<string>(nullable: true),
                    BaseDocCreationDate = table.Column<DateTime>(nullable: true),
                    NotManufacturerIndicator = table.Column<bool>(nullable: false),
                    ManufactureOrganizationId = table.Column<int>(nullable: false),
                    NotMemberOrganizationIndicator = table.Column<bool>(nullable: false),
                    NotAssemblerOrganizationIndicator = table.Column<bool>(nullable: false),
                    SbktsDocId = table.Column<string>(nullable: true),
                    SbktsStartDate = table.Column<DateTime>(nullable: true),
                    SbktsDocValidityDate = table.Column<DateTime>(nullable: true),
                    CertifiedMemberFullName = table.Column<string>(nullable: true),
                    VehicleMovementPermitIndicator = table.Column<bool>(nullable: false),
                    ShassisMovePermitionText = table.Column<string>(nullable: true),
                    AllRoadMoveAbilityLimit = table.Column<string>(nullable: true),
                    MinibusAbility = table.Column<string>(nullable: true),
                    OtherInformation = table.Column<string>(nullable: true),
                    ManufactureWithPrivilegeMode = table.Column<string>(nullable: true),
                    AcceptTerritoryRegistration = table.Column<string>(nullable: true),
                    AcceptCountryRegistration = table.Column<string>(nullable: true),
                    NotSateliteNavigationIndicator = table.Column<bool>(nullable: false),
                    SateliteNavigationDescription = table.Column<string>(nullable: true),
                    NotVehicleOfJobControllIndicator = table.Column<bool>(nullable: false),
                    VehicleOfJobControllDescription = table.Column<string>(nullable: true),
                    VehicleMadeInCountry = table.Column<string>(nullable: true),
                    DigitalPassportRegisterBaseDescription = table.Column<string>(nullable: true),
                    ManufacturerInformation = table.Column<string>(nullable: true),
                    ManufacturerPlateDetailId = table.Column<int>(nullable: true),
                    EnginePlateDetailId = table.Column<int>(nullable: true),
                    DocKindNsiId = table.Column<int>(nullable: true),
                    DocumentNotExistDescriptionNsiId = table.Column<int>(nullable: true),
                    DocKindCodeHandyNsiId = table.Column<int>(nullable: true),
                    VehicleCategoryCodeNsiId = table.Column<int>(nullable: true),
                    VehicleBodyColourCodeNsiId = table.Column<int>(nullable: true),
                    VehicleBodyColourCode2NsiId = table.Column<int>(nullable: true),
                    VehicleBodyColourCode3NsiId = table.Column<int>(nullable: true),
                    ManufactureMonthNsiId = table.Column<int>(nullable: true),
                    VehicleMakeNameNsiId = table.Column<int>(nullable: true),
                    VehicleTechCategoryCodeNsiId = table.Column<int>(nullable: true),
                    VehicleEcoClassCodeNsiId = table.Column<int>(nullable: true),
                    VehicleManufactureOptionNsiId = table.Column<int>(nullable: true),
                    ManufactureWithPrivilegeModeNsiId = table.Column<int>(nullable: true),
                    AcceptCountryRegistrationNsiId = table.Column<int>(nullable: true),
                    VehicleMadeInCountryNsiId = table.Column<int>(nullable: true),
                    DocType = table.Column<int>(nullable: false),
                    DeclineDate = table.Column<DateTime>(nullable: true),
                    DeclineDescription = table.Column<string>(nullable: true),
                    AspNetUserId = table.Column<string>(nullable: true),
                    StatusNsiId = table.Column<int>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<DateTime>(nullable: true),
                    KgdDataId = table.Column<int>(nullable: true),
                    MvdDataId = table.Column<int>(nullable: true),
                    ComplienceDocumentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalPassportDocument", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_AcceptCountryRegistration~",
                        column: x => x.AcceptCountryRegistrationNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_AspNetUsers_AspNetUserId",
                        column: x => x.AspNetUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_ComplienceDocuments_ComplienceDocum~",
                        column: x => x.ComplienceDocumentId,
                        principalTable: "ComplienceDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_DocKindCodeHandyNsiId",
                        column: x => x.DocKindCodeHandyNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_DocKindNsiId",
                        column: x => x.DocKindNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_DocumentNotExistDescripti~",
                        column: x => x.DocumentNotExistDescriptionNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_KgdDatas_KgdDataId",
                        column: x => x.KgdDataId,
                        principalTable: "KgdDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_ManufactureMonthNsiId",
                        column: x => x.ManufactureMonthNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_ManufactureWithPrivilegeM~",
                        column: x => x.ManufactureWithPrivilegeModeNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_MvdDatas_MvdDataId",
                        column: x => x.MvdDataId,
                        principalTable: "MvdDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_StatusNsiId",
                        column: x => x.StatusNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_VehicleBodyColourCode2Nsi~",
                        column: x => x.VehicleBodyColourCode2NsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_VehicleBodyColourCode3Nsi~",
                        column: x => x.VehicleBodyColourCode3NsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_VehicleBodyColourCodeNsiId",
                        column: x => x.VehicleBodyColourCodeNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_VehicleCategoryCodeNsiId",
                        column: x => x.VehicleCategoryCodeNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_VehicleEcoClassCodeNsiId",
                        column: x => x.VehicleEcoClassCodeNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_VehicleMadeInCountryNsiId",
                        column: x => x.VehicleMadeInCountryNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_VehicleMakeNameNsiId",
                        column: x => x.VehicleMakeNameNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_VehicleManufactureOptionN~",
                        column: x => x.VehicleManufactureOptionNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalPassportDocument_NsiValues_VehicleTechCategoryCodeNs~",
                        column: x => x.VehicleTechCategoryCodeNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssemblerOrganization",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NotAssemblerIndicator = table.Column<bool>(nullable: false),
                    AssemblerOrganizationId = table.Column<int>(nullable: false),
                    DigitalPassportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssemblerOrganization", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssemblerOrganization_DigitalPassportDocument_DigitalPasspo~",
                        column: x => x.DigitalPassportId,
                        principalTable: "DigitalPassportDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DigitalPassportStatusHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Description = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<DateTime>(nullable: true),
                    StatusNsiId = table.Column<int>(nullable: false),
                    DigitalPassportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalPassportStatusHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalPassportStatusHistory_DigitalPassportDocument_Digita~",
                        column: x => x.DigitalPassportId,
                        principalTable: "DigitalPassportDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalPassportStatusHistory_NsiValues_StatusNsiId",
                        column: x => x.StatusNsiId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EnginePlateDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DigitalPassportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnginePlateDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnginePlateDetail_DigitalPassportDocument_DigitalPassportId",
                        column: x => x.DigitalPassportId,
                        principalTable: "DigitalPassportDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExternalInteractHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ServiceName = table.Column<string>(nullable: true),
                    StatusCode = table.Column<string>(nullable: true),
                    Queue = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<DateTime>(nullable: true),
                    DigitalPassportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalInteractHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExternalInteractHistory_DigitalPassportDocument_DigitalPass~",
                        column: x => x.DigitalPassportId,
                        principalTable: "DigitalPassportDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ManufacturerPlateDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NotManufacturerPlateLocationIndicator = table.Column<bool>(nullable: false),
                    ManufacturerPlateLocation = table.Column<string>(nullable: true),
                    VehicleIdentLocation = table.Column<string>(nullable: true),
                    VehicleIdentStructure = table.Column<string>(nullable: true),
                    DigitalPassportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManufacturerPlateDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ManufacturerPlateDetail_DigitalPassportDocument_DigitalPass~",
                        column: x => x.DigitalPassportId,
                        principalTable: "DigitalPassportDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MemberOrganization",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NotMemberIndicator = table.Column<bool>(nullable: false),
                    MemberOrganizationId = table.Column<int>(nullable: false),
                    DigitalPassportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberOrganization", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberOrganization_DigitalPassportDocument_DigitalPassportId",
                        column: x => x.DigitalPassportId,
                        principalTable: "DigitalPassportDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleEngine",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    VehicleEngineIdentityNumberId = table.Column<string>(nullable: true),
                    DigitalPassportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleEngine", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleEngine_DigitalPassportDocument_DigitalPassportId",
                        column: x => x.DigitalPassportId,
                        principalTable: "DigitalPassportDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleImage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    VehiclePicture = table.Column<byte[]>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    DigitalPassportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleImage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleImage_DigitalPassportDocument_DigitalPassportId",
                        column: x => x.DigitalPassportId,
                        principalTable: "DigitalPassportDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleIdentLocation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Location = table.Column<string>(nullable: true),
                    EnginePlateDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleIdentLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleIdentLocation_EnginePlateDetail_EnginePlateDetailId",
                        column: x => x.EnginePlateDetailId,
                        principalTable: "EnginePlateDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleStructureCard",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Start = table.Column<string>(nullable: true),
                    End = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ManufacturerPlateDetailId = table.Column<int>(nullable: true),
                    EnginePlateDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleStructureCard", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleStructureCard_EnginePlateDetail_EnginePlateDetailId",
                        column: x => x.EnginePlateDetailId,
                        principalTable: "EnginePlateDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleStructureCard_ManufacturerPlateDetail_ManufacturerPl~",
                        column: x => x.ManufacturerPlateDetailId,
                        principalTable: "ManufacturerPlateDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MeaningExplanation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Explanation = table.Column<string>(nullable: true),
                    Meaning = table.Column<string>(nullable: true),
                    VehicleStructureCardId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeaningExplanation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MeaningExplanation_VehicleStructureCard_VehicleStructureCar~",
                        column: x => x.VehicleStructureCardId,
                        principalTable: "VehicleStructureCard",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssemblerOrganization_DigitalPassportId",
                table: "AssemblerOrganization",
                column: "DigitalPassportId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_AcceptCountryRegistrationNsiId",
                table: "DigitalPassportDocument",
                column: "AcceptCountryRegistrationNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_AspNetUserId",
                table: "DigitalPassportDocument",
                column: "AspNetUserId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_ComplienceDocumentId",
                table: "DigitalPassportDocument",
                column: "ComplienceDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_DocKindCodeHandyNsiId",
                table: "DigitalPassportDocument",
                column: "DocKindCodeHandyNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_DocKindNsiId",
                table: "DigitalPassportDocument",
                column: "DocKindNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_DocumentNotExistDescriptionNsiId",
                table: "DigitalPassportDocument",
                column: "DocumentNotExistDescriptionNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_KgdDataId",
                table: "DigitalPassportDocument",
                column: "KgdDataId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_ManufactureMonthNsiId",
                table: "DigitalPassportDocument",
                column: "ManufactureMonthNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_ManufactureWithPrivilegeModeNsiId",
                table: "DigitalPassportDocument",
                column: "ManufactureWithPrivilegeModeNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_MvdDataId",
                table: "DigitalPassportDocument",
                column: "MvdDataId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_StatusNsiId",
                table: "DigitalPassportDocument",
                column: "StatusNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_UniqueCode",
                table: "DigitalPassportDocument",
                column: "UniqueCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleBodyColourCode2NsiId",
                table: "DigitalPassportDocument",
                column: "VehicleBodyColourCode2NsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleBodyColourCode3NsiId",
                table: "DigitalPassportDocument",
                column: "VehicleBodyColourCode3NsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleBodyColourCodeNsiId",
                table: "DigitalPassportDocument",
                column: "VehicleBodyColourCodeNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleCategoryCodeNsiId",
                table: "DigitalPassportDocument",
                column: "VehicleCategoryCodeNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleEcoClassCodeNsiId",
                table: "DigitalPassportDocument",
                column: "VehicleEcoClassCodeNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleMadeInCountryNsiId",
                table: "DigitalPassportDocument",
                column: "VehicleMadeInCountryNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleMakeNameNsiId",
                table: "DigitalPassportDocument",
                column: "VehicleMakeNameNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleManufactureOptionNsiId",
                table: "DigitalPassportDocument",
                column: "VehicleManufactureOptionNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_VehicleTechCategoryCodeNsiId",
                table: "DigitalPassportDocument",
                column: "VehicleTechCategoryCodeNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportStatusHistory_DigitalPassportId",
                table: "DigitalPassportStatusHistory",
                column: "DigitalPassportId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportStatusHistory_StatusNsiId",
                table: "DigitalPassportStatusHistory",
                column: "StatusNsiId");

            migrationBuilder.CreateIndex(
                name: "IX_EnginePlateDetail_DigitalPassportId",
                table: "EnginePlateDetail",
                column: "DigitalPassportId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ExternalInteractHistory_DigitalPassportId",
                table: "ExternalInteractHistory",
                column: "DigitalPassportId");

            migrationBuilder.CreateIndex(
                name: "IX_ManufacturerPlateDetail_DigitalPassportId",
                table: "ManufacturerPlateDetail",
                column: "DigitalPassportId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MeaningExplanation_VehicleStructureCardId",
                table: "MeaningExplanation",
                column: "VehicleStructureCardId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberOrganization_DigitalPassportId",
                table: "MemberOrganization",
                column: "DigitalPassportId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEngine_DigitalPassportId",
                table: "VehicleEngine",
                column: "DigitalPassportId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleIdentLocation_EnginePlateDetailId",
                table: "VehicleIdentLocation",
                column: "EnginePlateDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleImage_DigitalPassportId",
                table: "VehicleImage",
                column: "DigitalPassportId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleStructureCard_EnginePlateDetailId",
                table: "VehicleStructureCard",
                column: "EnginePlateDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleStructureCard_ManufacturerPlateDetailId",
                table: "VehicleStructureCard",
                column: "ManufacturerPlateDetailId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssemblerOrganization");

            migrationBuilder.DropTable(
                name: "DigitalPassportStatusHistory");

            migrationBuilder.DropTable(
                name: "ExternalInteractHistory");

            migrationBuilder.DropTable(
                name: "MeaningExplanation");

            migrationBuilder.DropTable(
                name: "MemberOrganization");

            migrationBuilder.DropTable(
                name: "VehicleEngine");

            migrationBuilder.DropTable(
                name: "VehicleIdentLocation");

            migrationBuilder.DropTable(
                name: "VehicleImage");

            migrationBuilder.DropTable(
                name: "VehicleStructureCard");

            migrationBuilder.DropTable(
                name: "EnginePlateDetail");

            migrationBuilder.DropTable(
                name: "ManufacturerPlateDetail");

            migrationBuilder.DropTable(
                name: "DigitalPassportDocument");

            migrationBuilder.CreateTable(
                name: "DigitalPassport",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BodyMultiColourIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    BusinessEntityId = table.Column<string>(type: "text", nullable: true),
                    BusinessEntityIdKindId = table.Column<string>(type: "text", nullable: true),
                    BusinessEntityName = table.Column<string>(type: "text", nullable: true),
                    CommonDocCreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ComplienceDocumentId = table.Column<Guid>(type: "uuid", nullable: false),
                    DocCreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DocExistIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    DocId = table.Column<string>(type: "text", nullable: true),
                    EDocId = table.Column<Guid>(type: "uuid", nullable: false),
                    EngineIdCharacterDetails = table.Column<string>(type: "text", nullable: true),
                    EngineIdentificationNumberLocation = table.Column<string>(type: "text", nullable: true),
                    EventDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    HandyVehicleEPassportId = table.Column<string>(type: "text", nullable: true),
                    IdCharacterQuantity = table.Column<byte>(type: "smallint", nullable: false),
                    IdCharacterStartingOrdinal = table.Column<byte>(type: "smallint", nullable: false),
                    IdCharacterText = table.Column<string>(type: "text", nullable: true),
                    IdCharacterValueCode = table.Column<string>(type: "text", nullable: true),
                    IdCharacterValueDetails = table.Column<string>(type: "text", nullable: true),
                    IdCharacterValueText = table.Column<string>(type: "text", nullable: true),
                    KgdDataId = table.Column<int>(type: "integer", nullable: false),
                    ManufactureMonth = table.Column<string>(type: "text", nullable: true),
                    ManufactureYear = table.Column<int>(type: "integer", nullable: false),
                    MvdDataId = table.Column<int>(type: "integer", nullable: false),
                    NoSafetyAuthorityName = table.Column<string>(type: "text", nullable: true),
                    NoSafetyDocCreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    NoSafetyDocId = table.Column<string>(type: "text", nullable: true),
                    NoSafetyDocKindCode = table.Column<string>(type: "text", nullable: true),
                    NoSafetyDocKindName = table.Column<string>(type: "text", nullable: true),
                    NoSafetyDocName = table.Column<string>(type: "text", nullable: true),
                    NoSafetyPageQuantity = table.Column<short>(type: "smallint", nullable: false),
                    NoSafetyUnifiedCountryCode = table.Column<string>(type: "text", nullable: true),
                    NotVehicleBodyIdentityNumberIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    NotVehicleEmergencyCallIdentityNumberIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    NotVehicleEngineIdentityNumberIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    NotVehicleFrameIdentityNumberIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    NotVehicleIdentityNumberIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    NoteText = table.Column<string>(type: "text", nullable: true),
                    OwnerDocDetails = table.Column<string>(type: "text", nullable: true),
                    PreferentialManufacturingCountryCode = table.Column<string>(type: "text", nullable: true),
                    PreferentialManufacturingModeText = table.Column<string>(type: "text", nullable: true),
                    PreferentialManufacturingRegionCode = table.Column<byte>(type: "smallint", nullable: false),
                    RegistrationTerritory = table.Column<string>(type: "text", nullable: true),
                    RegistrationTerritoryIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    SatelliteIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    SatelliteNumberId = table.Column<string>(type: "text", nullable: true),
                    TachographIndicator = table.Column<byte>(type: "smallint", nullable: false),
                    TachographNumberId = table.Column<string>(type: "text", nullable: true),
                    UnifiedCountryCode = table.Column<string>(type: "text", nullable: true),
                    VehicleBodyColourCode = table.Column<string>(type: "text", nullable: true),
                    VehicleBodyColourName = table.Column<string>(type: "text", nullable: true),
                    VehicleBodyIdDetails = table.Column<string>(type: "text", nullable: true),
                    VehicleBodyIdentityNumberId = table.Column<string>(type: "text", nullable: true),
                    VehicleCategoryCode = table.Column<string>(type: "text", nullable: true),
                    VehicleEPassportBaseCode = table.Column<string>(type: "text", nullable: true),
                    VehicleEPassportBaseDetails = table.Column<string>(type: "text", nullable: true),
                    VehicleEPassportId = table.Column<string>(type: "text", nullable: true),
                    VehicleEPassportKindCode = table.Column<string>(type: "text", nullable: true),
                    VehicleEPassportRegistrationReasonCode = table.Column<int>(type: "integer", nullable: false),
                    VehicleEPassportStatusCode = table.Column<string>(type: "text", nullable: true),
                    VehicleEmergencyCallDeviceIdDetails = table.Column<string>(type: "text", nullable: true),
                    VehicleEmergencyCallIdentityNumberId = table.Column<string>(type: "text", nullable: true),
                    VehicleEngineIdDetails = table.Column<string>(type: "text", nullable: true),
                    VehicleEngineIdentityNumberId = table.Column<string>(type: "text", nullable: true),
                    VehicleFrameIdDetails = table.Column<string>(type: "text", nullable: true),
                    VehicleFrameIdentityNumberId = table.Column<string>(type: "text", nullable: true),
                    VehicleIdDetails = table.Column<string>(type: "text", nullable: true),
                    VehicleIdInfoDetails = table.Column<string>(type: "text", nullable: true),
                    VehicleIdentityNumberId = table.Column<string>(type: "text", nullable: true),
                    VehicleImportCountryCode = table.Column<string>(type: "text", nullable: true),
                    VehicleSatelliteNavigationIdDetails = table.Column<string>(type: "text", nullable: true),
                    VehicleTachographIdDetails = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalPassport", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalPassport_ComplienceDocuments_ComplienceDocumentId",
                        column: x => x.ComplienceDocumentId,
                        principalTable: "ComplienceDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalPassport_KgdDatas_KgdDataId",
                        column: x => x.KgdDataId,
                        principalTable: "KgdDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalPassport_MvdDatas_MvdDataId",
                        column: x => x.MvdDataId,
                        principalTable: "MvdDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassport_ComplienceDocumentId",
                table: "DigitalPassport",
                column: "ComplienceDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassport_KgdDataId",
                table: "DigitalPassport",
                column: "KgdDataId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassport_MvdDataId",
                table: "DigitalPassport",
                column: "MvdDataId");
        }
    }
}

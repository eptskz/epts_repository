﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class DigitalPassportAddDocTypeUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_DicDocTypes_DocTypeId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_DocTypeId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "DocTypeId",
                table: "DigitalPassportDocument");

            migrationBuilder.AddColumn<int>(
                name: "DocType",
                table: "DigitalPassportDocument",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocType",
                table: "DigitalPassportDocument");

            migrationBuilder.AddColumn<int>(
                name: "DocTypeId",
                table: "DigitalPassportDocument",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_DocTypeId",
                table: "DigitalPassportDocument",
                column: "DocTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_DicDocTypes_DocTypeId",
                table: "DigitalPassportDocument",
                column: "DocTypeId",
                principalTable: "DicDocTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleBodyworkDetails_Characteristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update public.\"VehicleBodyworkDetails\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");
            
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleBodyworkDetails_CharacteristicsDetail_Characteristic~",
                table: "VehicleBodyworkDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleBodyworkDetails_CharacteristicsDetailId",
                table: "VehicleBodyworkDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleBodyworkDetails");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleBodyworkDetails_CharacteristicId",
                table: "VehicleBodyworkDetails",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleBodyworkDetails_CharacteristicsDetail_Characteristic~",
                table: "VehicleBodyworkDetails",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleBodyworkDetails_CharacteristicsDetail_Characteristic~",
                table: "VehicleBodyworkDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleBodyworkDetails_CharacteristicId",
                table: "VehicleBodyworkDetails");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleBodyworkDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleBodyworkDetails_CharacteristicsDetailId",
                table: "VehicleBodyworkDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleBodyworkDetails_CharacteristicsDetail_Characteristic~",
                table: "VehicleBodyworkDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_EngineDetail_RemovePower : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_EngineMaxPowerMeasureUnitId",
                table: "EngineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_EngineMaxPowerShaftRotationFrequenc~",
                table: "EngineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_EngineMaxTorqueMeasureShaftRotation~",
                table: "EngineDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineDetails_NsiValues_EngineMaxTorqueMeasureUnitId",
                table: "EngineDetails");

            migrationBuilder.DropIndex(
                name: "IX_EngineDetails_EngineMaxPowerMeasureUnitId",
                table: "EngineDetails");

            migrationBuilder.DropIndex(
                name: "IX_EngineDetails_EngineMaxPowerShaftRotationFrequencyMeasureUn~",
                table: "EngineDetails");

            migrationBuilder.DropIndex(
                name: "IX_EngineDetails_EngineMaxTorqueMeasureShaftRotationFrequencyM~",
                table: "EngineDetails");

            migrationBuilder.DropIndex(
                name: "IX_EngineDetails_EngineMaxTorqueMeasureUnitId",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxPowerMeasure",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxPowerMeasureUnitId",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxPowerShaftRotationFrequencyMaxMeasure",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxPowerShaftRotationFrequencyMeasureUnitId",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxPowerShaftRotationFrequencyMinMeasure",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxTorqueMeasure",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxTorqueMeasureShaftRotationFrequencyMaxMeasure",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxTorqueMeasureShaftRotationFrequencyMinMeasure",
                table: "EngineDetails");

            migrationBuilder.DropColumn(
                name: "EngineMaxTorqueMeasureUnitId",
                table: "EngineDetails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "EngineMaxPowerMeasure",
                table: "EngineDetails",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "EngineMaxPowerMeasureUnitId",
                table: "EngineDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "EngineMaxPowerShaftRotationFrequencyMaxMeasure",
                table: "EngineDetails",
                type: "double precision",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EngineMaxPowerShaftRotationFrequencyMeasureUnitId",
                table: "EngineDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "EngineMaxPowerShaftRotationFrequencyMinMeasure",
                table: "EngineDetails",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "EngineMaxTorqueMeasure",
                table: "EngineDetails",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "EngineMaxTorqueMeasureShaftRotationFrequencyMaxMeasure",
                table: "EngineDetails",
                type: "double precision",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId",
                table: "EngineDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "EngineMaxTorqueMeasureShaftRotationFrequencyMinMeasure",
                table: "EngineDetails",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "EngineMaxTorqueMeasureUnitId",
                table: "EngineDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineMaxPowerMeasureUnitId",
                table: "EngineDetails",
                column: "EngineMaxPowerMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineMaxPowerShaftRotationFrequencyMeasureUn~",
                table: "EngineDetails",
                column: "EngineMaxPowerShaftRotationFrequencyMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineMaxTorqueMeasureShaftRotationFrequencyM~",
                table: "EngineDetails",
                column: "EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_EngineDetails_EngineMaxTorqueMeasureUnitId",
                table: "EngineDetails",
                column: "EngineMaxTorqueMeasureUnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_EngineMaxPowerMeasureUnitId",
                table: "EngineDetails",
                column: "EngineMaxPowerMeasureUnitId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_EngineMaxPowerShaftRotationFrequenc~",
                table: "EngineDetails",
                column: "EngineMaxPowerShaftRotationFrequencyMeasureUnitId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_EngineMaxTorqueMeasureShaftRotation~",
                table: "EngineDetails",
                column: "EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineDetails_NsiValues_EngineMaxTorqueMeasureUnitId",
                table: "EngineDetails",
                column: "EngineMaxTorqueMeasureUnitId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

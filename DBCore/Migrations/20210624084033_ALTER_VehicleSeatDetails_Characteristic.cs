﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleSeatDetails_Characteristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update public.\"VehicleSeatDetails\" set \"CharacteristicId\"=\"CharacteristicsDetailId\" where \"CharacteristicsDetailId\" is not null");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleSeatDetails_CharacteristicsDetail_CharacteristicsDet~",
                table: "VehicleSeatDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleSeatDetails_CharacteristicsDetailId",
                table: "VehicleSeatDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleSeatDetails");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSeatDetails_CharacteristicId",
                table: "VehicleSeatDetails",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleSeatDetails_CharacteristicsDetail_CharacteristicId",
                table: "VehicleSeatDetails",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleSeatDetails_CharacteristicsDetail_CharacteristicId",
                table: "VehicleSeatDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleSeatDetails_CharacteristicId",
                table: "VehicleSeatDetails");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleSeatDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSeatDetails_CharacteristicsDetailId",
                table: "VehicleSeatDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleSeatDetails_CharacteristicsDetail_CharacteristicsDet~",
                table: "VehicleSeatDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

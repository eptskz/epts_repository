﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_VehicleRunningGearDetail_Characteristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleRunningGearDetails_CharacteristicsDetail_Characteris~",
                table: "VehicleRunningGearDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleRunningGearDetails_CharacteristicsDetailId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.DropColumn(
                name: "CharacteristicsDetailId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRunningGearDetails_CharacteristicId",
                table: "VehicleRunningGearDetails",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleRunningGearDetails_CharacteristicsDetail",
                table: "VehicleRunningGearDetails",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleRunningGearDetails_CharacteristicsDetail",
                table: "VehicleRunningGearDetails");

            migrationBuilder.DropIndex(
                name: "IX_VehicleRunningGearDetails_CharacteristicId",
                table: "VehicleRunningGearDetails");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsDetailId",
                table: "VehicleRunningGearDetails",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRunningGearDetails_CharacteristicsDetailId",
                table: "VehicleRunningGearDetails",
                column: "CharacteristicsDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleRunningGearDetails_CharacteristicsDetail_Characteris~",
                table: "VehicleRunningGearDetails",
                column: "CharacteristicsDetailId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

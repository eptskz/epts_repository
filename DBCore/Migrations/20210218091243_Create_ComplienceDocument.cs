﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class Create_ComplienceDocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DicDocTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Code = table.Column<string>(nullable: true),
                    NameRu = table.Column<string>(nullable: true),
                    NameKz = table.Column<string>(nullable: true),
                    NameEn = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DicDocTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ComplienceDocuments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DocSeries = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    DocNumber = table.Column<string>(nullable: true),
                    DocName = table.Column<string>(nullable: true),
                    DocDate = table.Column<DateTime>(nullable: false),
                    DocStartDate = table.Column<DateTime>(nullable: true),
                    DocStartEnd = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DocTypeId = table.Column<int>(nullable: false),
                    Deleted = table.Column<DateTime>(nullable: true),
                    DocDistributionSign = table.Column<int>(nullable: true),
                    BatchCount = table.Column<int>(nullable: true),
                    CountryId = table.Column<int>(nullable: true),
                    AuthorityId = table.Column<int>(nullable: true),
                    ApplicantId = table.Column<int>(nullable: true),
                    ManufacturerId = table.Column<int>(nullable: true),
                    RepresentativeManufacturerId = table.Column<int>(nullable: true),
                    AssemblyPlantId = table.Column<int>(nullable: true),
                    AssemblyKitSupplierId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComplienceDocuments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComplienceDocuments_Organizations_ApplicantId",
                        column: x => x.ApplicantId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComplienceDocuments_Organizations_AssemblyKitSupplierId",
                        column: x => x.AssemblyKitSupplierId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComplienceDocuments_Organizations_AssemblyPlantId",
                        column: x => x.AssemblyPlantId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComplienceDocuments_Organizations_AuthorityId",
                        column: x => x.AuthorityId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComplienceDocuments_NsiValues_CountryId",
                        column: x => x.CountryId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComplienceDocuments_DicDocTypes_DocTypeId",
                        column: x => x.DocTypeId,
                        principalTable: "DicDocTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComplienceDocuments_Organizations_ManufacturerId",
                        column: x => x.ManufacturerId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComplienceDocuments_Organizations_RepresentativeManufacture~",
                        column: x => x.RepresentativeManufacturerId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_ApplicantId",
                table: "ComplienceDocuments",
                column: "ApplicantId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_AssemblyKitSupplierId",
                table: "ComplienceDocuments",
                column: "AssemblyKitSupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_AssemblyPlantId",
                table: "ComplienceDocuments",
                column: "AssemblyPlantId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_AuthorityId",
                table: "ComplienceDocuments",
                column: "AuthorityId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_CountryId",
                table: "ComplienceDocuments",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_DocTypeId",
                table: "ComplienceDocuments",
                column: "DocTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_ManufacturerId",
                table: "ComplienceDocuments",
                column: "ManufacturerId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplienceDocuments_RepresentativeManufacturerId",
                table: "ComplienceDocuments",
                column: "RepresentativeManufacturerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComplienceDocuments");

            migrationBuilder.DropTable(
                name: "DicDocTypes");
        }
    }
}

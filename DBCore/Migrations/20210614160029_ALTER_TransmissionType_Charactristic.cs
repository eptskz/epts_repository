﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_TransmissionType_Charactristic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicsId",
                table: "TransmissionTypes");

            migrationBuilder.DropIndex(
                name: "IX_TransmissionTypes_CharacteristicsId",
                table: "TransmissionTypes");

            migrationBuilder.DropColumn(
                name: "CharacteristicsId",
                table: "TransmissionTypes");

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionTypes_CharacteristicId",
                table: "TransmissionTypes",
                column: "CharacteristicId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicId",
                table: "TransmissionTypes",
                column: "CharacteristicId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicId",
                table: "TransmissionTypes");

            migrationBuilder.DropIndex(
                name: "IX_TransmissionTypes_CharacteristicId",
                table: "TransmissionTypes");

            migrationBuilder.AddColumn<int>(
                name: "CharacteristicsId",
                table: "TransmissionTypes",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransmissionTypes_CharacteristicsId",
                table: "TransmissionTypes",
                column: "CharacteristicsId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionTypes_CharacteristicsDetail_CharacteristicsId",
                table: "TransmissionTypes",
                column: "CharacteristicsId",
                principalTable: "CharacteristicsDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

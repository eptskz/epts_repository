﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Organization_HeadFIO : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HeadFirstName",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HeadLastName",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HeadSecondName",
                table: "Organizations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HeadFirstName",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "HeadLastName",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "HeadSecondName",
                table: "Organizations");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Transmission_Unit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_AxisDistributionId",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_MainGearTypeId",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_TransmissionBoxTypeId",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearT~",
                table: "TransmissionUnitGearDetails");

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionUnitGearTypeId",
                table: "TransmissionUnitGearDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<double>(
                name: "TransmissionUnitGearRateMax",
                table: "TransmissionUnitGearDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<double>(
                name: "TransmissionUnitGearRate",
                table: "TransmissionUnitGearDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionBoxTypeId",
                table: "TransmissionUnitDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MainGearTypeId",
                table: "TransmissionUnitDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "AxisDistributionId",
                table: "TransmissionUnitDetails",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_AxisDistributionId",
                table: "TransmissionUnitDetails",
                column: "AxisDistributionId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_MainGearTypeId",
                table: "TransmissionUnitDetails",
                column: "MainGearTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_TransmissionBoxTypeId",
                table: "TransmissionUnitDetails",
                column: "TransmissionBoxTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearT~",
                table: "TransmissionUnitGearDetails",
                column: "TransmissionUnitGearTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_AxisDistributionId",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_MainGearTypeId",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_TransmissionBoxTypeId",
                table: "TransmissionUnitDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearT~",
                table: "TransmissionUnitGearDetails");

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionUnitGearTypeId",
                table: "TransmissionUnitGearDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionUnitGearRateMax",
                table: "TransmissionUnitGearDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionUnitGearRate",
                table: "TransmissionUnitGearDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionBoxTypeId",
                table: "TransmissionUnitDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MainGearTypeId",
                table: "TransmissionUnitDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AxisDistributionId",
                table: "TransmissionUnitDetails",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_AxisDistributionId",
                table: "TransmissionUnitDetails",
                column: "AxisDistributionId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_MainGearTypeId",
                table: "TransmissionUnitDetails",
                column: "MainGearTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitDetails_NsiValues_TransmissionBoxTypeId",
                table: "TransmissionUnitDetails",
                column: "TransmissionBoxTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransmissionUnitGearDetails_NsiValues_TransmissionUnitGearT~",
                table: "TransmissionUnitGearDetails",
                column: "TransmissionUnitGearTypeId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_Organization_AddressNotRequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactVillageId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalVillageId",
                table: "Organizations");

            migrationBuilder.AlterColumn<int>(
                name: "JuridicalVillageId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "JuridicalRegionOrCityId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "JuridicalOkrugOrCityId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "JuridicalDistinctOrCityId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "FactVillageId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "FactRegionOrCityId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "FactOkrugOrCityId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "FactDistinctOrCityId",
                table: "Organizations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactDistinctOrCityId",
                table: "Organizations",
                column: "FactDistinctOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactOkrugOrCityId",
                table: "Organizations",
                column: "FactOkrugOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactRegionOrCityId",
                table: "Organizations",
                column: "FactRegionOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactVillageId",
                table: "Organizations",
                column: "FactVillageId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalDistinctOrCityId",
                table: "Organizations",
                column: "JuridicalDistinctOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalOkrugOrCityId",
                table: "Organizations",
                column: "JuridicalOkrugOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalRegionOrCityId",
                table: "Organizations",
                column: "JuridicalRegionOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalVillageId",
                table: "Organizations",
                column: "JuridicalVillageId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_FactVillageId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalDistinctOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalOkrugOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalRegionOrCityId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalVillageId",
                table: "Organizations");

            migrationBuilder.AlterColumn<int>(
                name: "JuridicalVillageId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "JuridicalRegionOrCityId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "JuridicalOkrugOrCityId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "JuridicalDistinctOrCityId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FactVillageId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FactRegionOrCityId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FactOkrugOrCityId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FactDistinctOrCityId",
                table: "Organizations",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactDistinctOrCityId",
                table: "Organizations",
                column: "FactDistinctOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactOkrugOrCityId",
                table: "Organizations",
                column: "FactOkrugOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactRegionOrCityId",
                table: "Organizations",
                column: "FactRegionOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_FactVillageId",
                table: "Organizations",
                column: "FactVillageId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalDistinctOrCityId",
                table: "Organizations",
                column: "JuridicalDistinctOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalOkrugOrCityId",
                table: "Organizations",
                column: "JuridicalOkrugOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalRegionOrCityId",
                table: "Organizations",
                column: "JuridicalRegionOrCityId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_DicKatos_JuridicalVillageId",
                table: "Organizations",
                column: "JuridicalVillageId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class CREATE_RegRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RegRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    OrganizationName = table.Column<string>(nullable: true),
                    OrganizationBin = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    Certificate = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<DateTime>(nullable: true),
                    StatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RegRequests_DicStatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "DicStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

           

            migrationBuilder.CreateIndex(
                name: "IX_RegRequests_StatusId",
                table: "RegRequests",
                column: "StatusId");


            migrationBuilder.InsertData("DicStatuses"
               , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
               , new object[] { "new", "Новый", "Новый", "New", DateTime.Now, "regrequest" });

            migrationBuilder.InsertData("DicStatuses"
               , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
               , new object[] { "accepted", "Принят", "Принят", "Accepted", DateTime.Now, "regrequest" });

            migrationBuilder.InsertData("DicStatuses"
               , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
               , new object[] { "rejected", "Отклонен", "Отклонен", "Rejected", DateTime.Now, "regrequest" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RegRequests");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_DigitalPassport_AcceptRegionRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Author",
                table: "ExternalInteractHistory",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AcceptRegionRegistrationId",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BaseVehicleMakeNameNsiId",
                table: "DigitalPassportDocument",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_AcceptRegionRegistrationId",
                table: "DigitalPassportDocument",
                column: "AcceptRegionRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalPassportDocument_BaseVehicleMakeNameNsiId",
                table: "DigitalPassportDocument",
                column: "BaseVehicleMakeNameNsiId");

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_DicKatos_AcceptRegionRegistrationId",
                table: "DigitalPassportDocument",
                column: "AcceptRegionRegistrationId",
                principalTable: "DicKatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DigitalPassportDocument_NsiValues_BaseVehicleMakeNameNsiId",
                table: "DigitalPassportDocument",
                column: "BaseVehicleMakeNameNsiId",
                principalTable: "NsiValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_DicKatos_AcceptRegionRegistrationId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropForeignKey(
                name: "FK_DigitalPassportDocument_NsiValues_BaseVehicleMakeNameNsiId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_AcceptRegionRegistrationId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropIndex(
                name: "IX_DigitalPassportDocument_BaseVehicleMakeNameNsiId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "Author",
                table: "ExternalInteractHistory");

            migrationBuilder.DropColumn(
                name: "AcceptRegionRegistrationId",
                table: "DigitalPassportDocument");

            migrationBuilder.DropColumn(
                name: "BaseVehicleMakeNameNsiId",
                table: "DigitalPassportDocument");
        }
    }
}

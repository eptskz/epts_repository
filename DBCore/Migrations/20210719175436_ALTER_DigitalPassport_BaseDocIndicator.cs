﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class ALTER_DigitalPassport_BaseDocIndicator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("ALTER TABLE \"DigitalPassportDocument\" ALTER COLUMN \"BaseDocIndicator\" DROP DEFAULT;");
            migrationBuilder.Sql("update  \"DigitalPassportDocument\" set \"BaseDocIndicator\"=0;");
            migrationBuilder.Sql("ALTER TABLE \"DigitalPassportDocument\" ALTER \"BaseDocIndicator\" TYPE bool USING \"BaseDocIndicator\"::boolean;");
            migrationBuilder.Sql("ALTER TABLE \"DigitalPassportDocument\" ALTER COLUMN \"BaseDocIndicator\" SET DEFAULT FALSE;");

            //migrationBuilder.AlterColumn<bool>(
            //    name: "BaseDocIndicator",
            //    table: "DigitalPassportDocument",
            //    nullable: false,
            //    oldClrType: typeof(string),
            //    oldType: "text",
            //    oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("ALTER TABLE \"DigitalPassportDocument\" ALTER COLUMN \"BaseDocIndicator\" DROP DEFAULT;");
            migrationBuilder.Sql("update  \"DigitalPassportDocument\" set \"BaseDocIndicator\"='';");
            migrationBuilder.Sql("ALTER TABLE \"DigitalPassportDocument\" ALTER \"BaseDocIndicator\" TYPE text USING \"BaseDocIndicator\"::text;");
            migrationBuilder.Sql("ALTER TABLE \"DigitalPassportDocument\" ALTER COLUMN \"BaseDocIndicator\" SET DEFAULT '';");

            //migrationBuilder.AlterColumn<string>(
            //    name: "BaseDocIndicator",
            //    table: "DigitalPassportDocument",
            //    type: "text",
            //    nullable: true,
            //    oldClrType: typeof(bool));
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class CREATE_OrganizationDocuments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OrganizationDocuments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OrgDocType = table.Column<string>(nullable: true),
                    DocTypeId = table.Column<int>(nullable: false),
                    DocSeries = table.Column<string>(nullable: true),
                    DocNumber = table.Column<string>(nullable: true),
                    DocName = table.Column<string>(nullable: true),
                    DocDate = table.Column<DateTime>(nullable: true),
                    DocValidDateStart = table.Column<DateTime>(nullable: true),
                    DocValidDateEnd = table.Column<DateTime>(nullable: true),
                    TnVedTcCode = table.Column<string>(nullable: true),
                    AgreementOrgName = table.Column<string>(nullable: true),
                    AgreementQuotaSize = table.Column<double>(nullable: true),
                    TechRegulationSubjectQuantity = table.Column<int>(nullable: true),
                    TechRegulationSubjectTypeId = table.Column<int>(nullable: true),
                    TechRegulationSubjectUnitId = table.Column<int>(nullable: true),
                    IssueCountryId = table.Column<int>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganizationDocuments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrganizationDocuments_NsiValues_DocTypeId",
                        column: x => x.DocTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrganizationDocuments_NsiValues_IssueCountryId",
                        column: x => x.IssueCountryId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrganizationDocuments_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrganizationDocuments_NsiValues_TechRegulationSubjectTypeId",
                        column: x => x.TechRegulationSubjectTypeId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrganizationDocuments_NsiValues_TechRegulationSubjectUnitId",
                        column: x => x.TechRegulationSubjectUnitId,
                        principalTable: "NsiValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationDocuments_DocTypeId",
                table: "OrganizationDocuments",
                column: "DocTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationDocuments_IssueCountryId",
                table: "OrganizationDocuments",
                column: "IssueCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationDocuments_OrganizationId",
                table: "OrganizationDocuments",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationDocuments_TechRegulationSubjectTypeId",
                table: "OrganizationDocuments",
                column: "TechRegulationSubjectTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationDocuments_TechRegulationSubjectUnitId",
                table: "OrganizationDocuments",
                column: "TechRegulationSubjectUnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrganizationDocuments");
        }
    }
}

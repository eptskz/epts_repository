﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBCore.Migrations
{
    public partial class SEED_EngineType_Gybrid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData("NsiValues", "Id", 5897, "NameRu", "Гибридное транспортное средство");
            migrationBuilder.UpdateData("NsiValues", "Id", 5897, "NameKz", "Гибридное транспортное средство");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBCore.Migrations
{
    public partial class CREATE_SupportTicket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SupportTickets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Number = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    AuthorId = table.Column<string>(nullable: true),
                    AuthorName = table.Column<string>(nullable: true),
                    Category = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    Deleted = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportTickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SupportTickets_DicStatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "DicStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SupportTicketComments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Comment = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    AuthorId = table.Column<string>(nullable: true),
                    AuthorName = table.Column<string>(nullable: true),
                    TicketId = table.Column<Guid>(nullable: false),
                    Deleted = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportTicketComments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SupportTicketComments_SupportTickets_TicketId",
                        column: x => x.TicketId,
                        principalTable: "SupportTickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicketComments_TicketId",
                table: "SupportTicketComments",
                column: "TicketId");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTickets_StatusId",
                table: "SupportTickets",
                column: "StatusId");


            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "new", "Новый", "Новый", "New", DateTime.Now, "supportticket" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "inproccessing", "В работе", "В работе", "Proccessing", DateTime.Now, "supportticket" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "closed", "Закрыто", "Закрыто", "Closed", DateTime.Now, "supportticket" }
            );
            migrationBuilder.InsertData("DicStatuses"
                , new string[] { "Code", "NameRu", "NameKz", "NameEn", "Created", "Group" }
                , new object[] { "rejected", "Отклонено", "Отклонено", "Rejected", DateTime.Now, "supportticket" }
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SupportTicketComments");

            migrationBuilder.DropTable(
                name: "SupportTickets");
        }
    }
}

﻿using DBCore.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NLog;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using DbLog.Base;
using DbLog.Models;
using DbLog;
using Newtonsoft.Json;

namespace DBCore
{
    public class GenericRepository<T> where T : class
    {
        readonly IMongoCollection<T> collection;
        protected readonly Logger _logger = LogManager.GetCurrentClassLogger();
        protected readonly MongoClient mongo_client;
        protected readonly EptsContext sql_client;
        internal DbSet<T> DbSet;
        internal LogContext _logContext;
       
        #region Mongo Repository
        
        public GenericRepository(IMongoDatabaseSettings mongo_settings, EptsContext _sql_client, LogContext logContext = null)
        {
            mongo_client = new MongoClient(mongo_settings.ConnectionString);
            var database = mongo_client.GetDatabase(mongo_settings.DatabaseName);
            collection = database.GetCollection<T>(typeof(T).Name);

            sql_client = _sql_client;
            this.DbSet = sql_client.Set<T>();

            _logContext = logContext;
        }

        public List<T> GetAll() =>
            collection.Find(x => true).ToList();

        public List<T> Get(BsonDocument filter) =>
            collection.Find(filter).ToList();

        public T GetById(string id)
        {
            var filter = new BsonDocument("_id", new BsonDocument("$eq", ObjectId.Parse(id)));
            return collection.Find<T>(filter).FirstOrDefault();
        }

        public T Create(T item)
        {
            collection.InsertOne(item);
            return item;
        }

        public async Task<T> CreateAsync(T item)
        {
            await collection.InsertOneAsync(item);
            return item;
        }

        public void UpdateById(string id, T item) {
            var filter = new BsonDocument ( "_id", new BsonDocument("$eq", ObjectId.Parse(id)));
            collection.ReplaceOne(filter, item);
        }

        public void UpdateById(ObjectId id, T item)
        {
            var filter = new BsonDocument("_id", new BsonDocument("$eq", id));
            collection.ReplaceOne(filter, item);
        }

        public void UpdateAll(BsonDocument filter, UpdateDefinition<T> item)
        {
            collection.UpdateMany(filter, item);
        }

        public void RemoveFirst(BsonDocument filter)
        {
            collection.DeleteOne(filter);
        }

        

        public void RemoveById(string id)
        {
            var filter = new BsonDocument("_id", new BsonDocument("$eq", ObjectId.Parse(id)));
            collection.DeleteOne(filter);
        }

        public void RemoveById(ObjectId id)
        {
            var filter = new BsonDocument("_id", new BsonDocument("$eq",id));
            collection.DeleteOne(filter);
        }


        public virtual async Task<IList<T>> GetAsync(BsonDocument filter = null, BsonDocument sort = null
                                                                , int? limit = null)
        {
            var result = new List<T>();
            try
            {
                if (filter == null) filter = new BsonDocument();
                if (sort != null)
                {
                    result = limit > 0 ? await collection.Find(filter).Sort(sort).Limit(limit).ToListAsync() : await collection.Find(filter).Sort(sort).ToListAsync();
                }
                else
                {
                    result = limit > 0 ? await collection.Find(filter).Limit(limit).ToListAsync() : await collection.Find(filter).ToListAsync();
                }

            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw e;
            }
            return result;
        }

        public virtual async Task<IList<BsonDocument>> GetAggregate(BsonDocument match, BsonDocument group, BsonDocument sort = null)
        {
            var results = new List<BsonDocument>();
            try
            {
                var aggregate = collection.Aggregate()
                    .Match(match)
                    .Group(group)
                    .Sort(sort);
                results = await aggregate.ToListAsync();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw e;
            }
            return results;
        }

        public IMongoCollection<T> GetCollection()
        {
            return collection;
        }

        // May use for mongo transaction
        public MongoClient GetMongoClient()
        {
            return mongo_client;
        }
        #endregion

        #region MSSQL Repository

        #endregion

        #region PostreSql repository
        public virtual IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<T> query = DbSet;

            if (filter != null)
                query = query.Where(filter);

            query = includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            if (orderBy != null)
                return orderBy(query).ToList();
            return query.ToList();
        }

        public virtual IQueryable<T> GetAsIQueryable(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "",
            bool isAsNoTracking = false)
        {
            IQueryable<T> query = isAsNoTracking ? DbSet.AsNoTracking() : DbSet;

            if (filter != null)
                query = query.Where(filter);

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            if (orderBy != null)
                return orderBy(query);

            return query;
        }

        public virtual T GetByID(object id)
        {
            return DbSet.Find(id);
        }

        public virtual T GetByIDs(params object[] ids)
        {
            return DbSet.Find(ids);
        }

        public virtual bool Insert(T entity)
        {
            DbSet.Add(entity);
            if (entity is IAuditEntity)
                InsertAuditLogAsync(entity as IAuditEntity, null, AuditActionType.Insert).ConfigureAwait(true);
            return true;
        }

        public virtual async Task<bool> InsertAsync(T entity)
        {
            await DbSet.AddAsync(entity);
            if (entity is IAuditEntity)
                await InsertAuditLogAsync(entity as IAuditEntity, null, AuditActionType.Insert);
            return true;
        }

        public virtual bool Delete(object id)
        {
            T entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
            return true;
        }

        public virtual bool Delete(params object[] ids)
        {
            T entityToDelete = DbSet.Find(ids);
            Delete(entityToDelete);
            return true;
        }

        public virtual void Delete(T entity)
        {
            if (sql_client.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            if (entity is IAuditEntity)
                InsertAuditLogAsync(entity as IAuditEntity, null, AuditActionType.Delete).ConfigureAwait(true);

            DbSet.Remove(entity);
        }

        public virtual bool Update(T entity)
        {
            DbSet.Attach(entity);
            sql_client.Entry(entity).State = EntityState.Modified;
            if (entity is IAuditEntity)
            {
                var newAuditEntity = entity as IAuditEntity;
                var oldAuditEntity = DbSet.Find(newAuditEntity._Id);
                InsertAuditLogAsync(newAuditEntity, oldAuditEntity as IAuditEntity, AuditActionType.Update).ConfigureAwait(true);
            }
            return true;
        }
        #endregion



        protected async Task InsertAuditLogAsync(IAuditEntity newEntity, IAuditEntity oldEntity, AuditActionType auditActionType)
        {
            try
            {
                var lastVersion = await _logContext.AuditLogs.Where(al => al.EntityId == newEntity._Id.ToString() 
                        && al.EntityTableName == newEntity.GetType().Name)
                    .OrderByDescending(al => al.Id)
                    .FirstOrDefaultAsync();

                var jsonSerializeSettings = new JsonSerializerSettings()
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                };

                string oldEntityJson = null;
                if (oldEntity != null)
                {
                    oldEntityJson = JsonConvert.SerializeObject(oldEntity, jsonSerializeSettings);
                }

                var newEntityJson = JsonConvert.SerializeObject(newEntity, jsonSerializeSettings);

                if (auditActionType == AuditActionType.Delete || lastVersion?.NewEntity != newEntityJson)
                {
                    int versionNo = (lastVersion?.VersionNo ?? 0) + 1;
                    var auditLogRow = new AuditLog()
                    {
                        VersionNo = versionNo,
                        UserId = newEntity._CurrenUserId,
                        UserName = newEntity._CurrenUserName,
                        ActionType = auditActionType,
                        ActionDate = DateTime.Now,
                        EntityTableName = newEntity.GetType().Name,
                        EntityId = newEntity._Id.ToString(),
                        OldEntity = oldEntityJson,
                        NewEntity = newEntityJson,
                        IpAddress = newEntity._IpAddress
                    };

                    _logger.Info("InsertAuditLogAsync: {0}", JsonConvert.SerializeObject(auditLogRow));

                    await _logContext.AuditLogs.AddAsync(auditLogRow);
                    await _logContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "AuditLog Failed.");
            }
        }
    }
}

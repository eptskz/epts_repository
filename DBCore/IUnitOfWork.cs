﻿using DBCore.DTOs;
using DBCore.DTOs.DigitalPassport;
using DBCore.Models;
using DBCore.Models.Administration;
using DBCore.Models.Administration.Contract;
using DBCore.Models.Administration.News;
using DBCore.Models.Administration.Support;
using DBCore.Models.ComplienceDocuments;
using DBCore.Models.Dictionaries;
using DBCore.Models.DocumentEvents;
using DBCore.Models.FileStorage;
using DBCore.Models.Notifications;
using DBCore.Models.NSI;
using DBCore.Models.PassportDetails;
using DBCore.Models.Registries.Invoice;
using DBCore.Models.Reports;
using DBCore.Repositories;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DBCore
{
    public interface IUnitOfWork :IDisposable
    {
        GenericRepository<AspNetUsers> UserRepository { get; }
        GenericRepository<AspNetUserRoles> UserRolesRepository { get; }
        GenericRepository<AspNetRoles> RolesRepository { get; }        
        GenericRepository<TokenCache> TokenCacheRepository { get; }

        OrganizationRepository OrganizationsRepository { get; }
        GenericRepository<Region> RegionsRepository { get; }

        GenericRepository<UserProfile> UserProfileRepository { get; }

        GenericRepository<Nsi> NsisRepository { get; }
        GenericRepository<NsiValue> NsiValuesRepository { get; }


        GenericRepository<DicDocType> DicDocTypeRepository { get; }
        GenericRepository<DicStatus> DicStatusesRepository { get; }
        GenericRepository<DicOrganizationType> DicOrganizationTypesRepository { get; }
        GenericRepository<DicKato> DicKatosRepository { get; }

        ComplienceDocRepository ComplienceDocumentRepository { get; }
        GenericRepository<DocumentEvent> DocEventRepository { get; }
        GenericRepository<VehicleGeneralView> VehicleGeneralViewRepository { get; }
        

        GenericRepository<DigitalPassportDocument> DigitalPassportRepository { get; }
        GenericRepository<VehicleImage> VehicleImageRepository { get; }
        GenericRepository<ExternalInteractHistory> ExternalInteractHistoryRepository { get; }
        GenericRepository<DigitalPassportStatusHistory> DigitalPassportStatusHistoryRepository { get; }
        GenericRepository<MvdDatas> MvdDataRepository { get; }
        GenericRepository<KgdDatas> KgdDataRepository { get; }
        GenericRepository<MshDatas> MshDataRepository { get; }
        GenericRepository<SignedDoc> SignedDocRepository { get; }
        GenericRepository<RegRequest> RegRequestRepository { get; }
        GenericRepository<ChangeApplication> ChangeApplicationRepository { get; }
        GenericRepository<SupportTicket> SupportTicketRepository { get; }
        GenericRepository<SupportTicketComment> SupportTicketCommentRepository { get; }
        GenericRepository<Contract> ContractRepository { get; }
        GenericRepository<Invoice> InvoiceRepository { get; }
        GenericRepository<News> NewsRepository { get; }
        FileStorageRepository FileStorageRepository { get; }
        GenericRepository<Notification> NotificationRepository { get; }

        GenericRepository<ComplienceDocReport> ComplienceDocReportRepository { get; }


        #region Mongo
        GenericRepository<CharacteristicDetails> CharacteristicDetailsMongoRepository { get; }
        GenericRepository<RootPassportDto> DraftPassportDtoMongoRepository { get; }
        GenericRepository<ChangeApplicationDto> DraftChangeApplicationMongoRepository { get; }
        
        #endregion
        Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction BeginTransaction();
        bool Commit();
        bool Rollback();
        void Save();
        Task SaveAsync();
    }
}

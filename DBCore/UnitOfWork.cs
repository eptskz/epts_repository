﻿using DBCore.DTOs;
using DBCore.DTOs.DigitalPassport;
using DBCore.Models;
using DBCore.Models.Administration;
using DBCore.Models.Administration.Contract;
using DBCore.Models.Administration.News;
using DBCore.Models.Administration.Support;
using DBCore.Models.ComplienceDocuments;
using DBCore.Models.Dictionaries;
using DBCore.Models.DocumentEvents;
using DBCore.Models.FileStorage;
using DBCore.Models.Notifications;
using DBCore.Models.NSI;
using DBCore.Models.PassportDetails;
using DBCore.Models.Registries.Invoice;
using DBCore.Models.Reports;
using DBCore.Repositories;
using DbLog;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DBCore
{
    public class UnitOfWork : IUnitOfWork
    {
        protected IMongoDatabaseSettings _mongo_settings;
        protected ISQLDatabaseSettings _sql_settings;
        private readonly EptsContext sql_client;
        private readonly LogContext _logContext;
        private Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction dbContextTransaction;

        public UnitOfWork(IMongoDatabaseSettings settings1, ISQLDatabaseSettings settings2, LogContext logContext)
        {
            _mongo_settings = settings1;
            _sql_settings = settings2;

            var optionsBuilder = new DbContextOptionsBuilder<EptsContext>();
            optionsBuilder.UseNpgsql(_sql_settings.ConnectionString);

            sql_client = new EptsContext(optionsBuilder.Options);
            _logContext = logContext;
        }

        #region SQL
        private GenericRepository<TokenCache> _tokenCacheRepository;
        public GenericRepository<TokenCache> TokenCacheRepository
        {
            get
            {
                return _tokenCacheRepository ?? (_tokenCacheRepository = new GenericRepository<TokenCache>(_mongo_settings, sql_client));
            }
        }

        private GenericRepository<AspNetUsers> _userRepository;
        public GenericRepository<AspNetUsers> UserRepository
        {
            get
            {
                return _userRepository ?? (_userRepository = new GenericRepository<AspNetUsers>(_mongo_settings, sql_client));
            }
        }

        private GenericRepository<AspNetUserRoles> _userRolesRepository;
        public GenericRepository<AspNetUserRoles> UserRolesRepository
        {
            get
            {
                return _userRolesRepository ?? (_userRolesRepository = new GenericRepository<AspNetUserRoles>(_mongo_settings, sql_client));
            }
        }

        private GenericRepository<AspNetRoles> _rolesRepository;
        public GenericRepository<AspNetRoles> RolesRepository
        {
            get
            {
                return _rolesRepository ?? (_rolesRepository = new GenericRepository<AspNetRoles>(_mongo_settings, sql_client));
            }
        }
          
        private OrganizationRepository _organizationsRepository;
        public OrganizationRepository OrganizationsRepository
        {
            get
            {
                return _organizationsRepository ?? (_organizationsRepository = new OrganizationRepository(_mongo_settings, sql_client, _logContext));
            }
        }

        private GenericRepository<UserProfile> _userProfileRepository;
        public GenericRepository<UserProfile> UserProfileRepository
        {
            get
            {
                return _userProfileRepository ?? (_userProfileRepository = new GenericRepository<UserProfile>(_mongo_settings, sql_client));
            }
        }



        private GenericRepository<Region> _regionsRepository;
        public GenericRepository<Region> RegionsRepository
        {
            get
            {
                return _regionsRepository ?? (_regionsRepository = new GenericRepository<Region>(_mongo_settings, sql_client));
            }
        }

        private GenericRepository<DicKato> _dicKatoRepository;
        public GenericRepository<DicKato> DicKatosRepository => _dicKatoRepository
            ?? (_dicKatoRepository = new GenericRepository<DicKato>(_mongo_settings, sql_client));


        private GenericRepository<Nsi> _nsisRepository;
        public GenericRepository<Nsi> NsisRepository => _nsisRepository 
            ?? (_nsisRepository = new GenericRepository<Nsi>(_mongo_settings, sql_client));


        private GenericRepository<NsiValue> _nsiValuesRepository;
        public GenericRepository<NsiValue> NsiValuesRepository => _nsiValuesRepository 
            ?? (_nsiValuesRepository = new GenericRepository<NsiValue>(_mongo_settings, sql_client));
        

        private GenericRepository<SignedDoc> _signedDocRepository;
        public GenericRepository<SignedDoc> SignedDocRepository => _signedDocRepository
            ?? (_signedDocRepository = new GenericRepository<SignedDoc>(_mongo_settings, sql_client));

        private GenericRepository<RegRequest> _regRequestRepository;
        public GenericRepository<RegRequest> RegRequestRepository => _regRequestRepository
            ?? (_regRequestRepository = new GenericRepository<RegRequest>(_mongo_settings, sql_client));

        private GenericRepository<SupportTicket> _supportTicketRepository;
        public GenericRepository<SupportTicket> SupportTicketRepository => _supportTicketRepository
            ?? (_supportTicketRepository = new GenericRepository<SupportTicket>(_mongo_settings, sql_client));

        private GenericRepository<SupportTicketComment> _supportTicketCommentRepository;
        public GenericRepository<SupportTicketComment> SupportTicketCommentRepository => _supportTicketCommentRepository
            ?? (_supportTicketCommentRepository = new GenericRepository<SupportTicketComment>(_mongo_settings, sql_client));

        private GenericRepository<Contract> _contractRepository;
        public GenericRepository<Contract> ContractRepository => _contractRepository
            ?? (_contractRepository = new GenericRepository<Contract>(_mongo_settings, sql_client));

        private GenericRepository<Invoice> _invoiceRepository;
        public GenericRepository<Invoice> InvoiceRepository => _invoiceRepository
            ?? (_invoiceRepository = new GenericRepository<Invoice>(_mongo_settings, sql_client));

        private FileStorageRepository _fileStorageRepository;
        public FileStorageRepository FileStorageRepository => _fileStorageRepository
            ?? (_fileStorageRepository = new FileStorageRepository(_mongo_settings, sql_client));

        private GenericRepository<Notification> _notificationRepository;
        public GenericRepository<Notification> NotificationRepository => _notificationRepository
            ?? (_notificationRepository = new GenericRepository<Notification>(_mongo_settings, sql_client));

        private GenericRepository<News> _newsRepository;
        public GenericRepository<News> NewsRepository => _newsRepository
            ?? (_newsRepository = new GenericRepository<News>(_mongo_settings, sql_client));

        #region Complience Documents

        private ComplienceDocRepository _complienceDocumentRepository;
        public ComplienceDocRepository ComplienceDocumentRepository => _complienceDocumentRepository
            ?? (_complienceDocumentRepository = new ComplienceDocRepository(_mongo_settings, sql_client, _logContext));

        private GenericRepository<DocumentEvent> _docEventRepository;
        public GenericRepository<DocumentEvent> DocEventRepository => _docEventRepository
            ?? (_docEventRepository = new GenericRepository<DocumentEvent>(_mongo_settings, sql_client));

        private GenericRepository<VehicleGeneralView> _vehicleGeneralViewRepository;
        public GenericRepository<VehicleGeneralView> VehicleGeneralViewRepository => _vehicleGeneralViewRepository
            ?? (_vehicleGeneralViewRepository = new GenericRepository<VehicleGeneralView>(_mongo_settings, sql_client));

        private GenericRepository<ComplienceDocReport> _complienceDocReportRepository;
        public GenericRepository<ComplienceDocReport> ComplienceDocReportRepository => _complienceDocReportRepository
            ?? (_complienceDocReportRepository = new GenericRepository<ComplienceDocReport>(_mongo_settings, sql_client));

        #endregion

        #region Dictionary

        private GenericRepository<DicStatus> _dicstatusesRepository;
        public GenericRepository<DicStatus> DicStatusesRepository
        {
            get
            {
                return _dicstatusesRepository ?? (_dicstatusesRepository = new GenericRepository<DicStatus>(_mongo_settings, sql_client));
            }
        }


        private GenericRepository<DicOrganizationType> _dicOrganizationTypesRepository;
        public GenericRepository<DicOrganizationType> DicOrganizationTypesRepository
        {
            get
            {
                return _dicOrganizationTypesRepository ?? (_dicOrganizationTypesRepository = new GenericRepository<DicOrganizationType>(_mongo_settings, sql_client));
            }
        }


        private GenericRepository<DicDocType> _dicDocTypeRepository;
        public GenericRepository<DicDocType> DicDocTypeRepository
        {
            get
            {
                return _dicDocTypeRepository ?? (_dicDocTypeRepository = new GenericRepository<DicDocType>(_mongo_settings, sql_client));
            }
        }

        #endregion

        #region DigitalPassport
        private GenericRepository<DigitalPassportDocument> _digitalPassportRepository;
        public GenericRepository<DigitalPassportDocument> DigitalPassportRepository => _digitalPassportRepository
            ?? (_digitalPassportRepository = new GenericRepository<DigitalPassportDocument>(_mongo_settings, sql_client));

        private GenericRepository<VehicleImage> _vehicleImageRepository;
        public GenericRepository<VehicleImage> VehicleImageRepository => _vehicleImageRepository
            ?? (_vehicleImageRepository = new GenericRepository<VehicleImage>(_mongo_settings, sql_client));

        private GenericRepository<ExternalInteractHistory> _externalInteractHistoryRepository;
        public GenericRepository<ExternalInteractHistory> ExternalInteractHistoryRepository => _externalInteractHistoryRepository
            ?? (_externalInteractHistoryRepository = new GenericRepository<ExternalInteractHistory>(_mongo_settings, sql_client));

        private GenericRepository<DigitalPassportStatusHistory> _digitalPassportStatusHistoryRepository;
        public GenericRepository<DigitalPassportStatusHistory> DigitalPassportStatusHistoryRepository => _digitalPassportStatusHistoryRepository
            ?? (_digitalPassportStatusHistoryRepository = new GenericRepository<DigitalPassportStatusHistory>(_mongo_settings, sql_client));

        private GenericRepository<MvdDatas> _mvdDataRepository;
        public GenericRepository<MvdDatas> MvdDataRepository => _mvdDataRepository
            ?? (_mvdDataRepository = new GenericRepository<MvdDatas>(_mongo_settings, sql_client));

        private GenericRepository<KgdDatas> _kgdDataRepository;
        public GenericRepository<KgdDatas> KgdDataRepository => _kgdDataRepository
            ?? (_kgdDataRepository = new GenericRepository<KgdDatas>(_mongo_settings, sql_client));

        private GenericRepository<MshDatas> _mshDataRepository;
        public GenericRepository<MshDatas> MshDataRepository => _mshDataRepository
            ?? (_mshDataRepository = new GenericRepository<MshDatas>(_mongo_settings, sql_client));


        #endregion

        #region ChangeApplication
        private GenericRepository<ChangeApplication> _changeApplicationRepository;
        public GenericRepository<ChangeApplication> ChangeApplicationRepository => _changeApplicationRepository
            ?? (_changeApplicationRepository = new GenericRepository<ChangeApplication>(_mongo_settings, sql_client));
        #endregion


        public Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction BeginTransaction()
        {
            dbContextTransaction = sql_client.Database.BeginTransaction();
            return dbContextTransaction;
        }

        public bool Commit()
        {
            try
            {
                dbContextTransaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                //Log Exception Handling message                      
                dbContextTransaction.Rollback();
                throw e;
            }
        }

        public bool Rollback()
        {
            try
            {
                dbContextTransaction.Rollback();
                return true;
            }
            catch (Exception e)
            {
                //Log Exception Handling message                      
                throw e;
            }
        }

        public void Save()
        {
            sql_client.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await sql_client.SaveChangesAsync();
        }
        #endregion



        #region MONGO
        private GenericRepository<CharacteristicDetails> _characteristicDetailsRepository;
        public GenericRepository<CharacteristicDetails> CharacteristicDetailsMongoRepository
        {
            get
            {
                return _characteristicDetailsRepository ?? (_characteristicDetailsRepository = new GenericRepository<CharacteristicDetails>(_mongo_settings, sql_client));
            }
        }
        private GenericRepository<RootPassportDto> _draftPassportDtoMongoRepository;
        public GenericRepository<RootPassportDto> DraftPassportDtoMongoRepository
        {
            get
            {
                return _draftPassportDtoMongoRepository ?? (_draftPassportDtoMongoRepository = new GenericRepository<RootPassportDto>(_mongo_settings, sql_client));
            }
        }

        private GenericRepository<ChangeApplicationDto> _draftChangeApplicationMongoRepository;
        public GenericRepository<ChangeApplicationDto> DraftChangeApplicationMongoRepository
        {
            get
            {
                return _draftChangeApplicationMongoRepository ?? (_draftChangeApplicationMongoRepository = new GenericRepository<ChangeApplicationDto>(_mongo_settings, sql_client));
            }
        }

        public void Dispose()
        {
            // close if context
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}

﻿using AutoMapper;
using DBCore.DTOs;
using DBCore.DTOs.Administration.Support;
using DBCore.DTOs.Dictionary;
using DBCore.DTOs.Registries;
using DBCore.Models;
using DBCore.Models.Administration.Contract;
using DBCore.Models.Administration.Support;
using DBCore.Models.Dictionaries;
using DBCore.Models.Registries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBCore.Mappings
{
    public class ContractProfile : Profile
    {
        public ContractProfile()
        {            
            CreateMap<Contract, ContractDto>()
                .ForMember(d => d.StatusCode, opt => opt.MapFrom(s => s.Status.Code))
                .ForMember(d => d.StatusName, opt => opt.MapFrom(s => s.Status.NameRu))
                .ForMember(d => d.ClientOrgBin, opt => opt.MapFrom(s => s.ClientOrg.Bin))
                .ForMember(d => d.ClientOrgName, opt => opt.MapFrom(s => s.ClientOrg.NameRu));
            CreateMap<ContractDto, Contract>();           
        }
    }
}

﻿using AutoMapper;
using DBCore.DTOs.DigitalPassport;
using DBCore.Models;
using DBCore.Models.PassportDetails;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Mappings
{
    public class DigitalPassportDocumentProfile : Profile
    {
        public DigitalPassportDocumentProfile()
        {
            CreateMap<RootPassportDto, DigitalPassportDocument>()
                .ForMember(d => d.VehicleEngines, opt => opt.MapFrom(s => s.VehicleEngineIdList))
                .ForMember(d => d.MemberOrganizations, opt => opt.MapFrom(s => s.MemberOrganizationList))
                .ForMember(d => d.AssemblerOrganizations, opt => opt.MapFrom(s => s.AssemblerOrganizationList))
                .ForMember(d => d.ManufacturerPlateDetail, opt => opt.MapFrom(s => s.ManufacturerPlateDetails))
                //.ForMember(d => d.AcceptRegionRegistrationId, opt => opt.MapFrom(s => s.AcceptRegionRegistration))
                .ForPath(d => d.ManufacturerPlateDetail.VehicleStructureCards, opt => opt.MapFrom(s => s.ManufacturerPlateDetails.VehicleStructureCardList))
                .ForPath(d => d.ManufacturerPlateDetail.VehicleIdentLocations, opt => opt.MapFrom(s => s.ManufacturerPlateDetails.VehicleIdentLocationList))
                .ForPath(d => d.ManufacturerPlateDetail.ManufacturerPlateLocations, opt => opt.MapFrom(s => s.ManufacturerPlateDetails.ManufacturerPlateLocationList))
                .ForMember(d => d.EnginePlateDetail, opt => opt.MapFrom(s => s.EnginePlateDetails))
                .ForPath(d => d.EnginePlateDetail.VehicleIdentLocations, opt => opt.MapFrom(s => s.EnginePlateDetails.VehicleIdentLocationList))
                .ForPath(d => d.EnginePlateDetail.VehicleStructureCards, opt => opt.MapFrom(s => s.EnginePlateDetails.VehicleStructureCardList))
                .ForMember(dest => dest.Id, act => act.Ignore());
            //CreateMap<RootPassportDto, DigitalPassportDocument>().ReverseMap();
            CreateMap<VehicleEngineIdList, VehicleEngine>();
            CreateMap<MemberOrganizationList, MemberOrganization>();
            CreateMap<AssemblerOrganizationList, AssemblerOrganization>();
            CreateMap<ManufacturerPlateDetails, ManufacturerPlateDetail>();
            CreateMap<VehicleStructureCardList, VehicleStructureCard>();
            CreateMap<EnginePlateDetails, EnginePlateDetail>();
            CreateMap<VehicleIdentLocationList, VehicleIdentLocation>()
                .ForMember(d => d.Location, opt => opt.MapFrom(s => s.VehicleIdentLocation));
            CreateMap<ManufacturerPlateLocationList, ManufacturerPlateLocation>()
                .ForMember(d => d.Location, opt => opt.MapFrom(s => s.ManufacturerPlateLocation));

            CreateMap<MeaningExplanationList, MeaningExplanation>();
            CreateMap<VehicleStructureCardList, VehicleStructureCard>()
                .ForMember(d => d.Start, opt => opt.MapFrom(s => s.StartDate))
                .ForMember(d => d.End, opt => opt.MapFrom(s => s.EndDate))
                .ForPath(d => d.MeaningExplanations, opt => opt.MapFrom(s => s.MeaningExplanationList));

            CreateMap<DigitalPassportDocument, RootPassportDto>()
                .ForMember(d => d.VehicleEngineIdList, opt => opt.MapFrom(s => s.VehicleEngines))
                .ForMember(d => d.MemberOrganizationList, opt => opt.MapFrom(s => s.MemberOrganizations))
                .ForMember(d => d.AssemblerOrganizationList, opt => opt.MapFrom(s => s.AssemblerOrganizations))
                .ForMember(d => d.ManufacturerPlateDetails, opt => opt.MapFrom(s => s.ManufacturerPlateDetail))
                //.ForMember(d => d.AcceptRegionRegistrationId, opt => opt.MapFrom(s => s.AcceptRegionRegistration))
                .ForPath(d => d.ManufacturerPlateDetails.VehicleStructureCardList, opt => opt.MapFrom(s => s.ManufacturerPlateDetail.VehicleStructureCards))
                .ForPath(d => d.ManufacturerPlateDetails.VehicleIdentLocationList, opt => opt.MapFrom(s => s.ManufacturerPlateDetail.VehicleIdentLocations))
                .ForPath(d => d.ManufacturerPlateDetails.ManufacturerPlateLocationList, opt => opt.MapFrom(s => s.ManufacturerPlateDetail.ManufacturerPlateLocations))
                .ForMember(d => d.EnginePlateDetails, opt => opt.MapFrom(s => s.EnginePlateDetail))
                .ForPath(d => d.EnginePlateDetails.VehicleIdentLocationList, opt => opt.MapFrom(s => s.EnginePlateDetail.VehicleIdentLocations))
                .ForPath(d => d.EnginePlateDetails.VehicleStructureCardList, opt => opt.MapFrom(s => s.EnginePlateDetail.VehicleStructureCards))
                .ForMember(dest => dest.Id, act => act.Ignore());
            CreateMap<VehicleEngine, VehicleEngineIdList>();
            CreateMap<MemberOrganization, MemberOrganizationList>();
            CreateMap<AssemblerOrganization, AssemblerOrganizationList>();
            CreateMap<ManufacturerPlateDetail, ManufacturerPlateDetails>();
            CreateMap<VehicleStructureCard, VehicleStructureCardList>();
            CreateMap<EnginePlateDetail, EnginePlateDetails>();
            CreateMap<VehicleIdentLocation, VehicleIdentLocationList>()
                    .ForMember(d => d.VehicleIdentLocation, opt => opt.MapFrom(s => s.Location));
            CreateMap<ManufacturerPlateLocation, ManufacturerPlateLocationList>()
                .ForMember(d => d.ManufacturerPlateLocation, opt => opt.MapFrom(s => s.Location));

            CreateMap<VehicleStructureCard, VehicleStructureCardList>()
                .ForMember(d => d.StartDate, opt => opt.MapFrom(s => s.Start))
                .ForMember(d => d.EndDate, opt => opt.MapFrom(s => s.End))
                .ForPath(d => d.MeaningExplanationList, opt => opt.MapFrom(s => s.MeaningExplanations)); ;
            CreateMap<MeaningExplanation, MeaningExplanationList>();


        }
    }
}

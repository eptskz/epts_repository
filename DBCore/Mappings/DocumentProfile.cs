﻿using AutoMapper;
using DBCore.DTOs.Documents;
using DBCore.Models.DocumentEvents;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Mappings
{
    public class DocumentProfile : Profile
    {
        public DocumentProfile()
        {
            CreateMap<DocumentEvent, DocumentEventDto>()
                .ForMember(d => d.EventDateTime, opt => opt.MapFrom(s => s.EventDateTime.ToString("dd.MM.yyyy HH:mm:ss")))
                ;
        }
    }
}

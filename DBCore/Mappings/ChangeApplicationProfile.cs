﻿using AutoMapper;
using DBCore.DTOs;
using DBCore.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Mappings
{
    public class ChangeApplicationProfile : Profile
    {
        public ChangeApplicationProfile()
        {
            CreateMap<ChangeApplicationDto, ChangeApplication>()
                .ForMember(d => d.Files, opt => opt.MapFrom(s =>s.Files))
                .ForMember(dest => dest.Id, act => act.Ignore());
            CreateMap<ChangeApplication, ChangeApplicationDto>()
                .ForMember(dest => dest.Id, act => act.Ignore());
            CreateMap<ChangeApplicationFieldDto, ChangeApplicationField>();
            CreateMap<ChangeApplicationField, ChangeApplicationFieldDto>();
            CreateMap<ChangeApplicationFileDto, ChangeApplicationFile>();
            //.ForMember(d => d.FileObject, opt => opt.MapFrom(s => Convert.FromBase64String(s.FileObject)));
            CreateMap<ChangeApplicationFile, ChangeApplicationFileDto>();
                //.ForMember(d => d.FileObject, opt => opt.MapFrom(s => Convert.ToBase64String(s.FileObject)));
        }
    }
}

﻿using AutoMapper;
using DBCore.DTOs;
using DBCore.DTOs.Administration.Support;
using DBCore.DTOs.Dictionary;
using DBCore.DTOs.Registries;
using DBCore.Models;
using DBCore.Models.Administration.Support;
using DBCore.Models.Dictionaries;
using DBCore.Models.Registries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBCore.Mappings
{
    public class SupportProfile : Profile
    {
        public SupportProfile()
        {            
            CreateMap<SupportTicket, SupportTicketDto>()
                .ForMember(d => d.StatusCode, opt => opt.MapFrom(s => s.Status.Code))
                .ForMember(d => d.StatusName, opt => opt.MapFrom(s => s.Status.NameRu));
            CreateMap<SupportTicketDto, SupportTicket>();

            CreateMap<SupportTicket, SupportTicketReestrDto>()
                .ForMember(d => d.StatusCode, opt => opt.MapFrom(s => s.Status.Code))
                .ForMember(d => d.StatusName, opt => opt.MapFrom(s => s.Status.NameRu));

            CreateMap<SupportTicketComment, SupportTicketCommentDto>().ReverseMap();
        }
    }
}

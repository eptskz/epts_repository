﻿using AutoMapper;
using DBCore.DTOs;
using DBCore.DTOs.Characteristics;
using DBCore.DTOs.Complience;
using DBCore.DTOs.Complience.Vehicle;
using DBCore.Models;
using DBCore.Models.ComplienceDocuments;
using DBCore.Models.ComplienceDocuments.Vehicle;
using DBCore.Models.Vehicle;
using DBCore.Models.Vehicle.Characteristics.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBCore.Mappings
{
    public class ComplienceDocumentProfile : Profile
    {
        public ComplienceDocumentProfile()
        {
            CreateMap<ComplienceDocument, ComplienceDocumenReestrtDto>()
                .ForMember(d => d.CountryCode, opt => opt.MapFrom(s => s.Country.Code))
                .ForMember(d => d.CountryName, opt => opt.MapFrom(s => s.Country.NameRu))
                .ForMember(d => d.DocTypeCode, opt => opt.MapFrom(s => s.DocType.Code))
                .ForMember(d => d.DocTypeName, opt => opt.MapFrom(s => s.DocType.NameRu))
                .ForMember(d => d.StatusCode, opt => opt.MapFrom(s => s.Status.Code))
                .ForMember(d => d.StatusName, opt => opt.MapFrom(s => s.Status.NameRu))
                .ForMember(d => d.AuthorityName, opt => opt.MapFrom(s => s.Authority.NameRu))               
                .ForMember(d => d.ApplicantName, opt => opt.MapFrom(s => s.Applicant.NameRu))
                .ForMember(d => d.ManufacturerName, opt => opt.MapFrom(s => s.Manufacturer.NameRu))
//                .ForMember(d => d.RepresentativeManufacturerName, opt => opt.MapFrom(s => s.RepresentativeManufacturer.NameRu))
//                .ForMember(d => d.AssemblyPlantName, opt => opt.MapFrom(s => s.AssemblyPlant.NameRu))

                .ForMember(d => d.CommercialName, opt => opt.MapFrom(s => s.VehicleTypeDetail.CommercialNames.Count > 0 
                    ? s.VehicleTypeDetail.CommercialNames.Select(cn => cn.CommercialName).Aggregate((i, j) => i + ", " + j)
                    : string.Empty))
                .ForMember(d => d.Type, opt => opt.MapFrom(s => s.VehicleTypeDetail.Types.Count > 0
                    ? s.VehicleTypeDetail.Types.Select(t => t.TypeValue).Aggregate((i, j) => i + ", " + j)
                    : string.Empty))
                .ForMember(d => d.BrandName, opt => opt.MapFrom(s => s.VehicleTypeDetail.Makes.Count > 0
                    ? s.VehicleTypeDetail.Makes.Select(t => t.Make.NameRu).Aggregate((i, j) => i + ", " + j)
                    : string.Empty))
                .ForMember(d => d.CategoryName, opt => opt.MapFrom(s => s.VehicleTypeDetail.TechCategories.Count > 0
                    ? s.VehicleTypeDetail.TechCategories.Select(t => t.Category.NameRu).Aggregate((i, j) => i + ", " + j)
                    : string.Empty))
                ;


            CreateMap<ComplienceDocument, ComplienceDocumentDto>()
                .ForMember(d => d.CountryCode, opt => opt.MapFrom(s => s.Country.Code))
                .ForMember(d => d.CountryName, opt => opt.MapFrom(s => s.Country.NameRu))
                .ForMember(d => d.DocTypeCode, opt => opt.MapFrom(s => s.DocType.Code))
                .ForMember(d => d.DocTypeName, opt => opt.MapFrom(s => s.DocType.NameRu))
                .ForMember(d => d.StatusCode, opt => opt.MapFrom(s => s.Status.Code))
                .ForMember(d => d.StatusName, opt => opt.MapFrom(s => s.Status.NameRu))

                .ForMember(d => d.AuthorityCode, opt => opt.MapFrom(s => s.Authority.Code))
                .ForMember(d => d.AuthorityName, opt => opt.MapFrom(s => s.Authority.NameRu))

                .ForMember(d => d.ApplicantName, opt => opt.MapFrom(s => s.Applicant.NameRu))
                .ForMember(d => d.ManufacturerName, opt => opt.MapFrom(s => s.Manufacturer.NameRu));
//                .ForMember(d => d.RepresentativeManufacturerName, opt => opt.MapFrom(s => s.RepresentativeManufacturer.NameRu));
//                .ForMember(d => d.AssemblyPlantName, opt => opt.MapFrom(s => s.AssemblyPlant.NameRu));
            CreateMap<ComplienceDocumentDto, ComplienceDocument>()
                .ForMember(d => d.RepresentativeManufacturers, opt => opt.MapFrom(s => s.RepresentativeManufacturers.Select(x =>
                    new RepresentativeManufacturer()
                    {
                        Id = x.Id,
                        DocumentId = x.DocumentId,
                        OrganizationId = x.OrganizationId,
                        State = x.State,
                    }
               )))
                .ForMember(d => d.AssemblyPlants, opt => opt.MapFrom(s => s.AssemblyPlants.Select(x =>
                    new AssemblyPlant()
                    {
                        Id = x.Id,
                        DocumentId = x.DocumentId,
                        OrganizationId = x.OrganizationId,
                        State = x.State,
                    }
               )))
                .ForMember(d => d.AssemblyKitSuppliers, opt => opt.MapFrom(s => s.AssemblyKitSuppliers.Select(x =>
                    new AssemblyKitSupplier()
                    {
                        Id = x.Id,
                        DocumentId = x.DocumentId,
                        OrganizationId = x.OrganizationId,
                        State = x.State,
                    }
               )));
            CreateMap<ComplienceDocument, ComplienceDocumentOrgDto>()
                .ForMember(d => d.DocTypeCode, opt => opt.MapFrom(s => s.DocType.Code))
                .ForMember(d => d.DocTypeName, opt => opt.MapFrom(s => s.DocType.NameRu));

            CreateMap<ComplienceDocument, ComplienceDocLabelingDto>()
                .ForMember(d => d.DocTypeCode, opt => opt.MapFrom(s => s.DocType.Code))
                .ForMember(d => d.DocTypeName, opt => opt.MapFrom(s => s.DocType.NameRu));

            CreateMap<RepresentativeManufacturer, RepresentativeManufacturerDto>()
                .ForMember(d => d.OrganizationId, opt => opt.MapFrom(s => s.OrganizationId))
                .ForMember(d => d.Organization, opt => opt.MapFrom(s => s.Organization.NameRu))
                .ForMember(d => d.DocumentId, opt => opt.MapFrom(s => s.DocumentId))
                .ForMember(d => d.Document, opt => opt.MapFrom(s => s.Document.DocNumber));
            CreateMap<RepresentativeManufacturerDto, RepresentativeManufacturer>();

            CreateMap<AssemblyPlant, AssemblyPlantDto>()
                .ForMember(d => d.OrganizationId, opt => opt.MapFrom(s => s.OrganizationId))
                .ForMember(d => d.Organization, opt => opt.MapFrom(s => s.Organization.NameRu))
                .ForMember(d => d.DocumentId, opt => opt.MapFrom(s => s.DocumentId))
                .ForMember(d => d.Document, opt => opt.MapFrom(s => s.Document.DocNumber));
            CreateMap<AssemblyPlantDto, AssemblyPlant>();

            CreateMap<AssemblyKitSupplier, AssemblyKitSupplierDto>()
                .ForMember(d => d.OrganizationId, opt => opt.MapFrom(s => s.OrganizationId))
                .ForMember(d => d.Organization, opt => opt.MapFrom(s => s.Organization.NameRu))
                .ForMember(d => d.DocumentId, opt => opt.MapFrom(s => s.DocumentId))
                .ForMember(d => d.Document, opt => opt.MapFrom(s => s.Document.DocNumber));
            CreateMap<AssemblyKitSupplierDto, AssemblyKitSupplier>();


            CreateMap<BlankNumber, BlankNumberDto>().ReverseMap();
            CreateMap<VinNumber, VinNumberDto>().ReverseMap();

            CreateMap<VehicleLabelingDetail, VehicleLabelingDetailDto>();
            CreateMap<VehicleLabelingDetailDto, VehicleLabelingDetail>();
            
            CreateMap<VehicleLabelLocation, VehicleLabelLocationDto>();
            CreateMap<VehicleLabelLocationDto, VehicleLabelLocation>();

            CreateMap<VehicleIdentificationNumberLocation, VehicleLabelLocationDto>();
            CreateMap<VehicleLabelLocationDto, VehicleIdentificationNumberLocation>();

            CreateMap<VehicleVinCharacterDetail, VehicleVinCharacterDetailDto>()
                .ForMember(d => d.DataTypeCode, opt => opt.MapFrom(s => s.DataType.Code))
                .ForMember(d => d.DataTypeName, opt => opt.MapFrom(s => s.DataType.NameRu))
                ;
            CreateMap<VehicleVinCharacterDetailDto, VehicleVinCharacterDetail>();
            CreateMap<VehicleVinCharacterDescription, VehicleVinCharacterDescriptionDto>().ReverseMap();

            CreateMap<VehicleTypeDetail, VehicleTypeDetailDto>().ReverseMap();

            CreateMap<VehicleTypeMake, VehicleTypeDicDto>()
                .ForMember(d => d.DicId, opt => opt.MapFrom(s => s.MakeId))
                .ForMember(d => d.DicCode, opt => opt.MapFrom(s => s.Make.Code))
                .ForMember(d => d.DicName, opt => opt.MapFrom(s => s.Make.NameRu));

            CreateMap<VehicleTypeDicDto, VehicleTypeMake>()
                .ForMember(d => d.MakeId, opt => opt.MapFrom(s => s.DicId));

            CreateMap<VehicleType, VehicleTypeDto>().ReverseMap();
            CreateMap<VehicleCommercialName, VehicleCommercialNameDto>().ReverseMap();

            CreateMap<VehicleTypeTechCategory, VehicleTypeDicDto>()
                .ForMember(d => d.DicId, opt => opt.MapFrom(s => s.CategoryId))
                .ForMember(d => d.DicCode, opt => opt.MapFrom(s => s.Category.Code))
                .ForMember(d => d.DicName, opt => opt.MapFrom(s => s.Category.NameRu));

            CreateMap<VehicleTypeDicDto, VehicleTypeTechCategory>()
                .ForMember(d => d.CategoryId, opt => opt.MapFrom(s => s.DicId));

            CreateMap<VehicleTypeClassCategory, VehicleTypeDicDto>()
                .ForMember(d => d.DicId, opt => opt.MapFrom(s => s.ClassCategoryId))
                .ForMember(d => d.DicCode, opt => opt.MapFrom(s => s.ClassCategory.Code))
                .ForMember(d => d.DicName, opt => opt.MapFrom(s => s.ClassCategory.NameRu));
            CreateMap<VehicleTypeDicDto, VehicleTypeClassCategory>()
                .ForMember(d => d.ClassCategoryId, opt => opt.MapFrom(s => s.DicId));

            CreateMap<VehicleTypeEcoClass, VehicleTypeDicDto>()
                .ForMember(d => d.DicId, opt => opt.MapFrom(s => s.EcoClassId))
                .ForMember(d => d.DicCode, opt => opt.MapFrom(s => s.EcoClass.Code))
                .ForMember(d => d.DicName, opt => opt.MapFrom(s => s.EcoClass.NameRu));
            CreateMap<VehicleTypeDicDto, VehicleTypeEcoClass>()
                .ForMember(d => d.EcoClassId, opt => opt.MapFrom(s => s.DicId));

            CreateMap<VehicleTypeModification, ModificationDto>().ReverseMap();

            CreateMap<VehicleTypeChassisDesign, VehicleTypeDicDto>()
                .ForMember(d => d.DicId, opt => opt.MapFrom(s => s.ChassisDesignId))
                .ForMember(d => d.DicCode, opt => opt.MapFrom(s => s.ChassisDesign.Code))
                .ForMember(d => d.DicName, opt => opt.MapFrom(s => s.ChassisDesign.NameRu));
            CreateMap<VehicleTypeDicDto, VehicleTypeChassisDesign>()
                .ForMember(d => d.ChassisDesignId, opt => opt.MapFrom(s => s.DicId));

            CreateMap<BaseVehicleTypeDetail, BaseVehicleTypeDetailDto>()
                .ForMember(d => d.MakeName, opt => opt.MapFrom(s => s.Make.NameRu))
                .ForMember(d => d.MakeCode, opt => opt.MapFrom(s => s.Make.Code));
            CreateMap<BaseVehicleTypeDetailDto, BaseVehicleTypeDetail>();
            
            CreateMap<VehicleUseRestriction, VehicleUseRestrictionDto>().ReverseMap();
            CreateMap<ShassisMovePermition, ShassisMovePermitionDto>().ReverseMap();
            CreateMap<VehicleRouting, VehicleRoutingDto>().ReverseMap();
            CreateMap<VehicleNote, VehicleNoteDto>().ReverseMap();

            CreateMap<CharacteristicsDetail, CharacteristicsDetailDto>()
                .ForMember(d => d.EngineTypeCode, opt => opt.MapFrom(s => s.EngineType.Code));
            CreateMap<CharacteristicsDetailDto, CharacteristicsDetail>();
                        
            CreateMap<VehicleRunningGearDetail, VehicleRunningGearDetailDto>()
                .ForMember(d => d.VehicleWheelFormulaCode, opt => opt.MapFrom(s => s.VehicleWheelFormula.Code))
                .ForMember(d => d.VehicleWheelFormulaName, opt => opt.MapFrom(s => s.VehicleWheelFormula.NameRu))
                .ForMember(d => d.PowerWheelQuantity, opt => opt.MapFrom(s => s.VehicleWheelFormula.PowerWheelQuantity))
                .ForMember(d => d.VehicleWheelQuantity, opt => opt.MapFrom(s => s.VehicleWheelQuantity != null ? s.VehicleWheelQuantity : s.VehicleWheelFormula.WheelQuantity))
                ;

            CreateMap<PoweredWheelLocation, PoweredWheelLocationDto>()
                .ForMember(d => d.WheelLocationName, opt => opt.MapFrom(s => s.WheelLocation.NameRu))
                .ForMember(d => d.WheelLocationCode, opt => opt.MapFrom(s => s.WheelLocation.Code))
                ;
            CreateMap<PoweredWheelLocationDto, PoweredWheelLocation>();

            CreateMap<VehicleRunningGearDetailDto, VehicleRunningGearDetail>();

            CreateMap<VehicleBodyworkDetail, VehicleBodyworkDetailDto>()
                .ForMember(d => d.VehicleBodyWorkTypeCode, opt => opt.MapFrom(s => s.VehicleBodyWorkType.Code))
                .ForMember(d => d.VehicleBodyWorkTypeName, opt => opt.MapFrom(s => s.VehicleBodyWorkType.NameRu))
                ;
            CreateMap<VehicleBodyworkDetailDto, VehicleBodyworkDetail>();

            CreateMap<VehicleSeatDetail, VehicleSeatDetailDto>().ReverseMap();
            CreateMap<VehicleSeatRawDetail, VehicleSeatRawDetailDto>().ReverseMap();


            CreateMap<VehicleLayoutPattern, VehicleLayoutPatternDto>()
                .ForMember(d => d.LayoutPatternCode, opt => opt.MapFrom(s => s.LayoutPattern.Code))
                .ForMember(d => d.LayoutPatternName, opt => opt.MapFrom(s => s.LayoutPattern.NameRu))
                ;
            CreateMap<VehicleLayoutPatternDto, VehicleLayoutPattern>();


            CreateMap<VehicleEngineLayout, VehicleEngineLayoutDto>()
                .ForMember(d => d.EngineLocationCode, opt => opt.MapFrom(s => s.EngineLocation.Code))
                .ForMember(d => d.EngineLocationName, opt => opt.MapFrom(s => s.EngineLocation.NameRu))
                ;
            CreateMap<VehicleEngineLayoutDto, VehicleEngineLayout>();

            CreateMap<VehicleCarriageSpaceImplementation, VehicleCarriageSpaceImplementationDto>().ReverseMap();
            CreateMap<VehicleCabin, VehicleCabinDto>().ReverseMap();
            CreateMap<VehiclePurpose, VehiclePurposeDto>()
                .ForMember(d => d.PurposeCode, opt => opt.MapFrom(s => s.Purpose.Code))
                .ForMember(d => d.PurposeName, opt => opt.MapFrom(s => s.Purpose.NameRu))
                .ForMember(d => d.OtherInfoCode, opt => opt.MapFrom(s => s.OtherInfo.Code))
                .ForMember(d => d.OtherInfoName, opt => opt.MapFrom(s => s.OtherInfo.NameRu))
                ;
            CreateMap<VehiclePurposeDto, VehiclePurpose>();

            CreateMap<VehicleFrame, VehicleFrameDto>().ReverseMap();
            CreateMap<VehiclePassengerQuantity, VehiclePassengerQuantityDto>().ReverseMap();
            CreateMap<VehicleTrunkVolumeMeasure, VehicleTrunkVolumeMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu))
                ;
            CreateMap<VehicleTrunkVolumeMeasureDto, VehicleTrunkVolumeMeasure>();

            CreateMap<VehicleLengthMeasure, VehicleLengthMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu));
            CreateMap<VehicleLengthMeasureDto, VehicleLengthMeasure>();            
            CreateMap<VehicleWidthMeasure, VehicleWidthMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu));
            CreateMap<VehicleWidthMeasureDto, VehicleWidthMeasure>();
                        
            CreateMap<VehicleHeightMeasure, VehicleHeightMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu));
            CreateMap<VehicleHeightMeasureDto, VehicleHeightMeasure>();

            CreateMap<VehicleLoadingHeightMeasure, VehicleLoadingHeightMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu));
            CreateMap<VehicleLoadingHeightMeasureDto, VehicleLoadingHeightMeasure>();

            CreateMap<VehicleMaxHeightMeasure, VehicleMaxHeightMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu));
            CreateMap<VehicleMaxHeightMeasureDto, VehicleMaxHeightMeasure>();
                        
            CreateMap<VehicleWheelbaseMeasure, VehicleWheelbaseMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu));
            CreateMap<VehicleWheelbaseMeasureDto, VehicleWheelbaseMeasure>();


            CreateMap<VehicleAxleDetail, VehicleAxleDetailDto>()
                .ForMember(d => d.AxleSweptPathMeasureUnitCode, opt => opt.MapFrom(s => s.AxleSweptPathMeasureUnit.Code))
                .ForMember(d => d.AxleSweptPathMeasureUnitName, opt => opt.MapFrom(s => s.AxleSweptPathMeasureUnit.NameRu))
                .ForMember(d => d.TechnicallyPermissibleMaxWeightOnAxleUnitCode, opt => opt.MapFrom(s => s.TechnicallyPermissibleMaxWeightOnAxleUnit.Code))
                .ForMember(d => d.TechnicallyPermissibleMaxWeightOnAxleUnitName, opt => opt.MapFrom(s => s.TechnicallyPermissibleMaxWeightOnAxleUnit.NameRu))
                ;
            CreateMap<VehicleAxleDetailDto, VehicleAxleDetail>();


            CreateMap<VehicleWeightMeasure, VehicleWeightMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu))
                .ForMember(d => d.MassTypeName, opt => opt.MapFrom(s => s.MassType.NameRu));
            CreateMap<VehicleWeightMeasureDto, VehicleWeightMeasure>();

            CreateMap<UnbrakedTrailerWeightMeasure, UnbrakedTrailerWeightMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu));
            CreateMap<UnbrakedTrailerWeightMeasureDto, UnbrakedTrailerWeightMeasure>();

            CreateMap<BrakedTrailerWeightMeasure, BrakedTrailerWeightMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu));
            CreateMap<BrakedTrailerWeightMeasureDto, BrakedTrailerWeightMeasure>();
            CreateMap<VehicleHitchLoadMeasure, VehicleHitchLoadMeasureDto>()
                .ForMember(d => d.UnitCode, opt => opt.MapFrom(s => s.Unit.Code))
                .ForMember(d => d.UnitName, opt => opt.MapFrom(s => s.Unit.NameRu));
            CreateMap<VehicleHitchLoadMeasureDto, VehicleHitchLoadMeasure>();

            CreateMap<VehicleHybridDesign, VehicleHybridDesignDto>().ReverseMap();
            
            CreateMap<EngineDetail, EngineDetailDto>()
                .ForMember(d => d.EngineCylinderArrangementCode, opt => opt.MapFrom(s => s.EngineCylinderArrangement.Code))
                .ForMember(d => d.EngineCylinderArrangementName, opt => opt.MapFrom(s => s.EngineCylinderArrangement.NameRu))
                .ForMember(d => d.EngineCapacityMeasureUnitCode, opt => opt.MapFrom(s => s.EngineCapacityMeasureUnit.Code))
                .ForMember(d => d.EngineCapacityMeasureUnitName, opt => opt.MapFrom(s => s.EngineCapacityMeasureUnit.NameRu))                
                ;
            CreateMap<EngineDetailDto, EngineDetail>();

            CreateMap<EnginePowerDetail, EnginePowerDetailDto>()
                .ForMember(d => d.EngineMaxPowerMeasureUnitCode, opt => opt.MapFrom(s => s.EngineMaxPowerMeasureUnit.Code))
                .ForMember(d => d.EngineMaxPowerMeasureUnitName, opt => opt.MapFrom(s => s.EngineMaxPowerMeasureUnit.NameRu))
                .ForMember(d => d.EngineMaxPowerShaftRotationFrequencyMeasureUnitCode, opt => opt.MapFrom(s => s.EngineMaxPowerShaftRotationFrequencyMeasureUnit.Code))
                .ForMember(d => d.EngineMaxPowerShaftRotationFrequencyMeasureUnitName, opt => opt.MapFrom(s => s.EngineMaxPowerShaftRotationFrequencyMeasureUnit.NameRu));
            CreateMap<EnginePowerDetailDto, EnginePowerDetail>();

            CreateMap<EngineTorqueDetail, EngineTorqueDetailDto>()
                .ForMember(d => d.EngineMaxTorqueMeasureUnitCode, opt => opt.MapFrom(s => s.EngineMaxTorqueMeasureUnit.Code))
                .ForMember(d => d.EngineMaxTorqueMeasureUnitName, opt => opt.MapFrom(s => s.EngineMaxTorqueMeasureUnit.NameRu))
                .ForMember(d => d.EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitCode, opt => opt.MapFrom(s => s.EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnit.Code))
                .ForMember(d => d.EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitName, opt => opt.MapFrom(s => s.EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnit.NameRu));
            CreateMap<EngineTorqueDetailDto, EngineTorqueDetail>();

            CreateMap<EngineFuelFeedDetail, EngineFuelFeedDetailDto>()
                .ForMember(d => d.FuelFeedCode, opt => opt.MapFrom(s => s.FuelFeed.Code))
                .ForMember(d => d.FuelFeedName, opt => opt.MapFrom(s => s.FuelFeed.NameRu));
            CreateMap<EngineFuelFeedDetailDto, EngineFuelFeedDetail>();

            CreateMap<VehicleFuelKind, VehicleFuelKindDto>()
                .ForMember(d => d.FuelKindCode, opt => opt.MapFrom(s => s.FuelKind.Code))
                .ForMember(d => d.FuelKindName, opt => opt.MapFrom(s => s.FuelKind.NameRu));
            CreateMap<VehicleFuelKindDto, VehicleFuelKind>();


            CreateMap<ElectricalMachineDetail, ElectricalMachineDetailDto>()
                .ForMember(d => d.ElectricalMachineVoltageMeasureUnitCode, opt => opt.MapFrom(s => s.ElectricalMachineVoltageMeasureUnit.Code))
                .ForMember(d => d.ElectricalMachineVoltageMeasureUnitName, opt => opt.MapFrom(s => s.ElectricalMachineVoltageMeasureUnit.NameRu))
                .ForMember(d => d.ElectricMotorPowerMeasureUnitCode, opt => opt.MapFrom(s => s.ElectricMotorPowerMeasureUnit.Code))
                .ForMember(d => d.ElectricMotorPowerMeasureUnitName, opt => opt.MapFrom(s => s.ElectricMotorPowerMeasureUnit.NameRu))
                .ForMember(d => d.ElectricalMachineKindCode, opt => opt.MapFrom(s => s.ElectricalMachineKind.Code))
                .ForMember(d => d.ElectricalMachineKindName, opt => opt.MapFrom(s => s.ElectricalMachineKind.NameRu))
                .ForMember(d => d.ElectricalMachineTypeCode, opt => opt.MapFrom(s => s.ElectricalMachineType.Code))
                .ForMember(d => d.ElectricalMachineTypeName, opt => opt.MapFrom(s => s.ElectricalMachineType.NameRu))
                ;
            CreateMap<ElectricalMachineDetailDto, ElectricalMachineDetail>()
                .ForMember(d => d.ElectricalMachineVoltageMeasureUnit, opt => opt.Ignore())
                .ForMember(d => d.ElectricMotorPowerMeasureUnit, opt => opt.Ignore())
                .ForMember(d => d.ElectricalMachineKind, opt => opt.Ignore());
                        
            CreateMap<ECUModelDetail, ECUModelDetailDto>().ReverseMap();
            
            CreateMap<VehicleIgnitionDetail, VehicleIgnitionDetailDto>()
                .ForMember(d => d.VehicleIgnitionTypeCode, opt => opt.MapFrom(s => s.VehicleIgnitionType.Code))
                .ForMember(d => d.VehicleIgnitionTypeName, opt => opt.MapFrom(s => s.VehicleIgnitionType.NameRu))
                ;
            CreateMap<VehicleIgnitionDetailDto, VehicleIgnitionDetail>();

            CreateMap<ExhaustDetail, ExhaustDetailDto>().ReverseMap();
            CreateMap<PowerStorageDeviceDetail, PowerStorageDeviceDetailDto>()
                .ForMember(d => d.PowerStorageDeviceTypeCode, opt => opt.MapFrom(s => s.PowerStorageDeviceType.Code))
                .ForMember(d => d.PowerStorageDeviceTypeName, opt => opt.MapFrom(s => s.PowerStorageDeviceType.NameRu))
                .ForMember(d => d.PowerStorageDeviceVoltageMeasureUnitCode, opt => opt.MapFrom(s => s.PowerStorageDeviceVoltageMeasureUnit.Code))
                .ForMember(d => d.PowerStorageDeviceVoltageMeasureUnitName, opt => opt.MapFrom(s => s.PowerStorageDeviceVoltageMeasureUnit.NameRu))
                .ForMember(d => d.VehicleRangeUnitName, opt => opt.MapFrom(s => s.VehicleRangeUnit.NameRu));
            CreateMap<PowerStorageDeviceDetailDto, PowerStorageDeviceDetail>();

            CreateMap<VehicleClutchDetail, VehicleClutchDetailDto>().ReverseMap();

            CreateMap<VehicleSteeringDetail, VehicleSteeringDetailDto>()
                .ForMember(d => d.SteeringWheelPositionCode, opt => opt.MapFrom(s => s.SteeringWheelPosition.Code))
                .ForMember(d => d.SteeringWheelPositionName, opt => opt.MapFrom(s => s.SteeringWheelPosition.NameRu));
            CreateMap<VehicleSteeringDetailDto, VehicleSteeringDetail>();

            CreateMap<VehicleSteeringDescriptionDto, VehicleSteeringDescription>().ReverseMap();

            CreateMap<VehicleSuspensionDetail, VehicleSuspensionDetailDto>()
                .ForMember(d => d.VehicleSuspensionKindCode, opt => opt.MapFrom(s => s.VehicleSuspensionKind.Code))
                .ForMember(d => d.VehicleSuspensionKindName, opt => opt.MapFrom(s => s.VehicleSuspensionKind.NameRu))
                ;
            CreateMap<VehicleSuspensionDetailDto, VehicleSuspensionDetail>();

            CreateMap<TransmissionType, TransmissionTypeDto>()
                .ForMember(d => d.TransTypeCode, opt => opt.MapFrom(s => s.TransType.Code))
                .ForMember(d => d.TransTypeName, opt => opt.MapFrom(s => s.TransType.NameRu));
            CreateMap<TransmissionTypeDto, TransmissionType>();

            CreateMap<TransmissionUnitDetail, TransmissionUnitDetailDto>()
                .ForMember(d => d.UnitKindName, opt => opt.MapFrom(s => s.UnitKind.NameRu)) 
                .ForMember(d => d.UnitKindCode, opt => opt.MapFrom(s => s.UnitKind.Code)) 
                .ForMember(d => d.AxisDistributionCode, opt => opt.MapFrom(s => s.AxisDistribution.Code)) 
                .ForMember(d => d.AxisDistributionName, opt => opt.MapFrom(s => s.AxisDistribution.NameRu)) 
                .ForMember(d => d.TransmissionBoxTypeCode, opt => opt.MapFrom(s => s.TransmissionBoxType.Code))
                .ForMember(d => d.TransmissionBoxTypeName, opt => opt.MapFrom(s => s.TransmissionBoxType.NameRu))
                .ForMember(d => d.MainGearTypeCode, opt => opt.MapFrom(s => s.MainGearType.Code))
                .ForMember(d => d.MainGearTypeName, opt => opt.MapFrom(s => s.MainGearType.NameRu))
                .ForMember(d => d.TransmissionTypeId, opt => opt.MapFrom(s => s.TransmissionTypeId))
                .ForMember(d => d.TransTypeId, opt => opt.MapFrom(s => s.TransmissionType.TransTypeId))
                .ForMember(d => d.TransmissionTypeName, opt => opt.MapFrom(s => s.TransmissionType.TransType.NameRu))
                .ForMember(d => d.TransmissionUnitGearQuantity, opt => opt.MapFrom(s => s.TransmissionUnitGears.Count))
                ; 
            CreateMap<TransmissionUnitDetailDto, TransmissionUnitDetail>();

            CreateMap<TransmissionUnitGearDetail, TransmissionUnitGearDto>()
                .ForMember(d => d.TransmissionUnitGearTypeCode, opt => opt.MapFrom(s => s.TransmissionUnitGearType.Code))
                .ForMember(d => d.TransmissionUnitGearTypeName, opt => opt.MapFrom(s => s.TransmissionUnitGearType.NameRu))
                .ForMember(d => d.TransmissionUnitGearValueCode, opt => opt.MapFrom(s => s.TransmissionUnitGearValue.Code))                
                .ForMember(d => d.TransmissionUnitGearValueName, opt => opt.MapFrom(s => s.TransmissionUnitGearValue.NameRu))                
                ;
            CreateMap<TransmissionUnitGearDto, TransmissionUnitGearDetail>();

            CreateMap<VehicleBrakingSystemDetail, VehicleBrakingSystemDetailDto>()
                .ForMember(d => d.VehicleBrakingSystemKindCode, opt => opt.MapFrom(s => s.VehicleBrakingSystemKind.Code))
                .ForMember(d => d.VehicleBrakingSystemKindName, opt => opt.MapFrom(s => s.VehicleBrakingSystemKind.NameRu))
                ;

            CreateMap<VehicleBrakingSystemDetailDto, VehicleBrakingSystemDetail>();
            CreateMap<VehicleTyreKindInfo, VehicleTyreKindInfoDto>()
                .ForMember(d => d.VehicleTyreKindSpeedCode, opt => opt.MapFrom(s => s.VehicleTyreKindSpeed.Code))
                .ForMember(d => d.VehicleTyreKindSpeedName, opt => opt.MapFrom(s => s.VehicleTyreKindSpeed.NameRu))
                ;
            CreateMap<VehicleTyreKindInfoDto, VehicleTyreKindInfo>();

            CreateMap<VehicleEquipmentInfo, VehicleEquipmentInfoDto>().ReverseMap();

            CreateMap<VehicleGeneralView, GeneralViewDto>()
                .ForMember(d => d.VehiclePicture, opt => opt.MapFrom(s => Convert.ToBase64String(s.VehiclePicture)))
                ;
        }
    }
}

﻿using AutoMapper;
using DBCore.DTOs;
using DBCore.DTOs.Administration;
using DBCore.Models;
using DBCore.Models.Administration;
using DBCore.Models.Administration.News;
using DBCore.Models.Notifications;

namespace DBCore.Mappings
{
    public class AdministrationProfile : Profile
    {
        public AdministrationProfile()
        {
            CreateMap<AspNetUsers, UserDto>()
                .ForMember(d => d.LastName, opt => opt.MapFrom(s => s.UserProfile.LastName))
                .ForMember(d => d.FirstName, opt => opt.MapFrom(s => s.UserProfile.FirstName))
                .ForMember(d => d.SecondName, opt => opt.MapFrom(s => s.UserProfile.SecondName))
                .ForMember(d => d.FullName, opt => opt.MapFrom(s => s.UserProfile.LastName + " " + s.UserProfile.FirstName + (string.IsNullOrEmpty(s.UserProfile.SecondName) ? "" : " " + s.UserProfile.SecondName)))
                
                .ForMember(d => d.OrganizationId, opt => opt.MapFrom(s => s.OrganizationId))
                .ForMember(d => d.OrganizationName, opt => opt.MapFrom(s => s.Organization.NameRu))
                
                .ForMember(d => d.StatusId, opt => opt.MapFrom(s => s.UserProfile.Status.Id))
                .ForMember(d => d.StatusCode, opt => opt.MapFrom(s => s.UserProfile.Status.Code))
                .ForMember(d => d.StatusName, opt => opt.MapFrom(s => s.UserProfile.Status.NameRu))
                .ForMember(d => d.UserRoles, opt => opt.MapFrom(s => s.AspNetUserRoles));
            CreateMap<UserDto, AspNetUsers>();

            CreateMap<AspNetRoles, RoleDto>();
            CreateMap<RoleDto, AspNetRoles>();

            CreateMap<AspNetUserRoles, UserRoleDto>();
            CreateMap<UserRoleDto, AspNetUserRoles>();
            
            CreateMap<RegrequestDto, RegRequest>();
            CreateMap<RegRequest, RegrequestDto>()
                .ForMember(d => d.StatusCode, opt => opt.MapFrom(s => s.Status.Code))
                .ForMember(d => d.StatusName, opt => opt.MapFrom(s => s.Status.NameRu))
                ;

            CreateMap<NotificationDto, Notification>();
            CreateMap<Notification, NotificationDto>();

            CreateMap<News, NewsDto>();
            CreateMap<NewsDto, News>();
        }
    }
}

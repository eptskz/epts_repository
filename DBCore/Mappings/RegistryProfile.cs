﻿using AutoMapper;
using DBCore.DTOs;
using DBCore.DTOs.Dictionary;
using DBCore.DTOs.Registries;
using DBCore.DTOs.Registries.Invoice;
using DBCore.Models;
using DBCore.Models.Dictionaries;
using DBCore.Models.Registries;
using DBCore.Models.Registries.Invoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBCore.Mappings
{
    public class RegistryProfile : Profile
    {
        public RegistryProfile ()
        {
            CreateMap<Organization, OrganizationDto>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NameRu))
                .ForMember(d => d.ShortName, opt => opt.MapFrom(s => s.ShortNameRu))
                .ForMember(d => d.NotOrganizationDocumentIndicator, opt => opt.MapFrom(s => s.NotOrganizationDocumentIndicator))
                /*
                .ForMember(d => d.Phones, opt => opt.MapFrom(s => s.CommunicationInfos
                    .Where(ot => ot.CommunicationType.Code == DictionaryCodes.CommunicationInfoCodes.Telephone)))                
                .ForMember(d => d.Faxes, opt => opt.MapFrom(s => s.CommunicationInfos
                    .Where(ot => ot.CommunicationType.Code == DictionaryCodes.CommunicationInfoCodes.Telefax)))
                .ForMember(d => d.Emails, opt => opt.MapFrom(s => s.CommunicationInfos
                    .Where(ot => ot.CommunicationType.Code == DictionaryCodes.CommunicationInfoCodes.Email)))
                */
                ;

            CreateMap<Organization, OrganizationRegistrDto>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NameRu))
                .ForMember(d => d.ShortName, opt => opt.MapFrom(s => s.ShortNameRu))
                .ForMember(d => d.StatusName, opt => opt.MapFrom(s => s.Status.NameRu))
                .ForMember(d => d.HeadFullName, opt => opt.MapFrom(s => (string.IsNullOrEmpty(s.HeadLastName) ? string.Empty : s.HeadLastName)
                            + (string.IsNullOrEmpty(s.HeadFirstName) ? string.Empty : " " + s.HeadFirstName)   
                            + (string.IsNullOrEmpty(s.HeadSecondName) ? string.Empty : " " + s.HeadSecondName)))
                .ForMember(d => d.OrganizationTypeNames, opt => opt.MapFrom(s => s.OrganizationTypes != null && s.OrganizationTypes.Count > 0 
                        ? s.OrganizationTypes.Select(ot => ot.OrgType.NameRu).Aggregate((p,n) => p + ", " + n) 
                        : ""))
                
                .ForMember(d => d.Phones, opt => opt.MapFrom(s => s.CommunicationInfos != null && s.CommunicationInfos.Any(ot => ot.CommunicationType.Code == DictionaryCodes.CommunicationInfoCodes.Telephone)
                ? s.CommunicationInfos.Where(ot => ot.CommunicationType.Code == DictionaryCodes.CommunicationInfoCodes.Telephone)
                    .Select(ot => ot.CommunicationValue).Aggregate((p, n) => p + ", " + n) : ""))
                
                .ForMember(d => d.Faxes, opt => opt.MapFrom(s => s.CommunicationInfos != null && s.CommunicationInfos.Any(ot => ot.CommunicationType.Code == DictionaryCodes.CommunicationInfoCodes.Telefax)
                ? s.CommunicationInfos.Where(ot => ot.CommunicationType.Code == DictionaryCodes.CommunicationInfoCodes.Telefax)
                    .Select(ot => ot.CommunicationValue).Aggregate((p, n) => p + ", " + n) : ""))

                .ForMember(d => d.Emails, opt => opt.MapFrom(s => s.CommunicationInfos != null && s.CommunicationInfos.Any(ot => ot.CommunicationType.Code == DictionaryCodes.CommunicationInfoCodes.Email) 
                ? s.CommunicationInfos.Where(ot => ot.CommunicationType.Code == DictionaryCodes.CommunicationInfoCodes.Email)
                    .Select(ot => ot.CommunicationValue).Aggregate((p, n) => p + ", " + n) : ""))

                .ForMember(d => d.FactAddressName, opt => opt.MapFrom(s => !string.IsNullOrEmpty(s.FactAddress) ? s.FactAddress :
                        (s.FactRegionOrCity != null ? s.FactRegionOrCity.NameRu : "")
                        + (s.FactDistinctOrCity != null ? ", " + s.FactDistinctOrCity.NameRu : "")
                        + (s.FactOkrugOrCity != null ? ", " + s.FactOkrugOrCity.NameRu : "")
                        + (s.FactVillage != null ? ", " + s.FactVillage.NameRu : "")
                        + ", " + s.FStreetName
                        + ", " + s.FBuildingNumber
                        + ", " + s.FRoomNumber))

                .ForMember(d => d.JuridicalAddressName, opt => opt.MapFrom(s => !string.IsNullOrEmpty(s.JuridicalAddress) ? s.JuridicalAddress :
                        (s.JuridicalRegionOrCity != null ?  s.JuridicalRegionOrCity.NameRu : "")
                        + (s.JuridicalDistinctOrCity != null ? ", " + s.JuridicalDistinctOrCity.NameRu : "")
                        + (s.JuridicalOkrugOrCity != null ? ", " + s.JuridicalOkrugOrCity.NameRu : "")
                        + (s.JuridicalVillage != null ?  ", " + s.JuridicalVillage.NameRu : "")
                        + ", " + s.JStreetName
                        + ", " + s.JBuildingNumber
                        + ", " + s.JRoomNumber));

            CreateMap<OrganizationDto, Organization>()                 
                .ForMember(d => d.NameRu, opt => opt.MapFrom(s => s.Name)) 
                .ForMember(d => d.ShortNameRu, opt => opt.MapFrom(s => s.ShortName)) 
                .ForMember(d => d.Bin, opt => opt.MapFrom(s => s.Bin))
                .ForMember(d => d.NotOrganizationDocumentIndicator, opt => opt.MapFrom(s => s.NotOrganizationDocumentIndicator))
                .ForMember(d => d.FactCountry, opt => opt.Ignore())
                .ForMember(d => d.Country, opt => opt.Ignore())
                ;

            CreateMap<OrganizationType, OrganizationTypeDto>()
                .ForMember(d => d.OrgTypeCode, opt => opt.MapFrom(s => s.OrgType.Code))
                .ForMember(d => d.OrgTypeName, opt => opt.MapFrom(s => s.OrgType.NameRu))
                .ForMember(d => d.OrgTypeId, opt => opt.MapFrom(s => s.OrgTypeId))
                .ForMember(d => d.OrganizationId, opt => opt.MapFrom(s => s.OrganizationId));
            CreateMap<OrganizationTypeDto, OrganizationType>();

            CreateMap<CommunicationInfo, CommunicationInfoDto>()
                .ForMember(d => d.CommunicationTypeId, opt => opt.MapFrom(s => s.CommunicationTypeId))
                .ForMember(d => d.CommunicationTypeCode, opt => opt.MapFrom(s => s.CommunicationType.Code))
                .ForMember(d => d.CommunicationTypeName, opt => opt.MapFrom(s => s.CommunicationType.NameRu));
            CreateMap<CommunicationInfoDto, CommunicationInfo>();


            CreateMap<DictionaryObjectModel, OrganizationDigPassportType>()
                .ForMember(d => d.OrganizationId, opt => opt.MapFrom(s => s.ObjectId))
                .ForMember(d => d.DigPassportTypeId, opt => opt.MapFrom(s => s.DicId));
            CreateMap<OrganizationDigPassportType, DictionaryObjectModel>()
                .ForMember(d => d.ObjectId, opt => opt.MapFrom(s => s.OrganizationId))
                .ForMember(d => d.DicId, opt => opt.MapFrom(s => s.DigPassportTypeId));


            CreateMap<OrgDocumentDto, OrganizationDocument>()
                .ForMember(d => d.DocType, opt => opt.Ignore())
                .ForMember(d => d.TechRegulationSubjectType, opt => opt.Ignore())
                .ForMember(d => d.TechRegulationSubjectUnit, opt => opt.Ignore())
                .ForMember(d => d.IssueCountry, opt => opt.Ignore())
                ;
            CreateMap<OrganizationDocument, OrgDocumentDto>()
                .ForMember(d => d.DocTypeCode, opt => opt.MapFrom(s => s.DocType.Code))
                .ForMember(d => d.DocTypeName, opt => opt.MapFrom(s => s.DocType.NameRu))
                .ForMember(d => d.TechRegulationSubjectTypeCode, opt => opt.MapFrom(s => s.TechRegulationSubjectType.Code))
                .ForMember(d => d.TechRegulationSubjectTypeName, opt => opt.MapFrom(s => s.TechRegulationSubjectType.NameRu))
                .ForMember(d => d.TechRegulationSubjectUnitCode, opt => opt.MapFrom(s => s.TechRegulationSubjectUnit.Code))
                .ForMember(d => d.TechRegulationSubjectUnitName, opt => opt.MapFrom(s => s.TechRegulationSubjectUnit.NameRu))
                .ForMember(d => d.IssueCountryCode, opt => opt.MapFrom(s => s.IssueCountry.Code))
                .ForMember(d => d.IssueCountryName, opt => opt.MapFrom(s => s.IssueCountry.NameRu));

            CreateMap<Invoice, InvoiceDto>()
                .ForMember(d => d.ContractNumber, opt => opt.MapFrom(s => s.Contract.Number))
                .ForMember(d => d.ContractDate, opt => opt.MapFrom(s => s.Contract.ContractDate))
                .ForMember(d => d.AccountNumber, opt => opt.MapFrom(s => s.Contract.AccountNumber))
                .ForMember(d => d.AccountDate, opt => opt.MapFrom(s => s.Contract.AccountDate))
                .ForMember(d => d.OrganizationBin, opt => opt.MapFrom(s => s.Contract.ClientOrg.Bin))                
                .ForMember(d => d.OrganizationName, opt => opt.MapFrom(s => s.Contract.ClientOrg.NameRu))
                .ForMember(d => d.StatusCode, opt => opt.MapFrom(s => s.Status.Code))
                .ForMember(d => d.StatusName, opt => opt.MapFrom(s => s.Status.NameRu));
            CreateMap<InvoiceDto, Invoice>()
                .ForMember(d => d.PaidDateTime, opt => opt.MapFrom(s => DateTime.Parse(s.PaidDateTime)))
                .ForMember(d => d.PaidFineDateTime, opt => opt.MapFrom(s => DateTime.Parse(s.PaidFineDateTime)))
                ;
        }
    }
}

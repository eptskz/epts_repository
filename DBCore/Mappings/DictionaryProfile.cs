﻿using AutoMapper;
using DBCore.DTOs;
using DBCore.DTOs.Dictionary;
using DBCore.Models;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Mappings
{
    public class DictionaryProfile : Profile
    {
        public DictionaryProfile()
        {
            CreateMap<NsiValue, DictionaryDto>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NameRu))      
                .ForMember(d => d.NsiCode, opt => opt.MapFrom(s => s.Nsi.Code));

            CreateMap<Nsi, NsiDto>().ReverseMap();
            CreateMap<NsiValue, NsiValueDto>()
                .ForMember(d => d.NsiCode, opt => opt.MapFrom(s => s.Nsi.Code))
                .ForMember(d => d.NsiName, opt => opt.MapFrom(s => s.Nsi.NameRu))
                ;

            CreateMap<NsiValueDto, NsiValue>();            

            CreateMap<DicOrganizationType, DictionaryDto>()
                .ForMember(d => d.Code, opt => opt.MapFrom(s => s.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NameRu));

            CreateMap<DicKato, DictionaryDto>()
                .ForMember(d => d.Code, opt => opt.MapFrom(s => s.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NameRu));

            CreateMap<DicStatus, DictionaryDto>()
                .ForMember(d => d.Code, opt => opt.MapFrom(s => s.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NameRu));

            CreateMap<DicDocType, DictionaryDto>()
                .ForMember(d => d.Code, opt => opt.MapFrom(s => s.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NameRu));
        }
    }
}

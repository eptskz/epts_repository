﻿using AutoMapper;
using DBCore.Models.FileStorage;


namespace DBCore.Mappings
{
    public class FileStorageProfile : Profile
    {
        public FileStorageProfile()
        {

            CreateMap<FileStorageItem, FileStorageItemDto>().ReverseMap();
        }
    }
}

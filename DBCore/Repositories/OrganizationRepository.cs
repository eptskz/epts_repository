﻿using DBCore.Models;
using DBCore.Models.ComplienceDocuments;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DBCore.Enums;
using DBCore.Models.Vehicle;
using DBCore.DTOs;
using DBCore.Models.Registries;
using DbLog;

namespace DBCore.Repositories
{
    public class OrganizationRepository : GenericRepository<Organization>
    {
        public OrganizationRepository(IMongoDatabaseSettings mongo_settings, EptsContext _sql_client, LogContext logContext) 
            : base(mongo_settings, _sql_client, logContext) { }

        public async Task AddOrUpdateAsync(Organization entity)
        {
            if (entity.Id == 0)
            {
                DbSet.Add(entity);
                sql_client.SaveChanges();

                if (entity.ParentId != null)
                {
                    var isExist = await sql_client.OrganizationRefs
                        .AnyAsync(or => or.ParentId == entity.ParentId.Value && or.ChildId == entity.Id);
                    if (!isExist)
                    {
                        await sql_client.OrganizationRefs.AddAsync(new OrganizationRef()
                        {
                            ChildId = entity.Id,
                            ParentId = entity.ParentId.Value
                        });
                    }
                }
            }
            else
            {
                DbSet.Attach(entity);
                sql_client.Entry(entity).State = EntityState.Modified;

                foreach (var e in entity.OrganizationTypes)
                {
                    if (e.Id == 0)
                        e.OrganizationId = entity.Id;

                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            var isExist = await sql_client.OrganizationType
                                .AnyAsync(or => or.OrganizationId == entity.Id && or.OrgTypeId == e.OrgTypeId);
                            if (!isExist)
                                sql_client.Entry(e).State = EntityState.Added;
                            e.State = ObjectStateEnum.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                    }
                }

                foreach (var e in entity.CommunicationInfos)
                {
                    if (e.Id == 0)
                        e.OrganizationId = entity.Id;
                    
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            e.State = ObjectStateEnum.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                    }
                }

                foreach (var e in entity.OrganizationDigPassportTypes)
                {
                    if (e.Id == 0)
                        e.OrganizationId = entity.Id;

                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            e.State = ObjectStateEnum.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                    }
                }
                if (entity.OrganizationDocuments != null)
                {
                    foreach (var e in entity.OrganizationDocuments)
                    {
                        if (e.Id == 0)
                            e.OrganizationId = entity.Id;

                        switch (e.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(e).State = EntityState.Added;
                                e.State = ObjectStateEnum.Unchanged;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(e).State = EntityState.Deleted;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(e).State = EntityState.Unchanged;
                                break;
                        }
                    }
                }

                if (entity.ParentId != null)
                {
                    var isExist = await sql_client.OrganizationRefs
                        .AnyAsync(or => or.ParentId == entity.ParentId.Value && or.ChildId == entity.Id);
                    if (!isExist)
                    {
                        await sql_client.OrganizationRefs.AddAsync(new OrganizationRef()
                        {
                            ChildId = entity.Id,
                            ParentId = entity.ParentId.Value
                        });
                    }
                }
            }
            await sql_client.SaveChangesAsync();
        }

        public override bool Delete(object id)
        {
            Organization entityToDelete = DbSet.Find(id);
            entityToDelete.Deleted = DateTime.Now;
            sql_client.Entry(entityToDelete).State = EntityState.Modified;
            sql_client.SaveChanges();

            return true;
        }

        public bool Restore(int id)
        {
            Organization entityToDelete = DbSet.Find(id);
            entityToDelete.Deleted = null;
            sql_client.Entry(entityToDelete).State = EntityState.Modified;
            sql_client.SaveChanges();

            return true;
        }

        public async Task<bool> DeleteNested(int id, int parentId)
        {
            var orgRef = await sql_client.OrganizationRefs
                .FirstOrDefaultAsync(or => or.ChildId == id && or.ParentId == parentId);
            sql_client.OrganizationRefs.Remove(orgRef);
            await sql_client.SaveChangesAsync();
            return true;
        }

        public async Task<int> UpdateOrgDocument(OrganizationDocument entity)
        {
            switch (entity.State)
            {
                case ObjectStateEnum.Added:
                    sql_client.Entry(entity).State = EntityState.Added;
                    break;
                case ObjectStateEnum.Unchanged:
                    sql_client.Entry(entity).State = EntityState.Unchanged;
                    break;
                case ObjectStateEnum.Deleted:
                    sql_client.Entry(entity).State = EntityState.Deleted;
                    break;
            }

            await sql_client.SaveChangesAsync();
            return entity.Id;
        }

        public IQueryable<OrganizationDocument> GetOrgDocumentsAsIQueryable()
        {
            return this.sql_client.Set<OrganizationDocument>();
        }
    }
}

﻿using DBCore.Models;
using DBCore.Models.ComplienceDocuments;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DBCore.Enums;
using DBCore.Models.Vehicle;
using DBCore.DTOs;
using DBCore.Interfaces;
using DBCore.Models.ComplienceDocuments.Vehicle;
using DbLog;
using DbLog.Base;
using DbLog.Models;

namespace DBCore.Repositories
{
    public class ComplienceDocRepository : GenericRepository<ComplienceDocument>
    {
        public ComplienceDocRepository(IMongoDatabaseSettings mongo_settings, EptsContext _sql_client, LogContext logContext) : base(mongo_settings, _sql_client, logContext)
        {

        }

        public void Restore(Guid entityId)
        {
            ComplienceDocument entityToRestore = DbSet.Find(entityId);
            if (sql_client.Entry(entityToRestore).State == EntityState.Detached)
            {
                DbSet.Attach(entityToRestore);
            }

            if (entityToRestore is IAuditEntity)
                InsertAuditLogAsync(entityToRestore as IAuditEntity, null, AuditActionType.Restore).ConfigureAwait(true);

            entityToRestore.Deleted = null;

            DbSet.Update(entityToRestore);
        }

        public async Task AddOrUpdateAsync(ComplienceDocument entity)
        {
            if (entity.Id == Guid.Empty)
            {
                DbSet.Add(entity);
                await this.InsertAuditLogAsync(entity, null, AuditActionType.Insert);
            }
            else
            {
                DbSet.Attach(entity);
                sql_client.Entry(entity).State = EntityState.Modified;

                foreach (var e in entity.RepresentativeManufacturers)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            var isExist = await sql_client.RepresentativeManufacturer.AnyAsync(or => or.DocumentId == entity.Id && or.OrganizationId == e.OrganizationId);
                            if (isExist)
                                e.State = ObjectStateEnum.Unchanged;
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                    }
                }

                foreach (var e in entity.AssemblyPlants)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            var isExist = await sql_client.AssemblyPlant.AnyAsync(or => or.DocumentId == entity.Id && or.OrganizationId == e.OrganizationId);
                            if (isExist)
                                e.State = ObjectStateEnum.Unchanged;
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                    }
                }

                foreach (var e in entity.AssemblyKitSuppliers)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            var isExist = await sql_client.AssemblyKitSupplier.AnyAsync(or => or.DocumentId == entity.Id && or.OrganizationId == e.OrganizationId);
                            if (isExist)
                                e.State = ObjectStateEnum.Unchanged;
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                    }
                }

                foreach (var e in entity.BlankNumbers)
                {
                    if (e.DocumentId == Guid.Empty)
                        e.DocumentId = entity.Id;

                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                    }
                }

                foreach (var e in entity.VinNumbers)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                if (entity.VehicleLabelingDetail != null)
                {
                    sql_client.Attach(entity.VehicleLabelingDetail);
                    sql_client.Entry(entity.VehicleLabelingDetail).State = EntityState.Modified;

                    foreach (var e in entity.VehicleLabelingDetail.VehicleLabelLocations)
                    {
                        switch (e.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(e).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(e).State = EntityState.Deleted;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(e).State = EntityState.Unchanged;
                                break;
                        }
                    }

                    foreach (var e in entity.VehicleLabelingDetail.VehicleIdentificationNumberLocations)
                    {
                        switch (e.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(e).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(e).State = EntityState.Deleted;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(e).State = EntityState.Unchanged;
                                break;
                        }
                    }


                    foreach (var e in entity.VehicleLabelingDetail.VinCharacterDetails)
                    {
                        switch (e.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(e).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(e).State = EntityState.Unchanged;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(e).State = EntityState.Deleted;
                                break;
                        }
                        
                        foreach (var cd in e.VinCharacterDescriptions)
                        {
                            switch (cd.State)
                            {
                                case ObjectStateEnum.Added:
                                    sql_client.Entry(cd).State = EntityState.Added;
                                    break;
                                case ObjectStateEnum.Deleted:
                                    sql_client.Entry(cd).State = EntityState.Deleted;
                                    break;
                                case ObjectStateEnum.Unchanged:
                                    sql_client.Entry(cd).State = EntityState.Unchanged;
                                    break;
                            }
                        }                        
                    }
                }

                if (entity.VehicleTypeDetail != null)
                {
                    sql_client.Attach(entity.VehicleTypeDetail);
                    sql_client.Entry(entity.VehicleTypeDetail).State = EntityState.Modified;

                    ProcessVehicleDetails(entity.VehicleTypeDetail);

                    ProcessCharacteristics(entity.VehicleTypeDetail);
                }

                var oldAuditEntity = DbSet.Find(entity.Id);
                await this.InsertAuditLogAsync(entity, oldAuditEntity, DbLog.Models.AuditActionType.Update);
            }
            await sql_client.SaveChangesAsync();
        }

        public IQueryable<CharacteristicsDetail> GetCharacteristicsDetails()
        {
            return sql_client.Set<CharacteristicsDetail>().AsQueryable();
        }

        public IQueryable<EngineDetail> GetEngineDetails()
        {
            return sql_client.Set<EngineDetail>().AsQueryable();
        }

        public IQueryable<ElectricalMachineDetail> GetElectricalMachineDetails()
        {
            return sql_client.Set<ElectricalMachineDetail>().AsQueryable();
        }

        
        public IQueryable<VehicleBodyworkDetail> GetVehicleBodyworkDetails()
        {
            return sql_client.Set<VehicleBodyworkDetail>().AsQueryable();
        }

        public IQueryable<VehicleSeatDetail> GetVehicleSeatDetails()
        {
            return sql_client.Set<VehicleSeatDetail>().AsQueryable();
        }

        public IQueryable<VehicleLayoutPattern> GetVehicleLayoutPatterns()
        {
            return sql_client.Set<VehicleLayoutPattern>().AsQueryable();
        }
        public IQueryable<VehicleLabelingDetail> GetVehicleLabelingDetail()
        {
            return sql_client.Set<VehicleLabelingDetail>().AsQueryable();
        }

       

        private void ProcessCharacteristics(VehicleTypeDetail entity)
        {
            if (entity.CharacteristicsDetail != null)
            {
                sql_client.Attach(entity.CharacteristicsDetail);
                sql_client.Entry(entity.CharacteristicsDetail).State = EntityState.Modified;

                foreach (var e in entity.CharacteristicsDetail.VehicleRunningGearDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleBodyworkDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleSeatDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }

                    foreach (var cd in e.VehicleSeatRawDetails)
                    {
                        switch (cd.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(cd).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(cd).State = EntityState.Deleted;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(cd).State = EntityState.Unchanged;
                                break;
                        }
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleLayoutPatterns)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleEngineLayouts)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleCarriageSpaceImplementations)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleCabins)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehiclePurposes)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleFrames)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehiclePassengerQuantities)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleTrunkVolumeMeasures)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.LengthRanges)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.WidthRanges)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.HeightRanges)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.LoadingHeightRanges)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.MaxHeightRanges)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.WheelbaseMeasureRanges)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleAxleDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleWeightMeasures)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.BrakedTrailerWeightMeasures)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.UnbrakedTrailerWeightMeasures)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleHitchLoadMeasures)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }


                foreach (var e in entity.CharacteristicsDetail.VehicleHybridDesigns)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.EngineDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Modified:
                            sql_client.Entry(e).State = EntityState.Modified;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }

                    foreach (var ch in e.EnginePowerDetails)
                    {
                        switch (ch.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(ch).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(ch).State = EntityState.Unchanged;
                                break;
                            case ObjectStateEnum.Modified:
                                sql_client.Entry(ch).State = EntityState.Modified;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(ch).State = EntityState.Deleted;
                                break;
                        }
                    }

                    foreach (var ch in e.EngineTorqueDetails)
                    {
                        switch (ch.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(ch).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(ch).State = EntityState.Unchanged;
                                break;
                            case ObjectStateEnum.Modified:
                                sql_client.Entry(ch).State = EntityState.Modified;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(ch).State = EntityState.Deleted;
                                break;
                        }
                    }

                    foreach (var ch in e.ECUModelDetails)
                    {
                        switch (ch.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(ch).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(ch).State = EntityState.Unchanged;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(ch).State = EntityState.Deleted;
                                break;
                        }
                    }

                    foreach (var ch in e.VehicleIgnitionDetails)
                    {
                        switch (ch.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(ch).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(ch).State = EntityState.Unchanged;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(ch).State = EntityState.Deleted;
                                break;
                        }
                    }

                    foreach (var ch in e.ExhaustDetails)
                    {
                        switch (ch.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(ch).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(ch).State = EntityState.Unchanged;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(ch).State = EntityState.Deleted;
                                break;
                        }
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.ElectricalMachineDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                    
                    foreach (var ch in e.PowerStorageDeviceDetails)
                    {
                        switch (ch.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(ch).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(ch).State = EntityState.Unchanged;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(ch).State = EntityState.Deleted;
                                break;
                        }
                    }
                }


                foreach (var e in entity.CharacteristicsDetail.VehicleClutchDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleSteeringDescriptions)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleSteeringDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleSuspensionDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleBrakingSystemDetails)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleTyreKindInfos)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleEquipmentInfos)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                }

                foreach (var e in entity.CharacteristicsDetail.VehicleTransmissionTypes)
                {
                    switch (e.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(e).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(e).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Modified:
                            sql_client.Entry(e).State = EntityState.Modified;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(e).State = EntityState.Deleted;
                            break;
                    }
                    foreach (var ch in e.TransmissionUnitDetails)
                    {
                        switch (ch.State)
                        {
                            case ObjectStateEnum.Added:
                                sql_client.Entry(ch).State = EntityState.Added;
                                break;
                            case ObjectStateEnum.Unchanged:
                                sql_client.Entry(ch).State = EntityState.Unchanged;
                                break;
                            case ObjectStateEnum.Modified:
                                sql_client.Entry(ch).State = EntityState.Modified;
                                break;
                            case ObjectStateEnum.Deleted:
                                sql_client.Entry(ch).State = EntityState.Deleted;
                                break;
                        }


                        foreach (var cch in ch.TransmissionUnitGears)
                        {
                            //sql_client.Attach(cch);
                            switch (cch.State)
                            {
                                case ObjectStateEnum.Added:
                                    sql_client.Entry(cch).State = EntityState.Added;
                                    break;
                                case ObjectStateEnum.Unchanged:
                                    sql_client.Entry(cch).State = EntityState.Unchanged;
                                    break;
                                case ObjectStateEnum.Modified:
                                    sql_client.Entry(cch).State = EntityState.Modified;
                                    break;
                                case ObjectStateEnum.Deleted:
                                    sql_client.Entry(cch).State = EntityState.Deleted;
                                    break;
                            }
                        }

                    }
                }
            }
        }

        private void ProcessVehicleDetails(VehicleTypeDetail entity)
        {
            foreach (var e in entity.Makes)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.CommercialNames)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.Types)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.TechCategories)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.ClassCategories)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.EcoClasses)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.Modifications)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.ChassisDesigns)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.BaseVehicleTypeDetails)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.VehicleUseRestrictions)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.ShassisMovePermitions)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.VehicleRoutings)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var e in entity.VehicleNotes)
            {
                switch (e.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(e).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(e).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(e).State = EntityState.Deleted;
                        break;
                }
            }
        }

        public async Task<Guid> UpdateVehicleTypeDetailAsync(VehicleTypeDetail entity)
        {
            if (entity.Id == Guid.Empty)
            {
                sql_client.Add(entity);
            }
            else
            {
                sql_client.Attach(entity);
            }
            
            ProcessVehicleDetails(entity);
            await sql_client.SaveChangesAsync();

            return entity.Id;
        }
    
        public async Task<int> UpdateCommercialNameAsync(VehicleCommercialName entity)
        {
            switch (entity.State)
            {
                case ObjectStateEnum.Added:
                    sql_client.Entry(entity).State = EntityState.Added;
                    break;
                case ObjectStateEnum.Unchanged:
                    sql_client.Entry(entity).State = EntityState.Unchanged;
                    break;
                case ObjectStateEnum.Deleted:
                    sql_client.Entry(entity).State = EntityState.Deleted;
                    break;
            }

            await sql_client.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<T> UpdateEntityStateAsync<T>(IEntityState<T> entity)
        {
            switch (entity.State)
            {
                case ObjectStateEnum.Added:
                    sql_client.Entry(entity).State = EntityState.Added;
                    break;
                case ObjectStateEnum.Unchanged:
                    sql_client.Entry(entity).State = EntityState.Unchanged;
                    break;
                case ObjectStateEnum.Deleted:
                    sql_client.Entry(entity).State = EntityState.Deleted;
                    break;
            }

            await sql_client.SaveChangesAsync();
            return entity.Id;
        }
        
        public async Task<EngineDetail> UpdateEngineDetailStateAsync(EngineDetail entity)
        {
            switch (entity.State)
            {
                case ObjectStateEnum.Added:
                    sql_client.Entry(entity).State = EntityState.Added;
                    break;
                case ObjectStateEnum.Modified:
                    sql_client.Entry(entity).State = EntityState.Modified;
                    break;
                case ObjectStateEnum.Unchanged:
                    sql_client.Entry(entity).State = EntityState.Unchanged;
                    break;
                case ObjectStateEnum.Deleted:
                    sql_client.Entry(entity).State = EntityState.Deleted;
                    break;
            }

            foreach (var ch in entity.EnginePowerDetails)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Modified:
                        sql_client.Entry(ch).State = EntityState.Modified;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var ch in entity.EngineTorqueDetails)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Modified:
                        sql_client.Entry(ch).State = EntityState.Modified;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var ch in entity.VehicleFuelKinds)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var ch in entity.EngineFuelFeedDetails)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var ch in entity.ECUModelDetails)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var ch in entity.VehicleIgnitionDetails)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            foreach (var ch in entity.ExhaustDetails)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            await sql_client.SaveChangesAsync();
            return entity;
        }
        
        public async Task<int> UpdateEMachineDetailStateAsync(ElectricalMachineDetail entity)
        {
            switch (entity.State)
            {
                case ObjectStateEnum.Added:
                    sql_client.Entry(entity).State = EntityState.Added;
                    break;
                case ObjectStateEnum.Modified:
                    sql_client.Entry(entity).State = EntityState.Modified;
                    break;
                case ObjectStateEnum.Unchanged:
                    sql_client.Entry(entity).State = EntityState.Unchanged;
                    break;
                case ObjectStateEnum.Deleted:
                    sql_client.Entry(entity).State = EntityState.Deleted;
                    break;
            }

            foreach (var ch in entity.PowerStorageDeviceDetails)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Modified:
                        sql_client.Entry(ch).State = EntityState.Modified;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            await sql_client.SaveChangesAsync();
            return entity.Id;
        }


        public IQueryable<TransmissionType> GetTransmissionType()
        {
            return sql_client.Set<TransmissionType>().AsQueryable();
        }

        public async Task<int> UpdateTransmissionTypeAsync(TransmissionType entity)
        {
            switch (entity.State)
            {
                case ObjectStateEnum.Added:
                    sql_client.Entry(entity).State = EntityState.Added;
                    break;
                case ObjectStateEnum.Modified:
                    sql_client.Entry(entity).State = EntityState.Modified;
                    break;
                case ObjectStateEnum.Unchanged:
                    sql_client.Entry(entity).State = EntityState.Unchanged;
                    break;
                case ObjectStateEnum.Deleted:
                    sql_client.Entry(entity).State = EntityState.Deleted;
                    break;
            }

            foreach (var ch in entity.TransmissionUnitDetails)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Modified:
                        sql_client.Entry(ch).State = EntityState.Modified;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }

                foreach (var cch in ch.TransmissionUnitGears)
                {
                    switch (cch.State)
                    {
                        case ObjectStateEnum.Added:
                            sql_client.Entry(cch).State = EntityState.Added;
                            break;
                        case ObjectStateEnum.Unchanged:
                            sql_client.Entry(cch).State = EntityState.Unchanged;
                            break;
                        case ObjectStateEnum.Modified:
                            sql_client.Entry(cch).State = EntityState.Modified;
                            break;
                        case ObjectStateEnum.Deleted:
                            sql_client.Entry(cch).State = EntityState.Deleted;
                            break;
                    }
                }
            }

            await sql_client.SaveChangesAsync();
            return entity.Id;
        }


        public IQueryable<VehicleRunningGearDetail> GetVehicleRunningGearDetails()
        {
            return sql_client.Set<VehicleRunningGearDetail>().AsQueryable();
        }
        public async Task<int> UpdateVehicleRunningGearDetailAsync(VehicleRunningGearDetail entity)
        {
            switch (entity.State)
            {
                case ObjectStateEnum.Added:
                    sql_client.Entry(entity).State = EntityState.Added;
                    break;
                case ObjectStateEnum.Modified:
                    sql_client.Entry(entity).State = EntityState.Modified;
                    break;
                case ObjectStateEnum.Unchanged:
                    sql_client.Entry(entity).State = EntityState.Unchanged;
                    break;
                case ObjectStateEnum.Deleted:
                    sql_client.Entry(entity).State = EntityState.Deleted;
                    break;
            }

            foreach (var ch in entity.PoweredWheelLocations)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Modified:
                        sql_client.Entry(ch).State = EntityState.Modified;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            await sql_client.SaveChangesAsync();
            return entity.Id;
        }


        public async Task<VehicleSeatDetail> UpdateVehicleSeatDetailAsync(VehicleSeatDetail entity)
        {
            switch (entity.State)
            {
                case ObjectStateEnum.Added:
                    sql_client.Entry(entity).State = EntityState.Added;
                    break;
                case ObjectStateEnum.Modified:
                    sql_client.Entry(entity).State = EntityState.Modified;
                    break;
                case ObjectStateEnum.Unchanged:
                    sql_client.Entry(entity).State = EntityState.Unchanged;
                    break;
                case ObjectStateEnum.Deleted:
                    sql_client.Entry(entity).State = EntityState.Deleted;
                    break;
            }

            foreach (var ch in entity.VehicleSeatRawDetails)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Modified:
                        sql_client.Entry(ch).State = EntityState.Modified;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            await sql_client.SaveChangesAsync();
            return entity;
        }

        public async Task<VehicleVinCharacterDetail> UpdateVehicleVinCharacterDetailAsync(VehicleVinCharacterDetail entity)
        {
            switch (entity.State)
            {
                case ObjectStateEnum.Added:
                    sql_client.Entry(entity).State = EntityState.Added;
                    break;
                case ObjectStateEnum.Modified:
                    sql_client.Entry(entity).State = EntityState.Modified;
                    break;
                case ObjectStateEnum.Unchanged:
                    sql_client.Entry(entity).State = EntityState.Unchanged;
                    break;
                case ObjectStateEnum.Deleted:
                    sql_client.Entry(entity).State = EntityState.Deleted;
                    break;
            }

            foreach (var ch in entity.VinCharacterDescriptions)
            {
                switch (ch.State)
                {
                    case ObjectStateEnum.Added:
                        sql_client.Entry(ch).State = EntityState.Added;
                        break;
                    case ObjectStateEnum.Unchanged:
                        sql_client.Entry(ch).State = EntityState.Unchanged;
                        break;
                    case ObjectStateEnum.Modified:
                        sql_client.Entry(ch).State = EntityState.Modified;
                        break;
                    case ObjectStateEnum.Deleted:
                        sql_client.Entry(ch).State = EntityState.Deleted;
                        break;
                }
            }

            await sql_client.SaveChangesAsync();
            return entity;
        }
    }
}
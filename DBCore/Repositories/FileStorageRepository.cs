﻿using DBCore.Models;
using DBCore.Models.FileStorage;
using DbLog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DBCore.Repositories
{
    public class FileStorageRepository : GenericRepository<FileStorageItem>
    {
        public FileStorageRepository(IMongoDatabaseSettings mongo_settings, EptsContext _sql_client, LogContext logContext = null) 
            : base(mongo_settings, _sql_client, logContext) { }

        public async Task<bool> FileExistAsync(string documentType, string subPath, string fileName)
        {
            return await DbSet.AnyAsync(fs => fs.DocumentType == documentType
                && fs.SubPath == subPath
                && fs.FileName == fileName);
        }
    }
}

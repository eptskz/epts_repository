﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public abstract class DocumentBase
    {
        public Guid Id { get; set; }
        public string DocSeries { get; set; }
        public string Version { get; set; }
        public string DocNumber { get; set; }
        public string DocName { get; set; }    
        
        public DateTime DocDate { get; set; }
        public DateTime? DocStartDate { get; set; }
        public DateTime? DocEndDate { get; set; }
        
        public string Description { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }

        public virtual int DocTypeId { get; set; }
        public virtual DicDocType DocType { get; set; }
    }
}

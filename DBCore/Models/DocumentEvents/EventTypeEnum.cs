﻿namespace DBCore.Models.DocumentEvents
{
    public enum EventTypeEnum
    {
        New,
        Consideration,
        Active,
        Rejected,
        Withdrawn,
    }
}

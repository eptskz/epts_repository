﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.DocumentEvents
{
    [Table("DocumentEvents")]
    public class DocumentEvent
    {
        public int Id { get; set; }
        public Guid DocumentId { get; set; }
        public string DocumentType { get; set; }
        public EventTypeEnum EventType { get; set; }
        public DateTime EventDateTime { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.FileStorage
{
    public class FileStorageItem
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public string DocumentType { get; set; }
        public string SubPath { get; set; }        
        public string MimeType { get; set; }
        public string EntityId { get; set; }
        public Guid OwnerId { get; set; }
        public string Description { get; set; }
    }
}

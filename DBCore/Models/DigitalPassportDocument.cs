﻿using DBCore.Models.ComplienceDocuments;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using DBCore.Models.PassportDetails;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class DigitalPassportDocument
    {
        public DigitalPassportDocument()
        {
            VehicleEngines = new HashSet<VehicleEngine>();
            MemberOrganizations = new HashSet<MemberOrganization>();
            AssemblerOrganizations = new HashSet<AssemblerOrganization>();
            VehicleImages = new HashSet<VehicleImage>();
        }
        // Сведения об электронном паспорте
        public int Id { get; set; }
        public Guid? SignId { get; set; }
        
        public string UniqueCode { get; set; }
        public string VehicleEPassportBaseCode { get; set; }
        public string VehicleEPassportBaseDescription { get; set; }
        public string DocKindCode { get; set; }
        public DateTime? DocDateFrom { get; set; }
        public DateTime? DocDateTo { get; set; }
        public string AuthorityName { get; set; }
        public string DocumentNotExistDescription { get; set; }
        public string DocKindCodeHandy { get; set; }
        public string DocNameHandy { get; set; }
        public string DocAuthorHandy { get; set; }
        public string DocIdHandy { get; set; }
        public DateTime? DocDateHandy { get; set; }
        public string DocPageCountHandy { get; set; }
        public bool documentNotExists { get; set; }
        public string VehicleIdentityNumberIndicator { get; set; }
        public string VehicleIdentityNumberId { get; set; }
        public bool NotVehicleIdentityNumberIndicator { get; set; }
        public string VehicleCategoryCode { get; set; }
        public bool NotVehicleEngineIdentityNumberIndicator { get; set; }
        public string VehicleFrameIdentityNumberId { get; set; }
        public bool NotVehicleFrameIdentityNumberIndicator { get; set; }
        public string VehicleBodyIdentityNumberId { get; set; }
        public bool NotVehicleBodyIdentityNumberIndicator { get; set; }
        public string VehicleEmergencyCallIdentityNumberId { get; set; }
        public bool NotVehicleEmergencyCallIdentityNumberIndicator { get; set; }
        public bool NotGearNumberIndicator { get; set; }
        public string GearNumber { get; set; }
        public bool NotBaseAxleNumberIndicator { get; set; }
        public string BaseAxleNumber { get; set; }
        public string MoverEpsm { get; set; }
        public bool BodyMultiColourIndicator { get; set; }
        public string VehicleBodyColourCode { get; set; }
        public string VehicleBodyColourCode2 { get; set; }
        public string VehicleBodyColourCode3 { get; set; }
        public string VehicleBodyColourName { get; set; }
        public string ManufactureYear { get; set; }
        public string ManufactureMonth { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DocValidityDate { get; set; }
        public DateTime? DocCreationDate { get; set; }
        public string VehicleMakeName { get; set; }
        public bool NotVehicleMakeNameIndicator { get; set; }
        public string VehicleCommercialName { get; set; }
        public bool NotVehicleCommercialNameIndicator { get; set; }
        public string VehicleTypeId { get; set; }
        public string VehicleTypeVariantId { get; set; }
        public bool notVehicleModificationIndicator { get; set; }
        public string VehicleTechCategoryCode { get; set; }
        public string VehicleClassCategoryCode { get; set; }
        public string VehicleEcoClassCode { get; set; }
        public bool NotVehicleEcoClassCodeIndicator { get; set; }
        public string VehicleManufactureOption { get; set; }
        public bool NotBaseVehicleIndicator { get; set; }
        public string BaseVehicleMakeName { get; set; }
        public string BaseVehicleTypeId { get; set; }
        public string BaseVehicleCommercialName { get; set; }
        public string BaseVehicleTypeVariantId { get; set; }
        public bool BaseDocIndicator { get; set; } // 0-база шасси на основе эп, 1-на бумажный паспорт
        public string BaseVehicleEPassportId { get; set; }
        public DateTime? BaseDocCreationDate { get; set; }
        public bool NotManufacturerIndicator { get; set; }
        public int ManufactureOrganizationId { get; set; }
        public bool NotMemberOrganizationIndicator { get; set; }
        public bool NotAssemblerOrganizationIndicator { get; set; }
        public string SbktsDocId { get; set; }
        public DateTime? SbktsStartDate { get; set; }
        public DateTime? SbktsDocValidityDate { get; set; }
        public string CertifiedMemberFullName { get; set; }
        public bool VehicleMovementPermitIndicator { get; set; }
        public string ShassisMovePermitionText { get; set; }
        public string AllRoadMoveAbilityLimit { get; set; }
        public string MinibusAbility { get; set; }
        public string OtherInformation { get; set; }
        public string ManufactureWithPrivilegeMode { get; set; }
        public string AcceptTerritoryRegistration { get; set; }

        //public string AcceptCountryRegistration { get; set; }
        //public int? AcceptRegionRegistrationId { get; set; }

        public bool NotSateliteNavigationIndicator { get; set; }
        public string SateliteNavigationDescription { get; set; }
        public bool NotVehicleOfJobControllIndicator { get; set; }
        public string VehicleOfJobControllDescription { get; set; }
        public string VehicleMadeInCountry { get; set; }
        public string DigitalPassportRegisterBaseDescription { get; set; }
        public string ManufacturerInformation { get; set; }
        public int? ManufacturerPlateDetailId { get; set; }
        public int? EnginePlateDetailId { get; set; }
        //public int? CharacteristicDetailId { get; set; }


        //public virtual DicKato AcceptRegionRegistration { get; set; }
        public virtual ManufacturerPlateDetail ManufacturerPlateDetail { get; set; }
        public virtual EnginePlateDetail EnginePlateDetail { get; set; }
       // public virtual CharacteristicDetail CharacteristicDetail { get; set; }

        //public virtual ManufactureOrganization ManufactureOrganization { get; set; }
        public virtual ICollection<VehicleImage> VehicleImages { get; set; }
        public virtual ICollection<VehicleEngine> VehicleEngines { get; set; }
        public virtual ICollection<MemberOrganization> MemberOrganizations { get; set; }
        public virtual ICollection<AssemblerOrganization> AssemblerOrganizations { get; set; }


        public virtual NsiValue DocKindNsi { get; set; }
        public virtual NsiValue DocumentNotExistDescriptionNsi { get; set; }
        public virtual NsiValue DocKindCodeHandyNsi { get; set; }
        public virtual NsiValue VehicleCategoryCodeNsi { get; set; }
        public virtual NsiValue VehicleBodyColourCodeNsi { get; set; }
        public virtual NsiValue VehicleBodyColourCode2Nsi { get; set; }
        public virtual NsiValue VehicleBodyColourCode3Nsi { get; set; }
        public virtual NsiValue ManufactureMonthNsi { get; set; }
        public virtual NsiValue VehicleMakeNameNsi { get; set; }
        public virtual NsiValue MoverEpsmNsi { get; set; }
        public virtual NsiValue BaseVehicleMakeNameNsi { get; set; }
        public virtual NsiValue VehicleTechCategoryCodeNsi { get; set; }
        public virtual NsiValue VehicleClassCategoryCodeNsi { get; set; }
        public virtual NsiValue VehicleEcoClassCodeNsi { get; set; }
        public virtual NsiValue VehicleManufactureOptionNsi { get; set; }
        public virtual NsiValue ManufactureWithPrivilegeModeNsi { get; set; }
        //public virtual NsiValue AcceptCountryRegistrationNsi { get; set; }
        public virtual NsiValue VehicleMadeInCountryNsi { get; set; }

        // Доп поля 

        //1 - EPTS; 2 - EPSHTS; 3 - EPSM
        public int DocType { get; set; }
        public DateTime? DeclineDate { get; set; }
        public string DeclineDescription { get; set; }
        public Guid AspNetUserId { get; set; }
        public int? StatusNsiId { get; set; }
        public virtual NsiValue StatusNsi { get; set; }
        public virtual AspNetUsers AspNetUser { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }

        // Интеграция
        public int? KgdDataId { get; set; }
        public int? MshDataId { get; set; }
        public int? MvdDataId { get; set; }
        public Guid? ComplienceDocumentId { get; set; }

        public virtual KgdDatas KgdData { get; set; }
        public virtual MshDatas MshData { get; set; }
        public virtual MvdDatas MvdData { get; set; }
        public virtual ComplienceDocument ComplienceDocument { get; set; }

    }
}

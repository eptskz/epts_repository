﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DBCore.Models.ComplienceDocuments;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class AssemblyPlantConfiguration : IEntityTypeConfiguration<AssemblyPlant>
    {
        public void Configure(EntityTypeBuilder<AssemblyPlant> builder)
        {
            builder.HasOne(ur => ur.Document)
                    .WithMany(ot => ot.AssemblyPlants)
                    .HasForeignKey(ot => ot.DocumentId)
                    .IsRequired();
        }

    }
}

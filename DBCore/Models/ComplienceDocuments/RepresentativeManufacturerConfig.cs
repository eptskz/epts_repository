﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DBCore.Models.ComplienceDocuments;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class RepresentativeManufacturerConfiguration : IEntityTypeConfiguration<RepresentativeManufacturer>
    {
        public void Configure(EntityTypeBuilder<RepresentativeManufacturer> builder)
        {
            builder.HasOne(ur => ur.Document)
                    .WithMany(ot => ot.RepresentativeManufacturers)
                    .HasForeignKey(ot => ot.DocumentId)
                    .IsRequired();
        }

    }
}

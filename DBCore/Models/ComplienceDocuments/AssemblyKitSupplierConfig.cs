﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DBCore.Models.ComplienceDocuments;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class AssemblyKitSupplierConfiguration : IEntityTypeConfiguration<AssemblyKitSupplier>
    {
        public void Configure(EntityTypeBuilder<AssemblyKitSupplier> builder)
        {
            builder.HasOne(ur => ur.Document)
                    .WithMany(ot => ot.AssemblyKitSuppliers)
                    .HasForeignKey(ot => ot.DocumentId)
                    .IsRequired();
        }

    }
}

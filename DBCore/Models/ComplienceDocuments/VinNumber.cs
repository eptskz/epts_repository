﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.ComplienceDocuments
{
    public class VinNumber : IEntityState<int>
    {
        public int Id { get; set; }
        public Guid DocumentId { get; set; }
        public string Number { get; set; }

        public virtual ComplienceDocument Document { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

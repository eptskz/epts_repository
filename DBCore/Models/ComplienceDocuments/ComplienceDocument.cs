﻿using DBCore.Interfaces;
using DBCore.Models.NSI;
using DBCore.Models.Vehicle;
using DbLog.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.ComplienceDocuments
{
    [Serializable]
    public class ComplienceDocument : DocumentBase, ISoftDelete, IAuditEntity
    {
        /// <summary>
        /// формат номера документа
        /// XX X-XX.XXXX.XXXXX
        /// </summary>

        public DateTime? Deleted { get; set; }

        public int? DocDistributionSign { get; set; }
        public int? BatchCount { get; set; }

        public int? BatchSmallSign { get; set; }

        public int? CountryId { get; set; }
        public virtual NsiValue Country { get; set; }

        public int? AuthorityId { get; set; }
        public virtual Organization Authority { get; set; }

        public bool NotApplicantOrgIndicator { get; set; }
        public int? ApplicantId { get; set; }
        public virtual Organization Applicant { get; set; }

        public bool NotManufacturerIndicator { get; set; }
        public int? ManufacturerId { get; set; }
        public virtual Organization Manufacturer { get; set; }

        /// <summary>
        /// Представитель производителя.
        /// </summary>
        public bool NotRepresentativeManufacturerIndicator { get; set; }
/*
        public int? RepresentativeManufacturerId { get; set; }
        public virtual Organization RepresentativeManufacturer { get; set; }
*/
        public virtual ICollection<RepresentativeManufacturer> RepresentativeManufacturers { get; set; }

        /// <summary>
        /// Зборочный завод.
        /// </summary>
        public bool NotAssemblyPlantIndicator { get; set; }
/*
        public int? AssemblyPlantId { get; set; }
        public virtual Organization AssemblyPlant { get; set; }
*/
        public virtual ICollection<AssemblyPlant> AssemblyPlants { get; set; }

        /// <summary>
        /// Поставщик сборочных комплектов.
        /// </summary>
        public bool NotAssemblyKitSupplierIndicator { get; set; }
/*
        public int? AssemblyKitSupplierId { get; set; }
        public virtual Organization AssemblyKitSupplier { get; set; }
*/
        public virtual ICollection<AssemblyKitSupplier> AssemblyKitSuppliers { get; set; }


        public int? StatusId { get; set; }
        public virtual DicStatus Status { get; set; }

        public virtual VehicleTypeDetail VehicleTypeDetail { get; set; }

        public virtual VehicleLabelingDetail VehicleLabelingDetail { get; set; }

        public virtual ICollection<BlankNumber> BlankNumbers { get; set; }
        public virtual ICollection<VinNumber> VinNumbers { get; set; }
        public virtual ICollection<VehicleGeneralView> VehicleGeneralViews { get; set; }


        public bool? IsDocSign { get; set; }
        public DateTime? DocSignDate { get; set; }


        public int? AuthorOrgId { get; set; }
        public virtual Organization AuthorOrg { get; set; }



        [NotMapped]
        public object _Id { get { return Id; } }

        [NotMapped]
        public string _CurrenUserName { get; set; }

        [NotMapped]
        public string _CurrenUserId { get; set; }

        [NotMapped]
        public string _IpAddress { get; set; }
    }
}

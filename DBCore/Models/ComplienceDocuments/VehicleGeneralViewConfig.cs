﻿using DBCore.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.ComplienceDocuments
{
    public class VehicleGeneralViewConfig : IEntityTypeConfiguration<VehicleGeneralView>
    {
        public void Configure(EntityTypeBuilder<VehicleGeneralView> builder)
        {
            builder.ToTable("VehicleGeneralViews");
            builder.HasKey(cd => cd.Id);
            builder.Property(cd => cd.Id)
                .IsRequired()
                .HasDefaultValueSql("uuid_generate_v4()");
        }
    }
}

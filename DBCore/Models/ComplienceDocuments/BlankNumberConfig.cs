﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.ComplienceDocuments
{
    public class BlankNumberConfiguration : IEntityTypeConfiguration<BlankNumber>
    {
        public void Configure(EntityTypeBuilder<BlankNumber> builder)
        {
            builder.HasComment("Список номеров бланков");
            builder.Ignore(cd => cd.State);
        }
    }
}

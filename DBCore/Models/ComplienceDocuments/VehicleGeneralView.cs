﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.ComplienceDocuments
{
    [Table("VehicleGeneralViews")]
    public class VehicleGeneralView
    {
        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }
        public virtual ComplienceDocument Document { get; set; }

        public byte[] VehiclePicture { get; set; }
        public string OrginalFileName { get; set; }
        public string FileName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

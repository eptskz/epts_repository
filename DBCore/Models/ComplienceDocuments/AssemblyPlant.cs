﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DBCore.Enums;

namespace DBCore.Models.ComplienceDocuments
{
    [Serializable]
    public class AssemblyPlant
    {
        public int Id { get; set; }
        public Guid DocumentId { get; set; }
        public virtual ComplienceDocument Document { get; set; }
        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }

}

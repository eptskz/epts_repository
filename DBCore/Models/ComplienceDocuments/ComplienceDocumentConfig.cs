﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.ComplienceDocuments
{
    public class ComplienceDocumentConfiguration : IEntityTypeConfiguration<ComplienceDocument>
    {
        public void Configure(EntityTypeBuilder<ComplienceDocument> builder)
        {
            /*
             modelBuilder.HasPostgresExtension("uuid-ossp")
                .Entity<ComplienceDocument>(entity => {
                    entity.Property(e => e.Id)
                        .IsRequired()
                        .HasDefaultValueSql("uuid_generate_v4()");
                });
             */

            builder.ToTable("ComplienceDocuments");
            builder.HasKey(cd => cd.Id);
            builder.Property(cd => cd.Id)
                .IsRequired()
                .HasDefaultValueSql("uuid_generate_v4()");

            builder.HasMany(ur => ur.RepresentativeManufacturers)
                .WithOne(ot => ot.Document)
                .HasForeignKey(ot => ot.DocumentId);

            builder.HasMany(ur => ur.AssemblyPlants)
                .WithOne(ot => ot.Document)
                .HasForeignKey(ot => ot.DocumentId);

            builder.HasMany(ur => ur.AssemblyKitSuppliers)
                .WithOne(ot => ot.Document)
                .HasForeignKey(ot => ot.DocumentId);

            builder.Property(cd => cd.CreateDate)
                .IsRequired()
                .HasDefaultValueSql("NOW()");
        }
    }
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.ComplienceDocuments.Vehicle
{
    public class VehicleVinCharacterDescription : IObjectState
    {
        public int Id { get; set; }
        
        public string IdCharacterValue { get; set; }
        public string IdCharacterValueText { get; set; }

        
        public int VinCharacterDetailId { get; set; }
        public virtual VehicleVinCharacterDetail VinCharacterDetail { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

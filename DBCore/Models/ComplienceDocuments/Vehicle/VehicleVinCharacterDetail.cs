﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.ComplienceDocuments.Vehicle
{
    public class VehicleVinCharacterDetail : IObjectState
    {
        public int Id { get; set; }
        public int IdCharacterStartingOrdinal { get; set; }
        public int IdCharacterEndingOrdinal { get; set; }
        public string IdCharacterText { get; set; }

        public int? DataTypeId { get; set; }
        public virtual NSI.NsiValue DataType { get; set; }


        public bool IsAlpha { get; set; }
        public bool IsDigit { get; set; }

        public bool IsIncludeChar { get; set; }
        public string IncludeCharStr { get; set; }

        public bool IsExcludeChar { get; set; }
        public string ExcludeCharStr { get; set; }


        public int VehicleLabelingDetailId { get; set; }
        public virtual VehicleLabelingDetail VehicleLabelingDetail { get; set; }


        public virtual ICollection<VehicleVinCharacterDescription> VinCharacterDescriptions { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

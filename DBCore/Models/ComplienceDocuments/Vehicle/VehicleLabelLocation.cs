﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.ComplienceDocuments.Vehicle
{
    [Table("VehicleLabelLocations")]
    public class VehicleLabelLocation : IEntityState<int>
    {
        public int Id { get; set; }

        public string LocationText { get; set; }

        public int VehicleLabelingDetailId { get; set; }
        public virtual VehicleLabelingDetail VehicleLabelingDetail { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

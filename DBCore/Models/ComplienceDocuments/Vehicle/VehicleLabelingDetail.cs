﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.ComplienceDocuments.Vehicle;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.ComplienceDocuments
{
    public class VehicleLabelingDetail : IEntityState<int>
    {
        public int Id { get; set; }
        
        public int NotManufacturerPlateIndicatorSign { get; set; }
        
        public string VehicleComponentLocationText { get; set; }
        public ICollection<VehicleLabelLocation> VehicleLabelLocations { get; set; }
        
        public string VehicleIdentificationNumberLocationText { get; set; }
        public ICollection<VehicleIdentificationNumberLocation> VehicleIdentificationNumberLocations { get; set; }

        public string EngineIdentificationNumberLocationText { get; set; }

        public string Vin { get; set; }

        public Guid DocumentId { get; set; }
        public virtual ComplienceDocument Document { get; set; }

        public virtual ICollection<VehicleVinCharacterDetail> VinCharacterDetails { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

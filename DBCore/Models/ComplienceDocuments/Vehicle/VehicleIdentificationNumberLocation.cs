﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.ComplienceDocuments.Vehicle;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.ComplienceDocuments
{
    [Table("VehicleIdentificationNumberLocations")]
    public class VehicleIdentificationNumberLocation : IEntityState<int>
    {
        public int Id { get; set; }

        public string LocationText { get; set; }

        public int VehicleLabelingDetailId { get; set; }
        public virtual VehicleLabelingDetail VehicleLabelingDetail { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.ComplienceDocuments
{
    public class VinNumberConfiguration : IEntityTypeConfiguration<VinNumber>
    {
        public void Configure(EntityTypeBuilder<VinNumber> builder)
        {
            builder.HasComment("Список Vin");
            builder.Ignore(cd => cd.State);
        }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace DBCore.Models
{
    public partial class AspNetRoles : IdentityRole<Guid>
    {

        public AspNetRoles() : base()
        {
            this.Id = Guid.NewGuid();
        }

        public string Description { get; set; }

        public virtual ICollection<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual ICollection<AspNetUserRoles> AspNetUserRoles { get; set; }
    }
}

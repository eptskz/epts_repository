﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace DBCore.Models
{
    public partial class KgdDatas
    {
        public int Id { get; set; }
        public string DocumentType { get; set; }
        public string DeclarationType { get; set; }
        public string CustomProdCode { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string OriginCountry { get; set; }
        public string DepartureCountry { get; set; }
        public string DestinationCountry { get; set; }
        public string CustomPoint { get; set; }
        public string DocNumber { get; set; }
        public string CustomLimitDescription { get; set; }
        public string ConsignorName { get; set; }
        public string ConsignorIinBin { get; set; }
        public string RecipientName { get; set; }
        public string DeclarantIinBin { get; set; }
        public string TnvedCode { get; set; }
        public string NetMass { get; set; }
        public string AddUnitCode { get; set; }
        public string UnitMeasurement { get; set; }
        public string NumberUnitMeasurement { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string VinCode { get; set; }
        public string EngVolume { get; set; }
        public string EngNumber { get; set; }
        public string ChassisNumber { get; set; }
        public string KabinNumber { get; set; }
        public string KuzovNumber { get; set; }
        public string IssueYear { get; set; }
        public string DocName { get; set; }
        public DateTime? CustomLimitBegin { get; set; }
        public DateTime? CustomLimitEnd { get; set; }
        public DateTime MessageDate { get; set; }
        public string MessageId { get; set; }
        public string SessionId { get; set; }
        public string CorrelationId { get; set; }
        public DateTime? Created { get; set; }
        public string CustomPointName { get; set; }
    }
}

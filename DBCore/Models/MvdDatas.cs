﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace DBCore.Models
{
    public partial class MvdDatas
    {
        public MvdDatas()
        {
            Encumbrances = new HashSet<Encumbrances>();
        }

        public int Id { get; set; }
        public string OperationType { get; set; }
        public DateTime OperationDate { get; set; }
        public string DocDescriptionBase { get; set; }
        public string RegisterRemoveReason { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string DigitalPassportId { get; set; }
        public string Grnz { get; set; }
        public string SrtsNumber { get; set; }
        public string VinCode { get; set; }
        public double? Price { get; set; }
        public int? Odometer { get; set; }
        public short SignTsChange { get; set; }
        public string DescriptionTsChange { get; set; }
        public short OwnerCategory { get; set; }
        public string OwnerIinBin { get; set; }
        public string CompanyName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime? BirthDate { get; set; }
        public string OwnerDocType { get; set; }
        public string OwnerDocNumber { get; set; }
        public DateTime? OwnerDocDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string TsLimitDescription { get; set; }
        public string AllRoadMoveAbility { get; set; }
        public string MinibusAbility { get; set; }
        public DateTime MessageDate { get; set; }
        public string MessageId { get; set; }
        public string SessionId { get; set; }
        public string CorrelationId { get; set; }
        public DateTime? Created { get; set; }

        public virtual ICollection<Encumbrances> Encumbrances { get; set; }
    }
}

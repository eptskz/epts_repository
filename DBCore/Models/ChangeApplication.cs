﻿using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class ChangeApplication
    {
        public ChangeApplication()
        {
            Files = new HashSet<ChangeApplicationFile>();
            Fields = new HashSet<ChangeApplicationField>();
        }
        public int Id { get; set; }
        public Guid? SignId { get; set; }
        public Guid? ExecuteSignId { get; set; }
        public DateTime? DeclineDate { get; set; }
        public string DeclineDescription { get; set; }
        public Guid UserCreatedId { get; set; }
        public Guid? UserExecutorId { get; set; }

        public bool IsFieldChange { get; set; }
        public int? ChangeStatusNsiId { get; set; }
        public virtual NsiValue ChangeStatusNsi { get; set; }
        public string ChangeStatusReason { get; set; }

        public int? StatusNsiId { get; set; }
        public virtual NsiValue StatusNsi { get; set; }
        public virtual AspNetUsers UserCreated { get; set; }
        public virtual AspNetUsers UserExecutor { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public string Comment { get; set; }

        public virtual ICollection<ChangeApplicationFile> Files { get; set; }
        public virtual ICollection<ChangeApplicationField> Fields { get; set; }
        public int DigitalPassportId { get; set; }
        public virtual DigitalPassportDocument DigitalPassport { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace DBCore.Models
{
    public partial class AspNetUserRoles : IdentityUserRole<Guid>
    {
        public AspNetUserRoles() : base()
        {
        }
        //public Guid UserId { get; set; }
        //public Guid RoleId { get; set; }

        public virtual AspNetRoles Role { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}

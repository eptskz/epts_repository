﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBCore.Models
{
    public partial class AspNetUserClaims
    {
        public int Id { get; set; }

        [Required]
        public Guid UserId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }

        public virtual AspNetUsers User { get; set; }
    }
}

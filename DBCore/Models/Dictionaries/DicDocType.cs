﻿using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    [Serializable]
    public partial class DicDocType : BaseModel, ISoftDelete
    {
        public DateTime? Deleted { get; set; }
    }
}

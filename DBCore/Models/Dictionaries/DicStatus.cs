﻿using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    [Serializable]
    public partial class DicStatus : BaseModel, ISoftDelete
    {
        public string Group { get; set; }
        public DateTime? Deleted { get; set; }
    }
}

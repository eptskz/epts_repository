﻿using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Dictionaries
{
    [Serializable]
    public class DicKato : BaseModel, ISoftDelete
    {
        public DateTime? Deleted { get; set; }
        public int? AreaType { get; set; }
        public int? Level { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }

        public int? ParentId { get; set; }
        public virtual DicKato Parent { get; set; }
    }
}

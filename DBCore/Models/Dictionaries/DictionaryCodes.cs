﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Dictionaries
{
    public static class DictionaryCodes
    {
        public static class StatusCodes
        {
            public static class Groups
            {
                /// <summary>
                /// Документы
                /// </summary>
                public static string ComplienceDoc = "compliencedoc";

                /// <summary>
                /// Запросы на регистрацию
                /// </summary>
                public static string RegRequest = "regrequest";

                /// <summary>
                /// Пользователи
                /// </summary>
                public static string User = "user";

                /// <summary>
                /// Пользователи
                /// </summary>
                public static string SupportTicket = "supportticket";

                /// <summary>
                /// Договор
                /// </summary>
                public static string Contract = "contract";
                
                /// <summary>
                /// Счет
                /// </summary>
                public static string Invoice = "invoice";
            }

            /// <summary>
            /// черновик
            /// </summary>
            public static string New = "new";

            /// <summary>
            /// На рассмотрении
            /// </summary>
            public static string Сonsideration = "consideration";

            /// <summary>
            /// Действующий
            /// </summary>
            public static string Active = "active";

            /// <summary>
            /// Принят
            /// </summary>
            public static string Accepted = "accepted";

            /// <summary>
            /// Отклоненный
            /// </summary>
            public static string Rejected = "rejected";

            /// <summary>
            /// Закрыта
            /// </summary>
            public static string Closed = "closed";

            /// <summary>
            /// Отозванный
            /// </summary>
            public static string Withdrawn = "withdrawn";

            /// <summary>
            /// Просроченные
            /// </summary>
            public static string Overdue = "overdue";

            /// <summary>
            /// Заблокирован
            /// </summary>
            public static string Locked = "locked";

            /// <summary>
            /// Удален
            /// </summary>
            public static string Deleted = "deleted";

            /// <summary>
            /// Не оплачен
            /// </summary>
            public static string NotPaid = "notpaid";

            /// <summary>
            /// Оплачен
            /// </summary>
            public static string Paid = "paid";
        }

        public static class CommunicationInfoCodes
        {
            /// <summary>
            /// телефон
            /// </summary>
            public static string Telephone = "TE";

            /// <summary>
            /// телефакс
            /// </summary>
            public static string Telefax = "FX";

            /// <summary>
            /// Элестронная почта
            /// </summary>
            public static string Email = "EM";
        }
    
        public static class DocumentTypeCodes
        {
            /// <summary>
            /// ОТТС
            /// </summary>
            public static string Otts = "otts";
            public static string Otts30 = "30";

            /// <summary>
            /// ОТШ
            /// </summary>
            public static string Otch = "otch";
            public static string Otch35 = "35";

            /// <summary>
            /// СБКТС
            /// </summary>
            public static string Sbkts = "sbkts";
            public static string Sbkts40 = "40";
        } 
    }
}

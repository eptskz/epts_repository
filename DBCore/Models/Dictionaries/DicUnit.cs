﻿using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Dictionaries
{
    public class DicUnit : NsiValue
    {
        public string Symbol { get; set; }
    }
}

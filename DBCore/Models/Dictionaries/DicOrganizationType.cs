﻿using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    [Serializable]
    public partial class DicOrganizationType : BaseModel, ISoftDelete
    {
       public string ShortName { get; set; }
       public DateTime? Deleted { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class ManufacturerPlateDetail
    {
        public ManufacturerPlateDetail()
        {
            VehicleStructureCards = new HashSet<VehicleStructureCard>();
            VehicleIdentLocations = new HashSet<VehicleIdentLocation>();
            ManufacturerPlateLocations = new HashSet<ManufacturerPlateLocation>();
        }
        public int Id { get; set; }
        public bool NotManufacturerPlateLocationIndicator { get; set; }
        public virtual ICollection<VehicleStructureCard> VehicleStructureCards { get; set; }
        public virtual ICollection<VehicleIdentLocation> VehicleIdentLocations { get; set; }
        public virtual ICollection<ManufacturerPlateLocation> ManufacturerPlateLocations { get; set; }
    }
}

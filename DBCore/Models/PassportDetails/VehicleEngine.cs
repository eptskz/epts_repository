﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class VehicleEngine
    {
        public int Id { get; set; }
        public string VehicleEngineIdentityNumberId { get; set; }
        public int DigitalPassportId { get; set; }

        public virtual DigitalPassportDocument DigitalPassport { get; set; }
    }
}

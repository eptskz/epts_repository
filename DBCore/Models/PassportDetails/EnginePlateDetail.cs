﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class EnginePlateDetail
    {
        public EnginePlateDetail()
        {
            VehicleIdentLocations = new HashSet<VehicleIdentLocation>();
            VehicleStructureCards = new HashSet<VehicleStructureCard>();
        }
        public int Id { get; set; }

        public virtual ICollection<VehicleIdentLocation> VehicleIdentLocations { get; set; }
        public virtual ICollection<VehicleStructureCard> VehicleStructureCards { get; set; }
    }
}

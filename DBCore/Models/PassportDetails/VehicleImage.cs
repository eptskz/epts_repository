﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class VehicleImage
    {
        public int Id { get; set; }
        public byte[] VehiclePicture { get; set; }
        public string FileName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public int DigitalPassportId { get; set; }

        public virtual DigitalPassportDocument DigitalPassport { get; set; }
    }
}

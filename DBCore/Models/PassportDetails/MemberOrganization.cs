﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class MemberOrganization
    {
        public int Id { get; set; }
        public bool NotMemberIndicator { get; set; }
        public int MemberOrganizationId { get; set; }
        public int DigitalPassportId { get; set; }

        public virtual DigitalPassportDocument DigitalPassport { get; set; }
    }
}

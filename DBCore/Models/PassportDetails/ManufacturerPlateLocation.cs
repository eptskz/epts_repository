﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class ManufacturerPlateLocation
    {
        public int Id { get; set; }
        public string Location { get; set; }
    }
}

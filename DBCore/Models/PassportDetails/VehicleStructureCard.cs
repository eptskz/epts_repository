﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class VehicleStructureCard
    {
        public VehicleStructureCard()
        {
            MeaningExplanations = new HashSet<MeaningExplanation>();
        }
        public int Id { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string Description { get; set; }
        public int? ManufacturerPlateDetailId { get; set; }
        public int? EnginePlateDetailId { get; set; }
        public virtual ManufacturerPlateDetail ManufacturerPlateDetail { get; set; }
        public virtual EnginePlateDetail EnginePlateDetail { get; set; }
        public virtual ICollection<MeaningExplanation> MeaningExplanations { get; set; }
        
    }
}
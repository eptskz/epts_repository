﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class MeaningExplanation
    {
        public int Id { get; set; }
        public string Explanation { get; set; }
        public string Meaning { get; set; }
        public int VehicleStructureCardId { get; set; }

        public virtual VehicleStructureCard VehicleStructureCard { get; set; }
    }
}

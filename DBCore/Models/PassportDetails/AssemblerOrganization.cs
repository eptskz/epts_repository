﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class AssemblerOrganization
    {
        public int Id { get; set; }
        public bool NotAssemblerIndicator { get; set; }
        public int AssemblerOrganizationId { get; set; }
        public int DigitalPassportId { get; set; }

        public virtual DigitalPassportDocument DigitalPassport { get; set; }
    }
}

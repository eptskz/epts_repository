﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class VehicleIdentLocation
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public int? ManufacturerPlateDetailId { get; set; }
        public int? EnginePlateDetailId { get; set; }
        public virtual ManufacturerPlateDetail ManufacturerPlateDetail { get; set; }
        public virtual EnginePlateDetail EnginePlateDetail { get; set; }
    }
}

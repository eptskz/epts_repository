﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class ChangeApplicationFile
    {
        public int Id { get; set; }
        public string FileObject { get; set; }
        public string FileName { get; set; }

        public int ChangeApplicationId { get; set; }

        public virtual ChangeApplication ChangeApplication { get; set; }
    }
}

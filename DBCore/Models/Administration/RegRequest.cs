﻿using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Administration
{
    public class RegRequest : ISoftDelete
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }

        public string OrganizationName { get; set; }
        public string OrganizationBin { get; set; }
        
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
                
        public string Certificate { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }

        public bool IsAccept { get; set; }

        public int StatusId { get; set; }
        public virtual DicStatus Status { get; set; }

        public Guid? RoleId { get; set; }
        public bool IsExistContract { get; set; }
    }
}

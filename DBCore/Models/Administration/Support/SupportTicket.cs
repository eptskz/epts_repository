﻿using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Administration.Support
{
    public class SupportTicket : ISoftDelete
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? Modified { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string AuthorId { get; set; }
        public string AuthorName{ get; set; }

        public TicketCategoryEnum Category { get; set; }

        public int StatusId { get; set; }
        public virtual DicStatus Status { get; set; }
        
        public virtual ICollection<SupportTicketComment> Comments { get; set; }
        public DateTime? Deleted { get; set; }
    }
}

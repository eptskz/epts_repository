﻿namespace DBCore.Models.Administration.Support
{
    public enum TicketCategoryEnum
    {
        Appeal,
        Proposal
    }
}

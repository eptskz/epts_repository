﻿using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Administration.Support
{
    public class SupportTicketComment : ISoftDelete
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public DateTime CreateDate { get; set; }
        
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }

        public Guid TicketId { get; set; }
        public virtual SupportTicket Ticket { get; set; }

        public DateTime? Deleted { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    [Serializable]
    public class SignedDoc
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        public string SignedXml { get; set; }
        public string DocType { get; set; }
        public Guid DocId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class ChangeApplicationField
    {
        public int Id { get; set; }
        public string Section { get; set; }
        public string Field { get; set; }
        public string PreValue { get; set; }
        public string PostValue { get; set; }
        public string Reason { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public int ChangeApplicationId { get; set; }

        public virtual ChangeApplication ChangeApplication { get; set; }
    }
}

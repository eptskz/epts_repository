﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DBCore.Models.Reports
{
    public class ComplienceDocReport
    {
        public Guid Id { get; set; }
        public string DocNumber { get; set; }
        public DateTime? DocStartDate { get; set; }
        public DateTime? DocEndDate { get; set; }
        public int DocTypeId { get; set; }
        public string DocTypeNameRu { get; set; }
        public string DocTypeNameKz { get; set; }
        public string DocTypeNameEn { get; set; }
        public int StatusId { get; set; }
        public string StatusNameRu { get; set; }
        public string StatusNameKz { get; set; }
        public string StatusNameEn { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int AuthorOrgId { get; set; }
        public string AuthorOrgBin { get; set; }
        public string AuthorOrgNameRu { get; set; }
        public string AuthorOrgNameKz { get; set; }
        public string AuthorOrgNameEn { get; set; }
        public int DigitalPassportCount { get; set; }
    }
}

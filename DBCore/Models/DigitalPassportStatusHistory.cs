﻿using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class DigitalPassportStatusHistory
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public int StatusNsiId { get; set; }
        public int DigitalPassportId { get; set; }
        public virtual NsiValue StatusNsi { get; set; }
        public virtual DigitalPassportDocument DigitalPassport { get; set; }
    }
}

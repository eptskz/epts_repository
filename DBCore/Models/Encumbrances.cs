﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace DBCore.Models
{
    public partial class Encumbrances
    {
        public int Id { get; set; }
        public int? MvdDataId { get; set; }
        public short? EncumbranceIndicator { get; set; }
        public string EncumbranceTypeCode { get; set; }
        public DateTime? EncumbranceDate { get; set; }
        public string EncumbranceOrgName { get; set; }
        public string EncumbranceDocName { get; set; }
        public DateTime? EncumbranceDocDate { get; set; }
        public DateTime? EncumbranceBegin { get; set; }
        public DateTime? EncumbranceEnd { get; set; }

        public virtual EncumbranceTypes EncumbranceType { get; set; }
        public virtual MvdDatas MvdData { get; set; }
    }
}

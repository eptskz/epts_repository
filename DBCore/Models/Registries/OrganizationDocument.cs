﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Registries
{
    public class OrganizationDocument
    {
        public int Id { get; set; }

        public string OrgDocType { get; set; }

        public int? DocTypeId { get; set; }
        public virtual NSI.NsiValue DocType { get; set; }

        public string DocSeries { get; set; }
        public string DocNumber { get; set; }
        public string DocName { get; set; }        
        public DateTime? DocDate { get; set; }
        public DateTime? DocValidDateStart { get; set; }
        public DateTime? DocValidDateEnd { get; set; }
        public string TnVedTcCode { get; set; }
        public string AgreementOrgName { get; set; }
        public double? AgreementQuotaSize { get; set; }

        public int? TechRegulationSubjectQuantity { get; set; }
        public int? TechRegulationSubjectTypeId { get; set; }
        public virtual NSI.NsiValue TechRegulationSubjectType { get; set; }
        public int? TechRegulationSubjectUnitId { get; set; }
        public virtual NSI.NsiValue TechRegulationSubjectUnit { get; set; }

        public int? IssueCountryId { get; set; }
        public  virtual NSI.NsiValue IssueCountry { get; set; }

        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

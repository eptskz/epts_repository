﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Registries
{
    public class OrganizationRef
    { 
        public int ParentId { get; set; }
        public virtual Organization Parent { get; set; }

        public int ChildId { get; set; }
        public virtual Organization Child { get; set; }
    }
}

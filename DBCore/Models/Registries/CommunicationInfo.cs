﻿using DBCore.Enums;
using DBCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Registries
{
    public class CommunicationInfo : IEntityState<int>
    {
        public int Id { get; set; }
        public int CommunicationTypeId { get; set; }
        public virtual NSI.NsiValue CommunicationType { get; set; }

        public string CommunicationValue { get; set; }

        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

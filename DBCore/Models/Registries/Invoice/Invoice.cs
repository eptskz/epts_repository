﻿using DBCore.Interfaces;
using DBCore.Models.Administration.Contract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Registries.Invoice
{
    public class Invoice : ISoftDelete
    {
        public int Id { get; set; }
        public string Number { get; set; }

        [Column("PeriodMonths", TypeName = "text[]")]
        public string[] PeriodMonths { get; set; }

        /// <summary>
        /// Количество оформлений
        /// </summary>
        public int DigitalPassportCount { get; set; }

        /// <summary>
        /// Изменения в ЭП (шт)
        /// </summary>
        public int ChangeDigitalPassportCount { get; set; }
        
        /// <summary>
        /// Тариф
        /// </summary>
        public double Rate { get; set; }

        /// <summary>
        /// Сумма к оплате
        /// </summary>
        public double? PayableAmount { get; set; }

        /// <summary>
        /// Оплаченная сумма
        /// </summary>
        public double? PaidAmount { get; set; }

        /// <summary>
        /// Дата и время оплаты
        /// </summary>
        public DateTime? PaidDateTime { get; set; }
        
        /// <summary>
        /// Пеня
        /// </summary>
        public double? Fine { get; set; }
        
        /// <summary>
        /// Срок начисления
        /// </summary>
        public int? AccrualTermFine { get; set; }

        /// <summary>
        /// Сумма к оплате Пени
        /// </summary>
        public double? PayableFine { get; set; }

        /// <summary>
        /// Оплаченная сумма
        /// </summary>
        public double? PaidFine { get; set; }

        /// <summary>
        /// Дата и время оплаты сумма
        /// </summary>
        public DateTime? PaidFineDateTime { get; set; }

        /// <summary>
        /// Сальдо
        /// </summary>
        public double? Balance { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }

        public Guid ContractId { get; set; }
        public virtual Contract Contract { get; set; }

        //public string AccountNumber { get; set; }
        //public DateTime? AccountDate { get; set; }


        public int StatusId { get; set; }
        public virtual DicStatus Status { get; set; }


        public Guid AuthorId { get; set; }
        public string AuthorName { get; set; }

        public string Description { get; set; }
    }
}

﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models
{
    public class OrganizationType
    {
        public int Id { get; set; }
        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        public int OrgTypeId { get; set; }
        public virtual DicOrganizationType OrgType { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

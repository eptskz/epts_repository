﻿using DBCore.Interfaces;
using DBCore.Models.Dictionaries;
using DBCore.Models;
using System;
using System.Collections.Generic;
using System.Text;
using DBCore.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using DBCore.Models.Registries;
using DBCore.Enums.Organizations;

namespace DBCore.Models
{
    [Serializable]
    public partial class Organization : BaseModel, ISoftDelete
    {
        public string Bin { get; set; }

        public string ShortNameRu { get; set; }
        public string ShortNameKz { get; set; }
        public string ShortNameEn { get; set; }

        public string Description { get; set; }

        public string Phone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }

        public DateTime? Deleted { get; set; }

        public string JuridicalPostalIndex { get; set; }
        public int? JuridicalRegionOrCityId { get; set; }
        public virtual DicKato JuridicalRegionOrCity { get; set; }
        public string JRegionOrCity { get; set; }

        public int? JuridicalDistinctOrCityId { get; set; }
        public virtual DicKato JuridicalDistinctOrCity { get; set; }
        public string JDistinctOrCity { get; set; }

        public int? JuridicalOkrugOrCityId { get; set; }
        public virtual DicKato JuridicalOkrugOrCity { get; set; }
        public string JOkrugOrCity { get; set; }

        public int? JuridicalVillageId { get; set; }
        public virtual DicKato JuridicalVillage { get; set; }
        public string JVillage { get; set; }

        public string JuridicalAddress { get; set; }
        public string JStreetName { get; set; }
        public string JBuildingNumber { get; set; }
        public string JRoomNumber { get; set; }


        public string FactPostalIndex { get; set; }
        public int? FactCountryId { get; set; }
        public virtual NSI.NsiValue FactCountry { get; set; }

        public int? FactRegionOrCityId { get; set; }
        public virtual DicKato FactRegionOrCity { get; set; }
        public string FRegionOrCity { get; set; }

        public int? FactDistinctOrCityId { get; set; }
        public virtual DicKato FactDistinctOrCity { get; set; }
        public string FDistinctOrCity { get; set; }

        public int? FactOkrugOrCityId { get; set; }
        public virtual DicKato FactOkrugOrCity { get; set; }
        public string FOkrugOrCity { get; set; }

        public int? FactVillageId { get; set; }
        public virtual DicKato FactVillage { get; set; }
        public string FVillage { get; set; }

        public string FactAddress { get; set; }
        public string FStreetName { get; set; }
        public string FBuildingNumber { get; set; }
        public string FRoomNumber { get; set; }

        public string HeadLastName { get; set; }
        public string HeadFirstName { get; set; }
        public string HeadSecondName { get; set; }


        public virtual ICollection<OrganizationType> OrganizationTypes { get; set; }

        public int StatusId { get; set; }
        public virtual DicStatus Status { get; set; }


        public virtual ICollection<CommunicationInfo> CommunicationInfos { get;set;}

        public RegisterTypeEnum RegisterType { get; set; }

        public int? CountryId { get; set; }
        public virtual NSI.NsiValue Country { get; set; }

        public int? LegalFormId { get; set; }
        public virtual NSI.NsiValue LegalForm { get; set; }

        public virtual ICollection<OrganizationDigPassportType> OrganizationDigPassportTypes { get; set; }

        public bool NotOrganizationDocumentIndicator { get; set; }
        public virtual ICollection<OrganizationDocument> OrganizationDocuments { get; set; }

        public virtual ICollection<OrganizationRef> ParentRefs { get; set; }
        public virtual ICollection<OrganizationRef> ChildRefs { get; set; }

        [NotMapped]
        public int? ParentId { get; set; }
        //public virtual Organization Parent { get; set; }
        
        //[NotMapped]
        //public virtual ICollection<Organization> ChildOrganizations { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models
{
    public class OrganizationDigPassportType
    {
        public int Id { get; set; }
        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        public int DigPassportTypeId { get; set; }
        public virtual NSI.NsiValue DigPassportType { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class OrganizationTypeConfiguration : IEntityTypeConfiguration<OrganizationType>
    {
        public void Configure(EntityTypeBuilder<OrganizationType> builder)
        {
            builder.HasOne(ur => ur.Organization)
                .WithMany(r => r.OrganizationTypes)
                .HasForeignKey(ur => ur.OrganizationId)
                .IsRequired();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class OrganizationConfiguration : IEntityTypeConfiguration<Organization>
    {
        public void Configure(EntityTypeBuilder<Organization> builder)
        {
            builder.HasMany(ur => ur.OrganizationTypes)
                    .WithOne(ot => ot.Organization)
                    .HasForeignKey(ot => ot.OrganizationId);

            builder.Property(cd => cd.Created)
                .IsRequired()
                .HasDefaultValueSql("NOW()");
        }
    }
}

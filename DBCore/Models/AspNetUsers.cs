﻿using DbLog.Base;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBCore.Models
{
    public partial class AspNetUsers : IdentityUser<Guid>, IAuditEntity
    {
        public AspNetUsers()
        {
            AspNetUserClaims = new HashSet<AspNetUserClaims>();
            AspNetUserLogins = new HashSet<AspNetUserLogins>();
            AspNetUserRoles = new HashSet<AspNetUserRoles>();
            AspNetUserTokens = new HashSet<AspNetUserTokens>();
        }

        public int? OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        public virtual DateTimeOffset? PasswordExpire { get; set; }

        public virtual ICollection<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual ICollection<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual ICollection<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual ICollection<AspNetUserTokens> AspNetUserTokens { get; set; }

        public virtual UserProfile UserProfile { get; set; }




        [NotMapped]
        public object _Id { get { return Id; } }

        [NotMapped]
        public string _CurrenUserName { get; set; }

        [NotMapped]
        public string _CurrenUserId { get; set; }

        [NotMapped]
        public string _IpAddress { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class ExternalInteractHistory
    {
        public int Id { get; set; }
        public string ServiceName { get; set; } // rop kgd msh mvd tson
        public string StatusCode { get; set; } // code -1- error, 5- ok
        public string Queue { get; set; } // may be for RF
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public string Author { get; set; } // user(from browser) or system (as service)
        public int? DigitalPassportId { get; set; }
        public virtual DigitalPassportDocument DigitalPassport { get; set; }
    }
}

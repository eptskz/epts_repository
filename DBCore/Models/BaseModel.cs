﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    [Serializable]
    public partial class BaseModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string NameRu { get; set; }
        public string NameKz { get; set; }
        public string NameEn { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}

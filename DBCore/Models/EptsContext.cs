﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DBCore.Interfaces;
using DBCore.Models.Administration;
using DBCore.Models.Administration.Contract;
using DBCore.Models.Administration.News;
using DBCore.Models.Administration.Support;
using DBCore.Models.ComplienceDocuments;
using DBCore.Models.Dictionaries;
using DBCore.Models.PassportDetails;
using DBCore.Models.Registries;
using DBCore.Models.FileStorage;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DBCore.Models.Notifications;
using DBCore.Models.Reports;
using DBCore.Models.Registries.Invoice;

namespace DBCore.Models
{
    public partial class EptsContext : IdentityDbContext<AspNetUsers, AspNetRoles, Guid, IdentityUserClaim<Guid>,
        AspNetUserRoles, IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>    
    {
        public EptsContext()
        {
        }

        public EptsContext(DbContextOptions<EptsContext> options)
            : base(options)
        {

        }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<TokenCache> TokenCache { get; set; }
       
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<OrganizationType> OrganizationType { get; set; }
        public DbSet<OrganizationRef> OrganizationRefs { get; set; }
        public DbSet<OrganizationDocument> OrganizationDocuments { get; set; }
       
        public DbSet<Region> Regions { get; set; }
        public DbSet<DocumentEvents.DocumentEvent> DocumentEvents { get; set; }
        public DbSet<SignedDoc> SignedDocs { get; set; }
        public DbSet<RegRequest> RegRequests { get; set; }
        public DbSet<SupportTicket> SupportTickets { get; set; }
        public DbSet<SupportTicketComment> SupportTicketComments { get; set; }        
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<FileStorageItem> FileStorageItems { get; set; }     
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<ComplienceDocReport> ComplienceDocReports { get; set; }

        #region Dictionaries

        public DbSet<NSI.Nsi> Nsis { get; set; }

        public DbSet<NSI.NsiValue> NsiValues { get; set; }

        public DbSet<DicStatus> DicStatuses { get; set; }

        public DbSet<DicOrganizationType> DicOrganizationTypes { get; set; }

        public DbSet<DicDocType> DicDocTypes { get; set; }
        
        public DbSet<DicKato> DicKatos { get; set; }

        #endregion

        #region ComplienceDocuments
        public DbSet<ComplienceDocument> ComplienceDocuments { get; set; }
        public DbSet<RepresentativeManufacturer> RepresentativeManufacturer { get; set; }
        public DbSet<AssemblyPlant> AssemblyPlant { get; set; }
        public DbSet<AssemblyKitSupplier> AssemblyKitSupplier { get; set; }
        public DbSet<BlankNumber> BlankNumbers { get; set; }
        public DbSet<VinNumber> VinNumbers { get; set; }
        public DbSet<VehicleGeneralView> VehicleGeneralViews { get; set; }

        #endregion

        #region Integrations
        public DbSet<KgdDatas> KgdDatas { get; set; }
        public DbSet<MshDatas> MshDatas { get; set; }
        public DbSet<MvdDatas> MvdDatas { get; set; }
        public DbSet<Encumbrances> Encumbrances { get; set; }
        public DbSet<EncumbranceTypes> EncumbranceTypes { get; set; }
        public DbSet<ExternalInteractHistory> ExternalInteractHistory { get; set; }
        public DbSet<DigitalPassportStatusHistory> DigitalPassportStatusHistory { get; set; }
        
        #endregion

        #region Digital Passport
        public DbSet<DigitalPassportDocument> DigitalPassportDocument { get; set; }
        public DbSet<ManufacturerPlateDetail> ManufacturerPlateDetail { get; set; }
        public DbSet<AssemblerOrganization> AssemblerOrganization { get; set; }
        public DbSet<EnginePlateDetail> EnginePlateDetail { get; set; }
        public DbSet<MeaningExplanation> MeaningExplanation { get; set; }
        public DbSet<MemberOrganization> MemberOrganization { get; set; }
        public DbSet<VehicleEngine> VehicleEngine { get; set; }
        public DbSet<VehicleIdentLocation> VehicleIdentLocation { get; set; }
        public DbSet<ManufacturerPlateLocation> ManufacturerPlateLocation { get; set; }
        
        public DbSet<VehicleImage> VehicleImage { get; set; }
        public DbSet<VehicleStructureCard> VehicleStructureCard { get; set; }
        #endregion

        #region Change Application
        public DbSet<ChangeApplication> ChangeApplication { get; set; }
        public DbSet<ChangeApplicationFile> ChangeApplicationFile { get; set; }
        public DbSet<ChangeApplicationField> ChangeApplicationField { get; set; }
        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                 //optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=eptsdb;User Id=postgres;Password=123QWEasd!;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); 
            modelBuilder.Ignore<AspNetUserLogins>();
            modelBuilder.Ignore<AspNetUserTokens>();
            modelBuilder.Ignore<AspNetUserClaims>();
            modelBuilder.Entity<AspNetUserRoles>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.AspNetUserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.AspNetUserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            modelBuilder.Entity<TokenCache>(entity =>
            {
                entity.Property(e => e.UserId)
                    .IsRequired();
            });

            modelBuilder.HasPostgresExtension("uuid-ossp");
            modelBuilder.Entity<DigitalPassportDocument>(
                builder =>
                {
                    //builder.HasOne(e => e.ManufacturerPlateDetail)
                    // .WithOne(e => e.DigitalPassport)
                    // .HasForeignKey<ManufacturerPlateDetail>(p => p.DigitalPassportId);
                    //builder.HasOne(e => e.EnginePlateDetail)
                    // .WithOne(e => e.DigitalPassport)
                    // .HasForeignKey<EnginePlateDetail>(p => p.DigitalPassportId);
                    builder.HasIndex(b => b.UniqueCode)
                        .IsUnique();
                    //builder.HasIndex(b => b.VehicleIdentityNumberId)
                    //    .IsUnique();
                });

            modelBuilder.Entity<AspNetUsers>()
                .HasOne(b => b.UserProfile)
                .WithOne(b => b.User)
                .HasForeignKey<UserProfile>(b => b.Id);

            modelBuilder.Entity<OrganizationRef>()
                .HasKey(e => new { e.ParentId, e.ChildId });

            modelBuilder.Entity<OrganizationRef>()
                .HasOne(e => e.Parent)
                .WithMany(e => e.ParentRefs)
                .HasForeignKey(e => e.ParentId);

            modelBuilder.Entity<OrganizationRef>()
                .HasOne(e => e.Child)
                .WithMany(e => e.ChildRefs)
                .HasForeignKey(e => e.ChildId);

            modelBuilder.Entity<ComplienceDocReport>()
                .ToView("ComplienceDocReportView");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {
            ChangeTracker.DetectChanges();

            var entries = ChangeTracker.Entries(); //.Where(x => x.State == EntityState.Deleted);

            foreach (var entry in entries)
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        if (entry.Entity is BaseModel baseModel)
                        {
                            baseModel.Modified = DateTime.Now;
                        }                        
                        break;

                    case EntityState.Added:
                        if (entry.Entity is BaseModel abaseModel)
                        {
                            abaseModel.Created = DateTime.Now;
                            abaseModel.Modified = DateTime.Now;
                        }
                        if (entry.Entity is DocumentBase docbaseModel)
                        {
                            docbaseModel.CreateDate = DateTime.Now;
                            docbaseModel.ModifyDate = DateTime.Now;
                        }
                        break;
                    case EntityState.Deleted:
                        if (entry.Entity is ISoftDelete entity)
                        {
                            entry.State = EntityState.Unchanged;
                            entity.Deleted = DateTime.Now;
                        }
                        break;
                }
                
            }
        }

    }
}

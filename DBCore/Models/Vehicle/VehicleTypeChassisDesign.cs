﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    [Table("VehicleTypeChassisDesigns")]
    public class VehicleTypeChassisDesign
    {
        public int Id { get; set; }
        public int ChassisDesignId { get; set; }
        public virtual NSI.NsiValue ChassisDesign { get; set; }
        
        public Guid VehicleTypeDetailId { get; set; }
        public virtual VehicleTypeDetail VehicleTypeDetail { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

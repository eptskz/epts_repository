﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    [Table("BaseVehicleTypeDetails")]
    public class BaseVehicleTypeDetail: IEntityState<int>
    {
        public int Id { get; set; }
        public string CommercialName { get; set; }
        public string Type { get; set; }

        public Guid VehicleTypeDetailId { get; set; }
        public virtual VehicleTypeDetail VehicleTypeDetail { get; set; }

        public int? MakeId { get; set; }
        public virtual NSI.NsiValue Make { get; set; }

        public string Modification { get; set; }
        public string Vin { get; set; }

        // public string PtsNumber { get; set; }
        // public string EptsNumber { get; set; }
        // public DateTime? EptsDate { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

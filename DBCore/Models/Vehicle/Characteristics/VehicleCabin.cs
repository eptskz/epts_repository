﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Кабина
    /// Отображается и заполняется в случае, если Код технической категории транспортного средства» (trsdo:VehicleTechCategoryCode) = N
    /// </summary>

    [Table("VehicleCabins")]
    public class VehicleCabin: IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public string CabinDescription { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

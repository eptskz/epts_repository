﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// информация о рулевом управлении транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    [Table("VehicleSteeringDetails")]
    public class VehicleSteeringDetail: IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int? SteeringWheelPositionId { get; set; }
        public virtual NsiValue SteeringWheelPosition { get; set; }

        /// <summary>
        /// Тип рулевого
        /// </summary>
        public string VehicleSteeringType { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string VehicleSteeringDescription { get; set; }


        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

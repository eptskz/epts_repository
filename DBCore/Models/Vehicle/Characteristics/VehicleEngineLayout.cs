﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Расположение двигателя
    /// </summary>
    [Table("VehicleEngineLayouts")]
    public class VehicleEngineLayout: IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        /// <summary> 
        /// Расположение двигателя
        /// </summary>
        /// <example>
        /// переднее поперечное
        /// </example>
        /// <code>
        /// Nsi047EngineLocation
        /// </code>
        public int EngineLocationId { get; set; }
        public NsiValue EngineLocation { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Схема компоновки транспортного средства
    /// </summary>
    [Table("VehicleLayoutPatterns")]
    public class VehicleLayoutPattern : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        /// <summary>
        /// Описание схемы компоновки транспортного средства
        /// Заполнение обязательно, если тег « Код технической категории транспортного средства» (trsdo:VehicleTechCategoryCode) ≠ O1 - O4
        /// </summary>
        /// <example>
        /// переднеприводная
        /// </example>
        /// <code>
        /// Nsi050LayoutPattern
        /// </code>
        public int LayoutPatternId { get; set; }
        public NsiValue LayoutPattern { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Описание оборудования транспортного средства
    /// </summary>
    [Table("VehicleEquipmentInfos")]
    public class VehicleEquipmentInfo : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        /// <summary>
        /// Описание оборудования транспортного средства
        /// </summary>
        public string VehicleEquipmentText { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// общий объем багажных отделений транспортного средства
    /// Заполнение обязательно, если тег « Код технической категории транспортного средства» 
    /// (trsdo:VehicleTechCategoryCode) = M3/M3G и поле Класс = III.
    /// </summary>
    [Table("VehicleTrunkVolumeMeasures")]
    public class VehicleTrunkVolumeMeasure: IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public double TrunkVolumeMeasure { get; set; }

        /// <summary>
        /// Ед.измерения 130113 Кубический метр
        /// </summary>
        public int UnitId { get; set; }
        public virtual NsiValue Unit { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

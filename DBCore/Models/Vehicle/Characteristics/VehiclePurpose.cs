﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    ///  назначение
    /// </summary>

    [Table("VehiclePurposes")]
    public class VehiclePurpose : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        /// <summary>
        /// наименование транспортного средства (шасси транспортного средства, самоходной машины и других видов техники), определяемое его конструкторскими особенностями и назначением
        /// Справочник специальной и специализированной техники
        /// </summary>
        /// <code>
        /// Nsi068
        /// </code>
        public int PurposeId { get; set; }
        public virtual NsiValue Purpose { get; set; }

        /// <summary>
        /// иное наименование транспортного средства (шасси транспортного средства, самоходной машины и других видов техники), определяемое его конструкторскими особенностями и назначением
        /// Иная специальная и специализированная техника
        /// </summary>
        /// /// <code>
        /// Nsi069
        /// </code>
        public int? OtherInfoId { get; set; }
        public virtual NsiValue OtherInfo { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

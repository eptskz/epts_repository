﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// информация об узле трансмиссии транспортного средства (шасси транспортного средства)
    /// </summary>
    [Table("TransmissionUnitDetails")]
    public class TransmissionUnitDetail
    {
        public int Id { get; set; }

        public int TransmissionTypeId { get; set; }
        public virtual TransmissionType TransmissionType { get; set; }

        /// <summary>
        /// NSI_018
        /// вид узла транспортного средства (шасси транспортного средства, самоходной машины и других видов техники
        /// </summary>
        public int UnitKindId { get; set; }
        public virtual NsiValue UnitKind { get; set; }

        /// <summary>
        /// Бесступенчатая коробка передач
        /// Отображается только для КОРОБКИ ПЕРЕДАЧ
        /// </summary>
        public int ContinuouslyVariableTransmissionIndicator { get; set; }

        /// <summary>
        /// Распределение по осям для множественной Главной передачи
        /// Отображается только для ГЛАВНОЙ ПЕРЕДАЧИ
        /// </summary>
        public int? AxisDistributionId { get; set; }
        public virtual NsiValue AxisDistribution { get; set; }

        /// <summary>
        /// наименование марки узла трансмиссии
        /// </summary>
        public string TransmissionUnitMakeName { get; set; }
        public string TransmissionUnitDescription { get; set; }


        /// <summary>
        /// описание конструктивных особенностей (типа) узла трансмиссии транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
        /// 
        /// Тип коробки передач
        /// </summary>
        ///<code>
        /// NSI_053
        /// </code>
        public int? TransmissionBoxTypeId { get; set; }
        public virtual NsiValue TransmissionBoxType { get; set; }

        ///<code>
        /// Тип главной передачи
        /// NSI_051 
        /// </code>
        public int? MainGearTypeId { get; set; }
        public virtual NsiValue MainGearType { get; set; }

        /// <summary>
        /// Тип раздаточной коробки
        /// </summary>
        public string TransferCaseType { get; set; }


        /// <summary>
        /// количество передач узла трансмиссии транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
        /// </summary>
        public int TransmissionUnitGearQuantity { get; set; }

        public virtual ICollection<TransmissionUnitGearDetail> TransmissionUnitGears { get; set; }


        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

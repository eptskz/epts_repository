﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// информация о передаче узла трансмиссии транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    [Table("TransmissionUnitGearDetails")]
    public class TransmissionUnitGearDetail
    {
        public int Id { get; set; }

        public int TransmissionUnitId { get; set; }
        public virtual TransmissionUnitDetail TransmissionUnit { get; set; }

        /// <summary>
        /// NSI_080
        /// Передача коробки передач
        /// </summary>
        public int? TransmissionUnitGearValueId { get; set; }
        public virtual NsiValue TransmissionUnitGearValue { get; set; }

        /// <summary>
        /// NSI_081
        /// Вид передаточного числа
        /// </summary>
        public int? TransmissionUnitGearTypeId { get; set; }
        public virtual NsiValue TransmissionUnitGearType { get; set; }


        /// <summary>
        /// передаточное число передачи узла трансмиссии (для бесступенчатой коробки передач – нижняя граница диапазона)
        /// </summary>
        public double TransmissionUnitGearRate { get; set; }
        /// <summary>
        /// передаточное число передачи узла трансмиссии (для бесступенчатой коробки передач – верхняя граница диапазона)
        /// </summary>
        public double? TransmissionUnitGearRateMax { get; set; }

        /// <summary>
        /// признак, определяющий передачу заднего хода: 1 - передача заднего хода; 0 - передача переднего хода
        /// </summary>
        public int TransmissionUnitReverseGearIndicator { get; set; }


        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// описание типа трансмиссии транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// NSI_058
    /// </summary>
    [Table("TransmissionTypes")]
    public class TransmissionType
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int TransTypeId { get; set; }
        public virtual NsiValue TransType { get; set; }

        public virtual ICollection<TransmissionUnitDetail> TransmissionUnitDetails { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

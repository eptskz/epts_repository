﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Система выпуска и нейтрализации отработавших газов
    /// </summary>
    [Table("ExhaustDetails")]
    public class ExhaustDetail
    {
        public int Id { get; set; }

        public int EngineDetailId { get; set; }
        public virtual EngineDetail EngineDetail { get; set; }

        /// <summary>
        /// Описание компонента
        /// </summary>
        public string ExhaustDescription { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

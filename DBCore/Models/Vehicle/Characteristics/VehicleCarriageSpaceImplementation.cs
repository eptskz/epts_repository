﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Исполнение загрузочного пространства
    /// (Для категорий N, O) Код технической категории транспортного средства» 
    /// (trsdo:VehicleTechCategoryCode) = любая категория N или О 
    /// </summary>
    
    [Table("VehicleCarriageSpaceImplementations")]
    public class VehicleCarriageSpaceImplementation : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public string CarriageSpaceImplementation { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

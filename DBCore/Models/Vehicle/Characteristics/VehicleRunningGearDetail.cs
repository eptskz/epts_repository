﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Количество и расположение колес (ОТТС)
    /// </summary>
    [Table("VehicleRunningGearDetails")]
    public class VehicleRunningGearDetail
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int? VehicleWheelFormulaId { get; set; }
        public virtual NsiValue VehicleWheelFormula { get; set; }
       
        public virtual ICollection<PoweredWheelLocation> PoweredWheelLocations { get; set; }
        /// <summary>
        /// Код технической категории транспортного средства» (trsdo:VehicleTechCategoryCode) ≠ О
        /// </summary>
        //public int? PoweredWheelQuantity { get; set; }

        /// <summary>
        /// Количество и расположение колес 
        /// Заполняется в случае, если "Код технической категории транспортного средства» (trsdo:VehicleTechCategoryCode) = любые L
        /// </summary>
        //public string WheelLocationQuantity { get; set; }
        
        public int? VehicleAxleQuantity { get; set; }

        public int? VehicleWheelQuantity { get; set; }

        public string VehicleWheelLocation { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

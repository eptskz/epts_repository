﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// информация о тормозной системе транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    [Table("VehicleBrakingSystemDetails")]
    public class VehicleBrakingSystemDetail : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        /// <summary>
        /// Наименование элемента тормозной системы 
        /// NSI_029
        /// </summary>
        public int VehicleBrakingSystemKindId { get; set; }        
        public virtual NsiValue VehicleBrakingSystemKind { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string VehicleBrakingSystemDescription { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

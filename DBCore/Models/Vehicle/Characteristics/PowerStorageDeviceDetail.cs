﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Устройство накопления энергии
    /// </summary>
    public class PowerStorageDeviceDetail : IEntityState<int>
    {
        public int Id { get; set; }

        public int? CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int? ElectricalMachineDetailId { get; set; }
        public virtual ElectricalMachineDetail ElectricalMachineDetail { get; set; }

        public int PowerStorageDeviceTypeId { get; set; }        
        public virtual NsiValue PowerStorageDeviceType { get; set; }

        public string PowerStorageDeviceDescription { get; set; }
        public string PowerStorageDeviceLocation { get; set; }
                
        public double? VehicleRangeMeasure { get; set; }
        public int VehicleRangeUnitId { get; set; }
        public virtual NsiValue VehicleRangeUnit { get; set; }     

        public double? PowerStorageDeviceVoltageMeasure { get; set; }
        public int PowerStorageDeviceVoltageMeasureUnitId { get; set; }
        public virtual NsiValue PowerStorageDeviceVoltageMeasureUnit { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

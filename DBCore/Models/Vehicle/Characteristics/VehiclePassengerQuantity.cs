﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// пассажировместимость транспортного средства (самоходной машины и других видов техники) 
    /// при максимальной разрешенной массе
    /// </summary>

    [Table("VehiclePassengerQuantities")]
    public class VehiclePassengerQuantity : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int PassengerQuantity { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

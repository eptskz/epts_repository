﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    public class PoweredWheelLocation : IEntityState<int>
    {
        public int Id { get; set; }

        /// <summary>
        /// Заполняется в случае, если «Код технической категории транспортного средства» (trsdo:VehicleTechCategoryCode) ≠ О
        /// </summary>
        public int WheelLocationId { get; set; }
        public virtual NsiValue WheelLocation { get; set; }


        /// <summary>
        /// Количество и расположение колес (ОТТС)
        /// </summary>
        public int VehicleRunningGearDetailId { get; set; }
        public virtual VehicleRunningGearDetail VehicleRunningGearDetail { get; set; }
        
        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Масса
    /// </summary>
    [Table("VehicleWeightMeasures")]
    public class VehicleWeightMeasure : IEntityState<int>
    {
        public int Id { get; set; }

        public double Weight { get; set; }
        public double? WeightMax { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int UnitId { get; set; }
        public virtual NsiValue Unit { get; set; }

        /// <summary>
        /// Вид массы
        /// </summary>
        public int MassTypeId { get; set; }
        public virtual NsiValue MassType { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

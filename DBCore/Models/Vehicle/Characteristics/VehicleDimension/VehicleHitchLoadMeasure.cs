﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Технически допустимая максимальная нагрузка на опорно сцепное устройство
    /// </summary>
    [Table("VehicleHitchLoadMeasure")]
    public class VehicleHitchLoadMeasure : IEntityState<int>
    {
        public int Id { get; set; }

        public double HitchLoadMeasure { get; set; }
        public double? HitchLoadMeasureMax { get; set; }
        public int UnitId { get; set; }
        public virtual NsiValue Unit { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

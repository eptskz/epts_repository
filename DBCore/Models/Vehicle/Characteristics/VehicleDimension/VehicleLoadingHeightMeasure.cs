﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Высота погрузочная
    /// </summary>
    [Table("VehicleLoadingHeightMeasures")]
    public class VehicleLoadingHeightMeasure: IEntityState<int>
    {
        public int Id { get; set; }

        public double LoadingHeight { get; set; }
        public double? LoadingHeightMax { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int UnitId { get; set; }
        public virtual NsiValue Unit { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }    
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Масса прицепа c тормозной системы
    /// </summary>
    [Table("BrakedTrailerWeightMeasures")]
    public class BrakedTrailerWeightMeasure : IEntityState<int>
    {
        public int Id { get; set; }
                
        public double WeightMeasure { get; set; }
        public double? WeightMeasureMax { get; set; }

        public int UnitId { get; set; }
        public virtual NsiValue Unit { get; set; }
        
        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

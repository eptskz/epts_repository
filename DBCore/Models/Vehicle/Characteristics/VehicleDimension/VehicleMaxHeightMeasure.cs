﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Высота максимально допустимая
    /// </summary>
    [Table("VehicleMaxHeightMeasures")]
    public class VehicleMaxHeightMeasure: IEntityState<int>
    {
        public int Id { get; set; }

        public double MaxHeightMeasure { get; set; }
        public double? MaxHeightMeasureMax { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int UnitId { get; set; }
        public virtual NsiValue Unit { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }    
}

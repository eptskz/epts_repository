﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// колесная база транспортного средства (шасси транспортного средства, самоходной машины и других видов техники
    /// </summary>
    [Table("VehicleWheelbaseMeasures")]
    public class VehicleWheelbaseMeasure:IEntityState<int>
    {
        public int Id { get; set; }

        public double Wheelbase { get; set; }
        public double? WheelbaseMax { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int UnitId { get; set; }
        public virtual NsiValue Unit { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

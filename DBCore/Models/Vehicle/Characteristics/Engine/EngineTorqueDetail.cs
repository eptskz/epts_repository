﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Максимальный крутящий момент
    /// </summary>
    [Table("EngineTorqueDetails")]
    public class EngineTorqueDetail
    {
        public int Id { get; set; }

        public int EngineDetailId { get; set; }
        public virtual EngineDetail EngineDetail { get; set; }

        /// <summary>
        /// Максимальный крутящий момент
        /// </summary>
        public double EngineMaxTorqueMeasure { get; set; }
        public int EngineMaxTorqueMeasureUnitId { get; set; }
        public NsiValue EngineMaxTorqueMeasureUnit { get; set; }


        /// <summary>
        /// информация о максимальной мощности двигателя внутреннего сгорания
        /// </summary>
        public double EngineMaxTorqueMeasureShaftRotationFrequencyMinMeasure { get; set; }
        public double? EngineMaxTorqueMeasureShaftRotationFrequencyMaxMeasure { get; set; }
        public int EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnitId { get; set; }
        public NsiValue EngineMaxTorqueMeasureShaftRotationFrequencyMeasureUnit { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle.Characteristics.Engine
{
    public class EngineFuelFeedDetail : IEntityState<int>
    {
        public int Id { get; set; }
        public int EngineDetailId { get; set; }
        public virtual EngineDetail EngineDetail { get; set; }

        public int FuelFeedId { get; set; }
        public virtual NsiValue FuelFeed { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

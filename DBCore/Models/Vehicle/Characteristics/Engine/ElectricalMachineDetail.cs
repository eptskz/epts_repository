﻿using DBCore.Enums;
using DBCore.Models.Dictionaries;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// информация об электродвигателе электромобиля или электромашине в составе трансмиссии гибридного транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    [Table("ElectricalMachineDetails")]
    public class ElectricalMachineDetail
    {
        public int Id { get; set; }

        public int CharacteristicsDetailId { get; set; }
        public virtual CharacteristicsDetail CharacteristicsDetail { get; set; }

        /// <summary>
        /// Вид электромашины
        /// </summary>
        public int? ElectricalMachineKindId { get; set; }
        public virtual NsiValue ElectricalMachineKind { get; set; }

        /// <summary>
        /// Тип электромашины
        /// </summary>
        public int? ElectricalMachineTypeId { get; set; }
        public virtual NsiValue ElectricalMachineType { get; set; }


        /// <summary>
        /// наименование марки электромашины транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
        /// </summary>
        public string VehicleComponentMakeName { get; set; }

        /// <summary>
        /// описание типа и характеристики (постоянного или переменного тока, для переменного тока - синхронный или асинхронный, количество фаз) электромашины транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
        /// </summary>
        public string VehicleComponentText { get; set; }

        /// <summary>
        /// максимальная полезная мощность системы электротяги при постоянном токе, которую система тяги может обеспечивать в среднем в течение 30-минутного периода
        /// </summary>
        public double ElectricMotorPowerMeasure { get; set; }
        public int ElectricMotorPowerMeasureUnitId { get; set; }
        public virtual NsiValue ElectricMotorPowerMeasureUnit { get; set; }


        /// <summary>
        /// рабочее напряжение электромашины транспортного средства (шасси транспортного средства, самоходной машины и других видов техники
        /// </summary>
        public double ElectricalMachineVoltageMeasure { get; set; }
        public int ElectricalMachineVoltageMeasureUnitId { get; set; }
        public virtual NsiValue ElectricalMachineVoltageMeasureUnit { get; set; }


        /// <summary>
        /// Устройство накопления энергии
        /// </summary>
        public int NotPowerStorageDeviceDetailIndicator { get; set; } = 0;
        public virtual ICollection<PowerStorageDeviceDetail> PowerStorageDeviceDetails { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Максимальная мощность двигателя
    /// </summary>
    [Table("EnginePowerDetails")]
    public class EnginePowerDetail
    {
        public int Id { get; set; }

        public int EngineDetailId { get; set; }
        public virtual EngineDetail EngineDetail { get; set; }

        /// <summary>
        /// максимальная мощность двигателя
        /// </summary>
        public double EngineMaxPowerMeasure { get; set; }
        public int EngineMaxPowerMeasureUnitId { get; set; }
        public NsiValue EngineMaxPowerMeasureUnit { get; set; }


        /// <summary>
        /// информация о максимальной мощности двигателя внутреннего сгорания
        /// </summary>
        public double EngineMaxPowerShaftRotationFrequencyMinMeasure { get; set; }
        public double? EngineMaxPowerShaftRotationFrequencyMaxMeasure { get; set; }
        public int EngineMaxPowerShaftRotationFrequencyMeasureUnitId { get; set; }
        public NsiValue EngineMaxPowerShaftRotationFrequencyMeasureUnit { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

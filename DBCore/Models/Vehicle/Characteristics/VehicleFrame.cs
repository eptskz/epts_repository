﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    ///  Описание рамы транспортного средства
    /// </summary>
    
    [Table("VehicleFrames")]
    public class VehicleFrame: IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }
        public string FrameText { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

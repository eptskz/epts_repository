﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Описание гибридного транспортного средства
    /// </summary>
    [Table("VehicleHybridDesigns")]
    public class VehicleHybridDesign
    {
        public int Id { get; set; }
        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }
                
        public string VehicleHybridDesignText { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Тип кузова/количество дверей 
    /// Код технической категории транспортного средства» (trsdo:VehicleTechCategoryCode) = любая категория M
    /// </summary>
    /// 
    [Table("VehicleBodyworkDetails")]
    public class VehicleBodyworkDetail: IEntityState<int>
    {
        public int Id { get; set; }

        public int? DoorQuantity { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public int? VehicleBodyWorkTypeId { get; set; }
        public virtual NsiValue VehicleBodyWorkType { get;set;}

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}
﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Сцепление
    /// </summary>
    [Table("VehicleClutchDetails")]
    public class VehicleClutchDetail : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicsDetailId { get; set; }
        public virtual CharacteristicsDetail CharacteristicsDetail { get; set; }

        /// <summary>
        /// Марка
        /// </summary>
        public string VehicleClutchMakeName { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public string VehicleClutchDescription { get; set; }


        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

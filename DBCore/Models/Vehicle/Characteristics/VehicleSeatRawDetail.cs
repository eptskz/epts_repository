﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Количество мест спереди/сзади
    /// </summary>
    [Table("VehicleSeatRawDetails")]
    public class VehicleSeatRawDetail
    {
        public int Id { get; set; }

        /// <summary>
        /// порядковый номер ряда мест для сидения
        /// </summary>
        public int SeatRawOrdinal { get; set; }

        /// <summary>
        /// количество мест для сидения в ряде мест для сидения
        /// </summary>
        public int SeatRawQuantity { get; set; }
        

        public int VehicleSeatDetailId { get; set; }
        public virtual VehicleSeatDetail VehicleSeatDetail { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Система зажигания NSI_055
    /// </summary>
    [Table("VehicleIgnitionDetails")]
    public class VehicleIgnitionDetail
    {
        public int Id { get; set; }

        public int EngineDetailId { get; set; }
        public virtual EngineDetail EngineDetail { get; set; }

        /// <summary>
        /// Описание компонента
        /// </summary>
        public int VehicleIgnitionTypeId { get; set; }
        public NsiValue VehicleIgnitionType { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

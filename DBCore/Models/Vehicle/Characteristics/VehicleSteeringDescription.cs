﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// информация о рулевом управлении транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    [Table("VehicleSteeringDescriptions")]
    public class VehicleSteeringDescription : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

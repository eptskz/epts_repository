﻿using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Vehicle
{
    public class CharacteristicsDetail
    {
        public int Id { get; set; }

        public Guid VehicleTypeDetailId { get; set; }
        public virtual VehicleTypeDetail VehicleTypeDetail { get; set; }

        public virtual ICollection<VehicleRunningGearDetail> VehicleRunningGearDetails { get; set; }
        public virtual ICollection<VehicleBodyworkDetail> VehicleBodyworkDetails { get; set; }
        public virtual ICollection<VehicleSeatDetail> VehicleSeatDetails { get; set; }
        public virtual ICollection<VehicleLayoutPattern> VehicleLayoutPatterns { get; set; }
        public virtual ICollection<VehicleEngineLayout> VehicleEngineLayouts { get; set; }
        public virtual ICollection<VehicleCarriageSpaceImplementation> VehicleCarriageSpaceImplementations { get; set; }

        public int IsSpecializedVehicle { get; set; } = 0;
        public virtual ICollection<VehicleCabin> VehicleCabins { get; set; }
        public virtual ICollection<VehiclePurpose> VehiclePurposes { get; set; }
        public virtual ICollection<VehicleFrame> VehicleFrames { get; set; }
        public virtual ICollection<VehiclePassengerQuantity> VehiclePassengerQuantities { get; set; }
        public virtual ICollection<VehicleTrunkVolumeMeasure> VehicleTrunkVolumeMeasures { get; set; }

        public int IsVariableHeight { get; set; } = 0;
        public virtual ICollection<VehicleLengthMeasure> LengthRanges { get; set; }
        public virtual ICollection<VehicleWidthMeasure> WidthRanges { get; set; }
        public virtual ICollection<VehicleHeightMeasure> HeightRanges { get; set; }
        public virtual ICollection<VehicleLoadingHeightMeasure> LoadingHeightRanges { get; set; }
        public virtual ICollection<VehicleMaxHeightMeasure> MaxHeightRanges { get; set; }
        public virtual ICollection<VehicleWheelbaseMeasure> WheelbaseMeasureRanges { get; set; }

        public virtual ICollection<VehicleAxleDetail> VehicleAxleDetails { get; set; }

        public int NotVehicleTrailerIndicator { get; set; } = 0;
        public virtual ICollection<VehicleWeightMeasure> VehicleWeightMeasures { get; set; }
        public virtual ICollection<UnbrakedTrailerWeightMeasure> UnbrakedTrailerWeightMeasures { get; set; }
        public virtual ICollection<BrakedTrailerWeightMeasure> BrakedTrailerWeightMeasures { get; set; }
        public virtual ICollection<VehicleHitchLoadMeasure> VehicleHitchLoadMeasures { get; set; }

        /// <summary>
        /// Двигатель 
        /// </summary>
        public int NotEngineDetailIndicator { get; set; } = 0;
        public int? EngineTypeId { get; set; }
        public NsiValue EngineType { get; set; }
        public string VehicleHybridDesignText { get; set; }
        public virtual ICollection<VehicleHybridDesign> VehicleHybridDesigns { get; set; }
        public virtual ICollection<EngineDetail> EngineDetails { get; set; }

        public int NotElectricalMachineDetailIndicator { get; set; } = 0;
        public virtual ICollection<ElectricalMachineDetail> ElectricalMachineDetails { get; set; }

        /// <summary>
        /// Устройство накопления энергии
        /// </summary>
        public int NotPowerStorageDeviceDetailIndicator { get; set; } = 0;
        public virtual ICollection<PowerStorageDeviceDetail> PowerStorageDeviceDetails { get; set; }

        /// <summary>
        /// Сцепление
        /// </summary>
        public int NotVehicleClutchDetailIndicator { get; set; } = 0;
        public virtual ICollection<VehicleClutchDetail> VehicleClutchDetails { get; set; }

        /// <summary>
        /// Трансмиссия
        /// </summary>
        public int NotVehicleTransmissionDetailIndicator { get; set; } = 0;
        public virtual ICollection<TransmissionType> VehicleTransmissionTypes { get; set; }


        /// <summary>
        /// Подвеска
        /// </summary>
        public virtual ICollection<VehicleSuspensionDetail> VehicleSuspensionDetails { get; set; }

        /// <summary>
        /// Рулевое управление
        /// </summary>
        public int NotVehicleSteeringDetailIndicator { get; set; } = 0;
        public string VehicleSteeringDetailDescription { get; set; }
        public virtual ICollection<VehicleSteeringDescription> VehicleSteeringDescriptions { get; set; }
        public virtual ICollection<VehicleSteeringDetail> VehicleSteeringDetails { get; set; }

        /// <summary>
        /// Тормозные системы
        /// </summary>
        public int NotVehicleBrakingSystemDetailIndicator { get; set; } = 0;
        public virtual ICollection<VehicleBrakingSystemDetail> VehicleBrakingSystemDetails { get; set; }

        /// <summary>
        /// Информация о шинах
        /// </summary>
        public virtual ICollection<VehicleTyreKindInfo> VehicleTyreKindInfos { get; set; }

        /// <summary>
        /// Описание оборудования транспортного средства
        /// </summary>
        public virtual ICollection<VehicleEquipmentInfo> VehicleEquipmentInfos { get; set; }

    }
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// информация о подвеске транспортного средства (шасси транспортного средства, самоходной машины и других видов техники
    /// </summary>
    [Table("VehicleSuspensionDetails")]
    public class VehicleSuspensionDetail : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicsDetailId { get; set; }
        public virtual CharacteristicsDetail CharacteristicsDetail { get; set; }

        /// <summary>
        /// Вид подвески
        /// </summary>
        public int VehicleSuspensionKindId { get; set; }        
        public virtual NsiValue VehicleSuspensionKind { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string VehicleSuspensionDescription { get; set; }


        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

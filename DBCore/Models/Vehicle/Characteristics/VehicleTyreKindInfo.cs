﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// информация о рулевом управлении транспортного средства (шасси транспортного средства, самоходной машины и других видов техники)
    /// </summary>
    [Table("VehicleTyreKindInfos")]
    public class VehicleTyreKindInfo : IEntityState<int>
    {
        public int Id { get; set; }

        public int CharacteristicsDetailId { get; set; }
        public virtual CharacteristicsDetail CharacteristicsDetail { get; set; }

        /// <summary>
        /// Обозначение размера
        /// </summary>
        public string VehicleTyreKindSize { get; set; }

        /// <summary>
        /// Индекс несущей способности для максимально допустимой нагрузки 
        /// </summary>
        public string VehicleTyreKindIndex { get; set; }

        /// <summary>
        /// Скоростная категория
        /// </summary>
        public int? VehicleTyreKindSpeedId { get; set; }
        public NsiValue VehicleTyreKindSpeed { get; set; }

        /// <summary>
        /// Запасная шина для временного использования
        /// </summary>
        public int IsSupplementVehicleTyre { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Interfaces;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Колея передних/задних колес
    /// </summary>
    [Table("VehicleAxleDetails")]
    public class VehicleAxleDetail : IEntityState<int>
    {
        public int Id { get; set; }

        /// <summary>
        /// Порядковый номер оси транспортного средства
        /// </summary>
        public int AxleOrdinal { get; set; }

        /// <summary>
        /// Признак ведущей оси
        /// </summary>
        public int? DrivingAxleIndicator { get; set; }

        /// <summary>
        ///  Величина колеи оси
        /// </summary>
        public double AxleSweptPathMeasure { get; set; }
        public double? AxleSweptPathMeasureMax { get; set; }
        public int AxleSweptPathMeasureUnitId { get; set; }
        public NsiValue AxleSweptPathMeasureUnit { get; set; }


        /// <summary>
        ///  Технически максимальная масса приходящаяся на ось
        /// </summary>
        public double? TechnicallyPermissibleMaxWeightOnAxleMeasure { get; set; }
        public double? TechnicallyPermissibleMaxWeightOnAxleMeasureMax { get; set; }
        public int TechnicallyPermissibleMaxWeightOnAxleUnitId { get; set; }
        public NsiValue TechnicallyPermissibleMaxWeightOnAxleUnit { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Блок управления
    /// </summary>
    [Table("ECUModelDetails")]
    public class ECUModelDetail
    {
        public int Id { get; set; }

        public int EngineDetailId { get; set; }
        public virtual EngineDetail EngineDetail { get; set; }

        /// <summary>
        /// Маркировка
        /// </summary>
        public string EcuModel { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Количество мест для сидения
    /// </summary>
    [Table("VehicleSeatDetails")]
    public class VehicleSeatDetail
    {
        public int Id { get; set; }
        
        /// <summary>
        /// Количество мест для сидения
        /// </summary>
        public int SeatQuantity { get; set; }

        /// <summary>
        /// Описание мест для сидения
        /// </summary>
        public string SeatDescription { get; set; }

        public int CharacteristicId { get; set; }
        public virtual CharacteristicsDetail Characteristic { get; set; }

        public virtual ICollection<VehicleSeatRawDetail> VehicleSeatRawDetails { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

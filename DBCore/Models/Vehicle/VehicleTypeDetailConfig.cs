﻿using DBCore.Models.ComplienceDocuments;
using DBCore.Models.NSI;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Тип транспортного средства
    /// </summary>
    public class VehicleTypeDetailConfig : IEntityTypeConfiguration<VehicleTypeDetail>
    {
        public void Configure(EntityTypeBuilder<VehicleTypeDetail> builder)
        {
            builder.ToTable("VehicleTypeDetails");
            builder.HasKey(cd => cd.Id);
            builder.Property(cd => cd.Id)
                .IsRequired()
                .HasDefaultValueSql("uuid_generate_v4()");
        }
    }
}

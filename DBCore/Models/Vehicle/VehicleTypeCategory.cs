﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    [Table("VehicleTypeTechCategories")]
    public class VehicleTypeTechCategory
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public virtual NSI.NsiValue Category { get; set; }

        public Guid VehicleTypeDetailId { get; set; }
        public virtual VehicleTypeDetail VehicleTypeDetail { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    [Table("VehicleTypeMakes")]
    public class VehicleTypeMake
    {
        public int Id { get; set; }
        public Guid VehicleTypeDetailId { get; set; }
        public virtual VehicleTypeDetail VehicleTypeDetail { get; set; }
        
        public int MakeId { get; set; }
        public virtual NSI.NsiValue Make { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

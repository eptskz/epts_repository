﻿using DBCore.Models.ComplienceDocuments;
using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// Тип транспортного средства
    /// </summary>
    [Table("VehicleTypeDetails")]
    public class VehicleTypeDetail
    {
        public Guid Id { get; set; }

        public int NotVehicleCommercialNameIndicator { get; set; } = 0;
        // public string CommercialName { get; set; }
        public virtual ICollection<VehicleCommercialName> CommercialNames { get; set; }

        // public string Type { get; set; }
        public virtual ICollection<VehicleType> Types { get; set; }

        public string YearIssue { get; set; }

        public Guid DocumentId { get; set; }
        public virtual ComplienceDocument Document { get; set; }


        public int NotVehicleMakeNameIndicator { get; set; } = 0;
        public virtual ICollection<VehicleTypeMake> Makes { get; set; }


        public int NotVehicleTechCategoryIndicator { get; set; } = 0;
        public virtual ICollection<VehicleTypeTechCategory> TechCategories { get; set; }

        public int NotVehicleClassCategoryIndicator { get; set; } = 0;
        public virtual ICollection<VehicleTypeClassCategory> ClassCategories { get; set; }

        public int NotVehicleEcoClassCategoryIndicator { get; set; } = 0;
        public virtual ICollection<VehicleTypeEcoClass> EcoClasses { get; set; }

        public int NotVehicleChassisDesignIndicator { get; set; } = 0;
        public virtual ICollection<VehicleTypeChassisDesign> ChassisDesigns { get; set; }

        public int NotVehicleModificationIndicator { get; set; } = 0; 
        public virtual ICollection<VehicleTypeModification> Modifications { get; set; }



        public int NotBaseVehicleTypeDetailIndicator { get; set; } = 0; 
        /// <summary>
        /// Базовое транспортное средство/шасси
        /// </summary>
        public virtual ICollection<BaseVehicleTypeDetail> BaseVehicleTypeDetails { get; set; }

        
        /// <summary>
        /// Общие характеристики ТС
        /// </summary>
        public virtual CharacteristicsDetail CharacteristicsDetail { get; set; }

        
        /// <summary>
        /// Описание ограничений использования транспортного средства
        /// </summary>
        public virtual ICollection<VehicleUseRestriction> VehicleUseRestrictions { get; set; }

        /// <summary>
        /// Возможность передвижения шасси своим ходом по дорогам общего пользования
        /// </summary>
        public virtual ICollection<ShassisMovePermition> ShassisMovePermitions { get; set; }

        /// <summary>
        /// Возможность использования в качестве маршрутного транспортного средства
        /// Может быть передано только для категорий по ТР ТС = М2, М3, М2G, M3G
        /// </summary>
        public virtual ICollection<VehicleRouting> VehicleRoutings { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        public virtual ICollection<VehicleNote> VehicleNotes { get; set; }
        
    }
}

﻿using DBCore.Models.NSI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.Vehicle
{
    /// <summary>
    /// наименование транспортного средства (шасси транспортного средства, 
    /// самоходной машины и других видов техники), 
    /// определяемое его конструкторскими особенностями и назначением
    /// </summary>
    public class VehicleCharacteristic
    {
        public int? CharacteristicId { get; set; }
        // public Nsi060Characteristic Characteristic { get; set; }
    }
}

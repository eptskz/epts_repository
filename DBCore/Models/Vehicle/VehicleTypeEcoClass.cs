﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    [Table("VehicleTypeEcoClasses")]
    public class VehicleTypeEcoClass
    {
        public int Id { get; set; }
        public int EcoClassId { get; set; }
        public virtual NSI.NsiValue EcoClass { get; set; }
            
        public Guid VehicleTypeDetailId { get; set; }
        public virtual VehicleTypeDetail VehicleTypeDetail { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

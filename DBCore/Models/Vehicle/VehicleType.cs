﻿using DBCore.Enums;
using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBCore.Models.Vehicle
{
    [Table("VehicleTypes")]
    public class VehicleType : IEntityState<int>
    {
        public int Id { get; set; }
        public string TypeValue { get; set; }
        public Guid VehicleTypeDetailId { get; set; }
        public virtual VehicleTypeDetail VehicleTypeDetail { get; set; }

        [NotMapped]
        public ObjectStateEnum State { get; set; }
    }
}

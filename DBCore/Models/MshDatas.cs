﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace DBCore.Models
{
    public partial class MshDatas
    {
        public int Id { get; set; }
        public string VehicleRegistrationDetails { get; set; }
        public string RegUnifiedCountryCode { get; set; }
        public string LegalPersonKindCode { get; set; }
        public string RegistrationEventKindCode { get; set; }
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime EventRegTime { get; set; }
        public string TerritoryCode { get; set; }
        public string RegNameDoc { get; set; }
        public string DeregReason { get; set; }
        public Boolean PrimaryRegSign { get; set; }
        public string MachineModificationDetails { get; set; }
        public string ModificUnifiedCountryCode { get; set; }
        public string VehicleModificationText { get; set; }
        public string VehicleFeatureText { get; set; }
        public string BusinessEntityName { get; set; }
        public string MachineModificationOrganizationName { get; set; }
        public string DocumentDetails { get; set; }
        public string DesDocUnifiedCountryCode { get; set; }
        public string DocKindCode { get; set; }
        public string DocKindName { get; set; }
        public string DocName { get; set; }
        public string DocSeriesId { get; set; }
        public string DocId { get; set; }
        public string DocCreationDate { get; set; }
        public string StartDate { get; set; }
        public string DocValidityDate { get; set; }
        public string DocValidityDuration { get; set; }
        public string AuthorityId { get; set; }
        public string AuthorityName { get; set; }
        public string DescriptionText { get; set; }
        public string PageQuantity { get; set; }
        public string OwnerDetails { get; set; }
        public string CompleteNameEntrep { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public string IinBin { get; set; }
        public string IdenDocName { get; set; }
        public string IdenDocNumber { get; set; }
        public DateTime? IdenDocDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string DocCertName { get; set; }
        public string DocCertNumber { get; set; }
        public DateTime? DocCertDate { get; set; }
        public DateTime? OwnerVehicleDate { get; set; }
        public string VehiclesPrice { get; set; }
        public string OdometerReading { get; set; }
        public string RestrictionDetails { get; set; }
        public string OperationType { get; set; }
        public string EncumbranceType { get; set; }
        public DateTime? OperationDate { get; set; }
        public DateTime? OperationTime { get; set; }
        public string CompanyName { get; set; }
        public string RestrictionDocName { get; set; }
        public string RestrictionDocNumber { get; set; }
        public DateTime? RestrictionDocDate { get; set; }
        public DateTime? EncumbranceDateFrom { get; set; }
        public DateTime? EncumbranceDateOn { get; set; }
        public string SelfMachineInformation { get; set; }
        public string Grnz { get; set; }
        public string TechnicalPassportNumber { get; set; }
        public string VinCode { get; set; }
        public string FactoryNumber { get; set; }
        public string SignDestrIdentNumber { get; set; }
        public string DescDestrIdentNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime MessageDate { get; set; }
        public string MessageId { get; set; }
        public string SessionId { get; set; }
        public string CorrelationId { get; set; }
        public DateTime? Created { get; set; }
    }
}

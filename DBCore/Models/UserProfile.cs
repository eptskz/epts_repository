﻿using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public class UserProfile : ISoftDelete
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SecondName { get; set; }

        public int? StatusId { get; set; }
        public virtual DicStatus Status { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public DateTime? Deleted { get; set; }

        public AspNetUsers User { get; set; }
    }
}

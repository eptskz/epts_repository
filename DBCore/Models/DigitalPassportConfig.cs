﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.PassportDetails
{
    public class DigitalPassportConfiguration : IEntityTypeConfiguration<DigitalPassportDocument>
    {
        public void Configure(EntityTypeBuilder<DigitalPassportDocument> builder)
        {
            builder.ToTable("DigitalPassportDocument");
            builder.HasKey(cd => cd.Id);
            //builder.HasOne(e => e.ManufacturerPlateDetail)
            // .WithOne(e => e.DigitalPassport)
            // .HasForeignKey<ManufacturerPlateDetail>(p => p.DigitalPassportId);
            //builder.HasOne(e => e.EnginePlateDetail)
            // .WithOne(e => e.DigitalPassport)
            // .HasForeignKey<EnginePlateDetail>(p => p.DigitalPassportId);
        }
    }
}

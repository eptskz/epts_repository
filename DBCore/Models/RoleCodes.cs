﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models
{
    public static class RoleCodes
    {
        public static string Administrator = "administrator";
        public static string ExternalUser = "externaluser";
    }
}

﻿using DBCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.NSI
{
    public class NsiBase : BaseModel, ISoftDelete
    {
        public DateTime? Deleted { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Models.NSI
{
    public class NsiValue : NsiBase
    {
        public int NsiId { get; set; }
        public virtual Nsi Nsi { get; set; }

        public int? WheelQuantity { get; set; }
        public int? PowerWheelQuantity { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DBCore.Models
{
    [Serializable]
    public partial class TokenCache
    {
        public int Id { get; set; }        
        public string Token { get; set; }

        public Guid UserId { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}

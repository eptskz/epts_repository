﻿using DBCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Interfaces
{
    public interface IObjectState
    {
        ObjectStateEnum State { get; set; }
    }
}

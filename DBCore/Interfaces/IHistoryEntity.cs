﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Interfaces
{
    public interface IHistoryEntity<T>
    {
        T ParentId {get;set;} 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Interfaces
{
    public interface ISoftDelete
    {
        DateTime? Deleted { get; set; }
    }
}

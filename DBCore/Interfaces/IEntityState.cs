﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBCore.Interfaces
{
    public interface IEntityState<T> : IObjectState
    {
        T Id { get; set; } 
    }
}
